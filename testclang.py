#!/usr/bin/env python3

import clang.cindex
import textwrap

def _dump_ast( cursor, out=None, depth=0):
    cleanup = lambda: None
    if out is None:
        out = sys.stdout
    if isinstance(out, str):
        out = open(out, 'w')
        cleanup = lambda : out.close()
    try:
        indent = depth*4*' '
        for cursor2 in cursor.get_children():
            def or_none(f):
                try:
                    return f()
                except Exception:
                    return
            result = or_none( cursor2.type.get_result)
            type_ = cursor2.type
            type_canonical = or_none( cursor2.type.get_canonical)

            text = indent
            text += f' {cursor2.kind=} {cursor2.displayname=} {cursor2.spelling=}'
            if result:
                text += f' {result.spelling=}'
            if type_:
                text += f' {type_.spelling=}'
            if type_canonical:
                text += f' {type_canonical.spelling=}'
            #text += '\n'
            if callable(out):
                out( text)
            else:
                out.write(text)

            _dump_ast( cursor2, out, depth+1)
    finally:
        cleanup()


def main():
    
    text = textwrap.dedent('''
            /* Workaround on Linux. size_t is defined internally in gcc. It isn't even
            in stdint.h. *.
            typedef unsigned long size_t;
            
            typedef size_t (*fz_image_get_size_fn)(int *, int *);

            typedef void (*cpp_test_fnptr_t)(int64_t a, size_t cpp_test_x);
            cpp_test_fnptr_t cpp_test_fnptr;
            size_t cpp_test_x;
            struct cpp_test_Foo
            {
                void (*fnptr)(int64_t a, size_t cpp_test_x);
            };
            //typedef struct Foo Foo;
            ''')
    path = 'testclang.c'
    with open( path, 'w') as f:
        f.write( text)
    index = clang.cindex.Index.create()
    tu = index.parse( path, '-v'.split(' '))
    path2 = 'testclang.c.c'
    tu.save(path2)
    print( f'Have saved to: {path2}')
    #parse.dump_ast( tu.cursor, 'ast')
    for diagnostic in tu.diagnostics:
        print(f'{diagnostic=}')
    for cursor in tu.cursor.get_children():
        if 'cpp_test_' in cursor.spelling:
            _dump_ast(cursor, out=print)

if __name__ == '__main__':
    main()
