#!/usr/bin/env python3

import aargs
import jlib
import jtest
import os
import platform
import pypackage
import pymupdf as pymupdf_module
import shlex
import urllib.request


def test(
        remote: jtest.Remote=None,
        pdf2docx='pdf2docx',
        pymupdf='PyMuPDF',
        mupdf='mupdf',
        build_mupdf=True,
        build_pymupdf=True,
        build_pdf2docx=True,
        p='',
        t='test/test.py',
        v=False,
        ):
    '''
    p:
        Extra command-line args to pytest.
    t:
        Specification of pytests to run, relative to `pdf2docx`. For example
        `test/test.py::TestConversion::test_multi_pages`.
    '''
    jlib.log(f'{build_mupdf=} {build_pymupdf=} {build_pdf2docx=}')
    if remote:
        pymupdf_module._run_remote(remote, pdf2docx, pymupdf, mupdf)
        return

    if build_pymupdf:
        root = os.path.abspath(f'{__file__}/../..')
        env_extra=dict()
        env_extra['PYMUPDF_SETUP_MUPDF_BUILD'] = os.path.abspath(mupdf)
        if not build_mupdf:
            env_extra['PYMUPDF_SETUP_MUPDF_REBUILD'] = '0'
    
        pypackage.venv_run(
                f'pip install -v {os.path.abspath(pymupdf)}',
                env_extra=env_extra,
                )
    
    if build_pdf2docx:
        pypackage.venv_run(f'pip install -v {os.path.abspath(pdf2docx)}')
    
    if platform.system() == 'Windows':
        officetopdf = 'OfficeToPDF.exe'
        if not os.path.exists(officetopdf):
            jlib.log(f'Downloading OfficeToPDF.exe')
            jlib.url_get('https://github.com/cognidox/OfficeToPDF/releases/download/v1.9.0.2/OfficeToPDF.exe', officetopdf)
    
    if v:
        pip_upgrade=False
    else:
        pip_upgrade=True
        pypackage.venv_run(f'pip install pytest', pip_upgrade=pip_upgrade)
    pypackage.venv_run(f'pytest {pdf2docx}/{t} {p}', pip_upgrade=pip_upgrade)


def test_dev(
        remote: jtest.Remote=None,
        pdf2docx='pdf2docx',
        pymupdf='PyMuPDF',
        mupdf='mupdf',
        build_mupdf=1,
        ):
    leaf_args = [
        #('book2-8-10.pdf', '--pages 2'),
        #('pdf2docx-lists-bullets.docx.pdf', ''),
        #('pdf2docx-lists-bullets2.docx.pdf', ''),
        ('pdf2docx-lists-bullets3.docx.pdf', ''),
    
        # Like 3 but with blank line before list. and pdf-2docx seems to
        # generate incorrect char for bullets.
        #('pdf2docx-lists-bullets5.docx.pdf', ''),
        ]
    
    def leaf_args_out():
        for leaf, args in leaf_args:
            for sort in 1,:#0, 1:
                for parse_stream_table in 0, 1:
                    out = f'{leaf}{".sort"*sort}.parse_stream_table={parse_stream_table}.docx'
                    args2 = (' '
                            + f' {"--sort"*sort}'
                            + f' --parse_stream_table={"True" if parse_stream_table else "False"}'
                            )
                    yield leaf, args + args2, out, 
    
    
    leafs = [leaf for leaf, args in leaf_args]
    leafs.sort()
    
    if remote:
        jlib.system(f'rsync -ai --rsh {shlex.quote(remote.ssh)} {" ".join(leafs)} :{remote.directory}')
        pymupdf_module._run_remote(remote, pdf2docx, pymupdf, mupdf)
        for leaf, args, out in leaf_args_out():
                jlib.system(f'rsync -ai --rsh {shlex.quote(remote.ssh)} :{remote.directory}{out} ./')
                jlib.system(f'unzip -o -d {out}.dir {out}')
                jlib.system(f'xmllint --format {out}.dir/word/document.xml > {out}.dir/word/document.xml.xml')
        for leaf, args, out in leaf_args_out():
            jlib.log(f'Have created:')
            jlib.log(f'    local file: {out}')
            jlib.log(f'    local file: {out}.dir/word/document.xml.xml')
        return
    
    root = os.path.abspath(f'{__file__}/../..')
    env_extra = dict()
    env_extra['PYMUPDF_SETUP_MUPDF_BUILD'] = os.path.abspath(mupdf)
    if not build_mupdf:
        env_extra['PYMUPDF_SETUP_MUPDF_REBUILD'] = '0'
    pypackage.venv_run(
            f'pip install -v {os.path.abspath(pymupdf)} {os.path.abspath(pdf2docx)}',
            env_extra=env_extra,
            )
    for leaf, args, out in leaf_args_out():
            jlib.log('#' * 40)
            jlib.log(f'### {leaf=} {args=} => {out=}.')
            pypackage.venv_run(f'pdf2docx convert {root}/{leaf} --docx_file {out} {args}', pip_upgrade=False)

if __name__ == '__main__':
    aargs.cli( show_return=False)
