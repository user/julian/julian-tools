#! /usr/bin/env python3

import re
import sys


import functools

def sort(path, out=None):
    items = list()
    lines = list()
    with open(path) as f:
        property_i = None
        current = None
        for i, line in enumerate(f):
            lines.append(line)
            m = re.match('^( *)(.*)', line)
            indent = len(m.group(1))
            tail = m.group(2)
            if tail:
                while current:
                    if current.indent < indent:
                        break
                    current = current.parent
            
            item = _Item(parent=current, indent=indent, line=i, type_=None, name=None)
            
            if tail.startswith('@'):
                if m:
                    property_i = i
                    item = None
            
            else:
                m = re.match('^((def)|(class)) ([a-zA-Z_][a-zA-Z_0-9]*)', tail)
                if m:
                    type_ = m.group(1)
                    name = m.group(4)
                    if type_ == '@property':
                        property_i = i
                        item = None
                    else:
                        if property_i:
                            item.line = property_i
                            item.line_end = i+1
                            property_i = None
                        item.type_ = type_
                        item.name = name
                        current = item
            
            if item:
                items.append(item)

    #_items_show(items)

    @functools.total_ordering
    class Nol:
        def __init__(self, nol):
            self.nol = nol
        def __lt__(self, rhs):
            assert isinstance(rhs, Nol)
            assert isinstance(self, Nol)
            if isinstance(self.nol, str) and isinstance(rhs.nol, int):
                return True
            if isinstance(self.nol, int) and isinstance(rhs.nol, str):
                return False
            return self.nol < rhs.nol
        def __eq__(self, rhs):
            return type(self.nol) == type(rhs.nol) and self.nol == rhs.nol
        def __repr__(self):
            return f'Nol:{self.nol!r}'

    a = Nol(123)
    b = Nol('sd')
    c = Nol(124)
    d = Nol('a')
    assert a < c
    assert not (c < a)
    assert d < b
    assert not (b < d)
    assert a > b
    
    a = [Nol(4)]
    b = [Nol(4), Nol(5)]
    assert a[0].nol == b[0].nol
    assert not (a[0] < b[0])
    assert not (b[0] < a[0])
    assert a[0] == b[0]
    assert a < b

    print('-'*80)
    def keyfn(item):
        ret = []
        if item.parent:
            ret += keyfn(item.parent)
        if item.type_ == 'def' and item.parent and item.parent.type_ == 'class':
            ret.append(Nol(item.name.replace('_', '.')))
        ret.append(Nol(item.line))
        return tuple(ret)
    
    def show(items):
        item_prev = None
        for item in items:
            text = ''
            if item_prev:
                key_prev = keyfn(item_prev)
                key = keyfn(item)
                text += f'{"True: " if key_prev < key else "False:"} '
            else:
                text = '       '
            item_prev = item
            for i, line in enumerate(lines[item.line:item.line_end]):
                width = 60
                if i==0:
                    t = str(keyfn(item))
                    t += ' ' * (width-len(t))
                    text += f'{t}: '
                text += ' ' * (60-len(text))
                sys.stdout.write(text)
                sys.stdout.write(line)
    
    items0 = items[:]
    items.sort(key=keyfn)
    #assert items != items0
    
    show(items0)
    print('='*80)
    show(items)
    #_items_show(items)
    
    if out:
        assert out != path
        with open(out, 'w') as f:
            for item in items:
                for i in range(item.line, item.line_end):
                    f.write(lines[i])
    

class _Item:
    def __init__(self, parent, indent, line, type_, name):
        self.parent = parent
        self.indent = indent
        self.line = line
        self.type_ = type_
        self.name = name
        self.line_end = line+1

def _items_show(items):
    prev = None
    for item in items:
        text = f'{" "*item.indent}{item.type_} {item.name} {item.line}'
        if 1 or item.line_end:
            text += f'..{item.line_end}'
        print(text)
        if prev:
            assert prev.line_end == item.line

def _get_indent(line):
    m = re.match('^( *)', line)
    if m:
        return len( m.group(1))
    assert 0

if __name__ == '__main__':
    import aargs
    aargs.cli()
