#!/usr/bin/env python3

'''
Generates html page showing status of recent workflow runs in all our Github
repositories.

We use the Github REST API to get information about repositories and workflows.

Avoiding Github rate limiting:

    We store Github REST responses in a cache and use Github conditional
    requests and the `ETag` header - a '304 Not Modified' response code means
    that we can safely use our cached response.

    We load our cache from file on startup and save it before terminating, so
    we can be run repeatedly without being rate-limited by Github.
    
    See _get0() for details.

Setting up with `cron` on `casper.ghostscript.com`:

    Follow instructions in `./gh_metrics.py` for syncing `julian-tools/` to
    casper and creating file containing github token.
    
    * Install cron job that runs with arg `casper`:
    
      `ssh julian@casper.ghostscript.com crontab -e`
      
      For example::
    
          # minute hour day-of-month month day-of-week command
          MAILTO=""
          */15 * * * * ./artifex-remote/julian-tools/github_status.py venv pylocal-gh casper --cron
    
'''

import aargs
import jlib
import pymupdf
import ppt_graph
import ppt_text

import calendar
import json
import os
import pickle
import pprint
import re
import sys
import textwrap
import time


def venv(path, install=True):
    '''
    Re-runs inside venv with installed packages.
    
    If we are not already running inside venv `path`, creates venv `path`,
    installs required packages and re-executes original command using the
    venv's python.
    
    Args:
        path:
            Name of venv.
        install:
            If false we don't try to install packages if venv already exists.
    '''
    if os.path.abspath(sys.prefix) == os.path.abspath(path):
        # We are already in the specified venv.
        jlib.log('Already running in venv {path!r}.')
        return
    
    if not os.path.exists( path):
        install = True
    if install:
        command = f'{sys.executable} -m venv {path}'
        command += f' && {path}/bin/python -m pip install --upgrade pip plotly numpy requests'
        jlib.system( command, verbose=1)
    
    args = [f'{path}/bin/python'] + sys.argv
    time.sleep(1)
    jlib.log('Calling os.execv with {args=}.')
    os.execv( args[0], args)


def _relpath(p):
    p = os.path.expanduser(p)
    p = os.path.abspath(p)
    p = os.path.relpath(p)
    return p

def casper(cron=False):
    '''
    For running on casper (typically via cron).
    
    Args:
        cron:
            If true, the generated index.html file will state that it has been
            updated by a cron job.
    '''
    token_file(
            _relpath('~/artifex-remote/julian-tools/github-token')
            )
    make(
            ppr_dir=_relpath('~/PyMuPDF-performance-results'),
            outdir=_relpath('~/public_html/ghstatus'),
            cron=cron,
            plotly = True,
            )
    jlib.log('Have created: https://ghostscript.com/~julian/ghstatus/')


def julian(ppr_update=True, workflows=True, ppt=True, verbose=True):
    '''
    For running on julian's system.
    '''
    make(
            ppr_dir = '/tmp/PyMuPDF-performance-results',
            outdir = '/tmp/ghstatus',
            ssh_id='~/.ssh/id_rsa2',
            plotly = True,
            ppr_update = ppr_update,
            workflows = workflows,
            ppt = ppt,
            verbose = True,
            )


def make(
        ppr_dir,
        outdir,
        ssh_id=None,
        cron=None,
        plotly=True,
        ppr_update=True,
        workflows=True,
        ppt=True,
        verbose=False,
        ):
    '''
    Makes all Github status files.
    
    Args:
        ppr_dir:
            Where to put checkout of
            ArtifexSoftware/PyMuPDF-performance-results. If it already exists
            we do `git pull`, otherwise we do `git clone`.
        outdir:
            Where to write .html files etc. We create a top-level
            `{outdir}/index.html` file.
        ssh_id:
            If specified we use the specified ssh_id file when running git.
        cron:
            .
        plotly:
            If true we use plotly to generate interactive graphs in a
            standalone .html file. Otherwise we use matplotlib which creates a
            static .svg file.
        ppr_update:
            Do 'git pull ...' in `ppr_dir`.
        workflows:
            Generate Github workflow page.
        ppt:
            Generate PyMuPDF performance pages.
        verbose:
            .
    '''
    os.makedirs( outdir, exist_ok=True)
    
    # Get updated PyMuPDF-performance-results checkout.
    #
    if ppr_update:
        ppr_pull( ppr_dir, ssh_id=ssh_id)
    
    # Generate graphs etc from PyMuPDF-performance-results checkout.
    #
    if ppt:
        python_performance_timings(ppr_dir, outdir, plotly)
    
    # Generate info about latest Github worflow runs.
    #
    if workflows:
        html_workflows( out=f'{outdir}/workflows.html', verbose=verbose)
    
    # Create top-level index.html.
    #
    title = 'Artifex Github status'
    graph2 = ''
    if plotly:
        graph2 = '<li><a href="ppt-graphs2.html">Graph comparing most recent pymupdf_mupdf_master vs mupdfpy_mupdf_master</a>.'
    text = textwrap.dedent(f'''
            <html>
            <header>
            <title>{title}</title>
            </header>
            <body>
            <h1>{title}</h1>
            <p><small>[Updated {time.strftime("%F (%a) %T")}{" by cron job" if cron else ""}.]</small>
            <ul>
                <li><p><a href="workflows.html">Github workflow runs - recent results</a>.
                <li><p>Python PDF timings (from <a href="https://github.com/ArtifexSoftware/PyMuPDF-performance-results">ArtifexSoftware/PyMuPDF-performance-results</a>):
                <ul>
                    <li><a href="ppt.html">Tables for latest run</a>.
                    <li><a href="{"ppt-graphs.html" if plotly else "ppt-graphs.svg"}">Graphs showing historical data</a>.
                    {graph2}
                </ul>
            </ul>
            </body>
            '''
            )
    with open( f'{outdir}/index.html', 'w') as f:
        f.write( text)
    cache_stats()
    jlib.log('Have created: {outdir}/index.html')


def python_performance_timings(ppr_dir, outdir, plotly=True):
    '''
    Generates files showing Python performance timings from
    https://github.com/ArtifexSoftware/PyMuPDF-performance-results.
    
    Args:
        outdir:
            Where to place output files.
        ppr_dir:
            Checkout of
            https://github.com/ArtifexSoftware/PyMuPDF-performance-results.
        plotly:
            If true we use plotly (which generates interactive graphs in a .htm
            file), otherwise matplotlib (which generates a static .svg file.)
    '''
    
    text = ppt_text.make( f'{ppr_dir}/results-latest.json', )
    
    # Generate graphs for latest Python pdf timings.
    #
    figure = ppt_graph.make( ppr_dir, plotly=plotly)
    
    if plotly:
        # Save `figure` to a .html file containing interactive graphs.
        figure, figure2 = figure
        path = f'{outdir}/ppt-graphs.html'
        figure.write_html(
                path,
                # See: https://github.com/plotly/plotly.js/blob/master/src/plot_api/plot_config.js
                config=dict(
                    scrollZoom=True,
                    #doubleClick='reset',
                    showTips=True,
                    )
                )
        # This appears to need an extra package.
        #figure.write_image( path_static)
        jlib.log( f'Have created: {path!r}.')
        path = f'{outdir}/ppt-graphs2.html'
        figure2.write_html(
                path,
                # See: https://github.com/plotly/plotly.js/blob/master/src/plot_api/plot_config.js
                config=dict(
                    scrollZoom=True,
                    #doubleClick='reset',
                    showTips=True,
                    )
                )
    else:
        # Save `figure` to a .svg file.
        #
        # Using bbox_inches='tight' is slow, but ensures that text annotations are
        # not truncated.
        path = f'{outdir}/ppt-graphs.svg'
        figure.savefig( path, bbox_inches='tight')
        jlib.log( f'Have created: {path!r}.')
        
        # Try to generate interactive graphs in a .html file using mpld3.
        #
        # mpld3 doesn't get things quite right:
        #   * Clips each plot to the axes, so hiding the tool name text
        #     annotations.
        #   * It does not show arrows in text annotations.
        #   * It does not show dates properly on axis.
        #
        try:
            import mpld3
            import mpld3.plugins
        except Exception as e:
            jlib.log('Not creating interactive graph because failed to import mpld3: {e}')
        else:
            mpld3.plugins.connect( figure, mpld3.plugins.Zoom(button=True, enabled=True))
            path = f'{outdir}/ppt-graphs-matplotlib-interactive.html'
            with open(path, 'w') as f:
                f.write(textwrap.dedent('''
                        <html>
                        <body>
                        '''))
                html = mpld3.fig_to_html(figure)
                f.write( html)
                f.write(textwrap.dedent('''
                        </body>
                        </html>
                        '''))
            jlib.log( f'Have created: {path!r}.')
    
    # Generate html tables showing details of latest results.
    #
    path = f'{outdir}/ppt.html'
    with open( path, 'w') as f:
        f.write( textwrap.dedent( '''
                <html>
                <head>
                
                <style>
                th {
                  border: 1px solid black;
                  border-collapse: collapse;
                }
                </style>
                
                <title>
                Python performance timings
                </title>
                </head>
                
                <body>
                <pre>
                '''))
        f.write( text)
        f.write( textwrap.dedent( '''
                </body>
                </html>
                '''))
    jlib.log( f'Have created {path}.')


def ppr_pull( ppr_dir, ssh_id=None):
    env_extra = None
    if ssh_id:
        env_extra = dict(GIT_SSH_COMMAND=f'ssh -i {ssh_id}')
    if os.path.exists( ppr_dir):
        e = jlib.system(
                f'cd {ppr_dir} && git pull -r',
                verbose=1,
                env_extra=env_extra,
                )
    else:
        remote = 'git@github.com:ArtifexSoftware/PyMuPDF-performance-results.git'
        jlib.system(
                f'cd {os.path.dirname(ppr_dir)} && git clone {remote} {os.path.basename(ppr_dir)}',
                verbose=1,
                env_extra=env_extra,
                )
        jlib.log( 'Have cloned {remote} to {ppr_dir}')


class CacheStatistics:
    def __init__(self):
        self.num_calls = 0
        self.num_calls_uncached = 0
        self.num_calls_cached_unchanged = 0
        self.num_calls_cached_modified = 0

_cache_statistics = CacheStatistics()
_cache_save_path = os.path.abspath( f'{__file__}.cache.pickle')
_cache = dict()


def cache_stats():
    '''
    Show stats for our use of Github conditional requests.
    '''
    jlib.log('Github conditional requests statistics:')
    jlib.log('    num_calls={_cache_statistics.num_calls}')
    jlib.log('    num_calls_uncached: {_cache_statistics.num_calls_uncached}')
    jlib.log('    num_calls_cached_unchanged: {_cache_statistics.num_calls_cached_unchanged}')
    jlib.log('    num_calls_cached_modified: {_cache_statistics.num_calls_cached_modified}')



def _cache_save():
    # We use pickle rather than json for saving _cache to file, because json
    # can't cope with tuple dict keys.
    #
    path = _cache_save_path
    path_temp = _cache_save_path + '-'
    with open(path_temp, 'wb') as f:
        pickle.dump(_cache, f)
    os.rename(path_temp, path)
    jlib.log('Have saved cache {len(_cache)=} to: {path}')

def _cache_load():
    path = _cache_save_path
    global _cache
    try:
        with open(path, 'rb') as f:
            _cache = pickle.load(f)
    except Exception as e:
        jlib.log('Warning: unable to load cache from {path}: {e}')
    jlib.log('Have loaded cache {len(_cache)=} from: {path}')


_get0_cache_only = False

def cache_only():
    '''
    Use request cache only where possible. Useful for development - much faster
    than going to Github all the time.
    '''
    global _get0_cache_only
    _get0_cache_only = True


def _get0(url, stream=False, params=None, headers=None):
    '''
    Makes a get request, using our cache and Github's conditional requests with
    `ETag` header values to avoid rate limiting.
    
    Returns a `requests.Response` instance.
    '''
    _cache_statistics.num_calls += 1
    def make_dict_hashable(d):
        if isinstance(d, dict):
            return json.dumps(params, sort_keys=True)
        return d
    key = (
            url,
            stream,
            make_dict_hashable(params),
            make_dict_hashable(headers),
            )
    response_etag = _cache.get( key)
    
    if response_etag is None:
        #jlib.log('Not in cache: {url=} {params=}')
        _cache_statistics.num_calls_uncached += 1
        response = pymupdf._gh_get( url, raise_for_status=False, params=params, headers=headers)
    else:
        # Make a conditional request.
        response, etag = response_etag
        #jlib.log('In cache: {etag=} {url=} {params=}')
        headers2 = headers.copy() if headers else dict()
        headers2['If-None-Match'] = etag
        if _get0_cache_only:
            use_cache = True
        else:
            response2 = pymupdf._gh_get(
                    url,
                    stream=stream,
                    raise_for_status=False,
                    params=params,
                    headers=headers2,
                    )
            use_cache = (response2.status_code == 304)
        if use_cache:
            # Not modified. We can use the result from our cache.
            #jlib.log('Not modified: {url=} {params=}')
            _cache_statistics.num_calls_cached_unchanged += 1
            return response
        
        # Modified content is in `response2`.
        _cache_statistics.num_calls_cached_modified += 1
        response = response2
        #jlib.log('New data. {etag=} {url=} {params=}')
    
    etag = response.headers.get('ETag')
    if etag is None:
        jlib.log('Not able to cache because no "ETag" header: {url=} {params=}')
    else:
        _cache[ key] = response, etag
    return response


def _get(url, stream=False, raise_for_status=True, params=None, headers=None):
    '''
    Makes a get request and returns `response.json()`.
    
    Args:
        url:
            .
        stream:
            .
        params:
            E.g. `page=3` or other Github Query parameters.
        headers:
            If not None, should be a dict to be used as base headers; a copy
            is taken so it will not be modified. We add new headers such as
            `Accept='application/vnd.github.v3+json'`.
    '''
    response = _get0(url, stream=stream, params=params, headers=headers)
    if raise_for_status:
        try:
            response.raise_for_status()
        except Exception as e:
            raise Exception( str(response.json())) from e
    return response.json()


def repositories(account, show=False):
    '''
    Returns list of dicts, one for each repository in `account`.
    
    Args:
        account:
            E.g. `ArtifexSoftware`.
    '''
    ret = []
    i = 0
    per_page=30
    while 1:
        r = _get(
                f'https://api.github.com/orgs/{account}/repos',
                params=dict(
                    page=i+1,
                    per_page=per_page,
                    ),
                )
        ret += r
        if len(r) < per_page:
            break
        i += 1
    if show:
        jlib.log( '{json.dumps(ret, indent="    ")}')
    else:
        return ret

def repository(name, show=False):
    '''
    Args:
        name:
            Full name of repository such as `ArtifexSoftware/mupdfpy`.
        show:
            If true we print results, otherwise we return results.
    '''
    if isinstance(name, dict):
        full_name = name['full_name']
    else:
        full_name = name
    r = _get( f'https://api.github.com/repos/{full_name}')
    if show:
        jlib.log( '{json.dumps(r, indent="    ")}')
    else:
        return r


def workflows(name, show=False):
    '''
    Returns list of workflows.
    
    Args:
        name:
            Identify a repository, e.g. `ArtifexSoftware/mupdf`.
        show:
            If true we print results, otherwise we return results.
    
    One workflow for each Action script?
    '''
    if isinstance(name, dict):
        full_name = name['full_name']
    else:
        full_name = name
    ret = _get( f'https://api.github.com/repos/{full_name}/actions/workflows')
    ret = ret['workflows']
    if show:
        jlib.log( '{json.dumps(ret, indent="    ")}')
    else:
        return ret

def workflow(name, id, show=False):
    '''
    Returns info about the specified workflow.
    Args:
        name:
            Identify a repository, e.g. `ArtifexSoftware/mupdf`.
        id:
            Integer workflow id.
        show:
            If true we print results, otherwise we return results.
    
    Not much info available:
        "id": 46660396,
        "node_id": "W_kwDOAF0qcs4Cx_ss",
        "name": "Test mupdf release branch",
        "path": ".github/workflows/test_mupdf-release-branch.yml",
        "state": "active",
        "created_at": "2023-01-27T16:20:30.000Z",
        "updated_at": "2023-01-27T16:20:30.000Z",
        "url": "https://api.github.com/repos/pymupdf/PyMuPDF/actions/workflows/46660396",
        "html_url": "https://github.com/pymupdf/PyMuPDF/blob/master/.github/workflows/test_mupdf-release-branch.yml",
        "badge_url": "https://github.com/pymupdf/PyMuPDF/workflows/Test%20mupdf%20release%20branch/badge.svg"
    '''
    full_name = name['full_name'] if isinstance(name, dict) else name
    r = _get( f'https://api.github.com/repos/{full_name}/actions/workflows/{id}')
    if show:
        jlib.log( '{json.dumps(r, indent="    ")}')
    else:
        return r

def workflow_runs(name, workflow, t_min=None, show=False):
    '''
    Returns list of recent workflow, sorted most recent first.
    Args:
        name:
            E.g. `ArtifexSoftware/mupdf-julian`.
        workflow:
            Integer id of workflow or workflow dict from Github.
        t_min:
            We only return workflow runs created on or after the beginning of
            the day containing this time.
        show:
            If true we print results, otherwise we return results.
    '''
    full_name = name['full_name'] if isinstance(name, dict) else name
    assert isinstance(full_name, str), f'{type(full_name)=} {full_name=}'
    id_ = workflow['id'] if isinstance(workflow, dict) else workflow
    assert len(str(id_)) < 100
    per_page = 2
    params = dict(per_page=per_page)
    if t_min is not None:
        if isinstance( t_min, str):
            t_min0 = t_min
            t = time.time()
            t_min = jlib.time_read_date3(t_min0)
        t_min = time.strftime("%Y-%m-%d", time.gmtime(t_min))
        params['created'] = f'{t_min}..*'
    ret = []
    i = 0
    while 1:
        params['page'] = i + 1
        url = f'https://api.github.com/repos/{full_name}/actions/workflows/{id_}/runs'
        r = _get( url, params = params)
        r = r['workflow_runs']
        ret += r
        if len(r) < per_page:
            break
        i += 1
    ret.sort( key=lambda wfr: -wfr['run_number'])
    if show:
        jlib.log( '{json.dumps(ret, indent="    ")}')
    else:
        return ret

def workflow_run(name, id, show=False):
    '''
    Returns a workflow.
    Args:
        name:
            E.g. `ArtifexSoftware/mupdf-julian`.
        id:
            Integer id of workflow or workflow dict from Github.
        show:
            If true we print results, otherwise we return results.
    '''
    full_name = name['full_name'] if isinstance(name, dict) else name
    r = _get( f'https://api.github.com/repos/{full_name}/actions/runs/{id}')
    if show:
        jlib.log( '{json.dumps(r, indent="    ")}')
    else:
        return r

def rate_limit(show=False):
    ret = _get( f'https://api.github.com/rate_limit')
    #jlib.log( '{json.dumps(ret, indent="    ")}')
    if show:
        jlib.log( '{json.dumps(ret, indent="    ")}')
    else:
        return ret

def test():
    workflows_1 = workflows('ArtifexSoftware/mupdf-julian')
    workflows_2 = workflows('ArtifexSoftware/mupdf-julian')
    assert workflows_2 == workflows_1


def html_workflows(repository_pattern=None, days=5, out=None, upload=None, verbose=False):
    '''
    Generate HTML page containing information about last `days` of Github
    workflow runs.
    
    Args:
        repository_pattern:
            If specified, should be a regex that matches required respository
            names, e.g. `^PyMuPDF`.
        days:
            Number of days of history for which we generate informations.
        out:
            Path of output file. If None we write to a default.
    '''
    t_today_begin = _time_daystart()
    #jlib.log('{jlib.date_time(t_today_begin)=}')
    t_min = t_today_begin - 24*3600 * (days-1)
    if out is None:
        out = os.path.relpath( os.path.abspath( f'{__file__}/../ghstatus.html'))
    out_temp = f'{out}-'
    with open( out_temp, 'w') as f:
    
        # We raise an exception if we write too many bytes to `f`. This
        # happened on 2023-09-01, possibly because of bad data from Github.
        #
        class State:
            def __init__(self):
                self.numbytes = 0
        s = State()
        def _write_check(text):
            s.numbytes += len(text)
            if s.numbytes > 1000*1000:
                message = f'Abandoning writing to {out_temp=} because {s.numbytes=}.'
                print(f'\n\n{message}\n\n', file=f)
                raise Exception(message)
        
        def writeln(text):
            print(text, file=f)
            _write_check(text)
        
        def write(text):
            f.write(text)
            _write_check(text)
        
        t_min_text = time.strftime("%F (%a)", time.gmtime(t_min))
        t_max_text = time.strftime("%F (%a)", time.gmtime(t_today_begin))
        title = f'Github workflow runs {t_min_text} - {t_max_text}'
        writeln('<html>')
        writeln('<style>')
        writeln('table, tr, th, td {')
        writeln('  border: 1px solid black;')
        writeln('  border-collapse: collapse;')
        writeln('}')
        writeln('</style>')
        writeln('</style>')
        writeln(f'<title>{title}</title>')
        writeln('<body>')
        writeln(f'<h1>{title}</h1>')
        writeln(f'This page generated at: {time.strftime("%F (%a) %T")}')
        def html_status(conclusion):
            if conclusion == None:
                return '<span style="color:orange">?</span>'
            elif conclusion == 'success':
                return '<span style="color:green">o</span>'
            else:
                return '<span style="color:red">X</span>'
        writeln(f'<ul>')
        writeln(f'<li> {html_status("success")} - success')
        writeln(f'<li> {html_status(" ")} - fail.')
        writeln(f'<li> {html_status(None)} - in progress.')
        writeln(f'</ul>')
        
        accounts = (
                'pymupdf',
                'ArtifexSoftware',
                )
        writeln(f'<table>')
        writeln(f'<p><table><tr><th style="text-align:left">Account</th><th style="text-align:left">Repository</th><th style="text-align:left">Workflow</th>')
        for t in range(t_today_begin, t_min-1, -24*3600):
            tt = time.strftime("%F (%a)", time.gmtime(t))
            write(f'<th>{tt}</th>')
        writeln('</tr>')
        
        for account in accounts:
            if verbose:
                jlib.log('Looking at {account=}')
            repositories_ = repositories(account)
            repositories_.sort( key=lambda r: r['html_url'])
            account_html_url = f'<a href="https://github.com/{account}">{account}</a>'
            writeln(f'<tr><td colspan="3">{account_html_url}</td>{"<td></td>" * days}</tr>')
            for repository in repositories_:
                jlib.log('Looking at {repository["name"]=}.')
                repository_html_url = f'<a href="{repository["html_url"]}">{repository["name"]}</a>'
                if repository_pattern and not re.match(repository_pattern, repository['name']):
                    continue
                #jlib.log('{repository["full_name"]=}')
                workflows_ = workflows(repository)
                if not workflows_:
                    continue
                #if repository["name"] == 'PyMuPDF-performance':
                #    break
                writeln(f'<tr><td></td><td colspan="2">{repository_html_url}</td>{"<td></td>" * days}</tr>')

                for workflow in workflows_:
                    # Seems odd that the `workflow` dict doesn't have an
                    # html_url entry; so we have to construct the URL manually.
                    jlib.log('Looking at {workflow["name"]=}.')
                    workflow_html_url = f'<a href="https://github.com/{repository["full_name"]}/actions/workflows/{os.path.basename(workflow["path"])}">{workflow["name"]}</a>'
                    #writeln(f'<tr><td>{account_html_url}</td><td>{repository_html_url}</td><td>{workflow_html_url}</td>')
                    writeln(f'<tr><td></td><td></td><td>{workflow_html_url}</td>')
                    write(f'    <td>')
                    t = t_today_begin
                    for workflow_run in workflow_runs(repository, workflow, t_min=t_min):
                        wfr_t = time.strptime( workflow_run['updated_at'], '%Y-%m-%dT%H:%M:%S%z')
                        wfr_t_daystart = _time_daystart(wfr_t)
                        while 1:
                            if t == wfr_t_daystart:
                                break
                            t -= 24*3600
                            write(f'</td><td>')
                        conclusion = workflow_run['conclusion']
                        #jlib.log('{repository["name"]=} {workflow["path"]=} {conclusion=}')
                        write(f'<a href="{workflow_run["html_url"]}">')
                        write(html_status(conclusion))
                        writeln('</a>')
                    while 1:
                        if t == t_min:
                            break
                        t -= 24*3600
                        write(f'</td><td>')
                    writeln(f'</td></tr>')
        writeln('</table>')
                    
        writeln('</body>')
        writeln('</html>')
    os.rename( out_temp, out)
    jlib.log('Have written html to: {out}')
    if upload:
        if upload == 'julian':
            upload = 'julian@ghostscript.com:public_html/ghstatus.html'
        jlib.system(f'rsync -ai {out} {upload}', verbose=1)


def gh_token(token):
    pymupdf.gh_token(token)

def token_file( path):
    '''
    Sets the global Github token to be used when getting data from github, from
    contents of specified file.
    '''
    token = jlib.fs_read( path).strip()
    gh_token(token)


def _time_daystart(t=None):
    if t is None:
        t = time.time()
    if isinstance(t, time.struct_time):
        tt = t
    else:
        tt = time.gmtime(t)
    tt = time.struct_time((
            tt.tm_year,
            tt.tm_mon,
            tt.tm_mday,
            0,
            0,
            0,
            0,
            0,
            tt.tm_isdst,
            ))
    return calendar.timegm(tt)

if __name__ == '__main__':
    # Always use our Github cache to avoid rate limiting.
    _cache_load()
    try:
        aargs.cli()
    finally:
        _cache_save()

