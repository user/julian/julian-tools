#! /usr/bin/env python3

'''
Support for maintaining local file containing github metrics for pymupdf, and
generating a web page with links to the data in **.csv** format.

We use `aargs.cli()` to give a command line iterface; run with `-h` for help.


**Setting up with `cron` on `casper.ghostscript.com`:**

    * Rsync `julian-tools/` directory to
      `casper.ghostscript.com:artifex-remote/julian-tools/`:
    
        `./julian-tools/jtest.py remote julian@casper.ghostscript.com,julian-tools ''`
    
    * Create file in remote `.../julian-tools/` directory containing github token
      for cron job to use (replace TOKEN with token text, usually starting with
      **ghp_**:
    
      `ssh julian@casper.ghostscript.com 'echo TOKEN > artifex-remote/julian-tools/github-token'`
    
    * Install cron job with:
    
      `ssh julian@casper.ghostscript.com crontab -e`
      
      For example::
    
          # minute hour day-of-month month day-of-week command
          43 15 * * * ./artifex-remote/julian-tools/gh_metrics.py token_file artifex-remote/julian-tools/github-token update_all
    
    * Data will be written to: `https://ghostscript.com/~julian/pymupdf_metrics`
'''

import calendar
import copy
import json
import os
import pickle
import shutil
import textwrap
import time

import aargs
import jlib
import pymupdf


def token_string( token: str):
    '''
    Sets the global Github token to be used when getting data from github.
    `token` usually starts with **ghp_**.
    '''
    pymupdf.g_gh_token = token


def token_file( path):
    '''
    Sets the global Github token to be used when getting data from github, from
    contents of specified file.
    '''
    pymupdf.g_gh_token = jlib.fs_read( path).strip()


g_directory = 'github_pymupdf_metrics'

def directory( path):
    '''
    Sets default directory for all generated files.
    '''
    global g_directory
    g_directory = path


def update_all( remote='julian'):
    '''
    Updates everything; calls `gh_metrics.update()` and `gh_metrics.html()`.

    If remote is true we also call `gh_metrics.html_sync(remote=remote)`.
    '''
    update()
    html()
    if remote:
        html_sync( doit=True, remote=remote)


def update():
    '''
    Pulls new metrics data from github and updates local **.pickle** database.
    '''
    directory, path_base = _paths()
    
    data_all_pickle = f'{path_base}.pickle'
    data_all_text = f'{path_base}.text'
    
    try:
        with open( data_all_pickle, 'rb') as f:
            data_all = pickle.load( f)
    except Exception as e:
        if os.path.exists( data_all_pickle):
            raise Exception( f'Failed to unpickle from {data_all_path=}') from e
        jlib.log( 'Creating new empty data because does not exist: {data_all_pickle}')
        data_all = dict()
    
    data_new = _data_new()
    _write_pickle_text( data_new, f'{path_base}-data-new', timestamp=True)
    
    _update( data_all, data_new)
    
    mtime = jlib.fs_mtime( data_all_pickle)
    if mtime and time.time() - mtime >= 1*23*3600:
        # Make copy of old data periodically.
        leaf = time.strftime( '%Y-%m-%d-%H-%M-%S')
        jlib.fs_copy( data_all_pickle, f'{data_all_pickle}-{leaf}')
        jlib.fs_copy( data_all_text, f'{data_all_text}-{leaf}')
        
    # Write new data to file, using temporary+rename to avoid corruption if
    # something goes wrong.
    #
    data_all_pickle_ = f'{data_all_pickle}_'
    with open( data_all_pickle_, 'wb') as f:
        pickle.dump( data_all, f)
    os.rename( data_all_pickle_, data_all_pickle)
    
    with open( f'{data_all_text}', 'w') as f:
        json.dump( data_all, f, indent=4)
    
    jlib.log( 'Have updated: {data_all_pickle}')
    return data_all


def html():
    '''
    Creates **index.html** and **.csv** files in **html/** sub-directory from
    **.pickle** database as updated by `gh_metrics.update()`.
    '''
    directory, path_base = _paths()
    d = f'{directory}/html'
    csv_base = f'{d}/pymupdf-metrics_'
    
    d_old = None
    if os.path.exists( d):
        # Preserve previous html directory to allow comparison.
        d_old = f'{d}_prev'
        jlib.fs_remove( d_old)
        shutil.copytree( d, d_old)
    
    jlib.fs_ensure_empty_dir( d)
    
    with open( f'{path_base}.pickle', 'rb') as f:
        data_all = pickle.load( f)
    
    class State:
        pass
    state = State()
    state.index_text = ''
    state.index_text += textwrap.dedent( f'''
            <html>
            <title>Github PyMuPDF metrics</title>
            <body>
            <h2>Github PyMuPDF metrics</h2>
            <p>Updated: {time.strftime('%F (%a) %T %z %Z', time.gmtime())}
            <ul>
            ''')
    
    def add( fn, name, headings, text):
        '''
        Creates a `.csv` file with data from `fn` and appends to
        `state.index_text`.
        '''
        path, num_lines = _write_csv( fn, name, csv_base, headings, '.csv.text')
        leaf = os.path.basename(path)
        state.index_text += f'    <li><a href="{leaf}">{leaf}</a>\n'
        state.index_text += f'        <ul>\n'
        state.index_text += f'        <li><code>{name}</code>\n'
        state.index_text += f'        <li>{text}\n'
        state.index_text += f'        <li>{num_lines} lines.\n'
        state.index_text += f'    </ul>\n'
    
    # For each .csv file that we create, we define a function that yields
    # each line's data in turn, and pass it to add().
    
    name = 'stats/code_frequency'
    def get():
        data = data_all[ name]
        for t in sorted( data.keys()):
            additions, deletions = data[ t]
            yield t, additions, deletions
    add( get(), name, 'time,additions,deletions', 'Additions and deletions for each week.')
    
    name = 'stats/commit_activity'
    def get():
        data = data_all[ name]
        for t in sorted( data.keys()):
            item = data[ t]
            yield [t] + item[ 'days']
    add( get(), name, 'time,mon,tue,wed,thu,fri,sat,sun', 'Per-day commits for for each week.')
    
    name = 'stats/punch_card'
    def get():
        data = data_all[ name]
        items = _dict_last( data)
        #jlib.log( '{name=}: {items=}')
        ret = []
        for day in range( 7):
            for hour in range( 24):
                n = items[ day][  hour]
                ret.append( n)
        yield ret
    headings = []
    for day in 'mon tue wed thu fri sat sun'.split():
        for hour in range( 24):
            headings.append( f'{day}-{hour}')
    add( get(), name, ','.join( headings), 'Commits per hour throughout week.')
    
    name = 'traffic/clones'
    names = 'time,count,uniques'
    def get():
        names2 = names.split( ',')
        data = data_all[ name]
        for t in sorted( data.keys()):
            item = data[ t]
            yield [t] + [ item[ name] for name in names2[1:]]
    add( get(), name, names, 'Clones/unique clones per week.')
    
    name = 'traffic/popular/paths'
    names = 'path,title,count,uniques'
    def get():
        names2 = names.split( ',')
        data = data_all[ name]
        for item in _dict_last( data):
            yield [ item[name] for name in names2]
    add( get(), name, names, 'Most recent list from github. Contains utf8 characters.')
    
    name = 'traffic/popular/referrers'
    names = 'referrer,count,uniques'
    def get():
        names2 = names.split( ',')
        data = data_all[ name]
        for item in _dict_last( data):
            yield [ item[name] for name in names2]
    add( get(), name, names, 'Most recent list from github.')
            
    name = 'traffic/views'
    names = 'time,count,uniques'
    def get():
        names2 = names.split( ',')
        data = data_all[ name]
        for t in sorted( data.keys()):
            item = data[ t]
            yield [t] + [ item[ name] for name in names2[1:]]
    add( get(), name, names, 'Page views per week.')
    
    state.index_text += textwrap.dedent( '''
            </ul>
            </body>
            </html>
            ''')
    path = f'{d}/index.html'
    jlib.fs_write( path, state.index_text)
    
    if d_old:
        jlib.log( 'Showing changes to index.html and .csv.text files:')
        jlib.system( f'diff -ur {d_old} {d}', raise_errors=0, out='log', verbose=1, prefix=4)
    
    jlib.log( 'Have created: {path}')


def html_sync( doit=False, remote='julian'):
    '''
    Uses **rsync** to copy files generated by `gh_metrics.html()` to remote
    location.
    
    Args:
        doit:
            If false we do nothing.
        remote:
            Rsync destination. Special case **julian** syncs to hard-coded
            location.
    '''
    directory, _ = _paths()
    d = f'{directory}/html'
    if remote == 'julian':
        remote = 'julian@casper.ghostscript.com:public_html/pymupdf_metrics'
        url = 'https://ghostscript.com/~julian/pymupdf_metrics'
    else:
        url = None
    if doit:
        command = f'rsync -ai {d}/ {remote}/'
        jlib.system( command, verbose=1)
        if url:
            jlib.log( f'Have updated: {url}')
        #jlib.log( 'ghostscript.com/~robin/ForKayla')
    

def test( make_testdata=None):
    '''
    Checks that our update code works.
    '''
    path_base = 'github_pymupdf_metrics_test/pymupdf-metrics'
    
    path_testdata = f'{path_base}-testdata'
    path_testdata_pickle = f'{path_testdata}.pickle'
    if make_testdata is None:
        make_testdata = not os.path.exists( path_testdata_pickle)
    if make_testdata:
        data = _data_new()
        _write_pickle_text( data, path_testdata)
    
    with open( path_testdata_pickle, 'rb') as f:
        data = pickle.load( f)
    
    data_all = dict()
    _update( data_all, copy.deepcopy( data), verbose=0)
    data_all0 = copy.deepcopy( data_all)
    
    # Update a second time. This should only give minor changes to known items.
    _update( data_all, copy.deepcopy( data), verbose=0)
    
    _diff_dicts( data_all0, data_all)
    
    _write_pickle_text( data, f'{path_base}-data')
    
    assert len( data_all['community/profile']) == 1

    _data_all_check( data_all0, data_all)


def _diff_dicts( a, b):
    '''
    Shows top-level differences between dicts.
    '''
    ret = 0
    for bk in b.keys():
        if bk not in a:
            jlib.log( '    + {bk}')
            ret = 1
    for ak in a:
        if ak not in b:
            jlib.log( '    - {ak}')
            ret = 1
    for k in a.keys():
        if k in b:
            if a[ k] != b[ k]:
                #jlib.log( '    * {k}')
                ret = 1
    return ret
    

# Names of github metrics that we download.
#
_github_metrics_names = (
        'community/profile',
        'stats/code_frequency',
        'stats/commit_activity',
        'stats/contributors',
        'stats/participation',
        'stats/punch_card',
        'traffic/clones',
        'traffic/popular/paths',
        'traffic/popular/referrers',
        'traffic/views',
        )


def _data_new():
    '''
    Returns single dict containing all available github metrics data.
    '''
    assert pymupdf.g_gh_token, f'Need to call `gh_metrics.token_file() or `gh_metrics.token_string()` first.'
    
    data_all = dict()
    for name in _github_metrics_names:
        jlib.log( 'Getting {name=}')
        r = pymupdf._gh_get( f'https://api.github.com/repos/pymupdf/PyMuPDF/{name}')
        data = r.json()
        data_all[ name] = data
    return data_all


def _write_pickle_text( data, path_base, timestamp=False):
    '''
    Writes `data` to <path_base>.pickle and <path_base>.text.

    If `timestamp` is true we write to
    <path_base>-YYYY-MM-DD-hh-mm-ss.{pickle,text}.
    '''
    if timestamp:
        path_base += time.strftime( '-%Y-%m-%d-%H-%M-%S')
    path_pickle = f'{path_base}.pickle'
    jlib.fs_ensure_parent_dir( path_pickle)
    with open( path_pickle, 'wb') as f:
        pickle.dump( data, f)
    path_text = f'{path_base}.text'
    with open( path_text, 'w') as f:
        json.dump( data, f, indent=4)


def _data_all_check( data_all0, data_all):
    '''
    Checks that changes are consistent with a single update.
    
    Args:
        data_all0:
            Original data.
        data_all:
            Data after _update().
    '''
    # Processing the same input data twice mostly leaves the data unchanged,
    # except for names where we always append, e.g. 'stats/punch_card'.
    for name in _github_metrics_names:
        #jlib.log( 'Checking {name=}')
        if name == 'stats/participation':
            continue
        value0 = data_all0.get( name)
        if value0 is None:
            continue
        value1 = data_all[ name]
        if name in (
                'community/profile',
                #'stats/contributors',
                ):
            # We sometimes add new item.
            if len( value1) == len( value0):
                if value1 != value0:
                    _diff_dicts( value0, value1)
                assert value1 == value0#, jlib.log_text( '{=value0 value1}')
            else:
                assert len( value1) == len( value0) + 1
                value0b = copy.deepcopy( value0)
                k = sorted( value1.keys())[-1]
                value0b[ k] = value1[ k]
                assert value1 == value0b
        elif name in ( 'stats/contributors',):
            for id0 in value0.keys():
                if id0 in value1.keys():
                    _dict_check_tmax(
                            name,
                            value0[ id0][ 'times'],
                            value1[ id0][ 'times'],
                            time.time() - 15 * 24*3600,
                            )
                    #jlib.log( '{=id0} {value0[ id0].keys()=}')
                    _dict_check_tmax(
                            name,
                            value0[ id0][ 'authors'],
                            value1[ id0][ 'authors'],
                            time.time() - 15 * 24*3600,
                            )
                    
        elif name in (
                'stats/punch_card',
                'traffic/popular/paths',
                'traffic/popular/referrers',
                ):
            # For these items, it's not possible to merge the data so
            # _update() simply appends the new data to a list.
            assert len( value1) == len( value0) + 1
            value0b = copy.deepcopy( value0)
            k = sorted( value1.keys())[-1]
            value0b[ k] = value1[ k]
            assert value1 == value0b
        elif name in (
                'stats/code_frequency',
                'stats/commit_activity',
                'traffic/clones',
                'traffic/views',
                ):
            # Looks like data was changed for:
            #   2022-06-26
            #   2022-07-10
            #   2022-07-17
            t = int( time.time())
            for day in range( t - 900 * 24*3600, t - 15 * 24*3600, 24*3600):
                v0 = dict()
                v1 = dict()
                for k, v in value0.items():
                    if k >= day and k < day + 24*3600:
                        v0[ k] = v
                for k, v in value1.items():
                    if k >= day and k < day + 24*3600:
                        v1[ k] = v
                if v0 != v1:
                    jlib.log( '{name=}: Data different for 24h starting {jlib.date_time(day)}')
                    jlib.log( '    v0:')
                    for k, v in v0.items():
                        jlib.log( '        {jlib.date_time(k)}: {v}')
                    jlib.log( '    v1:')
                    for k, v in v1.items():
                        jlib.log( '        {jlib.date_time(k)}: {v}')
            # We can modify data from last 14 days.
            t14 = time.time() - 15 * 24*3600
            value0_14 = dict()
            value1_14 = dict()
            for k, v in value0.items():
                if k < t14:
                    value0_14[ k] = v
            for k, v in value1.items():
                if k < t14:
                    value1_14[ k] = v
            #assert value0_14 == value1_14 #, jlib.log_text( '{=name value0_14 value1_14}')
        elif name == 'stats/contributors':
            t14 = time.time() - 15 * 24*3600
            value0_14 = copy.deepcopy( value0)
            value1_14 = copy.deepcopy( value1)
            def delete_recent( d):
                keys_to_delete = list()
                for k in data.keys():
                    if k >= t14:
                        keys_to_delete.append( k)
                for k in keys_to_delete:
                    del data[ k]
            
            for id, data in value0_14.keys():
                delete_recent( data)
            for id, data in value1_14.keys():
                delete_recent( data)
            assert value0_14 == value1_14
        else:
            assert value0 == value1


def _dict_last( d):
    '''
    Returns last value in dict `d`, by sorting the keys.
    '''
    assert d
    keys = sorted( d.keys())
    key = keys[ -1]
    return d[ key]


def _dict_check_tmax( name, a, b, t_max):
    '''
    Assert that dicts `a` and `b` are identical for keys less than `t_max`.
    '''
    def make( d):
        ret = dict()
        for t, value in d.items():
            if t < t_max:
                ret[ t] = value
        return ret
    aa = make( a)
    bb = make( b)
    if aa != bb:
        ts = set(aa.keys()).union( set(bb.keys()))
        for t in sorted(ts):
            aaa = aa.get(t)
            bbb = bb.get(t)
            if aaa != bbb:
                jlib.log( '{name} diff: t={jlib.date_time(t)}: {aaa=} {bbb=}')
    #assert aa == bb


def _csv_escape( field):
    '''
    Returns `field` with suitable escaping for use in a .csv file.
    '''
    field = str( field)
    field = field.replace( '"', '""')
    if ',' in field:
        field = f'"{field}"'
    return field


def _write_csv( items, name, path_base, names, suffix='.csv'):
    '''
    Creates a .csv file called:
    
        `<path_base><name.replace('/','_')><suffix>`
    
    Args:
        items:
            Sequence of lines. Each line is sequence of fields. Number of
            fields must match `names`.
        name:
            Name of data; used in path of .csv file.
        path_base:
            Base name of path of .csv file. We append `name` with forward
            slashes replaced by underscores.
        names:
            Comma-separated field names.
        suffix:
            Appended to path of generated file.
    '''
    names2 = names.split( ',')
    path_suffix = name.replace( '/', '_')
    path = f'{path_base}{path_suffix}{suffix}'
    with open( path, 'w') as f:
        f.write( f'{names}\n')
        for num_lines, item in enumerate( items):
            #jlib.log( '{item=}')
            for i, field in enumerate( item):
                field = _csv_escape( field)
                sep = ',' if i else ''
                f.write( f'{sep}{field}')
            f.write( '\n')
            assert i+1 == len( names2), jlib.log_text( '{=i len(names2) names names2}')
    jlib.log( 'Have created: {path}')
    return path, num_lines + 1


def _paths():
    directory = g_directory
    path_base = f'{directory}/pymupdf-metrics'
    return directory, path_base


def _update( data_all, data_new, verbose=0):
    '''
    Updates `data_all` with new metrics from github.
    
    Args:
        data_all:
            Dict containing old data, which we update in-place.
        data_new:
            If None, we read new metrics data from github's rest api.

            Otherwise a dict with keys matching tails of github rest api urls.
            
            Note that this dict will be modified.
    
    `data_all` is in the following format:
    
    `
        data_all
        {
            'community/profile':
            {
                time:
                {
                    'health_percentage':
                    'description'
                    ...
                }
            }
            
            'stats/code_frequency':
            {
                time:
                    (adds, deletes)
            }
            
            'stats/commit_activity':
            {
                time_week:
                {
                    'total':
                    'days':
                        [mon, tue, wed, thu, fri, sat, sun]
                }
            }
            
            'stats/contributors':
            {
                id:
                {
                    'times':
                    {
                        time:
                        {
                            'a': additions
                            'd': deletions
                            'c': commits
                        }
                    }
                    'authors':
                    {
                        time:
                        {
                            ...
                        }
                    }
                }
            }
            
            #'stats/participation'
            
            'stats/punch_card':
            {
                sample_time:
                {
                    day:    # day number from 0..6.
                    {
                        hour:   # hour number from 0.23.
                            commits
                    }
                }
            }
            
            'traffic/clones':
            {
                time_week:
                {
                    'count': count
                    'uniques': uniques
                }
            }
            
            'traffic/popular/paths':
            {
                time:
                [
                    {
                        'path':
                        'title':
                        'count':
                        'uniques':
                    }
                ]
            }
            
            'traffic/popular/referrers':
            {
                time:
                [
                    {
                        'referrer':
                        'count':
                        'uniques':
                    }
                ]
            }
            
            'traffic/views':
            {
                time_week:
                {
                    'count':
                    'uniques':
                }
            }
        }
    `
    '''
    t_current = time.time()
    
    def get( name):
        if data_new:
            #jlib.log( 'Getting {name=} from testdata.')
            return data_new[ name]
        else:
            assert 0
            jlib.log( 'Getting metrics from github: {name=}')
            r = pymupdf._gh_get( f'https://api.github.com/repos/pymupdf/PyMuPDF/{name}')
            return r.json()
    
    def timestamp_to_t( text):
        t = time.strptime( text, '%Y-%m-%dT%H:%M:%SZ')
        t = calendar.timegm( t)
        return t
    
    data_all0 = copy.deepcopy( data_all)

    name = 'community/profile'
    # We add new data if different from most recent old data.
    data = get( name)
    a = data_all.setdefault( name, dict())
    if not a or _dict_last( a) != data:
        if a:
            jlib.log( 'Appending modified community/profile')
        a[ t_current] = data
    
    name = 'stats/code_frequency'
    data = get( name)
    # <data> has complete historical data:
    #
    #   [
    #       [
    #           t,
    #           adds,
    #           deletes,
    #       ]
    #   ]
    #
    a = data_all.setdefault( name, dict())
    for t, adds, deletes in data:
        a_item = a.get( t)
        if a_item:
            a_adds, a_deletes = a_item
            assert adds >= a_adds, f'adds={adds} a_adds={a_adds}'
            # <deletes> is negative, so we expect it to stay the same or decrease:
            assert deletes <= a_deletes, f'deletes={deletes} a_deletes={a_deletes}'
        a[ t] = adds, deletes
    
    name = 'stats/commit_activity'
    data = get( name)
    # <data> has most recent 52 weeks of data.
    #
    #   [   # 52 items.
    #       {
    #           'week': t
    #           'days': [mon,tue,wed,thu,fri,sat,sun]
    #       }
    #   ]
    #
    a = data_all.setdefault( name, dict())
    for item in data:
        #jlib.log( '{json.dumps(item, indent=4)=}')
        # We extract the 'week' field as dict key.
        t_week = item.pop( 'week')
        a_item = a.get( t_week)
        if a_item:
            for i in range(7):
                assert item[ 'days'][ i] >= a_item[ 'days'][ i]
        a[ t_week] = item
    
    name = 'stats/contributors'
    data = get( name)
    # <data> has complete historical data for each contributer.
    #
    #   [   # On item for each contributer.
    #       {
    #           'weeks':
    #               [   # One item for each week of entire contributer history.
    #                   'w': t
    #                   'a': adds
    #                   'd': deletes
    #                   'c': commits
    #               ]
    #           'author':
    #               {
    #                   'id':
    #                   'login':
    #                   ...
    #               }
    #       }
    #   ]
    #
    a = data_all.setdefault( name, dict())
    for contributor in data:
        id = contributor[ 'author'][ 'id']
        a_id = a.setdefault( id, dict())
        a_id_times = a_id.setdefault( 'times', dict())
        
        if 0:
            # move any raw times into a_id_times.
            ks = []
            for k, v in a_id.items():
                if isinstance( k, int):
                    ks.append( k)
                    a_id_times[ k] = v
            for k in ks:
                del a_id[ k]
        
        for week_data in contributor[ 'weeks']:
            week = week_data.pop( 'w')
            week_last = week
            a_week_data = a_id_times.get( week)
            if a_week_data:
                assert week_data[ 'a'] >= a_week_data[ 'a']
                assert week_data[ 'd'] >= a_week_data[ 'd']
                assert week_data[ 'c'] >= a_week_data[ 'c']
            a_id_times[ week] = week_data
        
        # Add author a_id_authors['authors'][week] if they have changed.
        b_id_author = contributor[ 'author']
        author_changed = True
        a_id_authors = a_id.setdefault( 'authors', dict())
        if a_id_authors:
            if _dict_last( a_id_authors) == b_id_author:
                author_changed = False
        if author_changed:
            if a_id_authors:
                jlib.log( 'Appending changed author for stats/contributors')
            a_id_authors[ week_last] = b_id_author
    
    name = 'stats/participation'
    # Ignore - this is total commits in last 52 weeks, which is difficult to
    # update, and more detailed info is in stats/contributors anyway.
    
    name = 'stats/punch_card'
    data = get( name)
    # <data> is complete historical data:
    #
    #   [
    #       {
    #           day,    # 0..6
    #           hour,   # 0..23
    #           commits,
    #       }
    #   ]
    #
    # We always create a new item keyed on current time `t_current`, because
    # this item is overall statistics and not really mergable.
    #
    a = data_all.setdefault( name, dict())
    a_t = dict()
    a[ t_current] = a_t
    for weekday, hour, commits in data:
        a_t.setdefault( weekday, dict())[ hour] = commits
    
    name = 'traffic/clones'
    data = get( name)
    # <data> is:
    #
    #   {
    #       'clones':
    #           [
    #               {
    #                   'timestamp':
    #                   'count':
    #                   'uniques':
    #               }
    #           ]
    #   }
    #
    a = data_all.setdefault( name, dict())
    items = data[ 'clones']
    # First item seems to be partial count and can be lower than in a
    # previous run.
    for i, item in enumerate( items[1:], start=1):
        t0 = item.pop( 'timestamp')
        t = timestamp_to_t( t0)
        prev = a.get( t)
        if prev is not None:
            assert item[ 'count'] >= prev[ 'count'], jlib.log_text( '{=i t0 t prev["count"] item["count"]}')
            assert item[ 'uniques'] >= prev[ 'uniques'], jlib.log_text( '{=i t0 t prev["uniques"] item["uniques"]}')
        a[ t] = item
    
    name = 'traffic/popular/paths'
    data = get( name)
    # <data> is:
    #
    #   [   # 10 items.
    #       {
    #           'path':
    #           'title':
    #           'count':
    #           'uniques':
    #       }
    #   ]
    #
    a = data_all.setdefault( name, dict())
    a[ t_current] = data
    
    name = 'traffic/popular/referrers'
    data = get( name)
    # <data> is:
    #
    #   [   # 10 items.
    #       {
    #           'referrer':
    #           'count':
    #           'uniques':
    #       }
    #   ]
    #
    a = data_all.setdefault( name, dict())
    a[ t_current] = data
    
    name = 'traffic/views'
    data = get( name)
    # <data> is:
    #
    #   {
    #       'views':
    #           [   # 15 items.
    #               {
    #                   'timestamp':
    #                   'count':
    #                   'uniques':
    #               }
    #           ]
    #   }
    #
    a = data_all.setdefault( name, dict())
    items = data[ 'views']
    assert len( items) in (14, 15), jlib.log_text( '{len( items)=}')
    # Ignore first item because it seems to contain partial data.
    items = items[ 1:]
    for item in items:
        t = item.pop( 'timestamp')
        t = timestamp_to_t( t)
        a_value = a.get( t)
        if a_value:
            assert item[ 'count'] >= a_value[ 'count']
            assert item[ 'uniques'] >= a_value[ 'uniques']
        a[ t] = item
    
    _data_all_check( data_all0, data_all)


if __name__ == '__main__':
    aargs.cli( show_return=False)
