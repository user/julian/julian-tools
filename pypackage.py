#!/usr/bin/env python3

'''
Support for creating Python wheels and uploading to pypi.org.

Overview:

    Support for:
    
        * Building sdist.
        
        * Building wheels locally/remotely (uses `cibuildwheel`).
        
        * Uploading sdist and wheels to pypi.org (uses `twine`).
        
        * Testing that `python -m pip install <package-name>` works.
    
Examples:

    Build and release MuPDF sdist+wheels to pypi.org:
    
    [Replace $PYPI_TOKEN with pypi.org authentication token.]
    
        Make a test release - just build for python-3.10 and upload to
        test.pypi.org:

            ./julian-tools/pypackage.py \
                sdist mupdf \
                cibw_env CIBW_SKIP "pp* *i686 *-musllinux_* cp36* *win32*" \
                cibw_env CIBW_BUILD_VERBOSITY 3 \
                cibw_env CIBW_BUILD "cp310*" \
                build --test --remote jules-windows:artifex-remote \
                build --test --remote jules-devuan:artifex-remote \
                upload $PYPI_TOKEN \
                | tee out-mupdf-pypackage

        Full release:

            ./julian-tools/pypackage.py \
                sdist mupdf \
                cibw_env CIBW_SKIP "pp* *i686 *-musllinux_* cp36* *win32*" \
                cibw_env CIBW_BUILD_VERBOSITY 3 \
                build --test --remote jules-windows:artifex-remote \
                build --test --remote jules-devuan:artifex-remote \
                pypi_test false \
                upload $PYPI_TOKEN \
                | tee out-mupdf-pypackage
'''

import aargs
import doctest
import glob
import jtest
import os
import platform
import re
import shlex
import shutil
import sys
import tarfile
import textwrap
import time

import jlib

log = jlib.log


def system(
        command,
        raise_errors=True,
        return_output=False,
        prefix=None,
        caller=1,
        bufsize=-1,
        env_extra=None,
        out='log',
        ):
    '''
    Runs a command. See `jlib.system()`'s docs for details.
    '''
    if return_output:
        out = [out, 'return'] if out else 'return'
    return jlib.system(
            command,
            verbose=True,
            raise_errors=raise_errors,
            out=out,
            prefix=prefix,
            caller=caller+1,
            bufsize=bufsize,
            env_extra=env_extra,
            )


def windows():
    s = platform.system()
    return s == 'Windows' or s.startswith('CYGWIN')

def linux():
    s = platform.system()
    return s == 'Linux'

def macos():
    s = platform.system()
    return s == 'Darwin'

def openbsd():
    s = platform.system()
    return s == 'OpenBSD'


def env_string_to_dict( text):
    '''
    Converts string containing shell-style space-separated `name=value` pairs,
    into a dict. `text` can also be a list of strings, or a dict.
    '''
    if isinstance( text, dict):
        return text
    ret = dict()
    if text:
        if isinstance( text, str):
            text = text,
        for t in text:
            for item in shlex.split( t):
                n, v = item.split( '=', 1)
                assert n not in ret
                ret[ n] = v
    return ret


def make_tag(py_version=None):
    '''
    Returns platform tag for a particular Python version.
    Args:
        py_version:
            If None we use native Python version.
            Otherwise should be, for example, '3.9.4'.
    '''
    # Find platform tag used in wheel filename, as described in
    # PEP-0425. E.g. 'openbsd_6_8_amd64', 'win_amd64' or 'win32'.
    #
    import setuptools
    tag_platform = setuptools.distutils.util.get_platform().replace('-', '_').replace('.', '_')

    if py_version is None:
        py_version = platform.python_version()
    # Get two-digit python version, e.g. 3.8 for python-3.8.6.
    #
    tag_python = ''.join(py_version.split('.')[:2])

    # Final tag is, for example, 'py39-none-win32', 'py39-none-win_amd64'
    # or 'py38-none-openbsd_6_8_amd64'.
    #
    tag = f'cp{tag_python}-none-{tag_platform}'
    return tag


# We keep track of all the venv's we have created so that we can avoid
# unnecessarily running `python -m pip --upgrade`. todo: check this is actually
# necessary.
venv_installed = set()

def cpu_bits():
    '''
    Returns '32' or '64' depending on Python build.
    '''
    ret = f'{32 if sys.maxsize == 2**31 - 1 else 64}'
    #jlib.log(f'returning ret={ret!r}')
    return ret


def venv_run(
        commands,
        venv=None,
        py=None,
        clean=False,
        return_output=False,
        directory=None,
        prefix=None,
        pip_upgrade=True,
        bufsize=-1,
        raise_errors=True,
        env_extra=None,
        out=None,
        caller=2,
        system_site_packages=None,
        ):
    '''
    Runs commands inside a Python virtual environment, joined by `&&`.

    Args:
        commands:
            Command or list of commands to run.
        venv:
            Name of Python virtual environment to create and use. Relative to
            `directory` if `directory` is not `None`. If None we define a
            default containing python version and cpu.
        py:
            Python interpreter to run. If `None` we use `sys.executable`.
        clean:
            If true we first delete any existing virtual environment
            called `venv`. For safety we assert that `venv` starts with
            'pypackage-venv'.
        directory:
            Directory in which to create the virtual environment and run
            `commands`. If `None` we use the current directory.
        prefix:
            Prefix to prepend to each output line.
        pip_upgrade:
            If true (the default) we do `python -m pip install --upgrade pip`
            before running `commands`.
        bufsize:
            Use 0 if we expect interactive prompts.
        system_site_packages:
            If true, we create venv with `--system-site-packages`. If
            None, we use True on OpenBSD because libclang not available
            on pypi, so this enables use of system package py3-llvm.
    '''
    if system_site_packages is None and openbsd():
        system_site_packages = True
    if venv is None:
        venv = f'venv-pypackage-{platform.python_version()}-{cpu_bits()}'
    if isinstance(commands, str):
        commands = [commands]
    #jlib.log( '{sys.executable=}')
    #jlib.log( '{py=}')
    if py is None:
        #py = 'py' if windows() else sys.executable
        py = sys.executable

    def make_command( commands):
        if windows():
            command = '&&'.join( commands)
            if platform.system().startswith('CYGWIN'):
                command = f'cmd.exe /c {shlex.quote(command)}'
        else:
            command = ' && '.join( commands)
        return command

    pre = list()
    
    if directory:
        pre.append( f'cd {directory}')
        venv_path = f'{directory}/venv_path'
    else:
        venv_path = venv
    if clean or venv not in venv_installed:
        if clean:
            assert venv.startswith('pypackage-venv')
            log('Removing directory to get clean venv: {venv}')
            shutil.rmtree(venv, ignore_errors=1)
        venv_installed.add(venv)

    if not pip_upgrade and os.path.exists( venv_path):
        jlib.log( 'venv already exists so not creating it: {venv_path}')
    else:
        pre.append( f'{py} -m venv {"--system-site-packages" if system_site_packages else ""} {venv}')
            
    if windows():
        pre.append( f'{venv}\\Scripts\\activate.bat')
    else:
        pre.append( f'. {venv}/bin/activate')
    
    post = [f'deactivate']

    if pip_upgrade:
        pre.append( 'python -m pip install --upgrade pip')

    return system(
            make_command( pre + commands + post),
            raise_errors=raise_errors,
            return_output=return_output,
            prefix=prefix,
            bufsize=bufsize,
            env_extra=env_extra,
            out=out,
            caller=caller,
            )


def check_sdist(sdist):
    '''
    Checks sdist with `twine check`.
    '''
    venv_run([
            f'python -m pip install twine',
            f'twine check {sdist}',
            ])


def check_wheel(wheel):
    '''
    Checks wheel with `twine check`.
    '''
    # We don't install and use check-wheel-contents, because it thinks
    # top-level .dll files are incorrect.
    venv_run([
            f'python -m pip install twine',
            #f'check-wheel-contents {wheel}',
            f'twine check {wheel}',
            ])


def find_new_files(pattern, t):
    '''
    Returns list of files matching `pattern` whose mtime is >= `t`.
    '''
    paths = glob.glob(pattern)
    paths_new = []
    for path in paths:
        tt = os.path.getmtime(path)
        if tt >= t:
            paths_new.append(path)
    return paths_new


def find_new_file(pattern, t):
    '''
    Finds file matching `pattern` whose mtime is >= `t`. Raises exception if
    there isn't exactly one such file.
    '''
    paths_new = find_new_files(pattern, t)

    if len(paths_new) == 0:
        raise Exception(f'No new file found matching glob: {pattern}')
    elif len(paths_new) > 1:
        text = f'More than one file found matching glob: {pattern}\n'
        for path in paths_new:
            text += f'    {path}\n'
        raise Exception(text)

    return paths_new[0]


def make_sdist(package_directory, out_directory, env='', prefix=None):
    '''
    Creates sdist from source tree `package_directory`, in
    `out_directory`. Returns the path of the generated sdist.
    '''
    os.makedirs(out_directory, exist_ok=True)
    env_extra = env_string_to_dict( env)
    t = time.time()
    command = (
            f'cd {os.path.relpath(package_directory)}'
            f' && {sys.executable} ./setup.py sdist -d "{os.path.relpath(out_directory, package_directory)}"'
            )
    system(command, env_extra=env_extra, prefix=prefix)

    # Find new file in <sdist_directory>.
    sdist = find_new_file(f'{out_directory}/*.tar.gz', t)
    check_sdist(sdist)

    return sdist


def make_linux(
        sdist,
        abis=None,
        out_dir=None,
        test_direct_install=False,
        install_docker=None,
        docker_image=None,
        pull_docker_image=None,
        container_name=None,
        ):
    '''
    Builds Python wheels using a manylinux container.

    Args:
        sdist:
            Pathname of sdist file; we copy into container and extract there.
        abis
            List of string python versions for which we build
            wheels. Any '.' are removed and we then take the first two
            characters. E.g. `['3.8.4', '39']` is same as `['38', '39']`.
        out_dir
            If not `None`, the directory into which we generated wheels.
        test_direct_install
            If true we run `python -m pip install <sdist>` in the container.
            If `None` we default to false.
        install_docker
            If true we attempt to install docker (currently specific to
            Debian/Devuan).
            If `None` we default to false.
        docker_image
            Name of docker image to use.
            If `None` we default to: `quay.io/pypa/manylinux2014_x86_64`
        pull_docker_image
            If true we run `docker pull ...` to update the image.
            If `None` we default to true.
        container_name
            Name to use for the container.
            If `None` we default to 'pypackage'.

    Returns wheels, a list of wheel pathnames.
    '''
    if abis is None:
        abis = ['37', '38', '39']
    if test_direct_install is None:
        test_direct_install = False
    if install_docker is None:
        install_docker = False
    if docker_image is None:
        docker_image='quay.io/pypa/manylinux2014_x86_64'
    if pull_docker_image is None:
        pull_docker_image = True
    if container_name is None:
        container_name = 'pypackage'

    io_directory = 'pypackage-io'

    assert sdist.endswith('.tar.gz')
    sdist_leaf = os.path.basename(sdist)    # e.g. mupdf-1.18.0.20210504.1544.tar.gz
    package_root = sdist_leaf[ : -len('.tar.gz')]   # e.g. mupdf-1.18.0.20210504.1544

    check_sdist(sdist)

    if install_docker:
        # Note that if docker is not already installed, we will
        # need to add user to docker group, e.g. with:
        #   sudo usermod -a -G docker $USER
        system('sudo apt install docker.io')

    # Copy sdist into what will be the container's /io/ directory.
    #
    os.makedirs( io_directory, exist_ok=True)
    system( f'rsync -ai {sdist} {io_directory}/')

    if pull_docker_image:
        # Ensure we have latest version of the docker image.
        #
        system(f'docker pull {docker_image}')

    if 1:
        # Ensure docker instance is created and running. Also give it a name
        # <container_name> so we can refer to it below.
        #
        # '-itd' ensures docker instance runs in background.
        #
        # With '-v HOST:CONTAINER', HOST must be absolute path.
        #
        # It's ok for this to fail - container is already created and running.
        #
        e = system(
            f' docker run'
            f' -itd'
            f' --name {container_name}'
            f' -v {os.path.abspath(io_directory)}:/io'
            f' {docker_image}'
            ,
            raise_errors=False,
            )
        log(f'docker run: ignoring any error. e={e}')

        # Start docker instance if not already started.
        #
        e = system(f'docker start {container_name}', raise_errors=False,)
        log(f'docker start: e={e}')

    def docker_system(command, prefix='', return_output=False):
        '''
        Runs specified command inside container. Runs via bash so <command>
        can contain shell constructs. If return_output is true, we return the
        output text.
        '''
        command = command.replace('"', '\\"')
        # Manylinux container doesn't seem to have a python3
        # command/alias, which can break builds. Can't get alias to work. Have tried
        # defining a bash function but this also seems to be ignored by the time
        # we are in make:
        #   command = f'docker exec {container_name} bash -c "function python3 {{ python3.9 \\$* ; }} ; {command}"'
        #
        # So instead we create a softlink.
        #
        command = f'docker exec {container_name} bash -c "ln -fs \\`which python3.9\\` /bin/python3 && {command}"'
        log(f'Running: {command}')
        return system(
                command,
                return_output=return_output,
                prefix=None if return_output else f'container: {prefix}',
                caller=2,
                )

    if test_direct_install:
        # Test direct intall from sdist.
        #
        docker_system( f'pip3 -vvv install /io/{sdist_leaf}')

    container_pythons = []
    for abi in abis:
        vv = abi.replace('.', '')[:2] # E.g. '38' for Python-3.8.
        pattern = f'/opt/python/cp{vv}-cp{vv}*'
        paths = docker_system(f'ls -d {pattern}', return_output=1)
        log(f'vv={vv}: paths={paths!r}')
        paths = paths.strip().split('\n')
        assert len(paths) == 1, f'No single match for glob pattern in container. pattern={pattern}: {paths}'
        container_pythons.append( (vv, paths[0]))

    # In the container, extract <sdist>; we will use the extracted sdist to
    # build wheels.
    #
    docker_system( f'tar -xzf /io/{sdist_leaf}')

    # Build wheels inside container.
    wheels = []
    for vv, container_python in container_pythons:
        log(f'Building wheel with python {vv}: {container_python}')

        # Build wheel.
        t = time.time()
        docker_system(
                f'cd {package_root} && {container_python}/bin/python ./setup.py --dist-dir /io bdist_wheel',
                prefix=f'{container_python}: '
                )

        # Find new wheel.
        wheel = find_new_file(f'{io_directory}/{package_root}-cp{vv}-none-*.whl', t)
        wheels.append(wheel)
        check_wheel(wheel)
        if out_dir:
            # Rename the wheel to be a manylinux wheel so it can be uploaded to
            # pypi.org.
            #
            leaf = os.path.basename(wheel)
            m = re.match('^([^-]+-[^-]+-[^-]+-[^-]+-)linux(_[^.]+.whl)$', leaf)
            assert m, f'Cannot parse wheel: {wheel!r}'
            leaf2 = f'{m.group(1)}manylinux2014{m.group(2)}'
            log(f'changing leaf from {leaf} to {leaf2}')
            os.makedirs(out_dir, exist_ok=1)
            shutil.copy2(wheel, f'{out_dir}/{leaf2}')

    log( f'wheels are ({len(wheels)}):')
    for wheel in wheels:
        log( f'    {wheel}')

    return wheels


def make_unix_native(
        sdist,
        out_dir=None,
        test_direct_install=False,
        ):
    '''
    Makes wheel on native system.
    '''
    assert sdist.endswith('.tar.gz')
    sdist_leaf = os.path.basename(sdist)    # e.g. mupdf-1.18.0.20210504.1544.tar.gz
    package_root = sdist_leaf[ : -len('.tar.gz')]   # e.g. mupdf-1.18.0.20210504.1544

    check_sdist(sdist)
    # Extact sdist and run setup.py to build wheel inside the extracted
    # tree. We make basic checks that extraction will extract to a single
    # subdirectory.
    #
    log(f'Extracting sdist={sdist}')
    directory = jlib.untar( sdist)

    if out_dir is None:
        out_dir = 'pypackage-out'
    out_dir2 = os.path.relpath(os.path.abspath(out_dir), directory)
    t = time.time()
    system( f'cd {directory} && {sys.executable} setup.py -d {out_dir2} bdist_wheel')
    wheel = find_new_file(f'{out_dir}/*.whl', t)
    check_wheel(wheel)
    return [wheel]


def windows_python_from_abi(abi):
    '''
    Returns `(cpu, python_version, py)`.
    '''
    cpu, python_version = abi.split('-')
    python_version = '.'.join(python_version)
    assert cpu in ('x32', 'x64')
    py = f'py -{python_version}-{cpu[1:]}'
    return cpu, python_version, py


def make_windows(
        sdist,
        abis=None,
        out_dir=None,
        ):
    '''
    Builds Python wheels on Windows.

    Args:
        sdist:
            Pathname of sdist; we extract it and generate wheels inside the
            extracted tree. Note that this means that running a second time
            with the same sdist will not be a clean build.

        abis:
            List of `<cpu>-<pythonversion>` strings.
                cpu:
                    'x32' or 'x64'
                pythonversion:
                    String version number, may contain '.' characters,
                    e.g. '3.8' or '39'.
        out_dir:
            Directory into which we copy the generated wheels. Defaults to
            'pypackage-out'.

    Returns wheels, a list of wheel pathnames.
    '''
    log(f'sdist={sdist} abis={abis} out_dir={out_dir}')
    assert windows()
    if abis is None:
        abis = ['x32-38', 'x32-39', 'x64-38', 'x64-39']
    if out_dir is None:
        out_dir = 'pypackage-out'

    # Extact sdist and run setup.py to build wheel inside the extracted
    # tree. We make basic checks that extraction will extract to a single
    # subdirectory.
    #
    log(f'Extracting sdist={sdist}')
    directory = jlib.untar( sdist)

    os.makedirs(out_dir, exist_ok=True)
    out_dir2 = os.path.relpath(os.path.abspath(out_dir), directory)

    wheels = []
    for abi in abis:
        cpu, python_version, py = windows_python_from_abi(abi)
        t = time.time()
        log(f'*** Running venv_run() with directory={directory} os.getcwd()={os.getcwd()}')
        venv_run(
                [
                f'pwd',
                f'python setup.py -d {out_dir2} bdist_wheel',
                ],
                py=py,
                directory=directory,
                prefix=f'{python_version}-{cpu} wheel build: ',
                )
        wheel = find_new_file(f'{out_dir}/*.whl', t)
        check_wheel(wheel)
        wheels.append(wheel)

    return wheels


def test_internal(test_command, package_name, pip_install_arg, py):
    if test_command == '.':
        test_command = ''

    jlib.log('Testing {package_name=} {pip_install_arg=} {py=} {test_command!r=}')
    if not test_command:
        test_command = 'pypackage_test.py'
        code = (
                f'import {package_name}\n'
                f'print("Successfully imported {package_name}")\n'
                )
        log(f'Testing default code for package_name={package_name}:\n{textwrap.indent(code, "    ")}')
        with open(test_command, 'w') as f:
            f.write(code)

    venv_run(
            [
            f'python -m pip install {pip_install_arg}',
            f'python {test_command}',
            ],
            venv='pypackage-venv-test',
            py=py,
            clean=True,
            )

def test_local(test_command, wheels, py):
    '''
    Find wheel matching tag output by running `py` ourselves with 'tag' arg.
    inside a venv running `py`:
    '''
    log(f'Finding wheel tag for {py!r}')
    text = venv_run(
            f'python {sys.argv[0]} tag',
            return_output=True,
            venv='pypackage-venv-test',
            py=py,
            clean=True,
            pip_upgrade=False,  # Saves a little time.
            )
    m = re.search('tag: (.+)', text)
    assert m, f'Failed to find expected tag: ... in output text: {text!r}'
    tag = m.group(1).strip()    # Sometimes we get \r at end, so remove it here.
    py_py, py_abi, py_cpu = tag.split( '-')
    log(f'Looking for wheel matching tag {tag!r}')
    for wheel in wheels:
        wheel_name, wheel_version, wheel_py, wheel_abi, wheel_cpu = parse_wheel(wheel)
        # Allow loose matching of cpu because the cibuildwheel produces wheels
        # with newer names as described in:
        # https://packaging.python.org/en/latest/specifications/platform-compatibility-tags/
        #
        eq = False
        if py_cpu == 'linux_x86_64':
            cpu_match = ('linux' in wheel_cpu and 'x86_64' in wheel_cpu)
        else:
            cpu_match = (wheel_cpu == py_cpu)
        if cpu_match:
            if py_abi == 'none':
                eq = (wheel_py == py_py)
            else:
                eq = (wheel_py, wheel_abi) == (py_py, py_abi)
        if eq:
            log( f'Matching wheel: {wheel}')
            package_name = wheel_name
            pip_install_arg = wheel
            break
    else:
        text = f'Cannot find wheel matching: tag={tag!r}\n'
        text += f'Wheels are ({len(wheels)}):\n'
        for wheel in wheels:
            text += f'    {wheel}\n'
        assert 0, text
    test_internal(test_command, package_name, pip_install_arg, py)


def test_pypi(test_command, package_name, pypi_test, py):
    assert package_name, f'Cannot test installation from pypi.org because no package name specified.'
    pip_install_arg = ''
    if pypi_test:
        pip_install_arg += '-i https://test.pypi.org/simple '
    pip_install_arg += package_name
    test_internal(test_command, package_name, pip_install_arg, py)

        

def test_wheels(wheels, test_command=None):
    '''
    If on Windows, test with a python for each wheel in `wheels`. Otherwise
    test with native python, requiring that there is a matching wheel in
    `wheels`.

    Args:
        wheels:
            List of wheels or string containing space-separated list of wheels.
        test_command:
            If None we simply check that we can import the wheel.
    '''
    if isinstance( wheels, str):
        wheels = wheels.split()
    
    if windows():
        # Test with each wheel.
        log('Testing for python implied by each wheel.')
        for wheel in wheels:
            name, version, py, none, cpu = parse_wheel(wheel)
            pyv = f'{py[2]}.{py[3:]}'
            cpu_bits = 64 if cpu == 'win_amd64' else 32
            py = f'py -{pyv}-{cpu_bits}'
            test_internal(test_command, name, wheel, py)
    else:
        # Test with python=sys.executable.
        jlib.log( 'Looking for wheel matching {sys.executable=}')
        tag = jlib.system( f'{sys.executable} {__file__} internal_tag', out='return')
        tag = tag.strip()
        jlib.log( 'Tag for {sys.executable} is: {tag!r}')
        py_py, py_abi, py_cpu = tag.split( '-')
        jlib.log( '{=py_py py_abi py_cpu}')
        for wheel in wheels:
            wheel_name, wheel_version, wheel_py, wheel_abi, wheel_cpu = parse_wheel(wheel)
            m = re.match( '^manylinux_[0-9_]+_([^.]+).manylinux[0-9]+_(.+)$', wheel_cpu)
            if m:
                # Translate cibuildwheel's manylinux cpu specifications into
                # something that will match `tag`.
                cpu1 = m.group(1)
                cpu2 = m.group(2)
                assert cpu1 == cpu2, f'Unexpected differing cpu types in {wheel_cpu=}'
                wheel_cpu2 = f'linux_{cpu1}'
                jlib.log( 'Changing {wheel_cpu=} to {wheel_cpu2!r}')
                wheel_cpu = wheel_cpu2
            jlib.log( '{=wheel_py wheel_abi wheel_cpu}')
            if wheel_py == py_py and wheel_abi == py_abi and wheel_cpu == py_cpu:
                test_internal(test_command, wheel_name, wheel, sys.executable)
                break
        else:
            jlib.log( 'Not running test because failed to find wheel matching {tag=}.')


def wheels_for_sdist(sdist, outdir):
    '''
    Returns list of paths of wheels in `outdir` that match `sdist`.
    '''
    m = re.match('^([^-]+)-([^-]+)[.]tar.gz$', os.path.basename(sdist))
    assert m, f'Bad sdist name, expected .../<name>-<version>.tar.gz: {sdist!r}'
    name_version = f'{m.group(1)}-{m.group(2)}-'
    ret = []
    for i in os.listdir(outdir):
        if i.startswith(name_version) and i.endswith('.whl'):
            ret.append(os.path.join(outdir, i))
    return ret


def parse_sdist(sdist):
    '''
    Parses leafname of sdist, returning `(name, version)`.
    '''
    m = re.match('^([^-]+)-([^-]+)[.]tar.gz$', os.path.basename(sdist))
    assert m, f'Unable to parse sdist: {sdist!r}'
    return m.group(1), m.group(2)


def parse_wheel(wheel):
    '''
    Parses leafname of wheel, returning `(name, version, py, abi, cpu)`.
    
    >>> parse_wheel( 'mupdf-1.21.1.20230110.2358-cp310-none-win_amd64.whl')
    ('mupdf', '1.21.1.20230110.2358', 'cp310', 'none', 'win_amd64')
    '''
    m = re.match('^([^-]+)-([^-]+)-([^-]+)-([^-]+)-([^-]+)[.]whl$', os.path.basename(wheel))
    assert m, f'Cannot parse wheel path: {wheel}'
    name, version, py, abi, cpu = m.group(1), m.group(2), m.group(3), m.group(4), m.group(5)
    return name, version, py, abi, cpu


def upload0( files, pypi_test, token=None, prompt=True):
    destination = 'test.pypi.org' if pypi_test else 'pypi.org'
    command = ''
    command += 'python -m twine upload'
    command += f' --disable-progress-bar'
    if pypi_test:
        command += ' --repository testpypi'
    if token:
        command += ' -u __token__'
        command += f' -p {token}'
    command += ' ' + ' '.join( files)
    while 1:
        log(f'Uploading {len(files)} files to {destination}:')
        for file_ in sorted( files):
            log(f'    {file_}')
        jlib.log( 'command is: {command}')
        if prompt:
            input( jlib.log_text( 'Press <enter> to run command and upload to {destination} ... ? ', nl=0))
        try:
            venv_run(
                    [
                        f'python -m pip install twine',
                        command,
                    ],
                    bufsize=0,  # So we see login/password prompts.
                    raise_errors=True,
                    )
        except Exception  as e:
            jlib.log( 'Failed to upload: {e=}')
            input( jlib.log_text( 'Press <enter> to retry... ? ', nl=0))
        else:
            break


def build_cibuildwheel( sdist, args, env, outdir, prefix=None, sdist_inplace=False):
    '''
    Builds wheels by running cibuildwheel in a Python virtual environment.

    Args:
        sdist:
            An sdist file or directory containing setup.py.
        args:
            `None` or string of extra args to pass to cibuildwheel.
        env:
            Extra of environmental variable settings, as dict of name=value
            pairs or string of space-separated name=value pairs or sequence of
            such strings.

            For example: `CIBW_BUILD="*-cp39-*x86_64*" CIBW_SKIP="*musllinux*"`

    Returns list of created wheel filenames.
    '''
    log( 'Building wheels using cibuildwheel from {sdist=}. {=args env outdir os.sep sys.executable}')
    # We always need to specify 'cibuildwheel --platform ...'
    # because auto always seems to fail even on devuan.
    if linux():
        platform = 'linux'
    elif windows():
        platform = 'windows'
    elif macos():
        platform = 'macos'
    else:
        platform = 'auto'
    env_extra = env_string_to_dict( env)
    jlib.log( '{env_extra=}')
    t = time.time()
    command = ''
    outdir2 = outdir
    if sdist_inplace:
        outdir2 = os.path.relpath( outdir2, sdist)
        command += f'cd {sdist}&&'
    command += f'python -m cibuildwheel --output-dir {outdir2} --platform {platform}'
    if args:
        command += f' {args}'
    if not sdist_inplace:
        command += f' {sdist}'
    venv_run(
            [
                f'python -m pip install --upgrade cibuildwheel',
                command,
            ],
            env_extra=env_extra,
            prefix=prefix,
            )
    wheels = find_new_files( f'{outdir}/*.whl', t)
    jlib.log( '{outdir=} {wheels=}')
    return wheels


# Set up global state; some of this state is modified by top-level functions,
# e.g. from the command line.
#

class State:
    pass
state = State()
state.sdist = None
state.sdist_inplace = False
state.wheels = []
state.pypi_test = 1
state.abis = None
state.outdir = 'pypackage-out'
state.remote = None
state.cibw_env = []
if windows():
    pass
else:
    state.manylinux_container_name = None
    state.manylinux_install_docker = False
    state.manylinux_docker_image = None
    state.manylinux_pull_docker_image = None

def cibw_env( name, value):
    '''
    Append `(name, value)` to `state.cibw_env`.
    '''
    state.cibw_env.append( ( name, value))

def abis( abis):
    '''
    Set ABIs to build, comma-separated. Default is {state.abis}.

    If 'build --cibuild' is to be used, these items should
    be cibuildwheel style and separators can be commas or
    spaces. For example:

        abis cp39-manylinux_x86_64,cp310-manylinux_x86_64
        abis 'cp39-manylinux_x86_64 cp310-manylinux_x86_64'
    '''
    if isinstance( abis, str):
        abis = abis.split( ',')
    abis_prev = abis
    state.abis = abis
    log(f'Changing abis from {abis_prev} to {abis}')


def build(
        remote=None,
        abis=None,
        test=False,
        cibw=True,
        cibw_args=None,
        cibw_clear_outdir=False,
        ):
    '''
    Builds wheels from sdist/package-directory set by earlier call to
    `sdist()`.
    
    Args:
        remote:
            Build wheels on specified remote machine
            [<user>@]<host>:[<directory>] and copy them back to local machine.
            `remote` is used to construct a `jtest.Remote` instance.

            If `cibw` is true, this will remove pre-existing wheels with the
            specified name from pypackage-out/ on the remote machine; the
            removal may narrow down to wheels with the same version if this
            information is available (i.e. if `sdist` is a file). This is done
            because otherwise cibuildwheel refuses to overwrite existing files
            and terminates with an error.
        abis:
            [Ignored with cibuildwheel] Set ABIs to build remotely,
            comma-separated.
        test:
            Run basic "test ." import test. On Windows this is is done for each
            wheel we have built for which a matching python is available (using
            `py -0p`); otherwise we only test with native python.
        cibw:
            Use cibuildwheel instead of our own Linux docker/manylinux and
            Windows-py code.
        cibw_args:
            Extra args to specify to cibuildwheel.
        cibw_clear_outdir:
            Internal.
    
    Also uses environmental variables specified earlier by `cibw_env()`.
    '''
    if remote:
        # Do remote build.
        remote = jtest.Remote( remote)
        local_dir = os.path.dirname(__file__)

        # Rsync to remote.
        if state.sdist_inplace:
            jlib.log( 'Rsyncing git files from {state.sdist} to remote {remote=}.')
            jtest.sync_internal(
                    state.sdist,
                    remote.ssh,
                    remote.directory,
                    submodules=True,
                    dot_git=True,
                    )
            
        command = f'rsync -aP --rsh {shlex.quote(remote.ssh)} {local_dir}/pypackage.py {local_dir}/jlib.py {local_dir}/aargs.py {"" if state.sdist_inplace else state.sdist} :{remote.directory}'
        
        system(command, prefix=f'{remote}:rsync-to: ')

        # Run build on remote.
        assert state.sdist, f'Remote build requires sdist.'
        command_remote = ''
        if remote.directory:
            command_remote += f'cd {remote.directory} && '
        command_remote += f'./pypackage.py'
        command_remote += f' sdist{" --inplace" if state.sdist_inplace else ""} {os.path.basename(state.sdist)}'
        if abis:
            command_remote += f' abis {abis}'
        if cibw:
            for n, v in state.cibw_env:
                command_remote += f' cibw_env {shlex.quote(n)} {shlex.quote(v)}'
        command_remote += ' build'

        if cibw:
            command_remote += f' --cibw --cibw_clear_outdir'
            #if cibw_env:
            #    command_remote += ' --cibw_env ' + shlex.quote( cibw_env)
            if cibw_args:
                command_remote += f' --cibw_args ' + shlex.quote( cibw_args)
        if test:
            # Also run basic import test.
            command_remote += ' --test'
        command = f'{remote.ssh} {shlex.quote( command_remote)}'
        system(command, prefix=f'{remote.description}: ')

        if state.sdist_inplace:
            jlib.log( 'Not copying wheels from remote to local machine because sdist inplace was specified')
        else:
            # Copy remote wheels back to local machine.
            #
            sdist_prefix = os.path.basename( state.sdist)
            sdist_suffix = '.tar.gz'
            assert sdist_prefix.endswith( sdist_suffix)
            sdist_prefix = sdist_prefix[ : -len( sdist_suffix)]
            remote_pattern = f':{remote.directory}pypackage-out/{sdist_prefix}*'
            command = f'rsync -ai --rsh {shlex.quote(remote.ssh)} {shlex.quote(remote_pattern)} {state.outdir}/'
            system(command, prefix=f'{remote}:rsync-from: ')

    else:
        # Local build.
        if cibw:
            # Use cibuildwheel.
            if cibw_clear_outdir:
                if os.path.isdir(state.sdist):
                    prefix = os.path.basename( state.sdist)
                else:
                    name, version = parse_sdist( state.sdist)
                    prefix = f'{name}-{version}'
                pattern = f'{state.outdir}/{prefix}-*.whl'
                jlib.log( 'Deleting files matching {pattern=}.')
                for p in glob.glob( pattern):
                    jlib.log( f'Deleting: {p!r}')
                    os.remove( p)
            env_extra = dict()
            jlib.log( '{cibw_env=}')
            for n, v in state.cibw_env:
                jlib.log( 'Adding to env_extra: {n}={v!r}')
                env_extra[n] = v
            wheels = build_cibuildwheel(
                    state.sdist,
                    cibw_args,
                    env_extra,
                    state.outdir,
                    sdist_inplace=state.sdist_inplace,
                    )
            jlib.log( '{wheels=}')
        else:
            # Do builds ourselves.
            abis2 = state.abis
            if windows():
                if not abis2:
                    abis2 = ['x32-38', 'x32-39', 'x64-38', 'x64-39']
                wheels = make_windows( state.sdist, abis2, state.outdir)
            elif linux():
                if not abis2:
                    abis2 = ['37', '38', '39']
                wheels = make_linux(
                        state.sdist,
                        abis2,
                        state.outdir,
                        test_direct_install = False,
                        install_docker = manylinux_install_docker,
                        docker_image = manylinux_docker_image,
                        pull_docker_image = manylinux_pull_docker_image,
                        container_name = manylinux_container_name,
                        )
            else:
                wheels = make_unix_native( state.sdist, state.outdir)

        log(f'sdist: {state.sdist} {state.outdir=}')
        for wheel in wheels:
            log(f'    wheel: {wheel}')
        if test:
            # Run basic import test.
            test_wheels(wheels)


def do_doctest():
    doctest.testmod()

def pypi_test(test: bool):
    '''
    Whether to use test.pypi.org.
    '''
    state.pypi_test = test


def remote( uri, files=None, args=''):
    '''
    Sync to and run pypackage.py on remote machine.
    
    Args:
        uri:
            The remote machine, e.g. of the form
            `[<user>@]<host>[:<directory>]`. See `jtest.Remote` for full spec.
        files:
            Comma-separated files to sync to remote machine. (pypackage.py and
            other required .py files are always synced.)
        args:
            Args to pass to pypackage.py on remote machine.
    '''
    remote = jtest.Remote( uri)
    local_dir = os.path.dirname(__file__)
    sync_files = f'{local_dir}/pypackage.py,{local_dir}/jlib.py'
    if files:
        sync_files += f',{remote.s.files}'
    system(
            f'rsync -ai --rsh {shlex.quote(remote.ssh)} {sync_files.replace(",", " ")} {sdist if sdist else ""} :{remote.directory}',
            prefix=f'{uri}: ',
            )
    if args:
        remote_args = f'pypi-test {state.pypi_test} {args}'
        remote_command = ''
        if remote.directory:
            remote_command += f'cd {remote.directory} && '
        remote_command += f'./pypackage.py {remote_args}'
        system( f'{remote.ssh} {shlex.quote( remote_command)}',
                prefix=f'{uri}: ',
                )

def sdist( sdist, inplace=False):
    '''
    Specify an sdist or package source directory.
    
    Args:
        sdist:
            A sdist file, or a package directory with a `setup.py` file.
        inplace:
            If True, `sdist` must be a directory, and we build wheels directly
            without first creating a sdist. This avoids doing a clean build
            each time.

            This currently also means that remote builds (e.g. `build(
            remote=...)` will not copy back wheels to local machine (because we
            don't know the name of the wheels before we call cibuildwheel).
    '''
    if not sdist:
        state.sdist = None
    elif os.path.isfile( sdist):
        assert not inplace
        parse_sdist( sdist)
        state.sdist = sdist
    else:
        package_directory = sdist
        state.sdist_inplace = inplace
        if inplace:
            state.sdist = sdist
        else:
            state.sdist = make_sdist( sdist, state.outdir)

def internal_tag():
    '''
    Internal use only.
    '''
    print( make_tag())



def upload_wheels( token=None):
    '''
    Upload specific wheels to pypi, as specified by 'wheels ...'.
    
    Args:
        token:
            Authentication token for pypi.org; usually starts with 'pypi_'.
    '''
    log(f'Uploading wheels ({len(state.wheels)}')
    upload( state.wheels, state.pypi_test, token=token)

def wheels( pattern):
    '''
    Specify pre-existing wheels using glob pattern. A "*" is appended if
    `pattern` does not end with ".whl".
    '''
    if not pattern.endswith('.whl'):
        pattern += '*'
        log(f'Have appended "*" to get pattern={pattern!r}')
    wheels_raw = glob.glob(pattern)
    if not wheels_raw:
        log(f'Warning: no matches found for wheels pattern {pattern!r}.')
    state.wheels = []
    for wheel in wheels_raw:
        if wheel.endswith('.whl'):
            state.wheels.append(wheel)
    log(f'Found {len(state.wheels)} wheels with pattern {pattern}:')
    for wheel in state.wheels:
        log(f'    {wheel}')
    

def upload( token):
    '''
    Upload sdist and all matching wheels to pypi.'
    '''
    assert state.sdist, f'Cannot upload because no sdist has been set.'
    wheels = wheels_for_sdist( state.sdist, state.outdir)
    log(f'Uploading wheels ({len(wheels)}) and sdist: {state.sdist!r}')
    if not state.pypi_test:
        assert wheels, f'Refusing to upload zero wheels to pypi.org.'
    for wheel in sorted( wheels):
        log(f'    {wheel}')
    files = [state.sdist] + wheels
    upload0( files, state.pypi_test, token=token)

if __name__ == '__main__':
    import aargs
    aargs.cli()


if __name__ == '__main__':
    try:
        main()
    except Exception:
        jlib.exception_info()
        sys.exit(1)
