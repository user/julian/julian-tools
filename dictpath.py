'''
Support for nested dics.


With `dictpath.setpath()`, one specifies a path of element names, followed by a
value:

>>> d = dict()
>>> setpath(d, 'foo', 'bar', 42)
42
>>> d['foo']['bar']
42

>>> setpath(d, 'foo', 'bar2', 2.1)
2.1
>>> d['foo']['bar2']
2.1

>>> setpath(d, 'foo', 'bar3', 'qwerty', -2.3)
-2.3
>>> d['foo']['bar3']['qwerty']
-2.3

>>> str(d)
"{'foo': {'bar': 42, 'bar2': 2.1, 'bar3': {'qwerty': -2.3}}}"


`dictpath.get_flat()` yields all paths:

>>> for keyss in get_flat(d):
...     print(f'    {keyss}')
    ['foo', 'bar', 42]
    ['foo', 'bar2', 2.1]
    ['foo', 'bar3', 'qwerty', -2.3]

`dictpath.get_keys()` returns list of lists of all keys for each level. In this
doctest example we use sorted(list(...)) to ensure that the output is stable.

>>> for s in get_keys(d):
...     print(f'    {sorted(list(s))}')
    ['foo']
    ['bar', 'bar2', 'bar3']
    ['qwerty']
'''


def setpath(dict_, *elements):
    '''
    Sets nested dict value. `elements` is a sequence of text keys followed by a
    final value.
    '''
    d = dict_
    for element in elements[:-2]:
        d = d.setdefault(element, dict())
    d[elements[-2]] = elements[-1]
    return d[elements[-2]]

def setpathdefault(dict_, *elements):
    '''
    >>> d = dict()
    >>> setpathdefault(d, 'foo', 'bar', 0)
    {'bar': 0}
    >>> d
    {'foo': {'bar': 0}}
    '''
    d = dict_
    for element in elements[:-2]:
        d = d.setdefault(element, dict())
    d.setdefault(elements[-2], elements[-1])
    return d


def get_keys(dict_, keyss=None, depth=0):
    '''
    Returns a list where item `i` in the list is the set of the names that
    occurs at level `i` in nested dictionary `dict_`. Used to recover
    testnames, input files and toolnames from results dict.

    `keyss` and `depth` are internal use only.
    '''
    if keyss is None:
        keyss = list()
    if len(keyss) <= depth:
        keyss.append(set())
    for key, value in dict_.items():
        keyss[depth].add(key)
    for key, value in dict_.items():
        if isinstance(value, dict):
            get_keys(value, keyss, depth+1)
    return keyss

def get_flat(dict_):
    '''
    Yields lists, each containing path elements in nested dict.
    '''
    if isinstance( dict_, dict):
        for key, value in dict_.items():
            for p in get_flat(value):
                yield [key] + p
    else:
        yield [dict_]
            
