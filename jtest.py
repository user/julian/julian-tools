#!/usr/bin/env python3

'''
Support for building and testing ghostpdl and mupdf, locally and remotely.

Testing:

    Runs different binaries on input files, allowing comparison of
    behaviour and performance.

    Generate html descriptions of results, and copy files to a webserver.

Building

    We infer build configuration from name of target.

    For ghostpdl builds, we copy configure's generated Makefile to a
    unique name to avoid the need to re-configure when building different
    configurations.

Remote:

    Sync git checkouts to a remote host.  Run commands (including jtest.py
    itself) on a remote host.
'''

help_examples = '''
Examples:

    # Run commands on remote machine in 'artifex-remote' directory.
    #
    ./julian-tools/jtest.py -r jules-windows,mupdf 'cd mupdf && ./scripts/mupdfwrap.py --swig-windows-auto -b --csharp 123 --test-csharp'
    ./julian-tools/jtest.py -r jules-devuan,mupdf 'cd mupdf && ./scripts/mupdfwrap.py --swig-windows-auto -b --csharp all --test-csharp'

    # Create .bmpcmp.* output files comparing normal and luratech builds.
    #
    time ./jtest.py \
            --remote julian@peeved.ghostscript.com \
            --remote-sync \
            --remote-do \
            --binary ghostpdl/bin/gs \
            --binary ghostpdl/luratechbin/gs \
            --build .binaries . \
            --build .bmpcmp m \
            --device ppmraw \
            --params '-dBATCH -dNOPAUSE -r300' \
            --inputs-r '/home2/regression/cluster/tests*' '*.pdf' \
            --inputs-r '/home2/regression/cluster/tests_private/pdf/customer394' '*.pdf' \
            --inputs-exclude '*/fuzzing/*' \
            --inputs-exclude '*customer394/problems/normal_874.pdf' \
            --inputs-exclude '*customer394/problems/normal_1287.pdf' \
            --inputs-exclude-luratech-bad \
            --tskip 1 \
            --delete 2 \
            --run-compare-bmpcmp '-t 33' \
            --run-compare \
            2>&1 | tee out-peeved-compare

    # Copy bmpcmp outputs (created by above command) to website, and generate
    # an html file that shows these bmpcmp files.
    #
    # Creates: https://ghostscript.com/~julian/luratech-diffs.html
    #
    time ./jtest.py \
            --remote julian@peeved.ghostscript.com \
            --remote-sync \
            --remote-do \
            --device ppmraw \
            --inputs-r '/home2/regression/cluster/tests*' '*.pdf' \
            --inputs-exclude '*/fuzzing/*' \
            --inputs-exclude '*customer394/problems/normal_874.pdf' \
            --inputs-exclude '*customer394/problems/normal_1287.pdf' \
            --inputs-exclude-no-bmpcmp \
            --o-html-bmpcmp luratech \
            --rsync-bmpcmp ghostscript.com:public_html/ \
            --rsync-html ghostscript.com:public_html/ \
            2>&1 | tee out-peeved-compare-html


    # Create peformance info files for normal and luratech binaries.
    #
    # Creates one file for each binary+input combination, containing a list of
    timings.
    #
    # Very slow, e.g.: [real    4011m42.802s]
    #
    time ./jtest.py \
            --remote julian@peeved.ghostscript.com \
            --remote-sync \
            --remote-do \
            --binary ghostpdl/bin/gs \
            --binary ghostpdl/luratechbin/gs \
            --build .binaries cm \
            --params '-dBATCH -dNOPAUSE -r300' \
            --device ppmraw \
            --inputs-r '/home2/regression/cluster/tests*' '*.pdf' \
            --inputs-exclude '*/fuzzing/*' \
            --inputs-exclude '*customer394/problems/normal_874.pdf' \
            --inputs-exclude '*customer394/problems/normal_1287.pdf' \
            --inputs-exclude-luratech-bad \
            --tskip 1 \
            --run-performance 21 \
            2>&1 | tee -a out-peeved-performance21

    # Look at performence of individual file
    #
    ./jtest.py \
            --remote julian@peeved.ghostscript.com \
            --remote-sync \
            --remote-do \
            --binary ghostpdl/bin/gs \
            --binary ghostpdl/luratechbin/gs \
            --build .binaries . \
            --params '-dBATCH -dNOPAUSE -r300' \
            --device ppmraw \
            --inputs /home2/regression/cluster/tests/pdf/Jbig2_042_08.pdf \
            --run-performance 19
    
    # Generate new performance files for all inputs with particular existing
    # performance statistics.
    #
    time ./jtest.py \
            --remote julian@peeved.ghostscript.com \
            --remote-sync \
            --remote-do \
            --binary ghostpdl/bin/gs \
            --binary ghostpdl/luratechbin/gs \
            --build .binaries m \
            --params '-dBATCH -dNOPAUSE -r300' \
            --device ppmraw \
            --inputs-from-performance 18 "tmin_ratio >= 1.3" \
            --run-performance 20 \
    
    # get local copy of performance data:
    ./jtest.py \
        --remote julian@peeved.ghostscript.com \
        --remote-get-performance 21 performance-21.pickle \
    
    # Generate performance pages.
    time ./jtest.py \
            --load-performance performance-21.pickle \
            --o-graph-performance luratech-21-performance-graph.html \
            --o-html-performance-speed luratech-21 \
            --o-html-performance-memory luratech-21 \
            --rsync-html julian@ghostscript.com:public_html/ \
    
    https://ghostscript.com/~julian/luratech-performance-21-graph.html
    
    
    time ./jtest.py \
            --load-performance performance-21.pickle \
            --o-html-performance foo
    
    ../julian-tools/jtest.py --remote julian@casper.ghostscript.com --remote-get-testfile Ghent_V3.0/010_CMYK_OP_x3.pdf
    ./scripts/mupdfwrap.py --run-py scripts/mutool.py draw -r200 -D -F pam -c cmyk -stm -o out ../tests/Ghent_V3.0/010_CMYK_OP_x3.pdf
'''

import aargs
import errno
import fnmatch
import glob
import inspect
import io
import sys
import os
import pickle
import re
import shlex
import tarfile
import textwrap
import threading
import time
import math

import jlib

try:
    import mpld3
except ImportError:
    pass
else:
    import json
    import numpy
    # Overwrite mpld3._display.NumpyEncoder with fixed version, from
    # https://stackoverflow.com/questions/47808676/pythontypeerror-array-1-is-not-json-serializable
    #
    class NumpyEncoder(json.JSONEncoder):
        """ Special json encoder for numpy types """

        def default(self, obj):
            if isinstance(obj, (numpy.int_, numpy.intc, numpy.intp, numpy.int8,
                numpy.int16, numpy.int32, numpy.int64, numpy.uint8,
                numpy.uint16,numpy.uint32, numpy.uint64)):
                return int(obj)
            elif isinstance(obj, (numpy.float_, numpy.float16, numpy.float32, 
                numpy.float64)):
                return float(obj)
            elif isinstance(obj,(numpy.ndarray,)): #### This is the fix
                return obj.tolist()
            return json.JSONEncoder.default(self, obj)
    mpld3._display.NumpyEncoder = NumpyEncoder



class State:
    def __init__( self):
        self.ssh_command = None
        self.binaries = []
        self.delete = 1
        self.compare_repeat = 1
        self.bmpcmp = 0
        self.inputs = []
        self.tskip = 0
        self.device = None
        self.html_generated = []

state = State()


def log_t( on: int):
    '''
    Controls whether we prefix log output with date/time.
    '''
    current = False
    for i, item in enumerate( jlib.g_log_prefixes):
        if isinstance( item, jlib.LogPrefixTime):
            current = True
            break
    if not current and on:
        jlib.log( 'inserting jlib.LogPrefixTime')
        jlib.g_log_prefixes.insert( 0, jlib.LogPrefixTime( time_=False, elapsed=True))
    elif current and not on:
        jlib.log( 'removing jlib.LogPrefixTime')
        del jlib.g_log_prefixes[i]
    #jlib.log( '{jlib.g_log_prefixes=}')

def rusage_decode( rusage_text):
    '''
    Takes rusage output string from jlib.system(..., rusage=True) and returns
    dict mapping /usr/bin/time rusage codes to values, e.g. 'e' maps to
    execution time.
    '''
    p0 = 0
    p = p0
    ret = dict()
    while 1:
        if p >= len(rusage_text):
            break
        assert rusage_text[p+1] == '='
        k = rusage_text[p]
        p += 2
        if k == 'C':
            end = len(rusage_text)
        else:
            end = rusage_text.find(' ', p)
            if end == -1:
                end = len(rusage_text)
        ret[k] = rusage_text[ p : end]
        p = end + 1
    return ret

if 0:
    print( rusage_decode( 'D=0 E=0 F=0 I=0 K=0 M=2967948 O=96 P=99% R=0 S=0.47 U=2.16 W=0 X=0 Z=4096 c=6 e=2.64 k=0 p=0 r=0 s=0 t=0 w=4 x=0 C=ghostpdl/luratechbin/gs -sOutputFile=/dev/null -dBATCH -dNOPAUSE -r300 -sDEVICE=ppmraw /home2/regression/cluster/tests_private/pdf/customer394/problems/normal_788.pdf'))


def _sync( local_d, ssh_command, remote_dir, submodules=False, dot_git=False):
    '''
    Uses rsync to copy local git checkout to remote. If `dot_git` is true or
    `local_d` ends with /.git, we also transfer the .git directory.
    '''
    #jlib.log(f'local_d={local_d}')
    if local_d.endswith('/.git'):
        dot_git = True
        local_d = local_d[:-5]
    if local_d.endswith('/'):
        local_d = local_d[:-1]
    #jlib.log(f'local_d={local_d}')
    jlib.git_get_id( local_d)
    filenames = jlib.git_get_files( local_d, submodules)
    
    git_paths = f'jtest-git-paths-{os.getpid()}-{time.time()}'
    try:
        with open( git_paths, 'w') as f:
            for filename in filenames:
                filename2 = filename.strip()
                if filename2:
                    if os.path.isfile( os.path.join(local_d, filename2)):
                        print( os.path.join( local_d, filename2), file=f)
                    else:
                        jlib.log( 'ignoring {filename2=}')
            if dot_git:
                print( f'{local_d}/.git', file=f)
            if 0 and 'ghostpdl' in local_d and os.path.islink(f'{local_d}/extract'):
                print( f'{local_d}/extract', file=f)
        stats = ' --stats'
        stats = ''
        command = f'rsync -Raizr{stats} --rsh "{ssh_command}" --files-from={git_paths} . :{remote_dir}/'
        jlib.system( command, verbose=1)
    finally:
        os.remove(git_paths)

def sync_file( from_, ssh_command, to_):
    '''
    Uses rsync to copy local git checkout to remote.
    '''
    command = 'rsync -aiz --rsh "%s" %s :%s' % (ssh_command, from_, to_)
    jlib.system( command, verbose=1)


#def sync( ssh_command, remote_dir):
#    '''
#    Uses rsync to copy local source files to remote.
#    '''
#    sync_internal( 'ghostpdl/', ssh_command, remote_dir)
#    sync_internal( 'luratech/', ssh_command, remote_dir)


def out_filename( state, input_, i):
    '''
    Returns filename generated from `input_` (input file) and `i` (name of
    binary). We replace `/` by `^`.
    '''
    input_s = input_.replace( '/', '^')
    ret = 'jtest-out'
    if 0:
        # Put our pid into output filenames.
        ret += '-%s' % os.getpid()
    ret += '-%s' % input_s
    if state.device:
        ret += '.%s' % state.device
    ret += '.%s' % i
    return ret

def get_command( state, input_, binary_i, out=None, stdout=None, input_multiple=1):
    '''
    Returns gs command suitable for specified parameters.
    
    If `out` is true, we use: `-sOutputFile=<out>`
    
    If `stdout` is true, we send both stdout and stderr to `stdout`
    with: `1><stdout> 2>&1`
    '''
    command = state.binaries[ binary_i]
    
    if out:
        command += ' -sOutputFile=%s' % out
    
    command += ' %s' % state.params
    
    if state.device:
        command += ' -sDEVICE=%s' % state.device
    
    if input_:
        command += (' %s' % input_) * input_multiple
    
    if stdout:
        command += ' 1>%s 2>&1' % stdout
    
    #if not output_dev_null:
    #    command += ' && mv %s- %s' % (o, o)
    
    return command
            
def mtime( path):
    '''
    Returns mtime of specified path, or 0 if it doesn't exist.
    '''
    try:
        return os.path.getmtime( path)
    except Exception:
        return 0

def do_run_compare( state):
    '''
    Runs tests as specified in `state`.
    '''

    assert state.binaries
    
    if not state.inputs:
        # We expect input files to be specified in state.params. Ensure that
        # we run one iteration.
        state.inputs.append( '')

    input2error = dict()
    
    class Error:
        '''
        Info on a command where output differed from similar command.
        '''
        def __init__( self, binary, command):
            self.binary = binary
            self.command = command
        def __str__( self):
            return 'binary=%s command: %s' % (
                    self.binary,
                    self.command,
                    )
    
    for r in range( state.compare_repeat):
        out0 = jlib.StreamPrefix( sys.stdout, 'iteration %s/%s: ' % (r+1, state.compare_repeat))
        print( '', file=out0)

        num_errors = 0
        num_tests = 0

        # Look at each input file in turn.
        #
        input_i = 0
        for input_ in state.inputs:
            input_i += 1

            out = jlib.StreamPrefix( out0,
                    'input %s/%s: %s: ' % (input_i, len(state.inputs), input_)
                    )

            print( 'input file: %s' % input_, file=out)
            def out_md5(i):
                return '%s.md5' % out_filename( state, input_, i)
            def out_bmpcmp(i):
                return '%s.bmpcmp' % out_filename( state, input_, i)
                                
            # Check whether any bmpcmp's need creating.
            do_bmpcmp = False
            need_bmpcmp = state.bmpcmp
            if need_bmpcmp:
                for i in range(1, len(state.binaries)):
                    binary = state.binaries[i]
                    o_bmpcmp = out_bmpcmp(i)
                    t_in = max( mtime( input_), mtime( binary))
                    if mtime( o_bmpcmp) < t_in:
                        do_bmpcmp = True
            
            # Generate output file / .md5 files, running each binary with same
            # parameters except for different output file.
            #
            num_errors_local = 0
            for i, binary in enumerate( state.binaries):
                num_tests += 1
                t_in = max( mtime( input_), mtime( binary))
                o = out_filename(state, input_, i)
                prefix = '    %s: ' % i
                # We use <o>- as output file so that we can create <o>
                # atomically after the command returns using os.rename() - e.g.
                # this allows things to work if we are interrupted.
                command = get_command( state, input_, i, out='%s-' % o, stdout='%s.out' % o)
                o_md5 = out_md5(i)
                o_bmpcmp = out_bmpcmp(i)
                
                if state.tskip:
                    # Look at creation/modification times of input/output files
                    # and don't run command if output file already up to date.
                    do_o = False
                    do_md5 = False
                    
                    need_o = False
                    need_md5 = True
                    
                    if do_bmpcmp:
                        need_o = True
                    
                    if need_md5 and mtime( o_md5) < t_in:
                        do_md5 = True
                        need_o = True
                    
                    if need_o and mtime( o) < t_in:
                        do_o = True
                        do_md5 = True
                
                else:
                    do_o = True
                    do_md5 = True
                    do_bmpcmp = state.bmpcmp
                
                #print 'do_o:      %s' % do_o
                #print 'do_md5:    %s' % do_md5
                #print 'do_bmpcmp: %s' % do_bmpcmp
                
                if do_o:
                    t = time.time()
                    e = jlib.system( command, raise_errors=False, out=out, prefix=prefix)
                    t = time.time() - t
                    try:
                        os.rename( '%s-' % o, o)
                    except OSError:
                        # Carry on if there is no output file.
                        pass
                    print( 'command took: %.1f sec' % t, file=out)
                
                if do_md5:
                    o_md5 = out_md5(i)
                    command_md5 = 'md5sum %s |awk \'{print $1}\' | tee %s' % (o, o_md5)
                    jlib.system( command_md5, out=out, prefix=prefix, raise_errors=0)
                
                # Always compare .md5's.
                if i > 0:
                    o0_md5 = out_md5(0)
                    diff = jlib.system( 'diff -q %s %s' % (o0_md5, o_md5),
                            out=out,
                            prefix='diff: ',
                            raise_errors=False,
                            )
                    if diff:
                        o0 = out_filename(state, input_, 0)
                        print( 'output files differ:\n    %s\n    %s' % (o0, o), file=out)
                
                    if do_bmpcmp:
                        # Diff against first bitmap.
                        o0 = out_filename(state, input_, 0)
                        o_bmpcmp = out_bmpcmp(i)
                        for j in glob.glob( '%s*' % o_bmpcmp):
                            os.remove(j)
                        
                        if diff:
                            print( 'creating bmpcmp output for %s in: %s*' % (
                                    o,
                                    o_bmpcmp,
                                    ),
                                    file=out,
                                    )
                            if os.path.isfile(o0) and os.path.isfile(o):
                                bmpcmp_ops = '' if state.bmpcmp == '1' else state.bmpcmp
                                jlib.system(
                                        './ghostpdl/bmpcmp %s %s %s %s' % (
                                                bmpcmp_ops,
                                                o0,
                                                o,
                                                o_bmpcmp,
                                                ),
                                        out=out,
                                        prefix='    bmpcmp: ',
                                        )
                                n = len( glob.glob( '%s*' % o_bmpcmp))
                                print( 'number of bmpcmp output files: %s' % n, file=out)
                                if n:
                                    print( 'bmpcmp detected diffs', file=out)
                                else:
                                    print( 'bmpcmp did not detect diffs', file=out)
                                    diff = 0
                                
                            else:
                                print( 'not running bmpcmp because file(s) do not exist', file=out)
                        
                        # touch o_bmpcmp to indicate bmpcmp output is up to date.
                        with open( o_bmpcmp, 'w') as f: pass
                    
                    if diff:
                        num_errors_local += 1
                    
                        items = input2error.setdefault( input_, [])
                        if not items:
                            items.append(
                                    Error(
                                            state.binaries[0],
                                            get_command( state, input_, 0),
                                            )
                                    )
                        items.append(
                                Error(
                                        state.binaries[i],
                                        get_command( state, input_, i),
                                        )
                                )
                    
            num_errors += num_errors_local
            if state.delete > 0:
                if num_errors_local == 0 or state.delete == 2:
                    print( 'removing out files (num_errors_local=%s state.delete=%s)' % (
                            num_errors_local,
                            state.delete,
                            ))
                    for i in range( len(state.binaries)):
                        o = out_filename(state, input_, i)
                        if os.path.isfile(o):
                            print( 'removing: %s' % o, file=out)
                            os.remove( o)

        if num_errors:
            break
    
    print( 'Num of tests run: %s' % num_tests)
    print( 'Num diffs: %s' % num_errors)
    
    for input_, errors in input2error.items():
        print( '    diffs detected for input file: %s:' % input_)
        # Show commands, padding each argument so they are all aligned.
        widths = []
        for error in errors:
            i = 0
            for a in error.command.split():
                if len( widths) <= i:
                    widths.append(0)
                widths[i] = max( widths[i], len(a))
                i += 1
        for error in errors:
            line = '        '
            i = 0
            for a in error.command.split():
                pad = ' ' * (widths[i] - len(a))
                line += ' %s%s' % (a, pad)
                i += 1
            print( line)


class Stat:
    '''
    A sequence of timings.
    '''
    def __init__(self):
        self.n = 0
        self.ts = []
        self.rusages = []
        self.t = 0
        self.tt = 0
        self.command = None
    def add( self, t):
        self.ts.append( t)
        self.n += 1
        self.t += t
        self.tt += t*t;
    def add_rusage( self, line):
        self.rusages.append( line.strip())
        m = re.search( ' e=([0-9.:]+) .* C=(.+)$', line)
        assert m, 'failed to parse rusage line: %r' % line
        t = 0
        for t_text in m.group(1).split(':'):
            t *= 60
            t += float(t_text)
        self.add( t)
        command = m.group(2)
        if self.command:
            if self.command != command:
                raise Exception( 'rusage contains different commands')
        else:
            self.command = command
            self.binary = command.split(' ', 1)[0]
        
    def get_dev( self):
        v = self.tt / self.n - (self.t*self.t) / self.n / self.n
        v = v ** 0.5
        # Ignore any imaginary component.
        return v.real
    def get_average( self):
        return self.t / self.n
    def get_mem_average( self):
        if not self.rusages:
            return 0
        total = 0
        # We ignore first item - binary is not in cache so rusage values
        # slightly different.
        for rusage in self.rusages[1:]:
            p = rusage.find( ' M=')
            assert p >= 0
            p += 3
            pp = rusage.find( ' ', p)
            m = int( rusage[p:pp])
            total += m
        return 1.0 * total / len(self.rusages)
    def get_t_min( self):
        return min( self.ts)


class Stat2:
    '''
    Statistics from running a binary on an input file.
    '''
    def __init__(self, mtime, o, device, binary_i, min_t, stat, binary, binary_git_id):
        self.mtime = mtime
        self.o = o
        self.device = device
        self.binary_i = binary_i
        self.min_t = float( min_t)
        self.stat = stat
        self.binary = binary
        self.binary_git_id = binary_git_id


class GitId:
    def __init__( self, filename):
        try:
            with open( filename) as f:
                text = f.read()
                try:
                    line, self.diff = text.split('\n', 1)
                except ValueError:
                    line = text
                    self.diff = ''
                try:
                    self.sha, self.comment = line.split(' ', 1)
                except ValueError:
                    self.sha = line.strip()
                    self.comment = ''
        except FileNotFoundError:
            self.sha = None
            self.comment = None
            self.diff = None
    def __str__( self):
        text = ''
        if self.sha:
            text += self.sha
        if self.comment:
            text += ' %s' % self.comment
        if self.diff:
            text += ' (+%s)' % len(self.diff)
        return text
        

def do_run_performance( state, min_t):
    
    def get_performance_filename( state, input_, binary_i, min_t):
        b = state.binaries[ binary_i].replace('/', '^')
        return '%s.performance.%s' % (
                out_filename( state, input_, b),
                min_t,
                )
    
    input_i = 0
    for input_ in state.inputs:
        input_i += 1
        
        out = jlib.StreamPrefix(
                sys.stdout,
                'input %s/%s: %s: ' % (
                        input_i,
                        len(state.inputs),
                        input_,
                        )
                )

        if state.tskip:
            do_run = False
            for binary_i, binary in enumerate( state.binaries):
                o = get_performance_filename( state, input_, binary_i, min_t)
                if mtime( o) < max( mtime(binary), mtime(input_)):
                    do_run = True
                    break
            if not do_run:
                print( 'skipping because all performance files up to date: %s' % input_, file=out)
                continue

        print( 'input file: %s' % input_, file=out)
        
        ts = []
        for binary in state.binaries:
            ts.append( Stat())
        
        # Add multiples of input file until execution takes at least <tmin> s.
        #
        # todo: also time with no input files, and subtract this from all later
        # timings?
        #
        # We use binary 0 for this. could maybe try both and pick slowest?
        #
        input_multiple_prev = 1
        input_multiple = 1
        tmin = 2.5
        while 1:
            command = get_command(
                    state,
                    input_,
                    0,  # Use first binary for this calibration.
                    out='/dev/null',
                    stdout='/dev/null',
                    input_multiple=input_multiple,
                    )
            try:
                rusage = jlib.system(
                        command,
                        raise_errors=False,
                        out=out,
                        prefix='testing input_multiple=%s: ' % input_multiple,
                        verbose=False,
                        rusage=True,
                        )
            except OSError as e:
                if e.errno == errno.E2BIG:
                    print( 'errno.E2BIG. len(command)=%s' % len(command))
                    input_multiple = input_multiple_prev
                    break
                else:
                    print( 'failed to run command: %s' % e)
                    raise
            rusage = rusage_decode( rusage)
            t = float( rusage['e'])
            print( 'input_multiple=%s: t=%s' % (input_multiple, t), file=out)
            if t >= tmin:
                break
            input_multiple_prev = input_multiple
            input_multiple = int( input_multiple * (tmin / t))
            if input_multiple == input_multiple_prev:
                input_multiple += 1
        print( 'using input_multiple=%s (t=%s)' % (input_multiple, t), file=out)
        
        #t_end_min = time.time() + 10
        it = 0
        for i in range(5):
            for binary_i, binary in enumerate( state.binaries):
                prefix = '    it=%s: binary=%s: ' % (it, binary_i)
                if it:
                    print( prefix, file=out)
                command = get_command(
                        state,
                        input_,
                        binary_i,
                        out='/dev/null',
                        stdout='/dev/null',
                        input_multiple=input_multiple,
                        )
                rusage = jlib.system( command, raise_errors=False, out=out, prefix=prefix, verbose=(it==0), rusage=True)
                ts[ binary_i].add_rusage( rusage)
            it += 1
        
        for binary_i in range( len( state.binaries)):
            print( '    binary %s: its=%s min=%.3f average=%.3f dev=%.3f' % (
                    binary_i,
                    ts[ binary_i].n,
                    ts[ binary_i].get_t_min(),
                    ts[ binary_i].get_average(),
                    ts[ binary_i].get_dev(),
                    ))
            
            o = get_performance_filename( state, input_, binary_i, min_t)
            if 0: print( 'its=%s average=%.3f dev=%.3f binary=%s' % (
                    ts[ binary_i].n,
                    ts[ binary_i].get_average(),
                    ts[ binary_i].get_dev(),
                    state.binaries[ binary_i],
                    ),
                    file=f,
                    )
            text = '\n'.join( ts[ binary_i].rusages)
            with open( o, 'w') as f:
                f.write( text)
            
            binary_git_id = '%s.git-id' % state.binaries[ binary_i]
            if os.path.isfile( binary_git_id):
                jlib.system( 'cp -p %s %s.git-id' % (binary_git_id, o))


make_input_to_stats_cache = dict()

def make_input_to_stats( min_t):
    '''
    Returns dict mapping input file to Stats object.
    '''
    global make_input_to_stats_cache
    input_to_stats = make_input_to_stats_cache.get( min_t)
    if input_to_stats is None:
        input_to_stats = dict()

        # We gather stats first, populating <input_to_stats> so that we can
        # then sort things before outputing.

        gs = sorted( dglob( '*.performance.%s' % min_t))
        for gi, g in enumerate(gs):
            debug_periodic( 'Looking at .performance file %s/%s: %s' % (gi+1, len(gs), g))
            gg = g.split('.')
            if len(gg) <= 4:
                print( 'ignoring: %s' % g)
                continue
            o, device, binary_i, _, min_t = '.'.join(gg[:-4]), gg[-4], gg[-3], gg[-2], gg[-1]
            m = re.match( '^jtest-out-(.*)$', o)
            if not m:
                print( 'ignoring because does not match jtest-out-*:' % o)
                continue
            input_ = m.group(1).replace( '^', '/')
            with open(g) as ff:
                text = ff.read()
                mtime = os.fstat(ff.fileno()).st_mtime
            stat = Stat()
            m = re.match( '^(([0-9.]+ )*)binary=(.*)$', text)
            if m:
                ts_, _, binary = m.groups()
                for t in ts_.split():
                    stat.add( float(t))
                lines = None
            else:
                # 2020-01-06 new performance file format, containing rusage
                # info from /usr/bin/time.
                ts_ = []
                lines = text.split('\n')
                for line in lines:
                    stat.add_rusage( line)
                binary = line[ line.find(' C=') + 3:].split()[0]
            binary_git_id = GitId( '%s.git-id' % binary)
            input_to_stats.setdefault( input_, []).append(
                    Stat2( mtime, o, device, binary_i, min_t, stat, binary, binary_git_id)
                    )
        make_input_to_stats_cache[ min_t] = input_to_stats
    
    return input_to_stats


def ratio2(a, b):
    if a == 0 and b == 0:
        return 1
    if b == 0:
        b = 1e-9
    return a / b

def ratio( stats2, descriptions=False):
    '''
    Finds info comparing stats for input file `_input`.

    Returns `(tmin_ratio, mem_ratio)`, ratios of minimum times and average
    memory usages.

    if `descriptions` is true, we return:

        `tmin_ratio, mem_ratio, t_desc, mem_desc`

    - where:
        t_desc: human-readable summary of ratio of minimum times, e.g.:

            ghostpdl/bin/gs faster than ghostpdl/luratechbin/gs

        t_desc: human-readable summary of ratio of average memory use, e.g.:

            ghostpdl/bin/gs more memory than ghostpdl/luratechbin/gs

    '''
    #stats2 = input_to_stats[ input_]
    assert len(stats2) == 2,  'len(stats2)=%s' % len(stats2)
    a = stats2[0]
    b = stats2[1]
    a_tmin = min( a.stat.ts)
    b_tmin = min( b.stat.ts)
    a_mem = a.stat.get_mem_average()
    b_mem = b.stat.get_mem_average()
    tmin_ratio = ratio2( a_tmin, b_tmin)
    mem_ratio = ratio2( a_mem, b_mem)

    if not descriptions:
        return tmin_ratio, mem_ratio

    a_binary = a.binary
    b_binary = b.binary
    def make_description( ratio, less, equal, more):
        e = 0.01
        if ratio < 1-e:   s = less
        elif ratio > 1+e: s = more
        else:           s = equal
        return '%s <strong>%s</strong> %s' % (a_binary, s, b_binary)
    t_desc = make_description( tmin_ratio, 'is faster than', 'is same speed as', 'is slower than')
    mem_desc =make_description( mem_ratio, 'uses less memory than', 'uses same memory as', 'uses more memory than')
    return tmin_ratio, mem_ratio, t_desc, mem_desc,

def sort_ratio_tmin( input_to_stats, inputs):
    inputs.sort( key=lambda input_ : ratio(input_to_stats[input_])[0])

def sort_ratio_memory( input_to_stats, inputs):
    inputs.sort( key=lambda input_ : ratio(input_to_stats[input_])[1])


def performance_text( min_t, outfilename):
    if outfilename == '-':
        f = sys.stdout
    else:
        f = open( outfilename, 'w')
    try:
        input_to_stats = make_input_to_stats( min_t)
        inputs = [i for i in input_to_stats.keys()]
        inputs.sort()
        for i, input_ in enumerate( inputs):
            debug_periodic( 'Looking at input file %s/%s: %s' % (i+1, len(inputs), input_))
            stats2 = input_to_stats[ input_]
            info = dict()
            for stat2 in stats2:
                if stat2.stat.rusages:
                    mem_average = stat2.stat.get_mem_average()
                    t_min = stat2.stat.get_t_min()
                    binary = stat2.stat.binary
                    info[ binary] = (len(stat2.stat.rusages), t_min, mem_average, stat2.binary_git_id)
            def p(text):
                print( text, file=f)
            p( '%s' % input_)
            for binary in sorted( info.keys()):
                its, t_min, mem_average, binary_git_id = info[ binary]
                p( '    its=%s t_min=%s mem_average=%s binary=%s git: %s' % (
                        its, t_min, mem_average, binary, binary_git_id))
    finally:
        if f != sys.stdout:
            f.close()

def performance_tsv( min_t, outfilename):
    with open( outfilename, 'w') as f:
        # Describe the format:
        print( '# input N binary0 binary0_its binary0_tmin binary0_mem_average ... binaryN-1 binaryN-1_its binaryN-1_tmin binaryN-1_mem_average',
                file=f,
                )
        input_to_stats = make_input_to_stats( min_t)
        inputs = [i for i in input_to_stats.keys()]
        inputs.sort()
        for i, input_ in enumerate( inputs):
            debug_periodic( 'Looking at input file %s/%s: %s' % (i+1, len(inputs), input_))
            stats2 = input_to_stats[ input_]
            info = dict()
            for stat2 in stats2:
                if stat2.stat.rusages:
                    mem_average = stat2.stat.get_mem_average()
                    t_min = stat2.stat.get_t_min()
                    binary = stat2.stat.binary
                    info[ binary] = (len(stat2.stat.rusages), t_min, mem_average)
            line = '%s\t%s' % (input_, len(info))
            for binary in sorted( info.keys()):
                its, t_min, mem_average = info[ binary]
                line += '\t%s\t%s\t%s\t%s' % (binary, its, t_min, mem_average)
            print( line, file=f)
                


def performance_html( min_t, base, sort):
    '''
    Looks at all `*.performance.<min_t>` files in current directory and writes
    html description to `<base>-performance.html`.

    Does not use state - looks at all files in current directory.

    `sort` should be 'speed' or 'memory'; we sort items accordingly.
    '''
    assert sort in ('speed', 'memory')
    html = '%s-performance-%s.html' % (base, sort)
    with open( html, 'w') as f:
    
        def p( text):
            print( text, file=f)
        
        input_to_stats = make_input_to_stats( min_t)
        
        inputs = [i for i in input_to_stats.keys()]
        if sort == 'speed':
            sort_ratio_tmin( input_to_stats, inputs)
        elif sort == 'memory':
            sort_ratio_memory( input_to_stats, inputs)
        else:
            assert 0
        
        title = 'Performance sorted by %s' % sort
        
        p( '<html>')
        p( '<head><title>%s</title></head>' % title)
        
        p( '<body>')
        p( '<h2>%s</h2>' % title)
        
        p( '<p><a id="toggle_control" href="#"></a>')
        
        p( '<div class="toggled">')
        p( '<p>rusage values are from /usr/bin/time. See time(1); in particular:')
        man_time = (
                ('M', 'Maximum resident set size of the process during its lifetime, in Kilobytes.'),
                ('O', 'Number of file system outputs by the process.'),
                ('P', 'Percentage of the CPU that this job got.  This is just user + system times divided by the total running time. It also prints a percentage sign.'),
                ('S', 'Total number of CPU-seconds used by the system on behalf of the process (in kernel mode), in seconds.'),
                ('U', 'Total number of CPU-seconds that the process used directly (in user mode), in seconds.'),
                ('Z', 'System\'s page size, in bytes.  This is a per-system constant, but varies between systems.'),
                ('c', 'Number of times the process was context-switched involuntarily (because the time slice expired).'),
                ('e', 'Elapsed real (wall clock) time used by the process, in seconds.'),
                ('w', 'Number of times that the program was context-switched voluntarily, for instance while waiting for an I/O operation to complete.'),
                ('x', 'Exit status of the command.'),
                )
        p( '<table>')
        for t, d in man_time:
            p( '<tr><td><small><strong>%s</strong></small><td><small>%s</small>' % (t, d))
        p( '</table>')
        p( '</div>')
                
        i = 0
        for input_i, input_ in enumerate( inputs):
            debug_periodic( 'Looking at input file %s/%s: %s' % (input_i+1, len(inputs), input_))
            stats2 = input_to_stats[ input_]
            i += 1
            p( '<hr>')
            # Hack something that bmpcmp_outfiles() can use as a State - it
            # just looks for .device.
            class State:
                pass
            state = State
            state.device = stats2[0].device
            bmpcmps = bmpcmp_outfiles( state, input_)
            if bmpcmps:
                # There are bmpcmp diffs, so write a link to them.
                p( '<p>%s/%s: <a href="%s-diffs.html#%s">%s</a>' % (
                        i,
                        len( input_to_stats),
                        base,
                        input_,
                        input_,
                        ))
            else:
                p( '<p>%s/%s: %s' % (
                        i,
                        len( input_to_stats),
                        input_,
                        ))
            p( '<pre>')
            if len(stats2) == 2:
                r_time, r_mem, t_description, mem_description = ratio(input_to_stats[input_], descriptions=True)
                p( '    ratio of minimum times:  %.3f - %s.' % (r_time, t_description))
                p( '    ratio of average memory: %.3f - %s.' % (r_mem, mem_description))
            for stat2 in stats2:
                rusage_text = ''
                if stat2.stat.rusages:
                    rusage_text = ' mem_average=<strong>%s KB</strong>' % (
                            number_sep( '%i' % stat2.stat.get_mem_average()
                            ))
                p( '    iterations=%s t_minimum=<strong>%.3f s</strong>%s binary=<strong>%s</strong> t_average=%.3f s t_dev=%.3f s ts=%s date=%s git: %s' % (
                        stat2.stat.n,
                        min( stat2.stat.ts),
                        rusage_text,
                        stat2.binary,
                        stat2.stat.get_average(),
                        stat2.stat.get_dev(),
                        stat2.stat.ts,
                        time.strftime( '%F_%T', time.gmtime( (stat2.mtime))),
                        str(stat2.binary_git_id),
                        ))
                if stat2.stat.rusages:
                    p( '<div class="toggled">')
                    p( '    rusage:')
                    f.write('<small>')
                    for rusage in stat2.stat.rusages:
                        p( '        %s' % rusage)
                    p( '</small>')
                    p( '</div>')
            p( '</pre>')
        p( '''
        <script>
            var toggle_control = document.getElementById('toggle_control');
            var toggle_display = 'block';
            function toggle(id) {
                document.body.style.cursor = 'wait';
                if (toggle_display == 'block') {
                    toggle_display = 'none';
                    toggle_control.innerHTML = 'Show rusage info';
                }
                else {
                    toggle_display = 'block';
                    toggle_control.innerHTML = 'Hide rusage info';
                }
                document.querySelectorAll(id).forEach(
                    function(item) {
                        item.style.display = toggle_display;
                    }
                    );
                document.body.style.cursor = 'default';
            }
            toggle_control.addEventListener(
                'click',
                function (event) {
                    event.preventDefault();
                    toggle( "div.toggled");
                });
            toggle( "div.toggled");
        </script>
        ''')
        p( '</body></html>')
    
    return html


dglob_outfiles_cache = dict()
def dglob( g):
    '''
    Caching version of `glob.glob()`. Note that this doesn't behave in quite
    the same way as `glob.glob()` - we use `fnmatch.fnmatch()` on whole paths
    so `*` and ? match `/`.
    '''
    if 0:
        # For testing.
        return glob.glob(g)

    def find_or_end(large, small):
        '''
        Like str.find() but returns length of <large> instead of -1 if not
        found.
        '''
        p = large.find(small)
        if p == -1:
            p = len(large)
        return p
    
    def find_prefix(s):
        '''
        Returns initial portion of <s> that doesn't include wildcards. Also go
        back to previous '^'.
        '''
        p = s.rfind('^')
        if p == -1:
            p = len(s)
        p = min( find_or_end(s, '*'), find_or_end(s, '?'), p)
        return s[:p]

    g_prefix = find_prefix(g)
    
    # dglob_outfiles_cache[x] is list of all file paths that start with <x>.
    #
    # First find all entries in dglob_outfiles_cache that are supersets of what
    # we need.
    i = 0
    while 1:
        assert i < 2
        i += 1
        shorter = []
        for prefix, items in dglob_outfiles_cache.items():
            if g_prefix.startswith( prefix):
                shorter.append( prefix)
        if shorter:
            break
        #print 'finding g=%r g_prefix=%r' % (g, g_prefix)
        # No matching items, so create a new one:
        dglob_outfiles_cache[ g_prefix] = glob.glob( '%s*' % g_prefix)
        #print 'g_prefix=%r: numitems: %s' % (g_prefix, len(dglob_outfiles_cache[ g_prefix]))
    
    # The longest item in <shorter> will be the best to use.
    longest = max( shorter, key=len)
    ret = []
    cache = None
    if g_prefix != longest:
        # Also construct a new entry in dglob_outfiles_cache.
        cache = []
    #print '%r %r: looking at longest=%r: %s' % (g, g_prefix, longest, len(dglob_outfiles_cache[ longest]))
    for item in dglob_outfiles_cache[ longest]:
        m = fnmatch.fnmatch( item, g)
        #print 'm=%s. item=%r len(ret)=%s' % (m, item, len(ret))
        if m:
            #print '*** item=%r g=%r: m=%s. len(ret)=%s' % (item, g, m, len(ret))
            ret.append( item)
        if cache is not None:
            if item.startswith( g_prefix):
                cache.append( item)
    if cache is not None:
        #print 'setting cache[%r]: %s' % (g_prefix, len(cache))
        dglob_outfiles_cache[ g_prefix] = cache
    
    if 0:
        # For testing.
        ret0 = glob.glob(g)
        ret0.sort()
        ret1 = sorted(ret)
        if ret1 != ret0:
            print( 'error: g=%r' % g)
            print( 'ret0=%s' % ret0)
            print( 'ret1=%s' % ret1)
            print( 'len(dglob_outfiles_cache)=%s' % len(dglob_outfiles_cache))
            assert 0
    return ret

def bmpcmp_outfiles( state, input_):
    '''
    Returns list of bmpcmp output files, found using `dglob()`.
    '''
    #print 'bmpcmp_outfiles(): state.device=%s input_=%s' % (state.device, input_)
    g = '%s*.bmpcmp.*' % (out_filename(state, input_, ''))
    #print 'bmpcmp_outfiles(): g: %s' % (g)
    gg = dglob( g)
    return gg


def get_ghostpdl_dir():
    for p in (
            os.path.abspath( 'ghostpdl'),
            os.path.abspath( '.'),
            os.path.abspath( '../ghostpdl'),
            ):
        if os.path.isdir( p) and os.path.basename( p) == 'ghostpdl':
            ghostpdl_dir = os.path.abspath(p)
            return ghostpdl_dir
    return None
    raise Exception( 'cannot find ghostpdl directory')

def get_mupdf_dir():
    for p in (
            os.path.abspath( 'mupdf'),
            os.path.abspath( '.'),
            os.path.abspath( '../mupdf'),
            os.path.abspath( f'{__file__}/../../mupdf'),
            ):
        if os.path.isdir( p) and (
                os.path.basename( p) == 'mupdf'
                or os.path.basename( p).startswith('mupdf-')
                ):
            ghostpdl_dir = os.path.abspath(p)
            return ghostpdl_dir
    return None
    raise Exception( 'cannot find mupdf_dir directory')


def bug_file_get( attachment_number):
    leaf = f'attachment.cgi?id={attachment_number}'
    url = f'https://bugs.ghostscript.com/{leaf}'
    if os.path.exists( leaf):
        jlib.log( 'Not downloading from {url} because local file exists: {leaf}')
    else:
        jlib.system( f'wget {url}')


def command( command: str):
    '''
    Runs <command> locally.

    E.g.:
        --command 'rsync -ai foo.html ghostscript.com:public_html/'
    '''
    jlib.system( command, verbose=1)
    

run = command

def build( target, operations, j: int=None):
    '''
    Does build(s) inside the `ghostpdl/` or `mupdf/` directories.
    
    Args:
        target:
            Path of target.

            For ghostpdl, we find build flags from `target`, by ignoring any
            trailing '-bin' or 'bin' and then splitting by '-'. Supported flags
            are:

                debug
                luratech
                mem
                profile
                san
                ufst

            'luratech' and 'ufst' use a separate Makefile.

            Ordering of flags:

                Reordering flags will result in needless duplicate builds. For
                example these two targets:
                        `ghostpdl/debug-mem-bin`
                        `ghostpdl/mem-debug-bin`

                will result in duplicated builds of `.o` files in
                `ghostpdl/debug-mem-obj/` and `ghostpdl/mem-debug-obj/`.

            Example values for `target`:
                `ghostpdl/bmpcmp`:
                    Special target that builds the bmpcmp binary.
                `ghostpdl/bin/gs`:
                    Normal release build of gs.
                `ghostpdl/vs2005bin/gs`
                `ghostpdl/vs2008bin/gs`
                `ghostpdl/vs2019bin/gs`
                `ghostpdl/vs-bin/gs`
                    Assumes Windows, runs `cmd.exe` with vcvars etc then `nmake`.

                    2022-03-29: 'vs' is currently same as vs2019.

                    Puts `.obj` files into `ghostpdl/vs2005obj/`, but unfortunately
                    puts binaries into `ghostpdl/bin`, so this directory needs
                    to be manually deleted if one changes between different VS
                    versions.

                    Note that vs2005-obj fails - the '-' seems to cause
                    problems, perhaps because `obj/_dll.rc` does:
                    
                        `#define gstext_ico .\\vs2008-obj\\gswin.ico`
                bin/gs
                    As above if cwd is `ghostpdl/` directory.
                ghostpdl/luratech-bin/gs
                    Build gs with luratech. Requires `./luratech` to be a
                    luratech checkout.
                .
                    Shortcut to build `ghostpdl/bin/gs`.
                .binaries
                    Shortcut to build all binaries that have been added with
                    --binary.
                .bmpcmp
                    Shortcut to build `ghostpdl/bmpcmp`.

        operations:
            Any of these characters:
                c   - configure
                m   - make
        j:
            Concurrency.

    To allow building of Luratech binaries, we create/delete a softlink to
    ./luratech inside ghostpdl/ (i.e. ghostpdl/luratech -> ../luratech)
    as required.

    E.g.:
        --build . cm
        --build ghostpdl/bin/gs cm
        --build ghostpdl/mem-bin/gs cm
        --build ghostpdl/sodebugbin/gs cm
        --build .binaries cm
        -b mupdf/build/debug .
        -b mupdf/build/sanitize .
        -b mupdf/build/debug-extract .
        -b mupdf/build/shared-debug .
        -b mupdf/build/debug/examples .
        -b mupdf/platform/win32/Debug/mutool.exe .
        -b mupdf/platform/win32/Release/mutool.exe .
    '''
    out = jlib.StreamPrefix( sys.stdout, '%s: ' % target)
    
    if j is None:
        j = '3'
    j = '' if j == '0' else f'-j {j}'
    ghostpdl_dir = get_ghostpdl_dir()
    mupdf_dir = get_mupdf_dir()
    
    target_abs = os.path.abspath( target)
    #jlib.log('{target=} {target_abs=}')
    if target_abs.startswith( mupdf_dir):
        target_rel = os.path.relpath( target_abs, mupdf_dir)
        
        m = re.match( '^platform/win32/([^/]+)/([^/]+)$', target_rel)
        if m:
            if m.group(2) == 'mutool.exe':
                command = f'cd {mupdf_dir} && /cygdrive/c/Program\ Files\ \(x86\)/Microsoft\ Visual\ Studio/2019/Community/Common7/IDE/devenv.com platform/win32/mupdf.sln /Build {m.group(1)} /Project mutool'
            elif m.group(2) == 'mupdfcpp.dll':
                # Note that we append 'Python' to the /Build argument; this seems to work, but haven't investigated.
                command = f'cd {mupdf_dir} && /cygdrive/c/Program\ Files\ \(x86\)/Microsoft\ Visual\ Studio/2019/Community/Common7/IDE/devenv.com platform/win32/mupdf.sln /Build {m.group(1)}Python /Project mupdfcpp'
            else:
                assert 0, f'm.group(2)={m.group(2)!r}'
            jlib.log(f'running: {command}')
            jlib.system( command, out=out)
            return
        
        if os.uname()[0] == 'OpenBSD':
            command = 'CXX=clang++ gmake'
        else:
            command = 'make'
        command += ' verbose=yes'
        command += f' {j}'
        #command += ' verbose=yes'
        if 0: pass
        elif target_rel.startswith( 'build/shared-'):
            command += ' HAVE_GLUT=no HAVE_PTHREAD=yes shared=yes'
            flags = target_rel[ len('build/shared-'):].split('-')
            for flag in flags:
                if 0: pass
                elif flag == 'debug': command += ' build=debug'
                elif flag == 'release': command += ' build=release'
                elif flag == 'extract': command += ' extract=yes'
                else:
                    raise Exception(f'Unrecognised flag: {flag!r} in {flags!r}')
        elif target_rel == 'build/sanitize':
            command += ' sanitize'
        elif target_rel == 'build/debug':
            command += ' debug'
        elif target_rel == 'build/debug/examples':
            command += ' build=debug examples'
        elif target_rel == 'build/memento/examples':
            command += ' build=memento examples'
        elif target_rel == 'build/debug-extract':
            assert 0, 'extract is now default'
            command += ' extract=yes debug'
        elif target_rel == 'build/release-extract':
            assert 0, 'extract is now default'
            command += ' extract=yes release'
        elif target_rel == 'build/memento':
            command += ' build=memento'
        elif target_rel == 'build/memento-extract':
            assert 0, 'extract is now default'
            command += ' build=memento extract=yes'
        elif target_rel == 'build/wasm':
            command += ' wasm'
        elif target_rel == 'build/release':
            command += ' release'
        else:
            assert 0, f'Unrecognised target_rel={target_rel}'
        jlib.log( 'Building mupdf')
        jlib.system( f'cd {mupdf_dir} && {command}', verbose=1, out=out)
        return
    
    #elif 1:#target_abs.startswith( ghostpdl_dir) or target_abs.startswith( 'ghostpdl2/'):
    #if '/ghostpdl/' in target_abs or '/ghostpdl2/' in target_abs:
    g = target_abs.find( '/ghostpdl')
    if g >= 0:
        d = target_abs[ :g]
        gg = target_abs.find( '/', g+1)
        assert gg >= 0
        ghostpdl_dir = target_abs[ :gg]
        # Make target relative to ghostpdl directory.
        target_rel_ghostpdl = os.path.relpath( target_abs, ghostpdl_dir)
        #jlib.log('{target_abs=} {ghostpdl_dir=} {target_rel_ghostpdl=}')

        #jlib.log( '{ghostpdl_dir=} {target=} {os.path.abspath(target)=} {target_rel_ghostpdl=}')

        if target_rel_ghostpdl == 'bmpcmp':
            # The command that we use to build bmpcm comes from the
            # command documented inside ghostpdl/toolbin/bmpcmp.c
            # itself.
            jlib.system( 'cd %s' % ghostpdl_dir
                    + ' && cc -I./obj -I./libpng -I./zlib -o bmpcmp -DHAVE_LIBPNG ./toolbin/bmpcmp.c ./libpng/png.c ./libpng/pngerror.c ./libpng/pngget.c ./libpng/pngmem.c ./libpng/pngpread.c ./libpng/pngread.c ./libpng/pngrio.c ./libpng/pngrtran.c ./libpng/pngrutil.c ./libpng/pngset.c ./libpng/pngtrans.c ./libpng/pngwio.c ./libpng/pngwrite.c ./libpng/pngwtran.c ./libpng/pngwutil.c ./zlib/adler32.c ./zlib/crc32.c ./zlib/infback.c ./zlib/inflate.c ./zlib/uncompr.c ./zlib/compress.c ./zlib/deflate.c ./zlib/inffast.c ./zlib/inftrees.c ./zlib/trees.c ./zlib/zutil.c -lm',
                    out=out,
                    );
            return

        match = re.match( '^([a-z0-9-]+)/([^/]+)$', target_rel_ghostpdl)
        if not match:
            raise Exception( 'unrecognised target: %s. ghostpdl_dir=%s' % (target_rel_ghostpdl, ghostpdl_dir))

        directory = match.group(1)
        make_target = match.group(2)

        if directory.endswith( '-bin'):
            flags = directory[:-4].split('-')
        elif directory.endswith( 'bin'):
            flags = directory[:-3].split('-')
        else:
            raise Exception( 'in target %r, %r must end with "-bin" or "bin"' % (target, directory))

        command = 'cd %s' % ghostpdl_dir

        jlib.log( '{target=} {operations=} {ghostpdl_dir=} {target_rel_ghostpdl=} {directory=} {make_target=} {flags=} {command=}')

        # Different targets use different variable names for the build directories.
        prefix_var = 'BUILDDIRPREFIX'
        if 'debug' in flags:
            prefix_var = 'DEBUGDIRPREFIX'
            make_target += 'debug'
        elif 'profile' in flags:
            prefix_var = 'PGDIRPREFIX'
            make_target += 'pg'
        elif 'mem' in flags:
            prefix_var = 'MEMENTODIRPREFIX'
            make_target += 'memento'
        elif 'san' in flags:
            prefix_var = 'SANITIZEDIRPREFIX'
            make_target += 'sanitize'
            if os.uname()[0] == 'OpenBSD':
                pass
        elif 'sodebug' in flags:
            prefix_var = 'DEBUGDIRPREFIX'
            make_target = 'sodebug'
        
        vscommand = None
        for flag in flags:
            if flag.startswith('vs'):
                if flag == 'vs2005':
                    vscommand = f'cmd.exe /V /C @ "c:/Program Files (x86)/Microsoft Visual Studio 8/Common7/Tools/vsvars32.bat" "&&" nmake EXTRACT_DIR=extract PRODUCT_PREFIX={flag} -f psi/msvc.mak'
                elif flag == 'vs2008':
                    vscommand = f'cmd.exe /V /C @ "c:/Program Files (x86)/Microsoft Visual Studio 9.0/Common7/Tools/vsvars32.bat" "&&" nmake EXTRACT_DIR=extract PRODUCT_PREFIX={flag} -f psi/msvc.mak'
                elif flag == 'vs' or flag == 'vs2019':
                    vscommand = '/cygdrive/c/Program\ Files\ \(x86\)/Microsoft\ Visual\ Studio/2019/Community/Common7/IDE/devenv.com windows/GhostPDL.sln /Build Debug /Project ghostscript'
                    #vscommand = '/cygdrive/c/Program\ Files\ \(x86\)/Microsoft\ Visual\ Studio/2019/Community/Common7/IDE/devenv.com windows/GhostPDL.sln /ProjectConfig mupdfcpp'
                    #vscommand = '/cygdrive/c/Program\ Files\ \(x86\)/Microsoft\ Visual\ Studio/2019/Community/Common7/IDE/devenv.com windows/GhostPDL.sln /Build DebugPython /Project mupdfcpp'
                else:
                    raise Exception('Unrecognised vs flag={flag}')
       
        git_id = None
        
        if vscommand:
            command += f' && {vscommand}'

        else:
            # We use different makefiles for some builds.
            #
            # See base/unix-end.mak for available targets.
            #
            # Each time we run make, we make Makefile be a link to the specific
            # makefile, rather than use 'make -f ...', because the latter doesn't work
            # - generated makefiles do 'MAKEFILE=Makefile' then 'SUB_MAKE_OPTION=-f
            # $(MAKEFILE)', so would need to be patched up to work with -f.
            #
            makefile_suffix = '-default'
            if 'luratech' in flags:
                assert not 'ufst' in flags
                makefile_suffix = '-luratech'
                command += ' && ln -fs ../luratech'
            else:
                command += ' && rm -f luratech'
            if 'ufst' in flags:
                makefile_suffix += '-ufst'
            if 'extract' in flags:
                makefile_suffix = '-extract'

            clang = False
            clang = 'clang' in flags
            if clang:
                makefile_suffix += '-clang'

            # Add in commands to do configure and make:
            #
            if 'c' in operations:   # configure
                command += ' && echo doing operation "c"'
                env = ''
                if os.uname()[0] == 'OpenBSD':
                    # Need to specify autoconf version.
                    def find_version( base):
                        items = glob.glob( '%s*' % base)
                        assert items
                        items.sort()
                        return items[-1][len(base):]
                    env += ' AUTOCONF_VERSION=%s' % find_version( '/usr/local/bin/autoconf-')
                    env += ' AUTOMAKE_VERSION=%s' % find_version( '/usr/local/bin/automake-')
                    env += ' CONFIGURE_STYLE=gnu'
                    env += ' CC=cc'
                    env += ' CXX=c++'

                if clang:
                    env += ' CC=clang'
                #if not 'extract' in flags:
                #    env += ' EXTRACT='
                command += f' && {env}'
                if os.uname()[0] == 'OpenBSD':
                    command += ' /usr/local/bin/bash '
                command += ' ./autogen.sh --enable-mkromfs-quiet --enable-cluster'
                if 'ufst' in flags:
                    command += ' --with-ufst=../ufst'
                if 'extract' in flags:
                    command += ' --with-extract-dir=extract'
                else:
                    command += ' --with-extract-dir=no'
                if 0:
                    command += ' --help'
                command += ' && mv Makefile Makefile%s' % makefile_suffix

            if 'm' in operations:   # make
                command += ' && echo doing operation "m"'
                command += ' && ln -fs Makefile%s Makefile' % makefile_suffix
                if os.uname()[0] == 'OpenBSD':
                    command += ' && LIBRARY_PATH=/X11R6/lib CFLAGS=-L/X11R6/lib gmake --debug=n XTRALIBS="-lexecinfo"'
                else:
                    command += ' && make --debug=n'
                command += f' %s=%s {j} %s' % (prefix_var, directory[:-3], make_target)

                git_id = jlib.git_get_id( ghostpdl_dir)

        jlib.log( 'running {command=}')
        jlib.system( command, out=out)
        jlib.logx( '{git_id=}')
        if git_id:
            # Copy git id file to <target>.git-id.
            with open( '%s.git-id' % target, 'w') as f:
                f.write( git_id)
    else:
        raise Exception( f'Target is not in mupdf or ghostpdl directories: {target_abs}')




def copy_git_clean( source, destination, doit):
    '''
    Copies git files from `source` to `destination`, removing any other
    existing files in `destination`.
    '''
    print( 'doit=%r' % doit)
    filenames = jlib.git_get_files( source, submodules=True)
    if not filenames:
        raise Exception( 'source directory has no git files: %s' % source)
    with open( 'jtest-copy-git-temp', 'w') as f:
        for filename in filenames:
            print( '%s' % filename, file=f)
    for dest_dirpath, dest_dirnames, dest_filenames in os.walk( destination, topdown=True):
        if doit == 2:
            # Find all directories owned by root and use 'mv' to append
            # '._trash' to their names so they can be identified and cleaned up
            # separately, e.g. from within the docker with:
            #
            #   find ghostpdl -type d -name "*._trash" -exec rm -r '{}' ';'
            if 1:
                for leaf in dest_dirnames:
                    p = os.path.join( dest_dirpath, leaf)
                    s = os.stat(p)
                    #print( 'p=%r s=%s' % (p, s))
                    if s.st_gid == 0:
                        #print( 'would move to trash: %s' % p)
                        assert p.startswith( destination + '/')
                        assert ' ' not in p
                        assert '"' not in p
                        jlib.system( 'mv "%s" "%s._trash"' % (p, p))
            if 0:
                for leaf in dest_filenames:
                    p = os.path.join( dest_dirpath, leaf)
                    s = os.stat(p)
                    #print( 'p=%r s=%s' % (p, s))
                    if s.st_gid == 0:
                        #print( 'would move to trash: %s' % p)
                        assert p.startswith( destination + '/')
                        assert ' ' not in p
                        assert '"' not in p
                        jlib.system( 'mv "%s" "%s._trash"' % (p, p))
                
            for dest_filename in dest_filenames:
                p = os.path.join( dest_dirpath, dest_filename)
                ptail = p[ len(destination)+1:]
                if ptail not in filenames:
                    assert p.startswith( destination)
                    if doit:
                        print( 'deleting: %s' % p)
                        os.remove(p)
                    else:
                        print( 'would delete: %s' % p)
    n = 'n'
    if doit:
        n = ''
    #command = 'rsync -ai%s --delete-missing-args --files-from=jtest-copy-git-temp %s/ %s/' % (
    command = 'rsync -ai%s --files-from=jtest-copy-git-temp %s/ %s/' % (
            n,
            source,
            destination,
            )
    jlib.system( command)



mupdf_mutool_test_command_cache = None

def mupdf_mutool_test_command( test_id, do_sync):
    '''
    Returns the parameters passed to mutool.py for test identified by
    `test_id`.
    '''
    global mupdf_mutool_test_command_cache
    if mupdf_mutool_test_command_cache is None:
        filename = 'mupdf_mutool_jobs'
        if do_sync:
            jlib.system(
                    f'rsync -aiz julian@casper.ghostscript.com:~regression/cluster/auto/nightly/mudrawpy/jobs {filename}',
                    verbose=1,
                    )
        with open( filename) as f:
            text = f.read()
        lines = text.split( '\n')
        mupdf_mutool_test_command_cache = dict()
        for i, line in enumerate( lines):
            #jlib.debug_periodic( f'{i}/{len(lines)}')
            if not line or line.startswith( '#'):
                continue
            space, _ = jlib.strpbrk( line, ' \t')
            test_id = line[:space]
            mutool_py = line.rfind( 'mutool.py')
            assert mutool_py >= 0, f'i={i} line is: {line!r}'
            mutool_py = line.find( ' ', mutool_py)
            mutool_py += 1
            md5sum = line.find( '| md5sum', mutool_py)
            assert md5sum >= 0
            command = line[ mutool_py : md5sum]
            command = command.strip()
            mupdf_mutool_test_command_cache[ test_id] = command
            
    return mupdf_mutool_test_command_cache.get( test_id)
    
    
    

def mupdf_mutool_compare( do_sync, tabfile1=None, tabfile2=None):
    '''
    Shows information about mutool vs mutool.py test results.
    '''
    if tabfile1 is None:
        tabfile1 = 'julian@casper.ghostscript.com:~regression/cluster/mupdf-current.tab'
    if tabfile2 is None:
        tabfile2 = 'julian@casper.ghostscript.com:~regression/cluster/auto/nightly/mudrawpy/mudrawpy-temp.tab'
    
    class Result:
        def __init__( self, items):
            (
                self.testfile_device_res_band,
                self.error, 
                self.time1,
                self.time2,
                self.mem1,
                self.mem2,
                self.md5sum,
                self.rev,
                self.product,
                self.machine,
                self.intermediate_size, 
                self.intermediate_md5sum,
            ) = items
    
    def make_test( line):
        '''
        Returns None or a Result instance.
        '''
        items = line.split( '\t')
        if len( items) != 12:
            jlib.log( '{len(items)=}: {line!r=}')
            for i, item in enumerate(items):
                jlib.log( '    {i}: {item!r}')
            return None
        return Result( items)
    
    def make_tests( remote_name):
        '''
        Returns dict mapping from test_id's to Result instances.
        '''
        tests = dict()
        local_name = os.path.basename( remote_name)
        if do_sync:
            jlib.system( f'rsync -aiz {remote_name} {local_name}', verbose=1)
        with open( local_name) as f:
            for line in f:
                test = make_test( line)
                if test is None:
                    jlib.log( 'cannot parse .tab line: {line!r}')
                else:
                    #self.tests.append( test)
                    tests[ test.testfile_device_res_band] = test
        return tests
    
    results_mutool = make_tests( tabfile1)
    results_mutool_py = make_tests( tabfile2)
    
    # Find set of all test_id's.
    all_test_ids = set(results_mutool.keys()) | set(results_mutool_py.keys())
    all_test_ids = list( all_test_ids)
    all_test_ids.sort()
    
    jlib.log( '{len(all_test_ids)=} {len(results_mutool)=} {len(results_mutool_py)=}')
    
    def show_diffs( details=0, criteria=None):
        '''
        Shows information on differences between mutool and mutool.py test
        results. For the selected tests, we show the number of tests where
        mutool and mutool.py termination code or md5sum differ.
        
        details:
            If true, show individual tests.
        criteria:
            None or callable taking (b,p) args, where <b> and <p> are None or
            Results instances for mutool and mutool.py; should return true if
            this test_id should be included.
            
        '''
        jlib.log( 'Results for {details=} {criteria=}:')
        n_diff = 0
        n = 0
        test_id_max = 0
        # We make two passes, first pass only calculates <test_id_max> so that
        # the second pass can format things nicely.
        #
        for pass_ in 0, 1:
            for test_id in all_test_ids:
                c = results_mutool.get( test_id)
                p = results_mutool_py.get( test_id)

                if criteria and not criteria( c, p):
                    continue

                if pass_ == 0:
                    # Just update <test_id_max> so we can format things nicely.
                    test_id_max = max( test_id_max, len( test_id))
                    continue

                n += 1
                if c.error != p.error or c.md5sum != p.md5sum:
                    n_diff += 1

                if not details:
                    continue
                
                if ( details == 1
                        and c
                        and p
                        and c.error == p.error
                        and c.md5sum == p.md5sum
                        ):
                    continue

                text = f'    {test_id}:'
                text += ' ' * (test_id_max - len(test_id))
                if c and p:
                    if c.error != p.error:
                        text += f' error {c.error} => {p.error}.'
                    if c.md5sum != p.md5sum:
                        text += f' md5sum {c.md5sum} => {p.md5sum}.'
                else:
                    if c:
                        text += f' mutool=({c.error} {c.md5sum}).'
                    else:
                        text += ' mutool not run.'
                    if p:
                        text += f' mutool.py=({p.error} {p.md5sum}).'
                    else:
                        text += ' mutool.py not run.'

                command = mupdf_mutool_test_command( test_id, do_sync)
                text += f' command: {sys.argv[0]} --mupdf-mutool-run debug {command}'
                jlib.log( text)
        
        jlib.log( '    Total number of tests: {jlib.number_sep(len(all_test_ids))}')
        jlib.log( '    Number of selected tests: {jlib.number_sep(n)}')
        jlib.log( '    Number of selected tests that differ: {jlib.number_sep(n_diff)}')
    
    show_diffs(
            criteria=lambda b, p: b and p,
            details=1,
            )
    if 0:
        show_diffs(
                criteria=lambda b, p: b and p and (b.error != p.error or b.md5sum != p.md5sum),
                details=1,
                )

def args_to_command( args):
    command = ''
    while 1:
        try: arg = next( args)
        except StopIteration:   break
        arg = arg.replace( '"', '\\"')
        arg = arg.replace( "'", "\\'")
        #arg = arg.replace( ' ', '\\ ')
        command += ' "%s"' % arg.replace( '"', '\\"')
    return command


def rcommand( state, remote_command, login=None):
    # 2023-07-02: `-tt` seems to cause carriage returns to appear in output.
    #command = f'{state.remote.ssh} -Y -tt '
    command = f'{state.remote.ssh} '
    if 0:
        command += '-Y '
    if state.remote.directory:
        remote_command = f'cd {state.remote.directory} && {remote_command}'
    if login:
        remote_command = f'. /etc/profile && {remote_command}'
    command += shlex.quote( remote_command)
    jlib.system( command, 
            prefix='%s: ' % state.remote.description,
            verbose=1,
            out='log',
            #raise_errors=False,
            )

def sync_mupdf_cpp(state, directory):
    '''
    Syncs `<directory>/platform/c++/` to remote.
    '''
    assert 0, f'Should not use this fn - causes remote builds to rebuild unnecessarily.'
    command = (
            f'rsync'
            f' -aizR'
            f' --rsh "{state.remote.ssh}"'
            f' --exclude="*.html"'
            f' --exclude="*.js"'
            f' --exclude "*/html/*"'
            f' --exclude "*.tex"'
            f' {directory}/./platform/c++/'
            f' :{state.remote.directory}/{directory}/'
            )
    jlib.system( command, verbose=1)

def sync_this( remote):
    arg0 = os.path.realpath( sys.argv[0])
    jlib_py = os.path.relpath( '%s/../jlib.py' % arg0, '.')
    command = 'rsync -aiL --rsh "%s" %s %s :%s/' % (remote.ssh, arg0, jlib_py, remote.directory)
    jlib.system( command, 
            prefix='%s: ' % remote.description,
            verbose=1,
            )
    

class Remote:
    def __init__(
            self,
            text: str,
            d='artifex-remote',
            allow_sync_directories=True,
            ssh=None,
            login=False,
            ):
        '''
        Specification of remote machine and directory and (optionally) sync directories.
        
        Args:
            text:
                If ssh is None, this specifies everything:
            
                * Optional jump host.
                * Remote host.
                * Directory.
                * Comma-separated sync directories.

                    `[ssh <extra>] [<jump-host>::][<user>@]<host>[:<port>][:<remote-dir>][,<sync-directory>]*`
            
                Otherwise:
                * Comma-separated or list of sync directories.
            d:
                Remote directory.
            ssh:
                If set, should be a ssh command, which will be used with
                rsync's `--rsh` and to ssh to the remote. Typically this is
                used for complicated jump hosts and ports.
            login:
                If specified we explicitly source /etc/profile so that PATH etc
                are set up as for an interactive shell..
        
        Class members, derived from `text`:
            self.ssh:
                The ssh command. Can be used with `rsync` for example: 'rsync
                --rsh {shlex.quote(remote.ssh)} :{remote.directory}{leaf} ./'.
            self.directory:
                Remote directory; ends with '/'.
            self.sync_directories:
                List of sync directories. Does not exist if
                `allow_sync_directories` is false.
            self.description:
                A string of the form '[ssh extra] <remote-location> [sync-directories]'.
        
        Basic user at remote host:
            
            >>> r = Remote( 'user@host:')
            >>> r.ssh
            'ssh user@host'
            >>> r.directory
            ''
        
        Specify remote directory:
            
            >>> r = Remote( 'user@host:dir')
            >>> r.ssh
            'ssh user@host'
            >>> r.directory
            'dir/'
        
        If no remote ':', we default to 'artifex-remote':
            
            >>> r = Remote( 'user@host')
            >>> r.ssh
            'ssh user@host'
            >>> r.directory
            'artifex-remote/'
            
            >>> r = Remote( 'host,dir')
            >>> r.ssh
            'ssh host'
            >>> r.directory
            'artifex-remote/'
            >>> r.sync_directories
            ['dir']
        
        Specify a jump host
            
            >>> r = Remote( 'juser@jhost::user@host:dir')
            >>> r.ssh
            'ssh -J juser@jhost user@host'
            >>> r.directory
            'dir/'
        
        Specify extra ssh args with `ssh <args>`. These extra args must end
        with a space and do not contain a '@' character, so they are naturally
        terminated by the user/host/directory specifications.
        
            >>> r = Remote( 'ssh -F julian-tools/ssh-config julian@foo.com')
            >>> print( r.description_multiline())
            ssh: ssh -F julian-tools/ssh-config julian@foo.com
            remote directory: artifex-remote/
            <BLANKLINE>
            
            >>> r = Remote( 'ssh -F julian-tools/ssh-config foo.com,bar')
            >>> print( r.description_multiline())
            ssh: ssh -F julian-tools/ssh-config foo.com
            remote directory: artifex-remote/
            sync directory: bar
            <BLANKLINE>
        
        Specify extra ssh args, and a port for the jump host, and sync-directories:
            
            >>> r = Remote( 'ssh -F julian-tools/ssh-config julian@miles.ghostscript.com:5222::julian@mac2019:foo,mupdf,PyMuPDF')
            >>> r.description
            '[ssh -F julian-tools/ssh-config] julian@miles.ghostscript.com:5222::julian@mac2019:foo/ [mupdf PyMuPDF]'
            >>> print( r.description_multiline())
            ssh: ssh -F julian-tools/ssh-config -J julian@miles.ghostscript.com:5222 julian@mac2019
            remote directory: foo/
            sync directory: mupdf
            sync directory: PyMuPDF
            <BLANKLINE>
        '''
        self.directory = d
        self.login = login
        
        pre = '+macos'
        if not ssh and text.startswith(pre):
            text = text[len(pre):]
            ssh = 'ssh -o ServerAliveInterval=60 -J julian@miles.ghostscript.com:5222 julian@mac2019'
            self.login = True
        pre = '+pi'
        if not ssh and text.startswith(pre):
            text = text[len(pre):]
            ssh = 'ssh julian@rjw.ghostscript.com -p 2222'
            #self.login = True
        
        if ssh:
            self.ssh = ssh
            self.ssh_extra = ''
            #self.description = f'{ssh}'
            self.description = ssh.split()[-1]
            self.sync_directories = text
        
        else:
            re_user_at = '(?P<user_at>[^@]+@)'                          # <user>@
            re_host = '(?P<host>[^:@,]+)'                               # <host>
            re_user_host = f'(?P<user_at_host>{re_user_at}?{re_host})'  # [<user>@]<host>
            re_directory = '(:(?P<directory_name>[^:,]*))'              # :<directory>
            re_sync_directories = '(?P<directories>(,[^,:]*)*)'         # [,<sync-directory>]*
            re_ssh_extra = '((?P<ssh_extra>ssh [^@]+) )'                # ssh *
            re_port = '(:(?P<port>[0-9]+))'                             # :<port>
            re_jump = f'(?P<jump_outer>(?P<jump>{re_user_at}{re_host}{re_port}?)::)'  # [<juser>@]<jhost>::
            re_jump = re_jump.replace( 'user_at', 'jump_user_at')
            re_jump = re_jump.replace( 'host', 'jump_host')

            regex = f'^{re_ssh_extra}?{re_jump}?{re_user_host}{re_directory}?'
            if allow_sync_directories:
                regex += f'{re_sync_directories}'
            regex += '$'

            m = re.match( regex, text)
            assert m, f'Expected [[<user>@]<jumphost>::][<user>@]<hostname>:[<directory>] but: {text!r}'
            self.re_match = m

            self.ssh_extra = m.group('ssh_extra')
            jump = m.group('jump')
            user_at = m.group('user_at') or ''
            host = m.group('host')
            if m.group('directory_name'):
                self.directory = m.group('directory_name')
            jump = f' -J {jump}' if jump else ''
            self.ssh = self.ssh_extra if self.ssh_extra else 'ssh'
            if host == 'mac-mini':
                # These don't seem to prevent problem where our ssh hangs, but
                # can be resumed by entering data in a separate ssh login.
                self.ssh += ' -o ServerAliveInterval=60'
                self.ssh += ' -o TCPKeepAlive=yes'
            self.ssh += f'{jump} {user_at}{host}'
            if self.directory and not self.directory.endswith('/'):
                self.directory += '/'

            self.description = f'{user_at}{host}:{self.directory}'

            self.description_detailed = ''
            if self.ssh_extra:
                self.description_detailed += f'[{self.ssh_extra}] '
            if m.group('jump_outer'):
                self.description_detailed += m.group("jump_outer")
            self.description_detailed += f'{user_at}{host}:{self.directory}'
            if allow_sync_directories:
                self.sync_directories = m.group('directories')
        
        if allow_sync_directories:
            if self.sync_directories:
                if isinstance( self.sync_directories, str):
                    self.sync_directories = self.sync_directories.split( ',')
                if self.sync_directories[0] == '':
                    del self.sync_directories[0]
                #self.description += f' [{" ".join(self.sync_directories)}]'
            else:
                self.sync_directories = ()
        jlib.log('{self=}')
        
    def __str__( self):
        ret = f'-d {self.directory}'
        ret += f' --ssh {shlex.quote(self.ssh)}'
        if self.login:
            ret += ' --login'
        ret += f' {",".join(self.sync_directories)}'
        #jlib.log('{ret=}')
        return ret
    def __repr__(self):
        return str(self)
    
    def description_multiline( self):
        '''
        Returns multiline description.
        '''
        ret = ''
        ret += f'ssh: {self.ssh}\n'
        ret += f'remote directory: {self.directory}\n'
        if hasattr( self, 'sync_directories'):
            for d in self.sync_directories:
                ret += f'sync directory: {d}\n'
        return ret


def remote( remote: Remote, command: str, clean=True):
    '''
    Syncs specified directories to remote and runs `command` on remote.
    
    If `clean` is true, we do `git clean -f` in each remote directory after
    syncing.
    '''
    try:
        jlib.log( 'remote {remote!r} {command!r}')
        state.remote = remote
        sync_this( state.remote)
        command_pre = list()
        for directory in state.remote.sync_directories:
            jlib.log('{directory=}')
            if directory.endswith( '++'):
                # Force sync of mupdf platform/c++.
                directory = directory[:-2]
                sync_mupdf_cpp( state, directory)
            _sync( '%s' % directory, state.remote.ssh, state.remote.directory, submodules=True, dot_git=True)
            if clean:
                command_pre.append(f'(cd {directory} && git clean -f)')

        jlib.log( '{state.remote.ssh=} {state.remote.directory=} {state.remote.description=}')
        if command_pre:
            command_pre = '&&'.join(command_pre)
            if command:
                command = f'{command_pre} && {command}'
            else:
                command = command_pre
        jlib.log( '{command!r=}')
        if command:
            rcommand( state, command, remote.login)
    except Exception as e:
        raise
        #jlib.exception_info()
        #return e

def pymupdf( mupdf=None, clean: bool=True, remote: Remote=None):
    '''
    Builds PyMuPDF sdist and wheel.
    
    Need setuptools, e.g.: `sudo apt install python3-setuptools`
    '''
    try:
        jlib.log( 'pymupdf() {=mupdf clean}')
        assert os.path.isdir( 'PyMuPDF')
        if mupdf is None:
            # Download and extract MuPDF release.
            mupdf_url = 'https://mupdf.com/downloads/archive/mupdf-1.20.0-rc1-source.tar.gz'
            mupdf_leaf = os.path.basename( mupdf_url)
            tail = '.tar.gz'
            assert mupdf_leaf.endswith( tail)
            mupdf = mupdf_leaf[ : -len(tail)]
            assert mupdf.startswith( 'mupdf')
            if not os.path.isdir( mupdf):
                if not os.path.exists( mupdf_leaf):
                    jlib.system( f'wget {mupdf_url}', out='log', verbose=1)
                    assert os.path.exists( mupdf_leaf)
                jlib.fs_remove( mupdf)
                jlib.untar( mupdf_leaf, prefix=mupdf+'/')

        # Clean build mupdf. We overwrite mupdf's config file with PyMuPDF's.
        if clean:
            jlib.fs_remove( f'{mupdf}/build')
        jlib.fs_copy( 'PyMuPDF/fitz/_config.h', f'{mupdf}/include/mupdf/fitz/config.h')
        env = ''
        flags = 'HAVE_X11=no HAVE_GLFW=no HAVE_GLUT=no HAVE_LEPTONICA=yes HAVE_TESSERACT=yes'
        flags += ' verbose=yes'
        make = 'make'
        if os.uname()[0] == 'Linux':
            env += ' CFLAGS="-fPIC"'
        if os.uname()[0] == 'OpenBSD':
            make = 'gmake'
            env += ' CXX=clang++'
        command = f'cd {mupdf} && {env} {make} {flags}'
        jlib.system( command, out='log', verbose=1)

        # Build PyMuPDF.
        jlib.system( f'cd PyMuPDF && PYMUPDF_MUPDF_LOCAL={os.path.abspath(mupdf)} {sys.executable} setup.py build',
                out='log', verbose=1)

        # Create wheel by making a pipcl.Package() with the same settings as
        # used by PyMuPDF's setup.py's setuptools.setup().
        #
        # Use current mupdf, not {mupdf} for pipcl, so we can use latest
        # version.
        sys.path.append( f'mupdf/scripts')
        import pipcl
        del sys.path[-1]

        pymupdf_package = [None]

        def buildfn():
            root = ''
            so = 'fitz/_fitz.so'
            if os.uname()[0] == 'Linux':
                root = glob.glob( 'PyMuPDF/build/lib*/')
                assert len( root) == 1, f'root={root!r}'
                root = root[0]
                jlib.log( '{root=}')
                so = glob.glob( f'{root}fitz/*.so')
                assert len( so) == 1, f'root={root} so={so!r}'
                so = so[0]
                jlib.log( '{so=}')
                so = so[ len(root):]
                jlib.log( '{so=}')
                root = root[ len('PyMuPDF') + 1:]
                jlib.log( '{root=}')
            ret = []
            for l in [
                    f'fitz/utils.py',
                    so,
                    f'fitz/__main__.py',
                    f'fitz/fitz.py',
                    f'fitz/__init__.py'
                    ]:
                ret.append( ( f'{root}{l}', l))
            jlib.log( '{ret=}')
            return ret

        with open( 'PyMuPDF/README.md', encoding="utf-8") as f:
            readme = f.read()
        classifiers = [
            "Development Status :: 5 - Production/Stable",
            "Intended Audience :: Developers",
            "Intended Audience :: Information Technology",
            "Operating System :: MacOS",
            "Operating System :: Microsoft :: Windows",
            "Operating System :: POSIX :: Linux",
            "Programming Language :: C",
            "Programming Language :: Python :: 3 :: Only",
            "Programming Language :: Python :: Implementation :: CPython",
            "Topic :: Utilities",
            "Topic :: Multimedia :: Graphics",
            "Topic :: Software Development :: Libraries",
        ]
        pymupdf_package[0] = pipcl.Package(
                name="PyMuPDF",
                version="1.20.0",
                root='PyMuPDF',
                summary="Python bindings for the PDF toolkit and renderer MuPDF",
                description=readme,
                classifiers=classifiers,
                author="Artifex",
                author_email="support@artifex.com",
                license="GNU AFFERO GPL 3.0",
                url_home="https://github.com/pymupdf/PyMuPDF",
                url_docs="https://pymupdf.readthedocs.io/",
                url_source="https://github.com/pymupdf/pymupdf",
                url_tracker="https://github.com/pymupdf/PyMuPDF/issues",
                url_changelog="https://pymupdf.readthedocs.io/en/latest/changes.html",
                fn_build=buildfn,
                )

        jlib.log( 'Building wheel...')
        wheel_dir = 'pypackage-out'
        wheel_leaf = pymupdf_package[0].build_wheel( wheel_dir)
        wheel = os.path.join( wheel_dir, wheel_leaf)
        jlib.log( 'Have created wheel: {wheel}')
    except Exception as e:
        #jlib.log( '{e!r=}')
        jlib.log( jlib.exception_info(), nv=0)
        return e


def b0( target, operations, j: int=None):
    if target == '.binaries':
        for binary in state.binaries:
            build( target, operations, j)
    else:
        build( target, operations, j)
    


def main():

    parser = jlib.Arg('', help=__doc__,
            subargs=[
                    jlib.Arg('--binary <binary>',
                            help='Adds a binary to test. E.g.: --binary ghostpdl/bin/gs',
                            ),
                    
                    jlib.Arg('-b',
                            help='''
                            Does build(s) inside the ghostpdl/ or mupdf/ directories.

                            <target>:
                                Path of target.

                                For ghostpdl, we find build flags from <directory>, by ignoring any
                                trailing '-bin' or 'bin' and then splitting by '-'. Supported flags
                                are:

                                    debug
                                    luratech
                                    mem
                                    profile
                                    san
                                    ufst

                                'luratech' and 'ufst' use a separate Makefile.

                            Ordering of flags:

                                Reordering flags will result in needless duplicate builds. For example
                                these two targets:
                                        ghostpdl/debug-mem-bin
                                        ghostpdl/mem-debug-bin

                                - will result in duplicated builds of .o files in
                                ghostpdl/debug-mem-obj/ and ghostpdl/mem-debug-obj/.

                            Example values for <binary>:
                                ghostpdl/bmpcmp
                                    Special target that builds the bmpcmp binary.
                                ghostpdl/bin/gs
                                    Normal release build of gs.
                                ghostpdl/vs2005bin/gs
                                ghostpdl/vs2008bin/gs
                                ghostpdl/vs2019bin/gs
                                ghostpdl/vs-bin/gs
                                    Assumes Windows, runs cmd.exe with vcvars etc then nmake.
                                    
                                    2022-03-29: 'vs' is currently same as vs2019.

                                    Puts .obj files into ghostpdl/vs2005obj/, but unfortunately
                                    puts binaries into ghostpdl/bin, so this directory needs to be
                                    manually deleted if one changes between different VS versions.

                                    Note that vs2005-obj fails - the '-' seems to cause problems,
                                    perhaps because obj/_dll.rc does "#define gstext_ico
                                    .\vs2008-obj\gswin.ico".
                                bin/gs
                                    As above if cwd is ghostpdl/ directory.
                                ghostpdl/luratech-bin/gs
                                    Build gs with luratech. Requires ./luratech to be a luratech checkout.
                                .
                                    Shortcut to build ghostpdl/bin/gs.
                                .binaries
                                    Shortcut to build all binaries that have been added with
                                    --binary.
                                .bmpcmp
                                    Shortcut to build ghostpdl/bmpcmp.

                            <operations>:
                                Any of these characters:
                                    c   - configure
                                    m   - make

                            To allow building of Luratech binaries, we create/delete a softlink to
                            ./luratech inside ghostpdl/ (i.e. ghostpdl/luratech -> ../luratech)
                            as required.

                            E.g.:
                                --build . cm
                                --build ghostpdl/bin/gs cm
                                --build ghostpdl/mem-bin/gs cm
                                --build ghostpdl/sodebugbin/gs cm
                                --build .binaries cm
                                -b mupdf/build/debug .
                                -b mupdf/build/sanitize .
                                -b mupdf/build/debug-extract .
                                -b mupdf/build/shared-debug .
                                -b mupdf/platform/win32/Debug/mutool.exe .
                                -b mupdf/platform/win32/Release/mutool.exe .
                            ''',
                            multi=1,
                            subargs=[
                                    jlib.Arg('-j <N>'),
                                    jlib.Arg('<target> <operations>', required=1),
                                    ],
                            ),
                    
                    jlib.Arg('--command <command>',
                            help='''
                            Runs <command> locally.

                            E.g.:
                                --command 'rsync -ai foo.html ghostscript.com:public_html/'
                            ''',
                            ),
                    
                    jlib.Arg('--command-rr-repeat ...',
                            help='Runs <command> under rr repeatedly until it fails.',
                            ),
                    
                    jlib.Arg('--copy-git-clean <doit> <source> <destination>',
                            help='''
                            Makes <destination> be copy of just the git files in <source>.

                            Will remove any file in <destination> which is not in <source>.

                                <doit>:
                                    0 - don't do anything but show what would be done.
                                    1 - copy files as specified.
                                    2 - also rename directories owned by root in <destination>.
                                        The rename appends '._trash', which allows easy cleanup of
                                        these directories from within docker.
                            ''',
                            ),
                    
                    jlib.Arg('--delete <0|1|2>',
                            help='''
                            Controls whether to delete files created by gs.

                            .md5 and .bmpcmp files are never deleted.

                            0   - never delete output files.
                            1   - delete output files if no diff detected.
                            2   - always delete output files.

                            Default is 1.
                            ''',
                            ),

                    jlib.Arg('--dump-performance <min_t> <filename>',
                            help='''
                            Dumps information from performance files matching <min_t> to
                            <filename>.

                            Uses python's pickle module.

                            This data can be loaded using --load-performance. Also see
                            --remote-get-performance.
                            ''',
                            ),
                    
                    jlib.Arg('--help-examples'),

                    jlib.Arg('--inputs <glob>',
                            help='''
                            Adds all matching input filenames. Note that '*' does NOT match '/' (we
                            use python's glob.glob()).

                            We exit with an error if no matches are found for <glob>.
                            ''',
                            ),

                    jlib.Arg('--inputs-exclude <glob>',
                            help='''
                            Exclude any input files that match <glob>.

                            Note that unlike '--inputs <glob>', '*' DOES match '/' (we use python's
                            fnmatch.fnmatch(), which differs in this respect from glob.glob()).

                            E.g.:
                                --inputs-exclude '*/fuzzing/*'
                            ''',
                            ),

                    jlib.Arg('--inputs-exclude-luratech-bad',
                            help='''
                            Exclude hard-coded list of input files that luratech is known to fail
                            on.
                            ''',
                            ),

                    jlib.Arg('--inputs-exclude-no-bmpcmp',
                            help='''
                            Exclude input files for which no bmpcmp output files exist.

                            For example this is useful with --o-html-bmpcmp, to only include
                            information about differences.

                            Also useful for re-running only on input files that previously showed
                            diffs.
                            ''',
                            ),

                    jlib.Arg('--inputs-from-performance <min_t> <expression>',
                            help='''
                            Adds inputs matching <min_t> for which <expression>(tmin_ratio, mem_ratio) is false.

                            E.g.:
                                --inputs-from-performance 12 "tmin_ratio > 1.5 || mem_ratio > 1.8"
                            ''',
                            ),

                    jlib.Arg('--inputs-r <root-glob> <leaf-glob>',
                            help='''
                            Adds all paths within directories matching <root-glob>, where the
                            leafname matches <leaf-glob>.

                            Due to the differences between python's glob.glob() and
                            fnmatch.fnmatch():
                                In <root-glob>, '*' does NOT match '/'.
                                In <leaf-glob>, '*' DOES match '/'.

                            We exit with an error if no matches are found for <root-glob>.

                            If <root-glob> starts with '#', this option is ignored; this is to
                            allow testing on small number of files without large modification to
                            the command line.

                            E.g.:
                                --inputs-r '../tests_*' '*.pdf'
                            ''',
                            ),

                    jlib.Arg('--load-performance <filename>',
                            help='''
                            Loads performance information from file previously created by
                            --dump-performance.
                            ''',
                            ),
                    
                    jlib.Arg('--log-t <on>',
                            help='''
                            Controls whether we prefix output with date/time.
                            <on> should be 0 or 1.
                            ''',
                            multi=1,
                            ),

                    jlib.Arg('--mupdf-mutool-compare [tabfile] <do-sync>',
                            subargs=[jlib.Arg('-s <sync>'), jlib.Arg('-t <tabfile>')],
                            ),

                    jlib.Arg('--mupdf-mutool-find_command <test_id>'),

                    jlib.Arg('--mupdf-mutool-run <do-sync> <build> ...',
                            help='''
                            Runs mutool and mutool.py with same parameters.

                            do_sync:
                                0 or 1.
                            build:
                                Determines what paths are set when running mutool.py, and what
                                mutool binary is used.

                                E.g. 'debug' will use mupdf/build/shared-debug for PYTHONPATH, and
                                mupdf/build/debug/mutool.
                            ''',
                            ),

                    jlib.Arg('--oss-fuzz <project> <id> <update_image> <fuzzer> <testcase> <tree> <sanitizer-name>',
                            help=r'''
                            Attempts to reproduce an oss-fuzz bug by running 'python
                            infra/helper.py reproduce ...'.
                            
                            Notes:
                                All work happens within the oss-fuzz/ directory.
                            
                                Docker commands are run that require that we are in the 'docker' group.
                            
                                Also see: https://google.github.io/oss-fuzz/
                            
                                Use --oss-fuzz-helper to start a shell inside a particular docker instance.

                            Root files and directories created by docker instances:
                            
                                The docker instance mounts <tree> and built files (e.g. .o files) will be
                                created as owned by root, which can then prevent later updates to <tree> from
                                working. To avoid this, one could remove all files owned by root, but this will
                                also remove .o files and so cause builds to run slowly. Or one could remove
                                all directories owned by root, although this is generally a dangerous thing to
                                do.
                                
                                An alternative is to use '--copy-git-clean 2 ...' to create a copy of the tree
                                for use by --oss-fuzz, which will append '_trash' to all directories owned by
                                root; then one can find all the offending directories with:

                                    find ghostpdl-clean/ -name "*_trash*"

                                and after verifying that this looks ok, clean up with:

                                     find ghostpdl-clean/ -name "*_trash*" -print0|xargs -0 sudo rm -r

                            Args:

                                <project>
                                    Name of oss-fuzz project, e.g. ghostscript or mupdf.

                                <id>:
                                    Can be anything; used to identify temp files, e.g. use oss-fuzz
                                    issue number.

                                <update>:
                                    If contains 'i' we run python infra/helper.py pull_images and
                                    build_image. Otherwise we assume a suitably set up docker
                                    instance - see https://google.github.io/oss-fuzz/, e.g. go to
                                    'Advanced topics / Reproducing'.

                                    If contains 'u' we use 'git clone' or 'git pull -r' to
                                    ensure oss-fuzz is present and up to date. Otherwise
                                    we assume that oss-fuzz/ contains a checkout of
                                    https://github.com/google/oss-fuzz.git.

                                <fuzzer>:
                                    Passed to infra/helper.py reproduce as <fuzz_target_name>, e.g.
                                    'gstoraster_fuzzer' or 'pdf_fuzzer'.
                                    
                                    On the oss-fuzz.com/testcase-detail/.. page
                                    this is called 'Fuzz Target'.

                                <testcase>:
                                    If starts with 'http' should be URL from which we will download the
                                    testcase to local file testcase-<id>. Otherwise if longer than one
                                    character it is path of local file containing the testcase. Otherwise
                                    testcase is assumed to be local file testcase-<id>.

                                <tree>
                                    If longer than one character, this is a directory containing
                                    source code for <project>, e.g. 'ghostpdl', which will be
                                    mounted inside the docker instance as <project> and used by
                                    oss-fuzz to run tests.

                                    Otherwise oss-fuzz will default to using a clean checkout
                                    within the docker instance.

                                    Use --copy-git-clean to make a copy of a git checkout so that
                                    any modifications by the docker instance don't affect the git
                                    checkout.

                                <sanitizer-name>:
                                    If longer than one character, we do a clean build of the fuzzer
                                    with --sanitizer <sanitizer-name> (e.g. it could be 'address',
                                    'memory' or 'undefined').
                                    
                                    On the oss-fuzz.com/testcase-detail/.. page
                                    this is called 'Sanitizer'.

                            Example:

                                ./julian-tools/jtest.py \
                                        --remote julian@peeved.ghostscript.com \
                                        --remote-sync-dir oss-fuzz \
                                        --remote-sync-dir ghostpdl \
                                        --remote-do \
                                        --copy-git-clean 2 ghostpdl ghostpdl-clean \
                                        --oss-fuzz \
                                                ghostscript \
                                                15650 \
                                                i \
                                                gstoraster_fuzzer \
                                                https://oss-fuzz.com/download/753f5800-b481-43e0-ae3d-87051c813cb2?testcase_id=5674773134180352 \
                                                ghostpdl-clean \
                                                memory \
                                        2>&1|tee out-peeved-oss-fuzz

                                ./julian-tools/jtest.py \
                                        --remote julian@peeved.ghostscript.com \
                                        --remote-sync-dir oss-fuzz \
                                        --remote-sync-dir ghostpdl \
                                        --remote-do \
                                        --copy-git-clean 2 ghostpdl ghostpdl-clean \
                                        --oss-fuzz \
                                                ghostscript \
                                                16378 \
                                                . \
                                                gstoraster_fuzzer \
                                                https://oss-fuzz.com/download?testcase_id=5697138125701120 \
                                                ghostpdl-clean \
                                                memory \
                                        2>&1|tee out-peeved-oss-fuzz

                                ./julian-tools/jtest.py \
                                        --remote julian@peeved.ghostscript.com \
                                        --remote-sync-dir oss-fuzz \
                                        --remote-sync-dir ghostpdl \
                                        --remote-do \
                                        --copy-git-clean 2 ghostpdl ghostpdl-clean \
                                        --oss-fuzz \
                                                ghostscript \
                                                17403 \
                                                . \
                                                gstoraster_fuzzer \
                                                https://oss-fuzz.com/download?testcase_id=5701638165102592 \
                                                ghostpdl-clean \
                                                memory \
                                        2>&1|tee out-peeved-oss-fuzz

                                ./julian-tools/jtest.py \
                                        --remote julian@peeved.ghostscript.com \
                                        --remote-sync-dir oss-fuzz \
                                        --remote-sync-dir ghostpdl \
                                        --remote-do \
                                        --copy-git-clean 2 ghostpdl ghostpdl-clean \
                                        --oss-fuzz \
                                                ghostscript \
                                                15591 \
                                                . \
                                                gstoraster_fuzzer \
                                                https://oss-fuzz.com/download?testcase_id=5701638165102592 \
                                                ghostpdl-clean \
                                                coverage \
                                        2>&1|tee out-peeved-oss-fuzz

                                ./julian-tools/jtest.py \
                                        --remote julian@peeved.ghostscript.com \
                                        --remote-sync-dir oss-fuzz \
                                        --remote-sync-dir mupdf \
                                        --remote-do \
                                        --copy-git-clean 2 mupdf mupdf-clean \
                                        --oss-fuzz \
                                                mupdf \
                                                36463 \
                                                i \
                                                pdf_fuzzer \
                                                https://oss-fuzz.com/download?testcase_id=5993057836204032 \
                                                mupdf-clean \
                                                address \
                                        2>&1|tee out-peeved-oss-fuzz
                            ''',
                            ),

                    jlib.Arg('--oss-fuzz-helper <args>',
                            help='''
                            Runs oss-fuzz/infra/helper.py with specified args.

                            Examples:
                                --oss-fuzz-helper 'shell ghostscript'
                                    Starts a shell inside ghostscript oss-fuzz docker container.
                                --oss-fuzz-helper 'shell mupdf'
                                    Starts a shell inside mupdf oss-fuzz docker container.
                            '''
                            ),

                    jlib.Arg('--o-html-bmpcmp <base>',
                            help='''
                            For each input file, writes html containing information about bmpcmp
                            files to <base>-diffs.html.

                            --inputs-exclude-no-bmpcmp can be used to filter output input files for
                            which no bmpcmp files exist.

                            The html will contain links to .bmpcmp output files, so if <html>
                            is copied to a remote machine, one will typically want to use
                            --rsync-bmpcmp.

                            E.g.:
                                --o-html-bmpcmp luratech
                            ''',
                            ),

                    jlib.Arg('--o-html-performance-speed <base>',
                            help='''
                            Generates <base>-performance-speed.html.
                            ''',
                            ),

                    jlib.Arg('--o-html-performance-memory <base>',
                            help='''
                            Generates <base>-performance-memory.html.
                            ''',
                            ),

                    jlib.Arg('--o-graph-performance',
                            subargs=[
                                jlib.Arg('-b <backend>', help='bokeh | pygal | matplotlib'),
                                jlib.Arg('<outfilename>', required=1),
                                ],
                            help='''
                            Generates graph from data in all performance data that has been loaded
                            with --load-performance.

                            If more than one set of data is loaded, we display only inputs files
                            that are in all, and draw arrows between dots to show changes in
                            performance between the data sets. [This only works if using bokeh
                            backend.]

                            If optional parameter is not specified we choose a backend suitable for
                            the suffix of <outfilename>. [default for .html is bokeh.]

                            If <outfilename> doesn't have a '.', we use
                            <outfilename>-performance.html similarly to --o-html-performance-*.
                            ''',
                            ),

                    jlib.Arg('--o-text-performance <min_t> <outfilename>',
                            help='''
                            Outputs performance info from all <min_t> performance files to
                            <outfilename>.

                            If <outfilename> is '-', writes to stdout.
                            ''',
                            ),

                    jlib.Arg('--o-tsv-performance <min_t> <outfile>',
                            help='''
                            Write performance info to specified file in tab-separated format.

                            The generated file can be copied to website with --rsync-html as with
                            .html files.
                            ''',
                            ),

                    jlib.Arg('--params <params>',
                            help='''
                            Parameters to pass to binaries registered with --binary or --binaries.

                            <params>:
                                A space-separated parameters (e.g. in quotes); <params> should not
                                include -sOutputFile. If --inputs has been specified, <params> does
                                not need to contain an input filename.

                            E.g.:
                                 --params '-dBATCH -dNOPAUSE -sDEVICE=pbmraw'
                            ''',
                            ),

                    jlib.Arg('-r <remote> <command>',
                            help='''
                            Convenience deferring to remote host.
                            
                            remote:
                                As --remote:
                                
                                <ssh-params>[:<port>][:<remote-dir>][,<sync-directories>]
                                
                                If no ':', we default to <remote-dir>='artifex-remote'.
                            command:
                                The command to run on remote.
                            
                            E.g.:
                                -r jules-devuan,ghostpdl,ghostpdl/extract './jtest.py -b ghostpdl/mem-extract-bin/gpcl6 m'
                            ''',
                            ),
                            
                    jlib.Arg('--remote <remote>',
                            help='''
                            <ssh-params>[:<port>][:<remote-dir>][,<sync-directories>]

                            Specifies how to connect to remote host and remote directory.

                            E.g.:
                                --remote julian@peeved.ghostscript.com
                                --remote '-J julian@pi.ghostscript.com julian@peeved:artifex2,mupdf/.git,ghostpdl'
                                --remote julian@rods.ghostscript.com:2222

                            If <sync-directories> is specified it should be comma-separated list of
                            directories to sync, similar to --remote-sync-dirs.
                            
                            If a <sync-directory> ends with '++', we assume
                            it is a mupdf checkout and additionally sync
                            platform/c++/. We also do this if <remote> contains
                            'windows'.
                            ''',
                            ),

                    jlib.Arg('--remote-command <command>',
                            help='''
                            ssh's to remote host specified by earlier --remote, and runs specified
                            command (which should be a single arg, e.g. in quotes).
                            ''',
                            ),

                    jlib.Arg('--remote-do ...',
                            help='''
                            ssh's to remote host specified by earlier --remote, and runs this
                            script, passing all the remaining parameters.
                            ''',
                            ),

                    jlib.Arg('--remote-get-performance <min_t> <filename>',
                            help='''
                            Uses --dump-performance on remote to collect information from
                            performance files matching <min_t>, then rsync's to local machine.

                            E.g.:
                                --remote-get-performance 4 foo.pickle --load-performance foo.pickle
                            ''',
                            ),

                    jlib.Arg('--remote-get-testfile <path-tail>',
                            help='''
                            Looks on remote host specified by --remote for file
                            matching <path-tail> and copies into local tree
                            ghostpdl/../tests_private/...<path-tail>.
                            ''',
                            ),

                    jlib.Arg('--remote-sync',
                            help='''
                            Uses rsync to copy/update ./ghostpdl and ./luratech trees to the remote
                            host specified by earlier --remote. Usually specified after --remote
                            and before --remote-do.
                            ''',
                            ),

                    jlib.Arg('--remote-sync-mupdf-c++ <mupdf>',
                            help='''
                            Syncs <mupdf>/platform/c++/ to remote; useful on
                            Windows when clang-python is not available.
                            ''',
                            ),

                    jlib.Arg('--remote-sync-dir <directory>', multi=1,
                            help='''
                            ''',
                            ),
                    jlib.Arg('--remote-sync-dirs <directories>',
                            help='''
                            <directories> is space or comma-separated list.

                            Uses rsync to copy git files in <directory> or <directories> to remote
                            host specified by earlier --remote. Usually specified after --remote
                            and before --remote-do.

                            If a directory ends with '/.git' we also copy across the .git/
                            directory itself.
                            ''',
                            ),

                    jlib.Arg('--remote-sync-file <path_from> <path_to>',
                            help='''
                            Uses rsync to copy <path_from> to <path_to> on remote host specified by
                            earlier --remote.

                            If <path_to> is '.' we do path_to = path_from.
                            ''',
                            ),

                    jlib.Arg('--remote-sync-files <paths_from>',
                            help='''
                            Uses rsync to copy <paths_from> to remote host
                            specified by earlier --remote.
                            ''',
                            ),

                    jlib.Arg('--rsync-bmpcmp <destination>',
                            help='''
                            Uses rsync to copy all input files' .bmpcmp.* files to <destination>.

                            E.g.:
                                --rsync-bmpcmp ghostscript.com:public_html/
                            ''',
                            ),

                    jlib.Arg('--rsync-html <destination>',
                            help='''
                            Copies files created by earlier --o-* options to <destination>.

                            E.g.:
                                --rsync-html ghostscript.com:public_html/
                            ''',
                            ),

                    jlib.Arg('--run-compare',
                            help='''
                            Runs all binaries on all input files with other params as specified by
                            --params, diffs output files and creates/updates output files.

                            Output files are all created in the current directory, and their
                            filenames encode the name of the input file, the binary used, and
                            various other parameters.

                            For example, if the input file is
                            /home2/regression/cluster/tests_private/pdf/forms/v1.6/8111.pdf, output
                            files could be:
                                jtest-out-^home2^regression^cluster^tests_private^pdf^forms^v1.6^8111.pdf.ppmraw.ghostpdl^bin^gs.md5
                                jtest-out-^home2^regression^cluster^tests_private^pdf^forms^v1.6^8111.pdf.ppmraw.ghostpdl^luratechbin^gs.bmpcmp
                                jtest-out-^home2^regression^cluster^tests_private^pdf^forms^v1.6^8111.pdf.ppmraw.ghostpdl^luratechbin^gs.md5
                            ''',
                            ),

                    jlib.Arg('--run-compare-bmpcmp <bmpcmp-options>',
                            help='''
                            Sets whether later --run-compare runs bmpcmp on images if differences
                            are found. Default is 0.

                            If not 0, then if output files differ, creates an empty *.bmpcmp file
                            and .bmpcmp.*.{meta,png} files from running bmpcmp.
                            ''',
                            ),

                    jlib.Arg('--run-compare-repeat <N>',
                            help='''
                            Makes later '--run-compare' run <N> times, terminating early if any
                            diff is found.
                            ''',
                            ),

                    jlib.Arg('--run-performance <min_t>',
                            help='''
                            Runs binaries on all input files repeatedly and writes per-binary
                            performance statistics, to <output>.performance.<min_t>.

                            min_t:
                                This is an identifier that is used to differentiate between
                                different runs, and is included in generated filenames. e.g. it is
                                specified with the --o-html-performance option.

                            Creates files like:
                                jtest-out-^home2^regression^cluster^tests_private^pdf^forms^v1.5^sfkr_04.pdf.ppmraw.ghostpdl^bin^gs.performance.11
                                jtest-out-^home2^regression^cluster^tests_private^pdf^forms^v1.5^sfkr_04.pdf.ppmraw.ghostpdl^luratechbin^gs.performance.11

                            - these will each contain one line per run, each line containing
                            a full set of rusage values from /usr/bin/time. This allows later
                            calculation of the minimum/average execution times / memory usages etc
                            by --o-html-performance.

                            Also see --o-performance-html, which outputs html representation of
                            performance data.

                            We interleave runs of the different binaries in an attempt to
                            compensate for varying performance on the underlying machine (e.g. CPU
                            frequency changes).

                            Todo:
                                Pass input file multiple times on command line if command runs
                                quickly, so we reduce the time overhead of starting up gs.

                            E.g.:
                                --run-performance 11
                            ''',
                            ),

                    jlib.Arg('--tskip 0|1',
                            help='''
                            If 1, we don't run commands where output file is newer than binary and
                            input file.

                            Default is 0 - always run commands regardless of whether output files
                            already exist.
                            ''',
                            ),
                    
                    jlib.Arg('--xml-pretty <directory>',
                            help='For each %.xml file in <directory>, creates %.xml.pretty.xml',
                            ),
                    ],
    )
                            
    args = parser.parse(sys.argv[1:])
    #jlib.log('{args._list=}')

    for name, value, arg in args:

        name_orig = name

        try:
            if arg.binary:
                state.binaries.append( arg.binary)
            
            elif arg.b:
                target = arg.b.target.target
                operations = arg.b.target.operations
                j = arg.b.j.N if arg.b.j else None
                if target == '.binaries':
                    for binary in state.binaries:
                        build( target, operations, j)
                else:
                    build( target, operations, j)
            
            elif arg.command:
                jlib.log('{arg.command.command=}')
                jlib.system( arg.command.command, verbose=1)
            
            #elif arg.command_repeat_gdb':
            #    command = "gdb -ex 'handle SIGPIPE noprint nostop' -ex 'set print thread-events off' -ex 'set print pretty on' -ex run --args "
            #    while 1:
            #        try: arg = arg = next( args)
            #        except StopIteration:   break
            #        arg = arg.replace( '"', '\\"')
            #        arg = arg.replace( "'", "\\'")
            #        command += ' "%s"' % arg
            #    it = 0
            #    while 1:
            #        it += 1
            #        jlib.log( '{it}')
            
            
            elif arg.command_rr_repeat:
                
                command = 'rr record'
                for a in arg.command_repeat_rr:
                    a = a.replace( '"', '\\"')
                    a = a.replace( "'", "\\'")
                    command += ' "%s"' % a
                it = 0
                while 1:
                    it += 1
                    jlib.log( '{it}')
                    e, text = jlib.system( command, raise_errors=False, out='return')
                    jlib.log( '{e}')
                    if e:
                        print( text)
                        jlib.log( 'command has failed. exiting.')
                        break
                    else:
                        assert 'abort' not in text
            
            elif arg.copy_git_clean:
                jlib.log('calling copy_git_clean() {arg.copy_git_clean=}')
                copy_git_clean( arg.copy_git_clean.source, arg.copy_git_clean.destination, int(arg.copy_git_clean.doit))
            
            elif arg.delete:
                state.delete = int( arg.delete)
            
            elif arg == '--device':
                state.device = next( args)
            
            elif arg == '--dump-performance':
                min_t = int( next( args))
                outfilename = next( args)
                input_to_stats = make_input_to_stats( min_t)
                with open( outfilename, 'wb') as f:
                    pickle.dump( (min_t, input_to_stats), f)
            
            elif arg.help_examples:
                print( help_examples)

            elif arg.inputs: 
                if arg.inputs.startswith('#'):
                    print( '%s: ignoring: %r' % (arg, arg.inputs))
                else:
                    items = glob.glob( arg.inputs)
                    if not items:
                        raise Exception( 'No matches for glob: %r' % arg.inputs)
                    for i in items:
                        print( '%s: adding input: %s' % (arg, i))
                        state.inputs.append( i)
                    print( '%s: number of input files added: %s' % (arg, len(items)))
            
            elif arg.inputs_exclude:
                num_excludes = 0
                i = 0
                while i < len( state.inputs):
                    j = state.inputs[ i]
                    if fnmatch.fnmatch( state.inputs[ i], arg.inputs_exclude):
                        print( '%s: excluding: %s' % (arg, state.inputs[ i]))
                        del state.inputs[ i]
                        num_excludes += 1
                    else:
                        i += 1
                print( '%s: number of exclusions: %s' % (arg, num_excludes))
            
            elif arg.inputs_exclude_luratech_bad:
                for i in (
                        # These miss images
                        '/home2/regression/cluster/tests_private/pdf/sumatra/jbig2dec_memory_leak.pdf',
                        '/home2/regression/cluster/tests/pdf/Jbig2_042_22.pdf',
                        '/home2/regression/cluster/tests/pdf/Jbig2_042_21.pdf',
                        '/home2/regression/cluster/tests/pdf/Jbig2_042_24.pdf',
                        '/home2/regression/cluster/tests/pdf/Jbig2_042_23.pdf',
                        
                        # luratech segv
                        '/home2/regression/cluster/tests_private/pdf/customer394/problems/normal_871.pdf',
                        '/home2/regression/cluster/tests_private/pdf/customer394/problems/normal_1352.pdf',
                        ):
                    try:
                        state.inputs.remove(i)
                    except ValueError:
                        pass
                    else:
                        print( 'Have excluded: %s' % i)
            
            elif arg.inputs_exclude_no_bmpcmp:
                assert state.device
                inputs_new = []
                for input_ in state.inputs:
                    gg = bmpcmp_outfiles( state, input_)
                    if gg:
                        print( '%s: preserving (%4s): %s' % (arg, len(gg), input_))
                        #print '    glob is: %s' % g
                        inputs_new.append( input_)
                print( '%s: num items changed from %s to %s' % (arg, len( state.inputs), len(inputs_new)))
                state.inputs = inputs_new
            
            elif arg.inputs_from_performance:
                expression_text = next( args)
                expression = compile( arg.inputs_from_performance.expression, '', 'eval', dont_inherit=True)
                input_to_stats = make_input_to_stats( arg.inputs_from_performance.min_t)
                for input_, stats2 in input_to_stats.items():
                    tmin_ratio, mem_ratio = ratio( stats2)
                    r = eval(
                            expression,
                            dict(),
                            dict(tmin_ratio=tmin_ratio, mem_ratio=mem_ratio),
                            )
                    if r:
                        print( 'tmin_ratio=%s mem_ratio=%s: adding: %s' % (
                                tmin_ratio,
                                mem_ratio,
                                input_,
                                ))
                        state.inputs.append( input_)

            elif arg.inputs_r:
                if arg.inputs_r.root.startswith('#'):
                    print( '%s: ignoring %r %r' % (arg, arg.inputs_r.root, arg.inputs_r.g))
                else:
                    roots = glob.glob( arg.inputs_r.root)
                    if not roots:
                        raise Exception( 'no matches for %r' % root)
                    items = []
                    for root2 in roots:
                        for dirpath, dirnames, filenames in os.walk( root2):
                            for filename in filenames:
                                if fnmatch.fnmatch( filename, arg.inputs_r.g):
                                    path = os.path.join( dirpath, filename)
                                    print( '%s adding input: %s' % (arg, path))
                                    items.append( path)
                    if not items:
                        #raise Exception( 'No matches for %s:%r' % (root, arg.inputs_r.g))
                        print( '%s: warning: no matches for %s:%r' % (arg, arg.inputs_r.root, arg.inputs_r.g))
                    state.inputs += items
                    print( '%s: number of input files added: %s' % (arg, len(items)))
            
            elif arg.load_performance:
                with open( arg.load_performance.infilename, 'rb') as f:
                    min_t, input_to_stats = pickle.load(f)
                    make_input_to_stats_cache[ min_t] = input_to_stats
            
            elif arg.log_t:
                on = int( arg.log_t.on)
                jlib.log( '{=arg.log_t arg.log_t.on}')
                current = False
                for i, item in enumerate( jlib.g_log_prefixes):
                    if isinstance( item, jlib.LogPrefixTime):
                        current = True
                        break
                if on and not current:
                    jlib.log( 'inserting jlib.LogPrefixTime')
                    jlib.g_log_prefixes.insert( 0, jlib.LogPrefixTime( time_=False, elapsed=True))
                elif on and current:
                    jlib.log( 'removing jlib.LogPrefixTime')
                    del jlib.g_log_prefixes[i]
                jlib.log( '{jlib.g_log_prefixes=}')
                    
            
            elif arg.mupdf_mutool_compare:
                do_sync = int(arg.mupdf_mutool_compare.s)
                if do_sync is not None:
                    do_sync = int(do_sync)
                mupdf_mutool_compare( do_sync, tabfile2=arg.mupdf_mutool_compare.t)
            
            elif arg.mupdf_mutool_find_command:
                command = mupdf_mutool_test_command( arg.mupdf_mutool_find_command)
                jlib.log( '{command=}')
            
            elif arg.mupdf_mutool_run:
                do_sync = int(arg.mupdf_mutool_run.do_sync)
                build = arg.mupdf_mutool_run.build
                mutool_args = arg.mupdf_mutool_run.remaining_
                mutool_args_last = mutool_args[-1]
                if mutool_args_last.startswith( './'):
                    mutool_args_last = mutool_args_last[2:]
                
                def make_args( out):
                    '''
                    Returns args with -o <...> arg changed to -o <out>.
                    '''
                    jlib.remove( out)
                    ret = ''
                    i = 0
                    while 1:
                        if i >= len(mutool_args):
                            break
                        ret += ' ' + mutool_args[i]
                        if mutool_args[i] == '-o':
                            ret += ' ' + out
                            i += 1
                        i += 1
                    return ret.strip()
                
                if do_sync:
                    jlib.log( 'running rsync to get %s' % mutool_args_last)
                    jlib.system(
                            'rsync -aPRz julian@peeved.ghostscript.com:/home2/regression/cluster/./%s ./' % mutool_args_last,
                            verbose=True,
                            )
                
                jlib.log( '')
                jlib.log( 'running mutool...')
                command = './mupdf/build/%s/mutool %s' % (build, make_args( 'out-mutool'))
                jlib.system( command, raise_errors=False, verbose=1, prefix='    ')
                
                jlib.log( '')
                jlib.log( 'running mutool.py...')
                command = './mupdf/scripts/mupdfwrap.py -d mupdf/build/shared-%s --run-py mupdf/scripts/mutool.py %s' % (
                        build,
                        make_args( 'out-mutool-py'),
                        )
                e = jlib.system( command, raise_errors=False, verbose=1, prefix='    ')
                if e == 0:
                    jlib.log( '')
                    jlib.log( 'diff output files:')
                    e = jlib.system( 'diff -q out-mutool out-mutool-py', verbose=1, prefix='    ', encoding='latin_1', errors='replace')
            
            elif arg.oss_fuzz:
                jlib.log('{arg.oss_fuzz=}')
                project = arg.oss_fuzz.project
                n = arg.oss_fuzz.id
                update = arg.oss_fuzz.update_image
                fuzzer = arg.oss_fuzz.fuzzer
                testcase = arg.oss_fuzz.testcase
                tree = arg.oss_fuzz.tree
                sanitizer = arg.oss_fuzz.sanitizer_name
                merge_dir = None
                
                jlib.log('{arg.oss_fuzz=}')
                prefix0 = 'oss-fuzz %s %s: ' % (project, n)
                def system( command, prefix=''):
                    return jlib.system(
                            'cd oss-fuzz && %s' % command,
                            prefix=prefix0 + prefix,
                            verbose=1,
                            out='log',
                            bufsize=0,
                            caller=2,
                            )

                if 'u' in update:
                    if os.path.exists('oss-fuzz/.git'):
                        system('cd oss-fuzz && git pull -r')
                    else:
                        system('git clone https://github.com/google/oss-fuzz.git')
                
                if 'i' in update:
                    # Use -u to force unbuffered output otherwise prompts for
                    # input() don't appear.
                    system( 'python -u infra/helper.py pull_images', prefix='pull_images: ')
                    system( f'python -u infra/helper.py build_image --pull {project}', prefix='build_image: ')
                    
                testcase_local = 'testcase-%s' % n
                if len(testcase) > 1:
                    if testcase.startswith('http'):
                        system( 'wget --inet4-only "%s" -O %s' % (testcase, testcase_local), prefix='wget testcase: ')
                    else:
                        testcase_local = testcase
                
                if len(tree) > 1:
                    project_dir = os.path.abspath( tree)
                else:
                    project_dir = ''
                
                if len(sanitizer) > 1:
                    if project == 'mupdf':
                        # Need to touch .h files otherwise they can be rebuilt
                        # which breaks inside docker because it doesn't have
                        # python.
                        system( f'touch {project_dir}/source/pdf/cmaps/*.h && (cd {project_dir}; make nuke)', prefix='mupdf nuke: ')
                    # Use '-u' to make python stdout unbuffered, otherwise
                    # output is confusing.
                    #
                    system( 'python -u infra/helper.py build_fuzzers --sanitizer %s %s %s' % (
                                sanitizer,
                                project,
                                project_dir,
                                ),
                            prefix='build_fuzzers: ',
                            )
                
                if 1:
                    system( 'python -u infra/helper.py reproduce %s %s %s' % (
                                    project,
                                    fuzzer,
                                    testcase_local,
                                    ),
                            prefix='reproduce: ',
                            )
                
            elif arg.oss_fuzz_helper:
                jlib.system( f'python -u oss-fuzz/infra/helper.py {arg.oss_fuzz_helper.args}',
                        verbose=1,
                        bufsize=0,
                        out=None,   # Write directly to stdout; avoids seemingly unavoidable buffering.
                        )
                
            elif args.o_html_bmpcmp:
                out = '%s-diffs.html' % args.o_html_bmpcmp
                with open( out, 'w') as f:
                    def p( text):
                        print( text, file=f)
                    p( '<html><body>')
                    p( '<h2><a name="index">Index</a></h2>')
                    p( '<ul>')
                    for i, input_ in enumerate(state.inputs):
                        p( '<li>%s/%s: <a name="index-%s" href="#%s">%s</a>' % (
                                i+1,
                                len(state.inputs),
                                input_,
                                input_,
                                input_,
                                ))
                    p( '</ul>')
                    
                    for i, input_ in enumerate(state.inputs):
                        p( '<hr>')
                        p( '<h2>%s/%s: <a name="%s" href="#index-%s">%s</a></h2>' % (
                                i+1,
                                len(state.inputs),
                                input_,
                                input_,
                                input_,
                                ))
                        in_table = 0
                        for g in sorted( bmpcmp_outfiles( state, input_)):
                            if g.endswith( '.meta'):
                                if in_table:
                                    p( '</table>')
                                in_table = 1
                                p( '<p>%s' % g)
                                with open( g) as ff:
                                    text = ff.read()
                                p( '<pre>%s</pre>' % text)
                                p( '<table border="1"><tr>')
                            if g.endswith( '.png'):
                                p( '<td><img src="%s">' % g)
                        if in_table:
                            p( '</table>')
                    p( '</body></html>')
                
                state.html_generated.append( out)
            
            elif arg.o_html_performance_speed:
                if len( make_input_to_stats_cache) != 1:
                    raise Exception( 'need exactly one set of performance data to be loaded')
                for k in make_input_to_stats_cache.keys():
                    min_t = k
                name = performance_html( arg.o_html_performance_speed.min_t, arg.o_html_performance_speed.base, 'speed')
                state.html_generated.append( name)

            elif arg.o_html_performance_memory:
                if len( make_input_to_stats_cache) != 1:
                    raise Exception( 'need exactly one set of performance data to be loaded')
                for k in make_input_to_stats_cache.keys():
                    min_t = k
                base = arg.o_html_performance_memory.base
                name = performance_html( min_t, base, 'memory')
                state.html_generated.append( name)
            
            elif arg.o_graph_performance:
                backend = arg.o_graph_performance.backend
                if backend:
                    if backend not in ('bokeh', 'pygal', 'matplotlib'):
                        raise Exception( 'unrecognised backend: %r' % backend)
                outfilename = arg.o_graph_performance.outfilename
                if '.' not in outfilename:
                    outfilename = '%s-performance-graph.html' % outfilename
                
                if backend is None:
                    if outfilename.endswith( '.png'):
                        backend = 'pygal'
                    elif outfilename.endswith( '.svg'):
                        backend = 'pygal'
                    elif outfilename.endswith( '.html'):
                        backend = 'bokeh'
                    else:
                        raise Exception('Backend not specified but suffix not recognised')
                assert backend
                
                if 0:
                    pass
                elif backend == 'ggplot':
                    import ggplot
                    input_to_stats = make_input_to_stats( min_t)
                    xys = []
                    for input_, stats in input_to_stats.items():
                        xys.append( (tmin_ratio, mem_ratio))
                    data = pandas.DataFrame.from_dict( dict( xys))
                    p = ggplot(aes( x='time', y='memory'), data)
                    
                elif backend == 'matplotlib':
                    import matplotlib.pyplot
                    input_to_stats = make_input_to_stats( min_t)
                    figure, axes = matplotlib.pyplot.subplots()
                    axes.set_xlabel( 'time (normal/luratech)')
                    axes.set_ylabel( 'memory (normal/luratech)')
                    axes.loglog()
                    xys = []
                    xs = []
                    ys = []
                    for input_, stats in input_to_stats.items():
                        tmin_ratio, mem_ratio = ratio( stats)
                        xs.append( tmin_ratio)
                        ys.append( mem_ratio)
                        xys.append( (tmin_ratio, mem_ratio, input_))
                    axes.scatter( xs, ys, s=1)
                    if outfilename.endswith( '.html'):
                        # This doesn't work too well - e.g. mpld3 appears to
                        # get confused by the loglog, and uses base 3 and
                        # reverses the y axis.
                        import mpld3
                        with open( outfilename, 'w') as f:
                            mpld3.save_html( figure, f)
                        #mpld3.show( figure)
                        matplotlib.pyplot.show( figure)
                    else:
                        figure.savefig( outfilename)
                elif backend == 'pygal':
                    import pygal
                    chart = pygal.XY(
                            stroke=False,
                            show_legend=False,
                            #human_readable=True,
                            x_title='time (normal/luratech)',
                            y_title='memory (normal/luratech)',
                            logarithmic=True,
                            )
                    chart.title = 'Speed/memory ratio'
                    # Add each point individually so that tooltip over each dot
                    # shows the input .pdf file.
                    for input_, stats in input_to_stats.items():
                        tmin_ratio, mem_ratio = ratio( stats)
                        chart.add( input_, [(tmin_ratio, mem_ratio)])
                    chart.render_to_file( outfilename)
                elif backend == 'bokeh':
                    import bokeh.plotting
                    
                    if len( make_input_to_stats_cache) == 0:
                        raise Exception( 'no performance data loaded, e.g. use --load-performance.')
                    else:
                        min_t_to_input_stats = dict()
                        
                        # Find inputs common to all min_t values.
                        inputs = None
                        for min_t, input_to_stats in make_input_to_stats_cache.items():
                            keys = input_to_stats.keys()
                            if inputs is None:
                                inputs = set( keys)
                            else:
                                inputs = inputs.intersection( keys)
                        
                        # Prepare simple arrays for passing to
                        # bokeh.plotting.ColumnDataSource.
                        xs = []
                        ys = []
                        inputs2 = []
                        
                        tooltips = [
                                ('input', '@input'),
                                ('time', '$x'),
                                ('memory', '$y'),
                                ]
                        
                        figure = bokeh.plotting.figure(
                                title='Time/memory ratio',
                                x_axis_label='time (normal/luratech)',
                                y_axis_label='memory (normal/luratech)',
                                x_axis_type='log',
                                y_axis_type='log',
                                tooltips=tooltips,
                                )
                        
                        for input_i, input_ in enumerate( inputs):
                            x = None
                            y = None
                            i = 0
                            for min_t in sorted( make_input_to_stats_cache.keys()):
                                input2stats = make_input_to_stats_cache[ min_t]
                                stats = input2stats[ input_]
                                tmin_ratio, mem_ratio = ratio( stats)
                                
                                if x is not None:
                                    if (x-tmin_ratio)**2 + (y-mem_ratio)**2 > 0.001:
                                        # Draw arrow.
                                        arrow = bokeh.models.Arrow(
                                                x_start=x,
                                                y_start=y,
                                                x_end=tmin_ratio,
                                                y_end=mem_ratio,
                                                end=bokeh.models.VeeHead(size=10)
                                                )
                                        figure.add_layout( arrow)
                                
                                x = tmin_ratio
                                y = mem_ratio
                                xs.append( x)
                                ys.append( y)
                                inputs2.append( input_)
                        
                        # Draw dots.
                        data = dict( time=xs, memory=ys, input=inputs2,)
                        source = bokeh.plotting.ColumnDataSource( data=data)
                        figure.scatter( 'time', 'memory', source=source, size=5)
                        
                        bokeh.io.save(
                                figure,
                                outfilename,
                                title='Time/memory ratio',
                                resources=bokeh.resources.Resources(),
                                )
                    
                else:
                    raise Exception( 'unrecognised backend: %r' % backend)
                
                print( 'have generated: %s' % outfilename)
                state.html_generated.append( outfilename)

            elif arg.o_text_performance:
                min_t = int( arg.o_text_performance.min_t)
                performance_text( min_t, arg.o_text_performance.outfilename)
            
            elif arg.o_tsv_performance:
                min_t = int( arg.o_tsv_performance.min_t)
                performance_tsv( min_t, arg.o_tsv_performance.outfile)
                state.html_generated.append( outfile)

            elif arg.params:
                state.params =arg.params
            
            elif arg.r:
                jump, user_host, port, sync_directories = read_remote(arg.r.remote, state)
                #command = shlex.join( arg.r.remaining_)
                command = arg.r.command
                jlib.log( '{command!r=}')
                if command:
                    rcommand( state, command)

            elif arg.remote:
                jump, user_host, port, sync_directories = read_remote(arg.remote.remote, state)

            elif arg.remote_command:
                # Using 'ssh -t' makes windows output lots of ctrl codes.
                command = f'{state.remote.ssh} -Y -tt \''
                if state.directory:
                    command += f' cd {state.directory} && '
                #command += f' {args_to_command(args)}'
                #command += next(args)
                c = arg.remote_command.command
                c = c.replace( "'", "\\'")
                command += c
                command += '\''
                jlib.system( command, 
                    prefix='%s: ' % state.remote_description,
                    verbose=1,
                    )
                break
                
            elif arg.remote_do is not None:
                sync_this(remote, state)
                command = '%s -t' % state.remote.ssh
                command += " '"
                if state.directory:
                    command += ' cd %s &&' % state.directory
                command += ' ./%s' % os.path.basename(sys.argv[0])
                for a in arg.remote_do:
                    command += f' {a}'
                command += "'"
                jlib.system( command, 
                    prefix='%s: ' % state.remote_description,
                    verbose=1,
                    bufsize=0,
                    out='log',
                    )
                break
            
            elif arg.remote_get_performance:
                min_t = int( arg.remote_get_performance.min_t)
                outfilename = arg.remote_get_performance.filename
                command = 'rsync --rsh "%s" -aiL %s %s:' % (state.remote.ssh, sys.argv[0], state.directory)
                command += ' && %s -C -t ./%s --dump-performance %s %s' % (
                        state.remote.ssh,
                        os.path.basename( sys.argv[0]),
                        min_t,
                        outfilename,
                        )
                command += ' && rsync --rsh "%s" -aPz %s:%s .' % (
                        state.remote.ssh,
                        state.directory,
                        outfilename,
                        )
                jlib.system( command, prefix='%s: ' % state.directory)
            
            elif arg.remote_get_testfile:
                tail = arg.remote_get_testfile.path_tail
                command = '%s locate %s' % (state.remote.ssh, tail)
                text = jlib.system( command, verbose=sys.stdout, out='return')
                # /home/regression/cluster/tests_private.git
                # /home/regression/tests
                for line in text.split( '\n'):
                    #jlib.log( 'looking at {line=}')
                    if not line.endswith( tail):
                        jlib.log( 'does not end with {tail=}')
                        continue
                    found = False
                    for infix, local in (
                            ('/tests_private.git/', 'tests_private'),
                            ('/tests/', 'tests'),
                            ):
                        p = line.find( infix)
                        if p >= 0:
                            p += len(infix)
                            found = True
                            remote_root = line[ :p]
                            remote_tail = line[ p:]
                            outdir = os.path.abspath( '%s/../%s/' % (get_ghostpdl_dir(), local))
                            break
                    if found:
                        break
                else:
                    raise Exception( 'cannot find on remote: %s' % tail)
                command = 'rsync --rsh "%s" -aPR :%s./%s %s/' % (
                        state.remote.ssh,
                        remote_root,
                        remote_tail,
                        outdir,
                        )
                jlib.system( command, verbose=1)
                print(f'Local file is: {outdir}/{remote_tail}')
                    
            elif arg.remote_sync:
                sync( state.remote.ssh, state.directory)
            
            elif arg.remote_sync_mupdf_c:
                sync_mupdf_cpp(state, arg.remote_sync_mupdf_c.mupdf)
            
            elif arg.remote_sync_dir:
                sync_internal( '%s/' % arg.remote_sync_dir.directory, state.remote.ssh, state.directory, submodules=True)

            elif arg.remote_sync_dirs:
                directories = arg.remote_sync_dirs
                while directories:
                    directory, directories = jlib.split_first_of( directories, ' ,')
                    jlib.log('{directory=} {directories=}')
                    sync_internal( '%s' % directory, state.remote.ssh, state.directory, submodules=True)
            
            elif arg.remote_sync_file:
                from_ = arg.remote_sync_file.path_from
                to_ = arg.remote_sync_file.path_to
                if to_ == '.':
                    to_ = from_
                if not to_.startswith( '/') and not to_.startswith( './'):
                    to_ = os.path.join( state.directory, to_)
                sync_file( from_, state.remote.ssh, to_)

            elif arg.remote_sync_files:
                froms = narg.remote_sync_files.paths_from
                for from_ in froms.split( ' '):
                    to_ = os.path.join( state.directory, from_)
                    sync_file( from_, state.remote.ssh, to_)

            elif arg.rsync_bmpcmp:
                destination = arg.rsync_bmpcmp
                for input_ in state.inputs:
                    command = 'rsync -ai'
                    for g in sorted( bmpcmp_outfiles( state, input_)):
                        command += ' %s' % g
                    command += ' %s' % destination
                    jlib.system( command)
            
            elif arg.rsync_html:
                destination = arg.rsync_html
                assert destination
                # We try to show a public URL for each file we upload.
                #
                # If <destination> is:
                #       <user>@<host>/public_html/<tail>
                #
                # then for each file we upload, URL is likely to be:
                #       https://<host>/~<user>/<tail><leafname>
                #
                url_base = None
                m = re.match( '(^[^@]+)@([^:]+):public_html/(.*)$', destination)
                if m:
                    url_base = 'https://%s/~%s/%s' % (
                            m.group(2),
                            m.group(1),
                            m.group(3),
                            )
                if state.html_generated:
                    command = 'rsync -aiz'
                    for name in state.html_generated:
                        assert name
                        command += ' %s' % name
                        if url_base:
                            print( '    %s%s' % (url_base, name))
                    command += ' %s' % destination
                    jlib.system( command)

            elif arg.run_compare:
                do_run_compare( state)
            
            elif arg.run_compare_bmpcmp:
                state.bmpcmp = arg.run_compare_bmpcmp
                if state.bmpcmp == '0':
                    state.bmpcmp = 0
            
            elif arg.run_compare_repeat:
                state.compare_repeat = int(arg.run_compare_repeat)
            
            elif arg.run_performance:
                min_t = int( arg.run_performance)
                do_run_performance( state, min_t)
            
            elif arg.tskip:
                state.tskip = int( arg.tskip)
            
            elif arg.xml_pretty:
                for dirpath, dirnames, filenames in os.walk(arg.xml_pretty):
                    for filename in filenames:
                        if filename.endswith('.xml'):
                            path = os.path.join(dirpath, filename)
                            jlib.system(f'xmllint --format {path} > {path}.pretty.xml && mv {path}.pretty.xml {path}')
                
            else:
                text = 'unexpected parameter'
                for n, v in arg._attr.items():
                    if v is not None:
                        text += f' {n}={v}'
                raise Exception(f'unexpected parameter: {text}')
        
        except StopIteration:
            raise Exception( 'Not enough params after: %r' % name_orig)
        
        except Exception as e:
            print( 'Failed to handle option: %r' % name_orig)
            raise


def git_to_tgz( directory, out, submodules: bool=False, root=None):
    '''
    Creates `.tgz` file containing all files known to git.
    
    Args:
        directory:
            Directory in git checkout.
        out:
            Output `.tgz` file.
        submodules:
            If true, we include files in submodules.
        root:
            If not `None`, name of top-level directory within generated `.tgz`.
    '''
    files = jlib.get_gitfiles( directory, submodules)
    with tarfile.open( out, 'w:gz') as tar:
        for file_ in files:
            path = os.path.join( directory, file_)
            if root:
                file_ = os.path.join( root, file_)
            tar.add( path, file_)


def doctest():
    import doctest
    jlib.log( 'Running doctest.testmod()')
    doctest.testmod()

#import clize
#from clize import run, parameters, converters
#def main(pos1, pos2:clize.converters.file(), *, opt, mul: (clize.parameters.multi(), 'm')):
#    pass

if __name__ == '__main__':
    #import clize
    #clize.run( main)
    try:
        aargs.cli()
        
    finally:
        jlib.log( f'Finished at: {time.strftime( "%F-%T %z %Z")}')
