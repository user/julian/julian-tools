#! /usr/bin/env python3

'''
Test runner.


Args:

    --all
        Run all tests
    --list
        List all tests
    --repeat
        Repeatedly run specified test(s)
    <test>
        Adds all matching tests.
        
        '*' is converted to '.*', '?' is converted to '.', and the resulting
        string is used as a regex.

Details:
    Each test is implemented as a function whose name is the same as the test.
'''

import jlib

import inspect
import os
import re
import sys
import time


artifex_dir = os.path.abspath( f'{__file__}/../..')

openbsd = (os.uname()[0] == 'OpenBSD')
make = 'gmake' if openbsd else 'make'


def oneline( text):
    '''
    Replaces all sequences of spaces and newlines by a single space.
    '''
    text = text.replace( '\n', ' ')
    text = re.sub( '(  +)', ' ', text)
    return text


def system( command):
    '''
    Runs specified command. Output is prefixed with function name of our
    caller.
    '''
    caller_frame = inspect.stack()[ 1]
    caller_fn = caller_frame.function
    prefix = caller_fn.replace( '_', '.') + ': '
    return jlib.system( command, verbose=True, prefix=prefix)


def test_ghostpdl_extract_windows_build():
    '''
    Builds gs on windows.
    '''
    command = oneline( f'''
            {artifex_dir}/julian-tools/jtest.py
                -r jules-windows,ghostpdl,mupdf
                    "
                    rsync -ai mupdf/thirdparty/extract/ ghostpdl/extract/
                    && cd ghostpdl
                    && /cygdrive/c/Program\ Files\ \(x86\)/Microsoft\ Visual\ Studio/2019/Community/Common7/IDE/devenv.com
                            windows/GhostPDL.sln
                            /Build Debug
                            /Project ghostscript
                    "
            ''')
    system( command)

def test_ghostpdl_extract_windows_test():
    test_ghostpdl_extract_windows_build()
    system( oneline( f'''
            {artifex_dir}/julian-tools/jtest.py
                --remote jules-windows:artifex
                --remote-command
                    "
                    ./ghostpdl/debugbin/gswin64c.exe
                        -sDEVICE=docxwrite
                        -o foo.docx
                        extract/test/Python2.pdf
                    "
            '''))

def test_build_mupdf_extract():
    '''
    Builds mupdf with extract.
    '''
    system( f'{artifex_dir}/julian-tools/jtest.py -b {artifex_dir}/mupdf/build/debug-extract .')

def test_build_ghostpdl_extract():
    '''
    Builds gs with extract.
    '''
    system( f'{artifex_dir}/julian-tools/jtest.py -b {artifex_dir}/ghostpdl/extract-debug-bin/gs cm')

def test_extract():
    '''
    Run complete test of extract, including mupdf and gs embedded.
    '''
    test_build_mupdf_extract()
    test_build_ghostpdl_extract()
    command = oneline(f'''
            cd {artifex_dir}/mupdf/thirdparty/extract
            && {make}
                    mutool_e=../../build/debug-extract/mutool
                    gs_e=../../../ghostpdl/extract-debug-bin/gs
                    test
            ''')
    system( command)
    
    

def get_fns( test):
    '''
    Returns list of all test functions that match <test>.
    '''
    ret = []
    test = test.replace( '.', '_')
    test = test.replace( '*', '.*')
    test = test.replace( '?', '.?')
    test = re.compile( f'^{test}$')
    module = inspect.getmodule( do)
    fns = inspect.getmembers( module, inspect.isfunction)
    n = 0
    for fnname, fn in fns:
        jlib.logx( 'checking {fnname=}')
        if re.match( test, fnname):
            ret.append( (fnname, fn))
    return ret

def do( test):
    '''
    Runs all tests that match <test>.
    '''
    fns = get_fns( test)
    n = 0
    for fnname, fn in fns:
        if re.match( test, fnname):
            fn()
            n += 1
    if not n:
       raise Exception( f'Cannot find matching function for {test!r}')



def main():
    args = jlib.Args( sys.argv[1:])
    repeat = False
    tests = []
    while 1:
        try:
            arg = args.next()
        except StopIteration:
            break
        if arg == '--repeat':
            repeat = True
        elif arg == '--all':
            tests.append( '')
        elif arg == '--list':
            for fnname, fn in get_fns( 'test_*'):
                jlib.log( '    {fnname}')
        elif arg.startswith( '-'):
            raise Exception( f'Unrecognised arg: {arg!r}')
        else:
            tests.append( arg)
            
    if repeat:
        while 1:
            for test in tests:
                do( test)
    else:
        for test in tests:
            do( test)


if __name__ == '__main__':
    main()
