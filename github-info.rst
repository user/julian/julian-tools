Github repositories
===================

MuPDF
-----

* https://github.com/ArtifexSoftware/mupdf

  * Actions

    * **CodeQL**: Static code analysis.

      * Runs daily.

        Results are in: `Security/Code scanning
        <https://github.com/ArtifexSoftware/mupdf/security/code-scanning>`_

    * **Pip build/install and test of Python Bindings**: Python bindings
      build+test.

      * Runs daily.

PyMuPDF
-------

Main repository
~~~~~~~~~~~~~~~

* https://github.com/pymupdf/PyMuPDF
  
  * Actions:

    * **Build wheels**: builds full set of wheels using cibuildwheel.

    * **CLA Assistant**: helper for contributer license agreement.

    * Daily actions:

      * **Test**: Build+test current PyMuPDF with its hard-coded MuPDF.
      * **Test mupdf master branch**: Build+test current PyMuPDF with MuPDF current master branch.
      * **Test mupdf release branch**: Build+test current PyMuPDF with MuPDF current release branch.
      * **Test valgrind**: run test suite under valgrind (but ignoring leaks).

Performance testing
~~~~~~~~~~~~~~~~~~~

General info is at: https://github.com/ArtifexSoftware/PyMuPDF-performance-results

* https://github.com/ArtifexSoftware/PyMuPDF-performance

  * Actions

    * **Run performance tests**:

      Runs weekly.

      Runs performance tests on various Python PDF libraries: PyMuPDF built
      with MuPDF master and release branchs, pdfrw, pikepdf, pypdf2, pdf2jpg,
      poppler, pdfminer.

      Results are pushed to (private) repository
      https://github.com/ArtifexSoftware/PyMuPDF-performance-results.

* https://github.com/ArtifexSoftware/PyMuPDF-performance-results

  * Actions

    * **Summarise results**:

      Triggered by git push (typically from PyMuPDF-performance):

      Generates tables comparing performance of Python PDF libraries. These
      tables are visible in 'Summarise results'.

Rebased implementation, mupdfpy
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* https://github.com/ArtifexSoftware/mupdfpy-julian

  * Actions:

    * Test with mupdf master branch

      Runs daily.
