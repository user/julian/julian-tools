#!/usr/bin/env python3

'''
Functions and CLI for building, testing and releasing PyMuPDF.

Intended to be run with `<__file__>/../../` containing::

    mupdf
    PyMuPDF

We use the :py:obj:`aargs` module to allow use from command line - run with
**-h** to get help.

Many build/test functions have an optional `remote` arg; if specified this is
the name of a remote machine and we use rsync and ssh to defer to the remote
machine.
'''

import calendar
import copy
import getpass
import glob
import hashlib
import io
import json
import os
import pickle
import platform
import pprint
import queue
import re
import shlex
import shutil
import subprocess
import sys
import tarfile
import textwrap
import threading
import time
import typing
import zipfile

try:
    import requests
except ImportError:
    requests = None


import aargs
import jlib
import jtest
import pypackage


#jlib.log( '# sys.executable={sys.executable!r}')

# Old notes.
# 
# MuPDF limitations relative to Pymupdf:
# 
#   PyMuPDF has more information about links - fitz.LINK_GOTO, LINK_GOTOR,
#   fitz.LINK_LAUNCH, fitz.LINK_URI.
# 
#   PyMuPDF has abstraction for writing image files which calls
#   fz_save_pixmap_as_png() or fz_save_pixmap_as_pnm() etc, depending on the
#   filename.
# 
#   PyMuPDF can copy a TOC into a PdfDocument.
# 
#   Looks like PyMuPDF has fairly elaborate support for redactions. Makes use
#   of pdf_redact_page() and then writes on top of the redaction?

sys.excepthook = lambda type_, exception, traceback: jlib.exception_info( exception)

g_mupdf_location = os.path.abspath( f'{__file__}/../../mupdf')

if pypackage.windows():
    python_version = '.'.join(platform.python_version().split('.')[:2])

g_pymupdf_wheel = None

g_root = os.path.abspath( f'{__file__}/../..')
g_root_rel = os.path.relpath( g_root)

g_pymupdf_dir = os.path.abspath( f'{g_root}/PyMuPDF')


def system(command, env_extra=None, prefix=None):
    jlib.system( command, env_extra=env_extra, out='log', verbose=True, prefix=prefix)
    return

def system_output(command):
    t = subprocess.check_output(command, shell=True)
    return t.decode('latin-1').strip()



def _version_numbers( text):
    '''
    Yields (line_number, line, v, plus) for each pymupdf version number in `text`.
    
    >>> list( _version_numbers( 'abc 1.1547 * max'))
    []
    
    >>> list( _version_numbers( 'abc 1.20.0.'))
    [(1, 'abc 1.20.0.', '1.20.0', False)]
    
    >>> list( _version_numbers( 'abc 1.20.0 .'))
    [(1, 'abc 1.20.0 .', '1.20.0', False)]
    
    >>> list( _version_numbers( 'abc 1.20.0**'))
    [(1, 'abc 1.20.0**', '1.20.0', False)]
    
    >>> list( _version_numbers( 'abc v1.20.0.'))
    [(1, 'abc v1.20.0.', '1.20.0', False)]
    
    # We allow suffixes 'rc<N>' - see https://peps.python.org/pep-0440/.
    
    >>> list( _version_numbers( 'abc v1.20.0rc32.'))
    [(1, 'abc v1.20.0rc32.', '1.20.0rc32', False)]
    
    >>> list( _version_numbers( 'This documentation covers PyMuPDF v1.20.0 features as of **2022-06-16 00:00:01**.'))
    [(1, 'This documentation covers PyMuPDF v1.20.0 features as of **2022-06-16 00:00:01**.', '1.20.0', False)]
    
    >>> list( _version_numbers( 'abc 1.20.0+'))
    [(1, 'abc 1.20.0+', '1.20.0', True)]
    '''
    
    items = re.finditer( '[^0-9](1[.][0-9][0-9])(.*)', text)
    line_number = 1
    pos_prev = 0
    for i, m in enumerate( items):
        ab = m.group(1)
        c = m.group(2)
        
        #jlib.log( '{=ab!r c!r}')
        
        mm = None
        
        if not mm:
            # Try to match 1.20.2rc34 or 1.20.2test34
            mm = re.match( '([.][0-9][0-9]?(rc[0-9]+)?)($|[^0-9])', c)
            if mm:
                #jlib.log( '{mm.group()!r=} {mm.group(1)!r=} {mm.group(2)!r=} {mm.group(3)!r=}')
                v = ab + mm.group(1)
                #jlib.log( '{mm.group(1)!r=}: {v!r}')
        if not mm:
            # Try to match 1.20
            mm = re.match( '([^.0-9a-zA-Z])', c)
            if mm:
                v = ab
                #jlib.log( '{mm.group(1)!r=}: {v!r}')
        if not mm:
            # Try to match 1.20.
            mm = re.match( '([.]$|[^0-9])', c)
            if mm:
                v = ab
                #jlib.log( '{v!r}')
        if not mm:
            #jlib.log( 'No match for: {ab+c!r}')
            continue
        
        e = mm.end(1)
        #print(f'{e=} {len(c)=} {c=}')
        plus = True if e < len(c) and c[e] == '+' else False
            
        #jlib.log( 'yielding: {v!r}')
        pos = m.start()
        line_number += text[pos_prev:pos].count( '\n')
        pos_prev = pos
        
        # Search back/forward for entire line.
        line_begin = text.rfind( '\n', 0, pos)
        line_end = text.find( '\n', pos)
        if line_end == -1:
            line_end = len( text)
        line = text[ line_begin+1: line_end]
        
        yield line_number, line, v, plus

def _any_in( needles, haystack):
    for needle in needles:
        if needle in haystack:
            return True

# Version numbers used by check_release.
#
# When we make a release:
#
#   g_release_pymupdf_version should always be incremented.
#
#   g_release_pymupdfb_version can be left unchanged if g_release_mupdf_version
#   is unchanged, otherwise it should beincremented to equal
#   g_release_pymupdf_version.
#
#   g_release_mupdf_version can be incremented. If it is incremented,
#   g_release_pymupdfb_version must be incremented to match
#   g_release_g_release_pymupdf_version.
#

# PyMuPDF version:
g_release_pymupdf_version = '1.24.5'

# PyMuPDFb version. This is the PyMuPDF version whose PyMuPDFb wheels we will
# (re)use.
g_release_pymupdfb_version = '1.24.3'

# The MuPDF version.
g_release_mupdf_version = '1.24.2'


def check_release():
    '''
    Checks that version numbers and dates in PyMuPDF git files are correct.

    We distinguish between PyMuPDF and MuPDF version numbers. The expected
    version numbers are hard-coded in pymupdf.py itself.
    '''
    # This must work with _version_numbers().
    pymupdf_version = g_release_pymupdf_version
    mupdf_version = g_release_mupdf_version
    
    # Check <pymupdf_version> is a valid version number recognised by
    # _version_numbers().
    #
    c0 = [ (1, ' '+pymupdf_version, pymupdf_version, False)]
    t = ' ' +pymupdf_version
    c1 = list(_version_numbers( t))
    assert c1 == c0, f'{t=}\n    {c0=}\n    {c1=}'
    
    def make_major_version( version):
        version = version.split( '.')
        assert len( version) == 3
        version = '.'.join( version[:2])
        return version
    pymupdf_version_major = make_major_version( pymupdf_version)
    mupdf_version_major = make_major_version( mupdf_version)
    
    num_errors = 0
    
    jlib.log( '{=pymupdf_version!r pymupdf_version_major!r}')
    jlib.log( '{=mupdf_version!r mupdf_version_major!r}')
    jlib.log( '=' * 60)
    # Check version numbers.
    #
    if pymupdf_version_major != mupdf_version_major:
        num_errors += 1
        jlib.log( 'ERROR: PyMuPDF and MuPDF major version numbers do not match: {pymupdf_version=} {mupdf_version=}')
    
    t = time.time()
    date1 = time.strftime( '%Y-%m-%d', time.gmtime( t))
    date2 = time.strftime( '%Y%m%d000001', time.gmtime( t))
    date3 = time.strftime( '\nRelease date: %B %e, %Y\n')
    date4 = time.strftime( '\ncopyright = "2015-%Y, Artifex"\n')
    
    for p in jlib.git_get_files( g_pymupdf_dir):
        if p.startswith( ('.vs/', 'installation')):
            continue
        if p.endswith( ('.ico', '.png', '.jpg', '.pdf', '.epub', '.svg', '.otf')):
            continue
        if p.endswith( '.DS_Store'):
            continue
        if p.endswith( '.db'):
            continue
        if p.endswith( '.zip'):
            continue
        if p.endswith( '.pickle'):
            continue
        if p in (
                '.github/workflows/build_wheels.yml',
                '.readthedocs.yaml',
                ):
            continue
        if p == 'setup-old.py':
            continue
        if p == 'pipcl.py':
            continue
        if p.startswith('src_classic/'):
            continue
        if p in ('src_classic/__init__.py', 'src/__init__.py', 'src/extra.i'):
            # Warnings about legacy fns removed after 1.19.
            continue
        if p.startswith('docs/locales/ja/LC_MESSAGES/'):
            continue
        
        path = os.path.relpath( f'{g_pymupdf_dir}/{p}')
        #jlib.log( 'Looking at: {path}')
        try:
            text = jlib.fs_read( path)
        except Exception as e:
            raise Exception( f'Failed to load as text: {path}') from e
        
        if p == '.github/ISSUE_TEMPLATE/bug_report.yml':
            expected = f'        - {pymupdf_version}\n'
            if not expected in text:
                num_errors += 1
                jlib.log( 'ERROR: {path}: did not find expected text {expected!r}.')
            continue
            
        for i, (line_number, line, v, plus) in enumerate( _version_numbers( text)):
            #jlib.log( '{p}: {i}: {v}')
            if plus:
                #print(f'Ignoring version ending with "+": {line=} {v=}')
                continue
            if os.path.basename( p) == 'changes.txt':
                if i > 0:
                    # In changelog, allow all but first version number to be an
                    # old version number.
                    continue
                if date1 not in line:
                    num_errors += 1
                    jlib.log( 'ERROR: {path}:{line_number}: expecting date {date1=} in {line=}.')
            
            if (
                    ( p == 'src_classic/version.i' and line.startswith( 'VersionFitz'))
                    or
                    ( p == 'setup.py' and 'https://mupdf.com/downloads/archive' in line)
                    ):
                # Compare with MuPDF version.
                if v != mupdf_version:
                    num_errors += 1
                    jlib.log( 'ERROR: {path}:{line_number}: version does not match {mupdf_version=}: {v!r}: {line}')
            elif p == 'setup.py' and 'PYMUPDF_SETUP_MUPDF_BUILD=' in line:
                pass
            elif p == 'setup.py' and line.startswith( 'version_b ='):
                if v != g_release_pymupdfb_version:
                    num_errors += 1
                    jlib.log( 'ERROR: {path}:{line_number}: version_b should be {g_release_pymupdfb_version=}.')
            elif p == 'READMErb.md':
                if v != g_release_pymupdfb_version:
                    num_errors += 1
                    jlib.log( 'ERROR: {path}:{line_number}: should contain {g_release_pymupdfb_version=}.')
            elif 'PyMuPDF/dist/PyMuPDF-' in line:
                pass
            else:
                # Compare with PyMuPDF version.
                if v not in (pymupdf_version_major, pymupdf_version):
                    if p == 'tests/test_annots.py':
                        continue
                    if (p == 'tests/README.md'
                            and line.startswith( 'platform linux -- Python')
                            ):
                        continue
                    if ( p in ('src_classic/helper-other.i', 'src/__init__.py')
                            and 'create PDF object from given string (new in v1.14.0: MuPDF dropped it)'
                                in line
                            ):
                        continue
                    if (p == 'scripts/gh_release.py'
                            and 'PyMuPDF-1.23.2-cp311-none-emscripten_3_1_32_wasm32.whl'
                                in line
                            ):
                        continue
                    if p == 'tests/test_2548.py':
                        continue
                    if p == 'tests/test_general.py' and '# With mupdf later than 1.23.4, this special page contains no invalid' in line:
                        continue
                    if p == 'tests/test_2791.py':
                        continue
                    if p == 'tests/test_font.py':
                        continue
                    if p == 'tests/test_textextract.py' and 'mupdf-1.23.9 with addition of FZ_STEXT_USE_CID_FOR_UNKNOWN_UNICODE' in line:
                        continue
                    if p.endswith( '.py') and line.strip().startswith('#'):
                        #jlib.log(f'Ignoring python comment in {p=}: {line}')
                        continue
                    if p == 'docs/app3.rst' and '(= 1.39 inches or 3.53 centimeters)' in line:
                        continue
                    if p == 'tests/test_pixmap.py' and 'Not running because known to hang on MuPDF < 1.23.10.' in line:
                        continue
                    if p == 'tests/test_pagedelete.py' and 'pdf_rearrange_pages() since version 1.23.9' in line:
                        continue
                    if p == 'tests/test_textextract.py' and '2024-01-09: MuPDF master and 1.23.x give slightly different' in line:
                        continue
                    if p == 'tests/test_textextract.py' and 'Related Tickers' in line:
                        continue
                    if 'Fails starting with' in line:
                        continue
                    if p.startswith( 'docs/'):
                        # We allow various mentions of old version numbers in
                        # docs/, because there are lots of notes about when
                        # features were introduced etc.
                        vv = v.split( '.')
                        vv = [int(i) for i in vv[:2]]
                        if vv < [1, 19]:
                            # Ignore 1.18* and earlier - these do not have to be updated for 1.20.x and later.
                            continue
                        if _any_in(
                                (
                                    'new in ',
                                    'changed in ',
                                    'works for v',
                                    'starting with ',
                                    'since version ',
                                    'since v1.',
                                    'deprecated and removed in version following',
                                    'old names will remain available as deprecated aliases through mupdf version 1.19.0',
                                    '1.75 ns per loop',
                                    'versions < 1.19.0',
                                    'as of `pymupdf-1.20.0`, the required mupdf source code is already in the',
                                    '**except** that this font file is relatively large and adds about 1.65 mb',
                                    ':arg bool new: *deprecated* and ignored. will be removed some time after v1.20.0.',
                                    'earlier (pre 1.19.*) versions of the pymupdf documentation used to refer to this document',
                                    'see the :ref:`adobemanual`, table 1.76 on page 400',
                                    'is a new feature of pymupdf version ',
                                    ':orange-color:`1.76`',
                                    'prismjs 1.29.0',
                                    'or later',
                                    'https://ghostscript.com/~julian/pyodide/',
                                    'rust: "1.55"',
                                    'in versions 1.24.1+ of pymupdf',
                                    ),
                                line.lower(),
                                ):
                            continue
                    
                    num_errors += 1
                    jlib.log( 'ERROR: {path}:{line_number}: version does not match {pymupdf_version=}: {v!r}: {line}')
    
    # Check date strings are today's date.
    #
    
    # Check changes.txt.
    path = os.path.relpath( f'{g_pymupdf_dir}/changes.txt')
    with open(path) as f:
        for i, line in enumerate(f):
            if line.startswith( '**Changes in version '):
                expected = f'**Changes in version {g_release_pymupdf_version} ({date1})**\n'
                if line != expected:
                    jlib.log( 'ERROR: {path}:{i+1}: {line=} does not match {expected=}.')
                    num_errors += 1
                break
        else:
            jlib.log( 'ERROR: {path}: Cannot find item heading `**Changes in ...`')
            num_errors += 1
    
    if 0:
        # Check src_classic/version.i.
        text_expected = textwrap.dedent( f'''
                %pythoncode %{{
                VersionFitz = "{mupdf_version}" # MuPDF version.
                VersionBind = "{pymupdf_version}" # PyMuPDF version.
                VersionDate = "{date1} 00:00:01"
                version = (VersionBind, VersionFitz, "{date2}")
                pymupdf_version_tuple = tuple( [int(i) for i in VersionFitz.split('.')])
                %}}
                ''')
        text_expected = text_expected[1:]
        path = os.path.relpath( f'{g_pymupdf_dir}/src_classic/version.i')
        text_actual = jlib.fs_read( path)
        if text_actual != text_expected:
            num_errors += 1
            jlib.log(
                    'ERROR: {path}: Version error(s); contents do not match, expected:\n'
                    '--------\n'
                    '{textwrap.indent( text_expected, "    ")}'
                    '--------\n'
                    'But got:\n'
                    '--------\n'
                    '{textwrap.indent( text_actual, "    ")}'
                    '--------\n'
                    )
    
    # Check version in src/__init__.py.
    p = os.path.relpath( f'{g_pymupdf_dir}/src/__init__.py')
    with open(p) as f:
        matches = dict()
        for i, line in enumerate(f):
            for regex, group1 in [
                    ( '^VersionFitz = "(.+)" # MuPDF version.$', mupdf_version),
                    ( '^VersionBind = "(.+)" # PyMuPDF version.$', pymupdf_version),
                    ( '^VersionDate = "(.+)"\n', f'{date1} 00:00:01'),
                    ]:
                m = re.match(regex, line)
                if m:
                    matches.setdefault(regex, []).append(m)
                    if m.group(1) != group1:
                        jlib.log( f'ERROR: {p}:{i+1}: version error: incorrect match for {regex=}; expected {group1=} but {m.group(1)=}.')
                        num_errors += 1
        for regex, matches in matches.items():
            if len(matches) != 1:
                jlib.log( f'ERROR: {p}: version error: expected one match for {regex=}, but {len(matches)=}.')
                num_errors += 1
    
    # As of 2023-08-23, README.md does not contain the release date.
    #
    if 0:
        # Check release date in README.md:
        path = os.path.relpath( f'{g_pymupdf_dir}/README.md')
        text = jlib.fs_read( path)
        if date3 not in text:
            num_errors += 1
            jlib.log( 'ERROR: {path}: Date error, cannot find {date3!r}.')
    
    # Check release date in docs/version.rst.
    path = os.path.relpath( f'{g_pymupdf_dir}/docs/version.rst')
    text = jlib.fs_read( path)
    t = f'This documentation covers **PyMuPDF v{pymupdf_version}** features as of **{date1} 00:00:01**.'
    if t not in text:
        num_errors += 1
        jlib.log( 'ERROR: {path}: Date error, cannot find {t!r}.')
    # Check copyright date-range in docs/conf.py.
    path = os.path.relpath( f'{g_pymupdf_dir}/docs/conf.py')
    text = jlib.fs_read( path)
    if date4 not in text and 'thisday.year' not in text:
        num_errors += 1
        jlib.log( 'ERROR: {path}: Date error, cannot find {date4!r}.')    
    
    jlib.log( '=' * 60)
    jlib.log( '{num_errors=}')
    assert num_errors == 0, f'num_errors={num_errors}'
    jlib.log( 'PyMuPDF ok for release')
            

def mupdf_dir( location):
    '''
    Sets location of mupdf checkout. Default is 'pymupdf.py/../../mupdf'.
    If 'None' we set to `None` - use default download URL.
    '''
    global g_mupdf_location
    if location in ('None', None):
        g_mupdf_location = None
    elif location.startswith('git:') or '://' in location:
        g_mupdf_location = location
    else:
        # Important to use abspath() here, otherwise PyMuPDF's setup.py can end
        # up seeing .../PyMuPDF/mupdf because it is run with current directory
        # set to .../PyMuPDF.
        g_mupdf_location = os.path.abspath(location)


def pymupdf_dir( location):
    '''
    Sets location of PyMuPDF, e.g. '../PyMuPDF-jorj'. Default is
    'pymupdf.py/../../PyMuPDF'.
    '''
    location = os.path.abspath( location)
    assert os.path.basename( location).startswith( 'PyMuPDF'), f'location={location}'
    global g_pymupdf_dir
    g_pymupdf_dir = location


def sync(remote: jtest.Remote, dirs):
    assert remote
    if isinstance(dirs, str):
        dirs = dirs.split(',')
    _run_remote(remote, *dirs, tee=False, do_command=False)


class Build:
    def __init__(
            self,
            how='setup.py',
            doit: bool=True,
            git_sha=None,
            git_sha_mupdf=None,
            rebuild_git_details=False,
            t='release',
            ):
        '''
        Settings for building PyMuPDF.
        
        Args:
            how:
                How to do the build, one of:
                'setup.py'
                'pip'
                'cibuildwheel'
                'cibuildwheel,pytest'
            doit:
                If false, we do not build anything.
            git_sha
                .
            rebuild_git_details:
                If false (the default) we don't rebuild if only
                fitz/helper-git-versions.i has changed. See setup.py's
                PYMUPDF_SETUP_REBUILD_GIT_DETAILS.
            t:
                Build type, must be 'release', 'debug' or 'memento'.
        '''
        assert t in ('release', 'debug' or 'memento')
        assert how in (
                'setup.py',
                'pip',
                'cibuildwheel',
                'cibuildwheel,pytest',
                )
        self.how = how
        self.doit = doit
        self.git_sha = git_sha
        self.git_sha_mupdf = git_sha_mupdf
        self.rebuild_git_details = rebuild_git_details
        self.t = t
        jlib.log(f'Build.__init__(): {self}')
    def __repr__(self):
        return f'Build: {self.doit=} {self.git_sha=} {self.git_sha_mupdf=} {self.rebuild_git_details=} {self.t=}'


def _in_venv():
    '''
    Returns true if we are running in a venv.
    '''
    return sys.prefix != sys.base_prefix


def _env_append( d, name, *paths, transform=None, prepend=False):
    '''
    Adds specified paths to d[name], separating with `os.pathsep`.
    
    Args:
        d:
            Dictionary.
        name:
            Name in `d`.
        paths:
            List of paths.
        transform:
            None - do nothing.
            'absolute': transform paths with os.path.abspath().
            'relative': transform paths with os.path.relpath().
        prepend:
            If true, we prepend `paths` to any existing d[name], otherwise (the
            default) we append.
    '''
    if d.get( name) is None:
        d[ name] = os.environ.get( name, '')
    for path in paths:
        if transform is None:
            pass  
        elif transform == 'relative':
            path = os.path.relpath( path)
        elif transform == 'absolute':
            path = os.path.abspath( path)
        else:
            assert 0
        if d[ name]:
            if prepend:
                d[ name] = path + os.pathsep + d[ name]
            else:
                d[ name] = d[ name] + os.pathsep + path
        else:
            d[ name] = path

def _make_env_extra( env0):
    '''
    Returns dict containing settings for LD_LIBRARY_PATH and PYTHONPATH,
    appending to any existing values in os.environ.
    '''
    ret = dict() if env0 is None else env0.copy()
    if g_pymupdf_wheel:
        pass
    _env_append( ret, 'PYTHONPATH', f'{g_mupdf_location}/scripts', transform='relative')   # For jlib.
    return ret


def _env_rebased(env0=None):
    return _make_env_extra( env0)

_env_rebased_pythonpath = None


def vagrind_command():
    ret = f'PYTHONMALLOC=malloc PYMUPDF_RUNNING_ON_VALGRIND=1 valgrind --suppressions={g_pymupdf_dir}/valgrind.supp --error-exitcode=100 --errors-for-leak-kinds=none --fullpath-after= --num-callers=20'
    #ret += f'--suppressions={g_pymupdf_dir}/valgrind.supp '
    #ret += '--leak-check=full '
    #ret += '--gen-suppressions=yes '
    # This is slow.
    #ret += '--track-origins=yes '
    return ret


def set_build_type( type_):
    '''
    Sets global `g_build` to `type_`, which should be `debug`, `release` or
    `memento`; this is used when we build mupdf and pymupdf.
    '''
    types = ('debug', 'release', 'memento')
    assert type_ in types, f'{type_=} nmust be in: {types}'
    global g_build
    g_build = type_
    jlib.log( '{g_build=}')


def set_wheel( wheel):
    '''
    Override location of pymupdf to specified wheel name or filename.
    '''
    global g_pymupdf_wheel
    g_pymupdf_wheel = wheel


def _diff_dicts( a, b, values=False):
    '''
    Shows top-level differences between dicts.
    '''
    diff = 0
    ret = list()
    for bk in sorted(b.keys()):
        bv = b[bk]
        if bk not in a:
            if values:
                #jlib.log( '    + {bk}: {bv!r}')
                ret.append( (f'+ {bk}: {bv!r}', bv))
            else:
                #jlib.log( '    + {bk}')
                ret.append( (f'+ {bk}', bv))
            diff = 1
    for ak in sorted(a.keys()):
        av = a[ak]
        if ak not in b:
            if values:
                #jlib.log( '    - {bk}: {bv!r}')
                ret.append( (f'- {bk}: {bv!r}', bv))
            else:
                #jlib.log( '    - {bk}')
                ret.append( (f'- {bk}', bv))
            diff = 1
    for k in sorted(a.keys()):
        av = a[k]
        if k in b:
            bv = b[k]
            if av == bv:
                if values:
                    #jlib.log( '    = {k}:{av!r}')
                    ret.append( (f'= {k}:{av!r}', av))
            else:
                if values:
                    #jlib.log( '    * {k}: {av!r} {bv!r}')
                    ret.append( (f'* {k}: {av!r} {bv!r}', bv))
                diff = 1
    ret.sort(key=lambda x: x[1], reverse=True)
    for text, n in ret:
        print(text)
    return diff
    

def diff_cachegrind(a, b):
    '''
    Take output from test(cachegrind=True) and show differences between classic
    and rebased.
    '''
    def load(p):
        with open(p) as f:
            ret = dict()
            for line in f:
                #m = re.split('$ *(([0-9,]+) +([(][0-9.]+%[)]) +)+(.+)$', line)
                n = re.findall('( *[0-9,]+) ', line)[0]
                assert n
                f = re.search(' ([^ 0-9(].+)$', line).group(1)
                assert f
                #print(f'{n=} {f=}')
                #n = int(n.replace(',', ''))
                ret[f] = n
            return ret
    aa = load(a)
    bb = load(b)
    _diff_dicts(aa, bb, values=1)


def _timestring_to_time( time_string):
    return time.strptime( time_string, '%Y-%m-%dT%H:%M:%SZ')

def _gh_download( url, path):
    '''
    Downloads from github URL to local file.
    
    Args:
        url:
            URL from which to download.
        path:
            Local path for downloaded data.
    
    * We ask user for username/token and retry if we get an error.
    * We do not leave partial downloads in place - we write to `path+'_'` then
      rename to `path`.
    * Does nothing if `path` already exists.
    '''
    if os.path.exists( path):
        jlib.log( 'Not downloading because local path already exists: {os.path.relpath(path)}')
        return
    path_ = f'{path}_'
    jlib.fs_remove( path_)
    jlib.log( 'Downloading:')
    jlib.log( '    from: {url=}')
    jlib.log( '    to: {path}')
    r = requests.get( url, headers=_gh_headers(), stream=True)
    jlib.log( '{r.status_code=}')
    if r.status_code == 403:
        username = input( 'username ? ')
        password = getpass.getpass( 'password ? ')
        r = requests.get( url, headers=_gh_headers(), stream=True, auth=(username, password))
    try:
        r.raise_for_status()
    except Exception as e:
        # r.json() can have useful info about the error.
        raise Exception( str(r.json())) from e
    #jlib.log( '{r.status_code=}')
    t0 = t1 = time.time()
    bytes_ = 0
    with open( path_, 'wb') as f:
        for chunk in r.iter_content( chunk_size=None):
            #jlib.log( '{len(chunk)=}')
            bytes_ += len(chunk)
            f.write( chunk)
            t2 = time.time()
            if t2 - t1 >= 5:
                t1 = t2
                bytes_per_sec = int( bytes_/(t2-t0))
                jlib.log( f'{jlib.number_sep( bytes_):>12}B {jlib.number_sep(bytes_per_sec)}B/s')
    os.rename( path_, path)


def _unzip( path_zip, path_out_directory):
    '''
    Extracts zip file into a directory.
    
    Args:
        path_zip:
            The zip file.
        path_out_directory:
            Directory into which we extract.
    
    * We do not leave partial extract in place - we write to
    `path_out_directory+'_'` then rename to `path_out_directory`.
    * We do nothing if `path_out_directory` already exists.
    '''
    if os.path.exists( path_out_directory):
        jlib.log( 'Not extracting from {os.path.relpath(path_zip)} because already exists: {os.path.relpath(path_out_directory)}')
        return
    jlib.log( 'Extracting:')
    jlib.log( '    from: {path_zip}')
    jlib.log( '    to: {path_out_directory}')
    path_out_directory_ = f'{path_out_directory}_'
    jlib.fs_ensure_empty_dir( path_out_directory_)
    z = zipfile.ZipFile( path_zip)
    z.extractall( path_out_directory_)
    os.rename( path_out_directory_, path_out_directory)


g_gh_token = None
g_pypi_token = None

g_gh_pymupdf_url = 'https://api.github.com/repos/pymupdf/PyMuPDF'

def gh_pymupdf_url(url):
    '''
    Changes Github PyMuPDF git repository URL from default value.
    For example:
        https://api.github.com/repos/ArtifexSoftware/PyMuPDF-julian
    '''
    global g_gh_pymupdf_url
    g_gh_pymupdf_url = url


def gh_token( token):
    '''
    Sets the global Github token `g_gh_token` to be used by other `gh_*()` fns.
    
    `token` must start with `ghp_`.
    '''
    assert token.startswith( 'ghp_')
    global g_gh_token
    g_gh_token = token
    jlib.log( 'Have set {g_gh_token=}')


def gh_token_auto():
    '''
    Sets global Github token `g_gh_token` from hard-coded internal filename:
    
        ~/artifex/token-github.com
    '''
    path = os.path.expanduser( '~/artifex/token-github.com')
    with open(path) as f:
        token = f.read().strip()
    gh_token(token)


def pypi_token( token):
    '''
    Sets the global Pypi token `g_pypi_token` to be used when uploading to
    pypi, e.g. `twine upload -u __token__ -p <g_pypi_token>`.
    
    `token` must start with `pypi-`.
    '''
    assert token.startswith( 'pypi-')
    global g_pypi_token
    g_pypi_token = token
    jlib.log( 'Have set {g_pypi_token=}')


def pypi_token_auto(pypi_test: bool=False):
    '''
    Sets global pypi.org or test.pypi.org token `g_pypi_token` from hard-coded
    internal filenames:
    
        ~/artifex/token-pypi.org
        ~/artifex/token-test.pypi.org
    '''
    if pypi_test:
        path = os.path.expanduser( '~/artifex/token-test.pypi.org')
    else:
        path = os.path.expanduser( '~/artifex/token-pypi.org')
    with open(path) as f:
        token = f.read().strip()
    pypi_token(token)


def auto_tokens(pypi_test: bool=False):
    '''
    Sets access tokens from local files - sets Github token with
    `gh_token_auto()` and Pypi token with `pypi_token_auto()`.
    '''
    gh_token_auto()
    pypi_token_auto(pypi_test)


def _gh_headers(headers=None):
    '''
    Adds basic headers for gihub rest calls and returns dict.
    '''
    if headers is None:
        headers = dict()
    headers[ 'Accept'] = 'application/vnd.github.v3+json'
    if g_gh_token:
        headers[ 'Authorization'] = f'token {g_gh_token}'
    #jlib.log('{ret=}')
    return headers


def _gh_get( url, stream=False, raise_for_status=True, params=None, headers=None):
    '''
    Calls requests.get() for github URL `url`.
    
    params:
        For Github "Query parameters".
    '''
    r = requests.get(
            url,
            headers=_gh_headers(headers),
            params=params,
            stream=stream,
            )
    if raise_for_status:
        try:
            r.raise_for_status()
        except Exception as e:
            raise Exception( str(r.json())) from e
    return r


def _gh_post( url, json, raise_for_status=True):
    '''
    Calls requests.post() for github URL `url`.
    '''
    r = requests.post( url, headers=_gh_headers(), json=json)
    if raise_for_status:
        try:
            r.raise_for_status()
        except Exception as e:
            raise Exception( str(r.json())) from e
    return r


def _gh_workflow( id):
    '''
    Returns dict for specified workflow run.
    '''
    url = f'{g_gh_pymupdf_url}/actions/runs/{id}'
    r = _gh_get( url)
    workflow = r.json()
    return workflow

def gh_workflow( id='newest'):
    '''
    Prints workflow run details.
    '''
    if id == 'newest':
        id = _gh_runs_newest()
    ret = _gh_workflow( id)
    jlib.log( '{json.dumps( ret, indent=4)=}')

def _gh_runs_newest( verbose=0):
    '''
    Returns id of newest workflow run.
    '''
    url = f'{g_gh_pymupdf_url}/actions/runs'
    r = _gh_get( url)
    response = r.json()
    workflows = response[ 'workflow_runs']
    workflows.sort( key=lambda workflow: _timestring_to_time( workflow["created_at"]))
    if verbose >= 2:
        jlib.log( json.dumps( workflows[-1], indent=4, sort_keys=True), nv=0)
    if verbose >= 1:
        jlib.log( 'Workflows are (oldest first):')
        for i, workflow in enumerate( workflows):
            jlib.log( '{i}:')
            jlib.log( '    head_branch={workflow["head_branch"]}')
            jlib.log( '    id={workflow["id"]}')
            jlib.log( '    date={workflow["created_at"]}')
            jlib.log( '    name={workflow["name"]}')
            jlib.log( '    artifacts_url={workflow["artifacts_url"]}')
            jlib.log( '    logs_url={workflow["logs_url"]}')
    if verbose:
        jlib.log( 'Selecting newest workflow')
    workflow = workflows[-1]
    id = workflow[ 'id']
    id = str(id)
    jlib.log('Returning {id=}')
    return id


def gh_run_workflow(
        url,
        data,
        ):
    '''
    Starts new workflow run and returns its id.
    '''
    # Start new run by triggering the 'build wheels' workflow.
    #
    # https://docs.github.com/en/rest/actions/workflows#create-a-workflow-dispatch-event
    #
    run0_id = _gh_runs_newest()
    
    r = _gh_post( url, json=data)
    if 0:
        jlib.log( '{r=}')
        jlib.log( '{r.text!r=}')
        jlib.log( '{r.json()!r=}')
        jlib.log( '{r.content!r=}')
        jlib.log( '{r.raw!r=}')
        jlib.log( '{r.headers!r=}')
        jlib.log( '{json.dumps(r.headers,indent="    ")=}')
    jlib.log( 'Have started new workflow run: {url=}')

    # Unfortunately `r` does not contain any information about the id of
    # the run we have just created. E.g. see:
    #
    #   https://stackoverflow.com/questions/69479400/get-run-id-after-triggering-a-github-workflow-dispatch-event
    #
    # That link has a way to embed a uid in the new run and detect it. But
    # for now we'll do things more crudely - repeatedly look for a new run
    # - it will be most recent run with different id from workflow0.
    #
    jlib.log( 'Polling for first mention of the new workflow run we have just created...')
    while 1:
        run_id = _gh_runs_newest()
        jlib.log( '{=run0_id run_id}')
        #jlib.log( 'workflow:\n{json.dumps(workflow, indent="    ")}')
        #jlib.log( '-'*80)
        if run_id != run0_id:
            break
        time.sleep(10)
    #jlib.log( '='*80)
    #jlib.log( 'New workflow is:\n{json.dumps(workflow, indent="    ")}')
    jlib.log( 'New workflow run is: {run_id=}')
    
    return run_id

        
def gh_workflow_download( id='newest', verbose: int=0, block=True, local_dir=None):
    '''
    Waits for workflow to finish if it is in progress, then downloads the
    workflow's logs and artifact.
    
    Args:
        id:
            If `newest` we use the newest workflow. Otherwise the id of the
            workflow to use.
        verbose:
            .
    
    Returns the local download directory, or empty string if workflow was
    skipped, or None if `block` is false and data is not available.
    '''
    if id == 'newest':
        id = _gh_runs_newest()
        jlib.log( '{id=}')
    workflow = gh_workflow_wait( id=id, block=block)
    #jlib.log( '{json.dumps(workflow, indent=4)=}')
    #jlib.log( '{workflow=}')
    #jlib.log( '{workflow["conclusion"]=}')
    if not workflow:
        assert not block
        return
    name = workflow["name"].replace( ' ', '-')
    created_at = workflow["created_at"]
    created_at = created_at.replace(':', '-')   # colons are a pain in unix shell.
    created_at = created_at.replace('T', '-')
    local_dir_infix = f'{local_dir}/' if local_dir else ''
    root = f'{g_root}/{local_dir_infix}github_workflow_{name}_{created_at}_{id}'
    os.makedirs(f'{g_root}/{local_dir_infix}', exist_ok=1)
    
    # Get logs.
    #
    jlib.log( 'Downloading logs from workflow run.')
    path_logs = f'{root}_logs'
    path_logs_zip = f'{path_logs}.zip'
    jlib.log( 'Downloading logs to:')
    jlib.log( '    {path_logs_zip}')
    jlib.log( '    {path_logs}')
    _gh_download( workflow['logs_url'], path_logs_zip)
    _unzip( path_logs_zip, path_logs)

    # Get artifact.
    #
    # First get intermedate json from workflow['artifacts_url']; this will
    # contain the url from which we download the artifact.
    #
    if workflow['conclusion'] == 'skipped':
        jlib.log('Not downloading artifact because workflow run skipped')
        return ''
    
    if 1:
        # Carry on downloading even if workflow failed, because sometimes
        # failures can be spurious and not effect the artifacts we are
        # interested in.
        pass
        
    if 0 and workflow['conclusion'] != 'success':
        jlib.log(
                'Not downloading artifact because workflow run failed, {workflow["conclusion"]=}:\n'
                '{json.dumps(workflow, indent=4)}'
                )
        raise Exception( 'Workflow run failed')
    
    path_artifact = f'{root}_artifact'
    path_artifact_zip = f'{path_artifact}.zip'
    jlib.log( 'Downloading artifact to:')
    jlib.log( '    {path_artifact_zip}')
    jlib.log( '    {path_artifact}')
    r = _gh_get( workflow['artifacts_url'])
    jlib.log( '{json.dumps(r.json(), indent=4)=}')
    artifacts = r.json()['artifacts']
    if artifacts:
        assert len( artifacts) == 1, f'{len(artifacts)=}'
        artifact = artifacts[0]
        archive_download_url = artifact['archive_download_url']
        jlib.log( '{archive_download_url=}')
        _gh_download( archive_download_url, path_artifact_zip)
        _unzip( path_artifact_zip, path_artifact)
    else:
        jlib.log('No artifacts available.')

    if workflow['conclusion'] != 'success':
        jlib.log(
                'Workflow run failed, {root=} {workflow["conclusion"]=}:\n'
                '{json.dumps(workflow, indent=4)}'
                )
        raise Exception( 'Workflow run failed')
    return root
    

def _gh_artifact( branch='main', download=False, verbose=False):
    '''
    [This is probably obsolete] Gets most recent github artifact (e.g. wheels)
    on specified branch.
    
    Currently this is clumsier than `gh_workflow()`.
    
    Args:
        branch:
            If not empty string, restrict to artifacts from this branch.
        download:
            If true we download and extract newest artifact if not present
            locally.
        verbose:
            .
    
    See: https://docs.github.com/en/rest/actions/artifacts
    '''
    url = f'{g_gh_pymupdf_url}/actions/artifacts'
    response = _gh_get( url)
    assert response.status_code == 200, f'response.status_code={response.status_code}'
    artifacts = response.json()['artifacts']
    
    # Put matching artifacts into `artifacts2`.
    artifacts2 = []
    for i, artifact in enumerate( artifacts):
        if not branch or artifact["workflow_run"]["head_branch"] == branch:
            artifacts2.append( artifact)
    artifacts2.sort( key=lambda artifact: _timestring_to_time( artifact["created_at"]))
    if verbose:
        jlib.log( 'Matching artifacts are (oldest first):')
        for i, artifact in enumerate( artifacts2):
            print( i)
            json.dump( artifact, sys.stdout, indent='    ')
            print()
    
    if download:
        assert g_gh_token
        artifact = artifacts2[-1]
        jlib.log( 'Artifact is:')
        json.dump( artifact, sys.stdout, indent=4)
        print()
        
        artifact_id = artifact['id']
        path = f'{g_root}/github_artifacts/{artifact_id}'

        # Write json info to local file.
        path_json = f'{path}.json'
        if os.path.exists( path_json):
            pass
        else:
            path_json_ = f'{path_json}_'
            jlib.fs_remove( path_json_)
            jlib.fs_ensure_parent_dir( path_json_)
            with open( path_json_, 'w') as f:
                json.dump( artifact, f)
            os.rename( path_json_, path_json)

        # Download .zip if not already present.
        path_zip = f'{path}.zip'
        if os.path.exists( path_zip):
            jlib.log( 'Not downloading because already exists: {path_zip}')
        else:
            _gh_download(
                    f'{g_gh_pymupdf_url}/actions/artifacts/{artifact_id}/zip',
                    path_zip,
                    )
            jlib.log( 'Have downloaded to: {path_zip}')
            # Remove any existing extracted directory.
            jlib.fs_remove( path)

        # Extract zip if not already extracted.
        if os.path.exists( path):
            jlib.log( 'Not extracting because already exists: {path}')
        else:
            jlib.log( 'Extracting:')
            jlib.log( '    from: {path_zip}')
            jlib.log( '    to: {path}')
            path_ = f'{path}_'
            jlib.fs_ensure_empty_dir( path_)
            z = zipfile.ZipFile( path_zip)
            z.extractall( path_)
            os.rename( path_, path)

def gh_workflows():
    '''
    Shows and returns available workflows.
    '''
    # Get list of available workflows.
    url = f'{g_gh_pymupdf_url}/actions/workflows'
    r = _gh_get( url)
    response = r.json()
    jlib.log( 'Available workflows are:\n{json.dumps(response, indent="    ")}')
    return response


def gh_workflow_wait( id, block=True):
    '''
    Waits for specified workflow run to finish.
    '''
    if block:
        jlib.log( 'Waiting for workflow to finish: {id=}')
    t0 = time.time()
    dt = 0
    while 1:
        time.sleep( dt)
        t = time.time() - t0 + 1
        dt = t / 50
        dt = max( dt, 10)
        dt = min( dt, 5*60)
        try:
            run = _gh_workflow( id=id)
        except Exception as e:
            jlib.log(f'Ignoring failure to get Github workflow information: {e}')
            continue
        status = run[ 'status']
        if block:
            jlib.log( 'Workflow run {id=}: {t:.1f=} {dt:.1f=} {status=}')
        else:
            jlib.log( 'Workflow run {id=}: {status=}')
        if status == 'completed':
            jlib.log( 'Workflow has finished: {id=}')
            return run
        if not block:
            return


def gh_branch_info( branch, verbose=True):
    '''
    Show detailed info about a github branch.
    '''
    r = _gh_get( f'{g_gh_pymupdf_url}/branches/{branch}')
    ret = r.json()
    if verbose:
        jlib.log( json.dumps(ret, indent='    '), nv=0)
    return ret


def gh_community( verbose=True):
    '''
    Gets github community/profile info.
    '''
    r = _gh_get( f'{g_gh_pymupdf_url}/community/profile')
    ret = r.json()
    if verbose:
        jlib.log( json.dumps(ret, indent='    '), nv=0)
    return ret


def upload_pypi( paths, pypi_test: bool=False):
    '''
    Uploads .tgz and .whl files to pypi.org.
    
    Use `pypi_token()` to set the token to use.
    
    Args:
        paths:
            Comma-separated list of files and directories. We look for .tgz and
            .whl files.

            If `pypi_test` is false, to avoid uploading incomplete or otherwise
            bad files, we assert that there is exactly one .tgz file and
            multiple .whl files and no other files.
        pypi_test:
            If true (the default) we upload to test.pypi.org. Otherwise we
            upload to pypi.org.
    '''
    num_tgz = 0
    num_whl = 0
    num_other = 0
    files = []
    
    if isinstance( paths, str):
        paths = paths.split(',')
    
    def get_paths():
        for path in paths:
            if os.path.isfile(path):
                yield path
            else:
                for leaf in os.listdir( path):
                    yield os.path.join( path, leaf)
    
    for path in get_paths():
        if path.endswith( '.tar.gz'):
            num_tgz += 1
        elif path.endswith( '.whl'):
            num_whl += 1
        else:
            num_other +=1
        files.append( path)
    jlib.log( '{=num_tgz num_whl num_other}')
    #if not pypi_test:
    #    assert num_tgz == 1
    assert num_other == 0
    pypackage.upload0( files, pypi_test, token=g_pypi_token, prompt=True)


def _test_pypi_token( token=None, pypi_test=False):
    '''
    Not usable. Intended to check that `token` works with pypi.org or
    test.pypi.org. But don't know what params to use.
    '''
    if pypi_test:
        url = 'https://upload.test.pypi.org/legacy/'
    else:
        url = 'https://upload.pypi.org/legacy/'
    r = requests.get( url)
    try:
        r.raise_for_status()
    except Exception as e:
        raise Exception( str(r.json())) from e
    jlib.log( '{json.dumps( ret, indent=4)=}')


def upload_pypi_pymupdf( directory, pypi_test: bool=False, doit: bool=True):
    '''
    Uploads PyMuPDF wheels and sdist. Checks wheel and sdist sizes look ok.
    '''
    jlib.log( '{pypi_test=}')
    jlib.log( '{directory=}')
    n_wheels = 0
    n_sdists = 0
    for i in sorted(glob.glob( f'{directory}/*')):
        assert os.path.basename(i).startswith( ('PyMuPDF-', 'PyMuPDFb-')), \
                f'Unexpected filename: {i!r}'
        s = jlib.fs_filesize( i)
        ss = jlib.number_sep(s)
        jlib.log( f'Size={ss:>12}: {i!r}')
        assert s > 0, f'Zero-length file: {i!r}'
        if i.endswith( '.whl'):
            n_wheels += 1
            if os.path.basename(i).startswith( 'PyMuPDF-'):
                if s < 2.5*2**20 or s > 4.5*2**20:
                    jlib.log(f'## Wheel size {ss} looks wrong: {i!r}')
            elif os.path.basename(i).startswith( 'PyMuPDFb-'):
                if s < 8*2**20 or s > 20*2**20:
                    jlib.log(f'## Wheel size={s} looks wrong: {i!r}')
            else:
                assert 0, f'Unrecognised wheel name: {i!r}'
        elif i.endswith( '.tar.gz'):
            n_sdists += 1
            #assert s > 50*2**20 and s < 150*2**20, f'Sdist size={s} looks wrong: {i!r}'
            # 2024-02-29: Omit mupdf source from sdist.
            if os.path.basename(i).startswith( 'PyMuPDF-'):
                if s < 15*2**20 or s > 30*2**20:
                    jlib.log(f'## sdist size={s} looks wrong: {i!r}')
            elif os.path.basename(i).startswith( 'PyMuPDFb-'):
                if s < 20*2**10 or s > 50*2**10:
                    jlib.log(f'## sdist size={s} looks wrong: {i!r}')
            else:
                assert 0, f'Unrecognised sdist name: {i!r}'
        else:
            assert 0, f'Unexpected file is not wheel or sdist: {i!r}'
    jlib.log( '{n_sdists=} {n_wheels=}')
    # This will need updating to include arm64 wheels.
    #assert n_wheels == 20, f'{n_wheels=}'
    #assert n_sdists == 1, f'{n_sdists=}'
    if doit:
        if not pypi_test:
            while 1:
                yes = input( jlib.log_text( 'Will upload to pypi.org. Enter "yes" if you are sure... ? ', nl=0))
                if yes == 'yes':
                    break
        upload_pypi( directory, pypi_test)


def gh_release_linux_aarch64(    
        branch='main',
        upload='pypi',
        check=True,
        check_gitdiff=True,
        parallel=True,
        pyvs=None,
        PYMUPDF_SETUP_MUPDF_BUILD=None,
        pymupdf='PyMuPDF',
        ):
    '''
    Builds/uploads just Linux-aarch64 wheels, using gh_release().
    '''
    gh_release(
            branch=branch,
            sdist = False,
            wheels_linux_aarch64 = True,
            wheels_linux_auto = False,
            wheels_linux_pyodide = False,
            wheels_macos_arm64 = False,
            wheels_macos_auto = False,
            wheels_windows_auto = False,
            upload = 'pypi',
            check = check,
            parallel = parallel,
            pyvs = pyvs,
            PYMUPDF_SETUP_MUPDF_BUILD = PYMUPDF_SETUP_MUPDF_BUILD,
            pymupdf = pymupdf,
            check2 = False,
            check_gitdiff = check_gitdiff,
            )
    

def gh_release(
        branch='main',
        sdist: bool=True,
        wheels_linux_aarch64: bool=False,
        wheels_linux_auto: bool=True,
        wheels_linux_pyodide: bool=True,
        wheels_macos_arm64: bool=False,
        wheels_macos_auto: bool=True,
        wheels_windows_auto: bool=True,
        download=True,
        upload='pypi',
        check=True,
        parallel=True,
        pyvs=None,
        PYMUPDF_SETUP_MUPDF_BUILD=None,
        PYMUPDF_SETUP_MUPDF_BUILD_TYPE=None,
        pymupdf='PyMuPDF',
        check2=True,
        check_clean_tree=True,
        check_gitdiff=True,
        dual=True,
        ):
    '''
    Makes a release of PyMuPDF by building sdist and wheels on github, running
    PyMuPDF's pytest tests with each wheel, downloading sdist and wheels to
    local machine and then using `twine` to upload from local machine to
    pypi.org.
    
    Args:
        branch:
            Branch on github from which to make the release.
        sdist:
            Whether to build the sdist.
        wheels_linux_auto:
            Whether to build wheels for linux.
        wheels_linux_aarch64:
            Whether to build wheels for linux-aarch64 (these take a long time
            to build).
        wheels_macos_auto:
            Whether to build wheels for macos. As of 2024-05-08 this is used
            for both x64 and arm64.
        wheels_macos_arm64:
            Whether to build wheels for macos-arm64.
            As of 2024-05-08 this is ignored, because we seem
            to instead need to use macos-13/14 for x86/arm64. See:
            https://cibuildwheel.pypa.io/en/stable/faq/#how
        wheels_windows_auto:
            Whether to build wheels for windows.
        upload:
            None or empty string:
              Do not upload anything.
            'pypi'
              Upload sdist and wheels to `pypi.org`.

              We assert:

              * `sdist`, `wheels`, `wheels_linux_aarch64` and
                `wheels_macos_arm64` are all true.
              * `branch` is 'main'.
            'pypi_test':
              Upload sdist and wheels to `test.pypi.org`.
        check:
            If true (the default) we check version numbers and dates in
            documentation with `check_release()` before building.
        parallel:
            if true we run a separate workflow for each Python version, to
            avoid Githubs's 6h limit on individual workflow steps.
        pyvs:
            List or comma-separated string of python versions to build
            for. Default `None` builds for all supported versions.
        PYMUPDF_SETUP_MUPDF_BUILD:
            If not None, used for value of environment variable
            `PYMUPDF_SETUP_MUPDF_BUILD`.
        PYMUPDF_SETUP_MUPDF_BUILD_TYPE:
            .
        pymupdf:
            Location of local checkout.
        check2:
            .
        check_clean_tree:
            If true we require that current tree is identical to remote tree
            after push. Otherwise we require that current commited tree is
            identical to remote tree after push, i.e. we allow uncommited
            changes.
        dual:
            .
    
    **How a release is made:**
    
    *
        We assert that `g_gh_token` is set so that we can run github Actions.
        For example use `auto_tokens()`, or `pypi_token()` and `gh_token()`.
    *
        If `upload` is true, we assert that `g_pypi_token` is set so that we
        can upload to pypi.org.

        But note that we do not check that `g_pypi_token` will actually work
        with the destination (pypi.org or test.pypi.org), because we don't know
        how to do this; the pypi docs don't explain how the upload rest api
        works in any useful detail.
    *
        We check that the local PyMuPDF checkout is identical to the remote
        github `branch` HEAD.

        Default github repository can be changed with `gh_pymupdf_url()`, for
        example::
        
            gh_pymupdf_url https://api.github.com/repos/ArtifexSoftware/PyMuPDF-julian
    
    *
        We call `check_release()` to check the consistency of version numbers
        and dates etc of the local PyMuPDF checkout. Because we have checked
        that the local checkout is identical to the one on github, this
        guarantees that our build on github will have the correct version
        numbers and dates.

        Then:

        *
            If `upload` is `pypi` and we are not releasing a release candidate,
            we assert that we are doing a complete upload of sdist and all
            wheels, i.e. `sdist`, `wheels_linux_auto` etc are all true.
        * 
            2023-11-25: we no longer assert wheels_linux_aarch64 because these
            wheels takes ages to build and it is convenient to build and
            uploaded them separately.
        *
            We set a build going on github and wait for it to complete. This
            takes 5-6 hours, mainly because linux-aarch64 builds on Github
            are very slow. 2023-11-03: building linux-aarch64 release took
            just 3h40m; the improvement might be from recent changes to build
            mupdf's C library with `make -j`.
        *
            We then download the github artifact containing the sdist and
            wheels to the local machine.
        *   Then if `upload` is true:
      
        *
            If the upload is to pypi.org (not test.pypi.org) we ask for
            explicit 'yes' confirmation before continuing.
        *
            Then we upload sdist and wheels to `pypi.org` or `test.pypi.org`.
            This shows the twine command to be run and asks for confirmation
            before running it.
    
    **Before making a release:**

    *
        A note about Github issues.

        In general we do not close Github issues until the fix has been
        included in a release. Instead we mark the issue with label `Fixed in
        next release`.

        This allows us to ensure that changes.txt is correctly updated in each
        release, and makes things clear to users.

        However Github annoyingly automatically closes issues if an associated
        commit mentions the word `Fixed`, so this is a little fragile,
        requiring manual re-opening of the issue.

        So it is also recommended to keep changes.txt up to date as fixes are
        pushed to PyMuPDF main branch.
    *
        Check that the list of fixed bugs in changes.txt agrees with Github's
        Issues list:
    *
        Check each issues marked as fixed in changes.txt is labelled as 'Fixed
        in next release' (or 'Fix developed') on Github.
    *
        Check that each Github issue labelled as 'Fixed in next release' is
        marked as fixed in changes.txt.
    *
        Automated checking that that version numbers and dates are consistent:

        *
            Update pymupdf.py's hard-coded `g_release_pymupdf_version` and
            `g_release_mupdf_version` to match the new release.
        *
            Run::

                ./julian-tools/pymupdf.py check_release

            This will check that version numbers and dates are correct in all
            files within the repository. If there any errors, fixup and repeat.

    *   Optional: run `pymupdf.py build_test_gh`.
    *
        It can be convenient to put github and pypi.org tokens into local files
        and use them with `auto_tokens`.
    *
        If releasing from a non-standard Github repository, specify it before
        `gh_release`, e.g. with::

            gh_pymupdf_url https://api.github.com/repos/ArtifexSoftware/PyMuPDF-rebased

    **Build the release and upload it to Pypi:**

    *   Push release tree to Github.
    
    *   Release directly to pypi.org::

            ./julian-tools/pymupdf.py auto_tokens gh_release | tee out-gh_release
        
        *
            This will build wheels and download them to local machine, and ask
            for confirmation before uploading the wheels to pypi.org.
        *
            Before confirming, one can optionally do some manual testing of the
            wheels.
    *
        Or release to test.pypi.org::

            ./julian-tools/pymupdf.py auto_tokens --pypi_test=1 gh_release --upload pypi_test

        This allows manual testing of wheel installation from test.pypi.org
        before uploading to pypi.org.

        [Ideally this would allow the release on test.pypi.org to be updated if
        problems occur, but oddly this is not possible - test.pypi.org seems
        to use the same rules as pypi.org where uploaded wheels can never be
        overwritten.]

    *
        Upload Pyodide wheels to make them available.
        
        Look for line in output with local location of Pyodide wheels, such
        as::
        
            Pyodide wheel(s):
                leaf='PyMuPDF-1.24.4-cp311-none-emscripten_3_1_32_wasm32.whl' paths=['/home/jules/artifex/gh_release-9207503597/github_workflow_Build-Pyodide-wheel_2024-05-23-11-56-09Z_9207503597_artifact/PyMuPDF-1.24.4-cp311-none-emscripten_3_1_32_wasm32.whl']
    *
        Test wheels manually, e.g. with::

            python -m pip install -i https://test.pypi.org/simple/ PyMuPDF

    If something goes wrong after actions have been started, one can resume::
    
    *   Look for a line like this in the output:
    
            Repeat with: ./julian-tools/pymupdf.py gh_release_wait_upload --upload pypi --download=True 9020363949,9020366427,9020369174,9020371262,9020373598
    
    *   
        Run the command. It will re-download only those files that are not
        present on the local machine, so this is efficient.

    Alternatively to redo just the upload:
    
    *   Manually upload the wheels to pypi.org:

        *
            Find the local directory containing the wheels, something like::
                
                gh_release-7658240528-7658242583-7658244724-7658247845-7658250730-union
        
        *   Upload to pypi.org::

                ./julian-tools/pymupdf.py auto_tokens upload_pypi gh_release-7658240528-7658242583-7658244724-7658247845-7658250730-union

    * Upload problems.
    
      On 2024-05-30 we had this error:
             
          PyMuPDF-1.24.5-cp38-none-manylinux2014_x86_64.whl
          WARNING  Error during upload. Retry with the --verbose option for more details.                                                                                                                   
          ERROR    HTTPError: 400 Bad Request from https://upload.pypi.org/legacy/                                                                                                                          
                   The digest supplied does not match a digest calculated from the uploaded file.
    
      Retrying eventually worked.
    
    **Things to do after making a release:**

    *
        Check that pypi.org page: https://pypi.org/project/PyMuPDF/#history
        shows the new release.

    *   Do basic check that installation with pip works:

        *   If release candidate, specify the version explicitly::

                pip install PyMuPDF==1.21.0rc1
                python
                >> import fitz
                >> fitz.version

        * Otherwise just upgrade::

                pip install --upgrade PyMuPDF
                python
                >> import fitz
                >> fitz.version

        *   Use `pip install -i https://test.pypi.org/simple ...` for test.pypi.org.

        *
            If a PyMuPDF checkout is available, run tests (required packages
            are listed in `PyMuPDF:scripts/gh_release.py:test_packages`)::

                pip install pytest fontTools psutil pymupdf-fonts flake8 pillow
                pytest ./PyMuPDF
    *
        Tag the release (`<tagname>` is the raw version, e.g. `1.20.2`) and
        push the tag to github::

            git tag <tagname>
            git push origin <tagname>

        (This will also create `.zip` and `.tar.gz` archives for download from
        github.)

    *   For full releases (and possibly release-candidates also):

        *   Start build and upload Linux aarch64 wheels with::

                ./julian-tools/pymupdf.py auto_tokens gh_release_linux_aarch64 |tee out-gh_release_b
    
        *   Mark the release on github:

            *   Go to: https://github.com/pymupdf/PyMuPDF/releases
            *   Click `Draft a new release`.
            *   In `Choose a tag`, select the tag `<tagname>`.
            *   In `Release title` enter `PyMuPDF-<tagname> released`.
            *   Paste the release's changelog into the text field.

                *
                    Modify any ReST-style links to work as markdown, e.g. rely
                    on Github interpreting `#1234` as a link to issue 1234.
                *
                    Add header explaining pip install, similar to previous
                    releases.
            *
                Ensure that `Set as the latest release` is checked.
            *
                Ensure that `Create a discussion for this release` is checked,
                with `Category: Announcements`. [Alternatively create an
                announcement discussion separately.]
            *
                Click on 'Publish release'.
            *
                Go to Discussions and pin the announcement discussion so that
                it appears at the top of the discussion page.
            *
                Unpin previous release's announcement discussion if present.
            *
                For all github issues that are labeled as 'fixed in next
                release':

                *   Check that they are mentioned in the release's changelog.
                *   Remove the `Fixed in next release` label.
                *   Close the issue with text `Fixed in 1.22.x`.

            *
                Check in the announcement that all fixed issues are shown as
                closed.

            *
                If the new release uses a new version of MuPDF, optionally
                remove all code that is specific to the previous major release
                of MuPDF. E.g. grep for `FZ_VERSION` or `mupdf_version_tuple`.

            *   Update pymupdf.readthedocs.io:

                *
                    Check that
                    https://pymupdf.readthedocs.io/en/latest/changes.html has
                    the latest release's changelog.
                *   Add the release tag:

                    *   Log in to readthedocs.
                    *
                        In `Versions` / `Activate a version`, look for release
                        tag and click on `Activate`. Choose `Active` and `Not
                        hidden`.
                    *
                        In `Builds`, select the release tag and click on `Build
                        version`.
        
        *   When build and upload Linux aarch64 wheels has finished:

            Update release discussion to say that Linux aarch64 wheels are now
            available, e.g. with a post saying::

                Linux-aarch64 wheels are now available; install in the usual way with pip.
    
    '''
    assert g_gh_token, 'Github build requires github token'
    if upload:
        # Make checks now so we detect problems before doing the build.
        #
        assert g_pypi_token, 'Uploading requires pypi token'
        if upload == 'pypi':
            if check2 and 'rc' not in g_release_pymupdf_version:
                if ( sdist
                        #and wheels_linux_aarch64
                        and wheels_linux_auto
                        #and wheels_linux_pyodide
                        #and wheels_macos_arm64
                        and wheels_macos_auto
                        and wheels_windows_auto
                        ):
                    pass
                else:
                    assert 0, jlib.log_text(
                        'Upload of non-release-candidate to pypi.org'
                            ' must include sdist and all wheels:'
                            ,
                        nl=0,
                        )
            pypi_test = False
        elif upload == 'pypi_test':
            pypi_test = True
        else:
            assert 0, f'Unrecognised upload={upload!r}'
    
    if pyvs is None:
        pyvs = '38,39,310,311,312'
    if isinstance(pyvs, str):
        pyvs = pyvs.split(',')
    for pyv in pyvs:
        assert re.match('^[0-9]+$', pyv), f'Bad python version specifier: {pyv=}.'
    
    # Get HEAD of `branch` on github and check it matches local checkout.
    sha = gh_branch_info( branch, verbose=0)['commit']['sha']
    d = os.path.relpath( f'{g_root}/{pymupdf}')
    e, text = jlib.system(
            f'cd {d} && git diff {"" if check_clean_tree else "--cached"} {sha}',
            raise_errors=0,
            out=[('log', 'git diff: '), ('return', '')],
            verbose=1,
            )
    jlib.log( '{e=} {text!r=}')
    # Seems that, unlike `diff`, `git diff` always returns 0, even if diff is
    # non-empty? So we need to look at output `text`, not `e`.
    if text:
        if check_gitdiff:
            raise Exception( f'Local checkout {d} differs from remote sha={sha}')
        else:
            jlib.log(f'Ignoring diff between local repository and github.')
    jlib.log( 'Local tree {d} is identical to remote {branch=} {sha=}.')
    
    # Check version numbers and date of local PyMuPDF checkout. This is known
    # to be identical to remote `branch` so this is also checking what we will
    # be building from on github,
    #
    if check:
        check_release()

    url = f'{g_gh_pymupdf_url}/actions/workflows/build_wheels.yml/dispatches'
    
    # We only want to upload to pypi if building sdist and all wheels
    # from branch main.
    #
    if upload == 'pypi' and check2:
        assert sdist
        #assert wheels_linux_aarch64
        assert wheels_linux_auto
        #assert wheels_linux_pyodide
        #assert wheels_macos_arm64
        assert wheels_macos_auto
        assert wheels_windows_auto
        assert branch == 'main'

    if parallel:
        
        # Run separate workflow for each python version, to avoid
        # running into Github's 6 hour limit for workflows.
        #
        
        # Start workflows.
        #
        workflow_ids_string = ''
        ids = list()
        def datas():
            jlib.log(f'{pyvs=}')
            for i, pyv in enumerate(pyvs):
                
                def data_true(data):
                    for n, v in data['inputs'].items():
                        if n == 'flavours':
                            pass
                        elif v == 'true':
                            return True
                
                data = dict(
                        ref=branch,
                        inputs=dict(
                            sdist='true' if i==0 and sdist else 'false',
                            wheels_linux_aarch64    ='true' if wheels_linux_aarch64 else 'false',
                            wheels_linux_auto       ='true' if wheels_linux_auto    else 'false',
                            #wheels_macos_arm64      ='true' if wheels_macos_arm64   else 'false',
                            wheels_macos_auto       ='true' if wheels_macos_auto    else 'false',
                            wheels_windows_auto     ='true' if wheels_windows_auto  else 'false',
                            wheels_cps = f'cp{pyv}*',
                            flavours                ='true' if dual else 'false',
                            )
                        )
                data['inputs']['wheels_linux_aarch64'] = 'false'
                if PYMUPDF_SETUP_MUPDF_BUILD is not None:
                    data['inputs']['PYMUPDF_SETUP_MUPDF_BUILD'] = PYMUPDF_SETUP_MUPDF_BUILD
                if PYMUPDF_SETUP_MUPDF_BUILD_TYPE is not None:
                    data['inputs']['PYMUPDF_SETUP_MUPDF_BUILD_TYPE'] = PYMUPDF_SETUP_MUPDF_BUILD_TYPE
                if data_true(data):
                    yield data

                # Build linux-aarch64 in separate workflow, because can be
                # very slow.
                if wheels_linux_aarch64:
                    for k, v in data['inputs'].items():
                        if k != 'flavours' and v == 'true':
                            data['inputs'][k] = 'false'
                    data['inputs']['wheels_linux_aarch64'] = 'true'
                    if data_true(data):
                        yield data

        for data in datas():
            jlib.log('Starting new workflow for with {data=}.')
            id_ = gh_run_workflow(
                    f'{g_gh_pymupdf_url}/actions/workflows/build_wheels.yml/dispatches',
                    data
                    )
            ids.append( id_)
            if workflow_ids_string:
                workflow_ids_string += ','
            workflow_ids_string += id_
            jlib.log(f'Workflow id for {pyv=} is: {id_=}.')
        jlib.log(f'{workflow_ids_string=}')
        
        if wheels_linux_pyodide:
            # Pyodide wheels are built in separate workflow using test_pyodide.yml.
            data = dict(ref=branch)
            if PYMUPDF_SETUP_MUPDF_BUILD is not None:
                data['inputs']['PYMUPDF_SETUP_MUPDF_BUILD'] = PYMUPDF_SETUP_MUPDF_BUILD
            id_ = gh_run_workflow(
                    f'{g_gh_pymupdf_url}/actions/workflows/test_pyodide.yml/dispatches',
                    data,
                    )
            ids.append( id_)
            if workflow_ids_string:
                workflow_ids_string += ','
            workflow_ids_string += id_
            jlib.log(f'Workflow id for Pyodide is: {id_=}.')
        
        jlib.log(f'Calling gh_release_wait_upload with {ids=} {upload=}.')
        local_dir = gh_release_wait_upload(ids, upload, download=download)
        jlib.log(f'{local_dir=}')
        return local_dir
                    
    else:
        # Not parallel.
        #
        # Find the most recent run before we schedule our new run, so we can
        # detect the new run afterwards.
        #
        run0_id = _gh_runs_newest()
        
        if pyvs:
            pyvs2 = ''
            for pyv in pyvs:
                pyvs2 += f'cp{pyv}* ';
            pyvs2 = pyvs2.strip()
            jlib.log( '{pyvs2=}')
            data['inputs']['wheels_cps'] = pyvs2
    
        # Start new run by triggering the 'build wheels' workflow.
        #
        # https://docs.github.com/en/rest/actions/workflows#create-a-workflow-dispatch-event
        #
        r = _gh_post( url, json=data)
        if 0:
            jlib.log( '{r=}')
            jlib.log( '{r.text!r=}')
            jlib.log( '{r.json()!r=}')
            jlib.log( '{r.content!r=}')
            jlib.log( '{r.raw!r=}')
            jlib.log( '{r.headers!r=}')
            jlib.log( '{json.dumps(r.headers,indent="    ")=}')
        jlib.log( 'Have started new workflow run: {url=}')
        
        # Unfortunately `r` does not contain any information about the id of
        # the run we have just created. E.g. see:
        #
        #   https://stackoverflow.com/questions/69479400/get-run-id-after-triggering-a-github-workflow-dispatch-event
        #
        # That link has a way to embed a uid in the new run and detect it. But
        # for now we'll do things more crudely - repeatedly look for a new run
        # - it will be most recent run with different id from workflow0.
        #
        jlib.log( 'Polling for first mention of the new workflow run we have just created...')
        while 1:
            run_id = _gh_runs_newest()
            jlib.log( '{=run0_id run_id}')
            #jlib.log( 'workflow:\n{json.dumps(workflow, indent="    ")}')
            #jlib.log( '-'*80)
            if run_id != run0_id:
                break
            time.sleep(10)
        #jlib.log( '='*80)
        #jlib.log( 'New workflow is:\n{json.dumps(workflow, indent="    ")}')
        jlib.log( 'New workflow run is: {run_id=}')
        
        # Wait for run to finish and download sdist and wheels to local
        # machine.
        #
        jlib.log( 'Waiting for new workflow to finish, then download sdist and wheels to local machine.')
        if download:
            root = gh_workflow_download( id=run_id)
        else:
            gh_workflow_wait( id=run_id)
            return
        
        directory = f'{root}_artifact'
        jlib.log( '{root=}')
    
        jlib.log( '{directory=}')

        if upload:
            upload_pypi_pymupdf( directory, pypi_test=pypi_test)


def gh_release_wait_upload(ids, upload='pypi', download=True):
    '''
    Wait for workflows to finish, downloads to local machine, uploads to
    pypi.
    
    Returns local directory containing downloaded sdists and wheels.
    
    Can be safely run multiple times with the same `ids` - we don't re-download
    the same workflow results.
    
    ids:
        List, or comma/dash-separated string, of ids.
    '''
    jlib.log('{ids=}')
    if isinstance(ids, str):
        if ',' in ids:
            ids = ids.split(',')
        elif '-' in ids:
            ids = ids.split('-')
    else:
        ids = ids.copy()
    jlib.log('{ids=}')
    jlib.log('{",".join(ids)=}')
    ids0 = ids.copy()
    local_dir = f'gh_release-{"-".join(ids)}'
    local_dir_union = f'gh_release-{"-".join(ids)}-union'
    jlib.log('{ids=} {local_dir=}')
    directorys = [None] * len(ids)
    jlib.log( 'Waiting for workflows to finish: {ids=}')
    jlib.log(f'Repeat with: {sys.argv[0]} gh_release_wait_upload --upload {upload} --download={download} {",".join(ids)}')
    t0 = time.time()
    dt = 0
    num_workflows_finished = 0
    errors = list()
    while 1:
        time.sleep( dt)
        t = time.time() - t0 + 1
        dt = t / 50
        dt = max( dt, 10)
        dt = min( dt, 5*60)
        for i, id in enumerate(ids):
            if not id:
                continue
            e = None
            directory = None
            if download:
                try:
                    directory = gh_workflow_download(id=id, block=False, local_dir=local_dir)
                except Exception as ee:
                    e = ee
                    ee_detailed = jlib.exception_info(file="return")
                    jlib.log( 'Workflow failed: {id=} {ee}:\n{ee_detailed}')
                    errors.append(e)
                if directory or directory == '':
                    jlib.log( 'Workflow run has finished: {id=} {directory=}')
                    directorys[i] = directory + '_artifact' if directory else ''
                if e or directory or directory == '':
                    ids[i] = None
                    num_workflows_finished += 1
                    jlib.log( f'{len(ids)=} {num_workflows_finished=}')
                    if num_workflows_finished == len(ids):
                        break
            else:
                run = gh_workflow_wait(id=id, block=False)
                if run:
                    ids[i] = None
                    num_workflows_finished += 1
                    jlib.log( f'{len(ids)=} {num_workflows_finished=}')
                    if num_workflows_finished == len(ids):
                        break
        if num_workflows_finished == len(ids):
            break

    if not download:
        jlib.log('All workflow(s) have finished. Not downloading/uploading.')
        for id_ in ids0:
            run = _gh_workflow( id=id_)
            #jlib.log(f'Info for {id_=}:\n{json.dumps(run, indent=4)}', nv=0)
            jlib.log(f'URL for {id_=}: {run.get("html_url")}')
        return
        
    jlib.log('Have downloaded wheels for each python version:')

    # Show contents of download directory.
    #
    leaf_to_paths = dict()
    for directory in directorys:
        if not directory:
            continue
        jlib.log( f'    {directory}/')
        for leaf in os.listdir(directory):
            path = os.path.join(directory, leaf)
            jlib.log( '        {path}')
            paths = leaf_to_paths.setdefault(leaf, []).append(path)

    # Some files such as sdist and PyMuPDFb-*.whl are generated by
    # each python-version-specific build.
    #
    # To be sure things are working, we check that these duplicate
    # files contain identical file tree. (The zip/tar files
    # themselves can be different due to being created with
    # different datestamps, so we extract each one and compare with
    # command-line diff -r.)
    #
    jlib.log('Checking duplicate sdist/wheels')
    num_diffs = 0
    for leaf in sorted(leaf_to_paths.keys()):
        paths = leaf_to_paths[ leaf]
        jlib.log('{leaf}:')
        try:
            #def i_path(i):
            #    return f'pymupdf-temp-{i}'
            extracted_paths = []
            for i, path in enumerate(paths):
                st = os.stat(path)
                with open(path, 'rb') as f:
                    data = f.read()
                md5 = hashlib.md5(data)
                jlib.log( f'    size={jlib.number_sep(st.st_size):>12} {md5.hexdigest()=} {path}')
                extracted_path = f'pymupdf-temp-{i}-{os.path.basename(path)}'
                jlib.fs_ensure_empty_dir(extracted_path)
                #jlib.log('Extracting to temporary: {path} -> {path_extracted}')
                if path.endswith( '.whl'):
                    z = zipfile.ZipFile(path)
                    z.extractall(extracted_path)
                elif path.endswith( '.tar.gz'):
                    z = tarfile.open(path)
                    z.extractall(extracted_path)
                else:
                    assert 0, f'Unrecognised suffix: {path=}'
                extracted_paths.append(extracted_path)
            # Looks like cibuildwheel or auditwheel on Linux might be
            # reordering the items in RECORD files, so we sort them here.
            for path in extracted_paths:
                for dirpath, dirnames, filenames in os.walk(path):
                    for filename in filenames:
                        if filename == 'RECORD':
                            p = os.path.join(dirpath, filename)
                            jlib.log(f'Sorting: {p}')
                            t = jlib.fs_read(p)
                            t2 = t.split('\n')
                            t2.sort()
                            t3 = '\n'.join(t2)
                            assert len(t3) == len(t)
                            jlib.fs_write(p, t3)
            nd = 0
            for i in range(len(extracted_paths)-1):
                excludes = list()
                excludes_text = ''
                if 'win32' in leaf or 'win_amd64' in leaf:
                    # Windows .dll's seem to contain timestamps so are
                    # not identical between builds.
                    excludes += ['*.dll', 'RECORD']
                if 'macosx_11_0_arm64' in leaf:
                    # 2024-03-21: libmupdf.dylib sometimes differ.
                    excludes += [
                            'libmupdf.dylib',
                            'RECORD',
                            'libmupdfcpp.so',   # Added 2024-05-08.
                            ]
                for ex in excludes:
                    excludes_text += f' -x "{ex}"'
                e = jlib.system(
                        f'diff -qr {excludes_text} {extracted_paths[i]} {extracted_paths[i+1]}',
                        raise_errors=0,
                        out='log',
                        )
                if e:
                    nd += 1
                if excludes:
                    e = 0
                    for dirpath, dirnames, filenames in os.walk(extracted_paths[i]):
                        for filename in filenames:
                            match = 0
                            for exclude in excludes:
                                import fnmatch
                                if fnmatch.fnmatch( filename, exclude):
                                    match = 1
                                    break
                            if match:
                                p1 = os.path.join(dirpath, filename)
                                assert p1.startswith(extracted_paths[i])
                                p2 = extracted_paths[i+1] + p1[len(extracted_paths[i]):]
                                jlib.log('Comparing just the sizes of {p1} and {p2}.')
                                assert p1 != p2
                                p1_size = jlib.fs_filesize(p1, -1)
                                p2_size = jlib.fs_filesize(p2, -1)
                                assert p1_size != -1 and p2_size != -1
                                if p1_size != p2_size:
                                    jlib.log(f'Files differ in size:')
                                    jlib.log(f'    {p1_size: 10}: {p1}')
                                    jlib.log(f'    {p2_size: 10}: {p2}')
                                    e = 1
                    if e:
                        nd += 1
            if nd:
                num_diffs += nd
            else:
                for extracted_path in extracted_paths:
                    assert extracted_path.startswith('pymupdf-temp-')
                    jlib.fs_remove(extracted_path)
        finally:
            for extracted_path in extracted_paths:
                if num_diffs:
                    jlib.log(f'Not removing extracted files in: {extracted_path}')
                else:
                    jlib.log(f'Would remove: {extracted_path=}')
                    #jlib.fs_remove(extracted_path)

    jlib.log('{num_diffs=}')
    assert num_diffs == 0

    pyodide_wheels = list()
    
    # Copy sdist and wheels into new directory.
    #
    jlib.log('Copying sdist and wheels into: {local_dir_union}/')
    os.makedirs(local_dir_union, exist_ok=1)
    for leaf, paths in leaf_to_paths.items():
        if g_release_pymupdfb_version != g_release_pymupdf_version:
            if leaf.startswith('PyMuPDFb'):
                jlib.log('Ignoring {leaf=} because {g_release_pymupdfb_version=} != {g_release_pymupdf_version=}')
                continue
        if 'emscripten' in leaf:
            jlib.log(f'Pyodide wheel: {leaf=} {paths=}')
            pyodide_wheels.append( (leaf, paths) )
            continue
        path = paths[0]
        jlib.system( f'rsync -ai {path} {local_dir_union}/')
    jlib.log( '{local_dir_union=}')
    
    if errors:
        raise Exception( f'One or more workflows failed: {errors}')
    # Upload.
    if upload:
        assert upload in ('pypi', 'test.pypi'), f'{upload=}'
        upload_pypi_pymupdf( local_dir_union, pypi_test=(upload=='test.pypi'))
    
    if pyodide_wheels:
        jlib.log(f'Pyodide wheel(s):')
        for leaf, paths in pyodide_wheels:
            jlib.log(f'    {leaf=} {paths=}')
    return local_dir_union
    

def gh_rate_limit():
    '''
    Show rate limits.
    '''
    r = requests.get( 'https://api.github.com/rate_limit', raise_for_status=False)
    jlib.log( '{r.status_code=}')
    try:
        r.raise_for_status()
    except Exception as e:
        # r.json() can have useful info about the error.
        raise Exception( str(r.json())) from e
    
    jlib.log( json.dumps( r.json(), indent='    '), nv=0)
    #s = json.dumps( r.json(), indent='    ')
    #jlib.log( s, nv=0)


def gh_feeds():
    '''
    Show available github feeds. If `gh_token()` has been called this will
    include additional user-specific feeds.
    '''
    r = _gh_get( f'https://api.github.com/feeds')
    ret = r.json()
    jlib.log( json.dumps(ret, indent='    '), nv=0)


def _venv_internal(pip_upgrade=True, caller=0):
    '''
    Runs calling fn inside venv.
    '''
    command = f'{sys.executable} {sys.argv[0]} venv '
    if not pip_upgrade:
        command += '--pip_upgrade=0 '
    command += aargs.fncall_text(caller+1)
    jlib.system( command)


def venv(
        command=None,
        venv=None,
        packages=None,
        system_site_packages=None,
        pip_upgrade=True,
        ):
    '''
    Runs remaining args, or the specified command if present, in a venv.
    
    venv:
        Name of venv. If None we define a default containing python version.
    packages:
        List of packages (or comma-separated string) to install.
    system_site_packages:
        Force whether to build venv with --system-site-packages. If
        None, we use True on OpenBSD.
    '''
    if venv is None:
        venv = f'venv-pymupdf-{platform.python_version()}'
    command2 = ''
    if packages:
        if isinstance(packages, str):
            packages = packages.split(',')
        command2 += 'python -m pip install ' + ' '.join(packages)
        command2 += ' && '
    if command is None:
        command = ''
        command += 'python ' + shlex.quote(sys.argv[0])
        argv, a0, a = aargs.g_current_args
        for arg in argv[a:]:
            command += ' ' + shlex.quote(arg)
        aargs.g_current_args = None
        jlib.log('Have set {command=}')
    command2 += command
    pypackage.venv_run(
            command2,
            venv=venv,
            prefix=f'{venv}: ',
            system_site_packages=system_site_packages,
            caller=1,
            pip_upgrade=pip_upgrade,
            )


def do_doctest():
    '''
    Run doctest tests in this module.
    '''
    import doctest
    jlib.log( 'Running doctest.testmod()')
    doctest.testmod()



def build_pyodide(
        remote: jtest.Remote=None,
        mupdf='mupdf',
        pymupdf='PyMuPDF',
        upload=True,
        implementations='b',
        ):
    '''
    Builds a PyMuPDF wheel for wasm using Pyodide, and uploads to casper.
    
    Args:
        
        remote:
            Remote host to use, if any. If set we sync to remote machine
            and re-run `build_pyodide()` remotely with the remaining args.
        mupdf:
            Location of MuPDF checkout.
        pymupdf:
            Location of PyMuPDF checkout.
        upload:
            If true we upload the wheel to a hard-coded location on casper.
        implementations:
            Controls whether we create classic and/or rebased
            implementation. Is used for value of PYMUPDF_SETUP_IMPLEMENTATIONS
            - see PyMuPDF:setup.py's documentation for details.
    
    To use the wheel:
    
        Upload the wheel
        (e.g. `PyMuPDF-1.22.3-cp311-cp311-emscripten_3_1_32_wasm32.whl`) to
        a webserver which has been configured to allow Cross-origin resource
        sharing (https://en.wikipedia.org/wiki/Cross-origin_resource_sharing).
        
        The wheel can be used in a Pyodide console running in a web browser, or a
        JupyterLite notebook running in a web browser.
        
        To create a Pyodide console, go to:
        
            https://pyodide.org/en/stable/console.html
        
        To create a JupyterLite notebook, go to:
        
            https://jupyterlite.readthedocs.io/en/latest/_static/lab/index.html
        
        In both these cases, use the following code to download the wheel
        (replace the example URL with the URL of the uploaded wheel) and import
        it::
        
            import pyodide_js
            await pyodide_js.loadPackage( 'https://ghostscript.com/~julian/pyodide/PyMuPDF-1.22.5-cp311-none-emscripten_3_1_32_wasm32.whl')
            import fitz
        
        Note that `micropip.install()` does not work with PyMuPDF's shared
        libraries.
        
        To create a simple JupyterLite notebook with PyMuPDF already installed
        and imported, use a .html file containing::

            <html>
            <body>

            <iframe
              src="https://jupyterlite.github.io/demo/repl/index.html?kernel=python&toolbar=1
              &code=import micropip
              &code=await micropip.install( 'https://ghostscript.com/~julian/pyodide/PyMuPDF-1.22.3-cp311-cp311-emscripten_3_1_32_wasm32.whl')
              &code=import fitz
              &code=print(f'{fitz.version=}')"
              width="100%"
              height="100%"
            >
            </iframe>

            </body>
            </html>
        
        Then you should be able to enter Python code that uses `fitz`.
        
        (Probably needs modifying to use pyodide_js.loadPackage()).
    
    Loading a PDF document from a URL into PyMuPDF:
    
        Pyodide browser console does not have generic network access so,
        for example, `urllib.request.urlopen(URL)` fails. But pyodide has a
        built-in `pyodide.http` module that uses javascript internally::
    
            import pyodide.http
            r = await pyodide.http.pyfetch('https://ghostscript.com/~julian/test_checkbox-2.pdf')
            data = await r.bytes()
            doc = fitz.Document(stream=data)
        
        It looks like this only works with `https://`, not `http://`.

    Pre-made PyMuPDF environments:
    
    * Pyodide:
    
      * https://ghostscript.com/~jamie/developers/pymupdf-console.html
      * https://ghostscript.com/~jamie/developers/pymupdf-samples.html
    
    * JupyterLite:
    
      * https://ghostscript.com/~julian/pymupdf-jupyter-lite.html
        
    More info:
    
        https://pyodide.org/en/stable/usage/loading-packages.html
        https://emscripten.org/docs/compiling/Building-Projects.html
        
        We build MuPDF without libcrypto because it is not available in
        Pyodide:
        
            https://github.com/emscripten-core/emscripten/issues/12759
    '''
    if remote:
        return _run_remote(remote, pymupdf, mupdf)
    
    # We use PyMuPDF/scripts/gh_release.py.
    assert platform.system() == 'Linux', 'Must be run on Linux'
    t = time.time()
    env_extra = dict()
    env_extra['inputs_wheels_default'] = '0'
    env_extra['inputs_wheels_linux_pyodide'] = '1'
    env_extra['inputs_wheels_implementations'] = implementations
    if mupdf:
        env_extra['PYMUPDF_SETUP_MUPDF_BUILD'] = f'../{mupdf}'
    jlib.system(
            f'cd {pymupdf} && {sys.executable} scripts/gh_release.py build',
            env_extra=env_extra,
            )
    wheels = jlib.fs_newfiles( f'{pymupdf}/dist', t)
    assert len(wheels) == 1, f'New files are: {wheels}'
    wheel = wheels[0]
    jlib.log( f'Wheel is: {wheel}')
    if upload:
        pyodide_upload( wheel, implementations)


def pyodide_upload(wheel, subdir=None):
    #infix = '' if implementations == 'a' else f'{implementations}/'
    infix = f'{subdir}/' if subdir else ''
    jlib.log( 'Uploading Pyodide wheel: {wheel}')
    jlib.system( f'rsync -ai {wheel} julian@ghostscript.com:public_html/pyodide/{infix}')
    jlib.log( 'Wheel is at: http://ghostscript.com/~julian/pyodide/{infix}{os.path.basename(wheel)}')
    
    if 0:
        jlib.log( '')
        jlib.log( '[This does not work for secondary shared libraries.]')
        jlib.log( '* Install in Pyodide console with:')
        jlib.log( '    # https://pyodide.org/en/stable/console.html')
        jlib.log( '    import micropip')
        jlib.log( '    await micropip.install( "https://ghostscript.com/~julian/pyodide/{infix}{os.path.basename(wheel)}")')
    
    jlib.log( '')
    jlib.log( 'Install in Pyodide console with:')
    jlib.log( '    # https://pyodide.org/en/stable/console.html')
    jlib.log( '    import pyodide_js')
    jlib.log( '    await pyodide_js.loadPackage( "https://ghostscript.com/~julian/pyodide/{infix}{os.path.basename(wheel)}")')
    
    
def test_swig_multiple():
    '''
    Demonstration of generating a single Python extension module with a single
    .py file and single .so file, from two .i files.
    
    The intention here is to do the same things with the MuPDF Python bindings'
    platform/python/mupdfcpp_swig.i file and .o files that make up the MuPDF C
    and C++ API, and rebased PyMuPDF's extra.i.
    '''
    jlib.fs_ensure_empty_dir( 'test_swig_multiple')
    jlib.fs_ensure_empty_dir( 'test_swig_multiple/build')
    
    sys.path.append( 'PyMuPDF')
    import pipcl
    
    approach = 1
    #approach = 2
    
    def build():
        jlib.fs_update(
                textwrap.dedent('''
                    %module aaa

                    %{
                        int aaa_foo(int x)
                        {
                            return x * 2;
                        }
                    %}

                    int aaa_foo(int x);
                    '''),
                'test_swig_multiple/aaa.i',
                )
        jlib.fs_update(
                textwrap.dedent('''
                    %module bbb

                    %{
                        extern int aaa_foo(int);
                        int bbb_foo(int x)
                        {
                            return aaa_foo(x + 1);
                        }
                    %}

                    int bbb_foo(int x);
                    '''),
                'test_swig_multiple/bbb.i',
                )
        
        cpp, pythonflags = pipcl.base_compiler(cpp=True)
        
        if approach == 1:
            jlib.fs_update(
                    textwrap.dedent('''
                        #include <Python.h>
                        
                        extern "C" PyObject* PyInit__aaa(void);
                        extern "C" PyObject* PyInit__bbb(void);
                        
                        extern "C" PyObject* PyInit__ccc(void)
                        {
                            const char* loader = getenv("SWIG_loader");
                            fprintf(stderr, "loader=%p\\n", loader);
                            //fflush(stdout);
                            fprintf(stderr, "loader=%s\\n", loader);
                            //fflush(stdout);
                            if (!strcmp( loader, "aaa"))
                            {
                                return PyInit__aaa();
                            }
                            if (!strcmp( loader, "bbb"))
                            {
                                return PyInit__bbb();
                            }
                            fprintf(stderr, "Unrecognised value for $SWIG_loader: %s\\n", loader);
                            return NULL;
                        }
                        '''),
                    'test_swig_multiple/ccc.cpp',
                    )
            jlib.system( 'cd test_swig_multiple && swig -c++ -Wall -python -outdir . -o aaa.i.cpp aaa.i')
            jlib.system( 'cd test_swig_multiple && swig -c++ -Wall -python -outdir . -o bbb.i.cpp bbb.i')
            jlib.system( f'cd test_swig_multiple && {cpp} {pythonflags.includes} {pythonflags.ldflags} aaa.i.cpp bbb.i.cpp ccc.cpp -fPIC --shared -o _ccc.so')
        
        else:
            jlib.system( 'cd test_swig_multiple && swig -c++ -Wall -python -outdir . -o bbb.i.cpp bbb.i')
            jlib.system( f'cd test_swig_multiple && {cpp} {pythonflags.includes} {pythonflags.ldflags} bbb.i.cpp -fPIC --shared -o _bbb.so')
        
        if approach == 1:
            for p in 'aaa', 'bbb':
                pp = f'test_swig_multiple/{p}.py'
                text = jlib.fs_read( pp)
                text2 = text.replace( f'import _{p}', f'import _ccc as _{p}')
                assert text2 != text
                prefix = textwrap.dedent(f'''
                        import os
                        os.putenv('SWIG_loader', '{p}')
                        ''')
                text3 = prefix + text2
                jlib.fs_write( pp, text3)
        
        ret = list()
        if approach == 1:
            ret.append( ('test_swig_multiple/_ccc.so', 'ccc/'))
            ret.append( ('test_swig_multiple/aaa.py', 'ccc/'))
            ret.append( ('test_swig_multiple/bbb.py', 'ccc/'))
        else:
            ret.append( ('test_swig_multiple/_bbb.so', 'ccc/'))
            ret.append( ('test_swig_multiple/bbb.py', 'ccc/'))
        return ret
    
    p = pipcl.Package( 'ccc', '1.2.3', fn_build = build)
    
    p.install()
    #wheel_leaf = p.build_wheel( 'test_swig_multiple')
    
    if approach == 1:
        # This almost works, but second import, of `ccc.bbb`, which does
        # `import _ccc as _bbb`, does not actually reload _ccc.so, so we end up
        # with `bbb` being an alias for `aaa`.
        import ccc.aaa
        x = ccc.aaa.aaa_foo(3)
        jlib.log('{x=}')
        import ccc.bbb
        x = ccc.bbb.bbb_foo(3)
        jlib.log('{x=}')
    else:
        import ccc.bbb
        x = ccc.bbb.bbb_foo(3)
        jlib.log('{x=}')


def test_sysconfig():
    import sysconfig
    for n, v in sysconfig.get_paths().items():
        jlib.log( f'    {n}: {v!r}')
    jlib.log( '{sysconfig.get_path("platlib")=}')

def test_subprocess():
    jlib.system( 'echo this is with jlib.system')
    subprocess.run( 'echo this is with subprocess.run text=1', shell=1, text=1)
    subprocess.run( 'echo this is with subprocess.run text=0', shell=1, text=0)
    
    jlib.system( "ssh jules-devuan -Y 'cd artifex-remote/ && echo jlib.system'", verbose=0)
    jlib.system( "ssh jules-devuan -Y 'cd artifex-remote/ && echo jlib.system out=log'", verbose=0, out='log')
    jlib.system( "ssh jules-devuan -Y 'cd artifex-remote/ && echo jlib.system'", verbose=0)
    os.system( "ssh jules-devuan -Y 'cd artifex-remote/ && echo os.system'")


def _args(locals_, names):
    ret = ''
    for name in names:
        value = locals_.get(name)
        if value is not None:
            ret += f' --{name}={value}'
    return ret


def test(
        remote: jtest.Remote=None,
    
        command=None,
        
        b=None,
        d: bool=None,
        f=True,
        i=None,
        k=None,
        m='mupdf',
        p=None,
        t=None,
        v=2,
        build=True,
        build_isolation: bool=None,
        build_mupdf=True,
        env=None,
        gdb=None,
        system_site_packages=False,
        timeout=None,
        valgrind: bool=None,
        
        py=None,
        pymupdf='PyMuPDF',
        ):
    '''
    Builds and tests using PyMuPDF/scripts/test.py.
    
    Args:
        command:
            'build', 'test', 'buildtest'.
        b:
            build type ('release', 'debug').
        d:
            If true, use debug build.
        f:
            If true we also test 'fitz' alias.
        i:
            implementation, c=classic r=rebased R=rebased unoptimised.
        k <expression>:
            Specify tests by keyword expression; passed directly to pytest with
            `-k <expression>`. See: https://docs.pytest.org/en/latest/how-to/usage.html
        m:
            location of mupdf.
            E.g.:
                git:--branch 1.24.x https://git.ghostscript.com/mupdf.git
                git:--branch 1.24.x https://github.com/ArtifexSoftware/mupdf.git
        p:
            Pytest options, passed directly to pytest. e.g. p='-vss'.
        t:
            Pytest test directory, module etc, passed directly to pytest.
        v:
            0 - do not use venv.
            1 - use venv but don't install packages if already exists.
            2  -use venv.
        build:
            If true, (re)build pymupdf.
        build_isolation: bool=None,
        build_mupdf:
            If true, (re)build mupdf.
        env:
            .
        gdb:
            .
        timeout:
            .
        valgrind:
            .
        py:
            The remote python command, e.g. `py -3.10-32`.
        pymupdf:
            location of PyMuPDF.
    '''
    if remote:
        if m.startswith('git:'):
            _run_remote(remote, pymupdf, py=py)
        else:
            _run_remote(remote, pymupdf, m, py=py)
    else:
        jlib.log(f'{valgrind=} {d=}')
        command2 = f'{sys.executable} {pymupdf}/scripts/test.py'
        if b:   command2 += f' -b {b}'
        if d:   command2 += f' -d'
        if not f:   command2 += ' -f 0'
        if i:   command2 += f' -i {i}'
        if k:   command2 += f' -k {shlex.quote(k)}'
        if m:   command2 += f' -m {shlex.quote(m)}'
        if p:   command2 += f' -p {shlex.quote(p)}'
        if t:   command2 += f' -t {shlex.quote(t)}'
        if v != 2:  command2 += f' -v {v}'
        if build_isolation: command2 += f' --build_isolation 1'
        if not build_mupdf: command2 += f' --build-mupdf 0'
        if gdb:             command2 += f' --gdb 1'
        if system_site_packages:    command2 += f' --system-site-packages 1'
        if timeout:         command2 += f' --timeout {timeout}'
        if valgrind:        command2 += f' --valgrind 1'
        if env:
            if isinstance(env, str):
                env2 = dict()
                for e in env.split(','):
                    eq = e.find('=')
                    assert eq >= 0
                    n, v = e[:eq], e[eq+1:]
                    env2[n] = v
                env = env2
        if build is None:
            if command is None:
                command = 'buildtest'
        else:
            assert command is None
            command = 'buildtest' if build else 'test'
        if command:
            command2 += f' {shlex.quote(command)}'
        jlib.system(command2, env_extra=env)


def test_pipcl(remote=None, py=None, pymupdf='PyMuPDF'):
    '''
    Does doctest of PyMuPDF's pipcl.py.
    '''
    if remote:
        return _run_remote(remote, pymupdf)
    pypackage.venv_run(
            [
                f'pip install swig',
                f'cd {pymupdf} && python -m doctest pipcl.py',
            ],
            )


def build_test_gh(
        pyvs = None,
        sdist: bool=None,
        wheels_linux_aarch64: bool=None,
        wheels_linux_auto: bool=None,
        wheels_linux_pyodide: bool=None,
        wheels_macos_arm64: bool=None,
        wheels_macos_auto: bool=None,
        wheels_windows_auto: bool=None,
        pymupdf = 'PyMuPDF',
        mupdf = 'mupdf',
        dual = True,
        check_clean_tree = True,
        download=False,
        PYMUPDF_SETUP_MUPDF_BUILD_TYPE = None,
        ):
    '''
    Builds wheels using github, pushing current tree to
    github.com:ArtifexSoftware/PyMuPDF-julian.git.
    
    Will fail if current tree has uncommited changes.
    
    Args:
        pyvs:
            E.g. '311' or '310,311'.
            If None, uses current python version.
        sdist
        wheels_linux_aarch64
        wheels_linux_auto
        wheels_linux_pyodide
        wheels_macos_arm64
        wheels_macos_auto
        wheels_windows_auto
            Whether to build specific wheels. 
        pymupdf:
            Location of local PyMuPDF directory.
        mupdf:
            Location of MuPDF. If '' or None we use hard-coded location
            in setup.py. If starts with 'git:' is used to set
            PYMUPDF_SETUP_MUPDF_BUILD. Otherwise should be local git checkout
            which we force push to github.com:ArtifexSoftware/MuPDF-julian.git.
        dual:
            Whether to build separate PyMuPDF and PyMuPDFb wheels. (Does not
            effect Pyodide wheels, which are always a single PyMuPDF wheel.)
        check_clean_tree:
            If true we require current tree to be clean, otherwise we allow
            uncommited changes (which wil not be tested).
        download:
            .
        PYMUPDF_SETUP_MUPDF_BUILD_TYPE:
            .
        
    Does:
    
    * Call gh_token_auto().
    * Call gh_pymupdf_url() to set to ArtifexSoftware/mupdf-julian.
    * Force-push to git@github.com:ArtifexSoftware/mupdf-julian.git.
    * Run gh_release(), with download=False and upload=None to prevent upload
      to pypi.org.
    
    Note sometimes the force-push doesn't seem to complete before we run
    gh_release(). We do a short sleep, but if this isn't enough, running us
    again is a workaround.
    
    Args:
        pyvs:
            E.g. '311' or '310,311'.
            If None, uses current python version.
    '''
    if not pyvs:
        pyvs = ''.join(platform.python_version().split('.')[:2])
    gh_token_auto()
    gh_pymupdf_url('https://api.github.com/repos/ArtifexSoftware/PyMuPDF-julian')
    jlib.system(
            f'cd {pymupdf} && GIT_SSH_COMMAND="ssh -i ~/.ssh/id_rsa2" git push -fv git@github.com:ArtifexSoftware/PyMuPDF-julian.git',
            )
    if mupdf in (None, ''):
        PYMUPDF_SETUP_MUPDF_BUILD = None
    elif mupdf.startswith( 'git:'):
        PYMUPDF_SETUP_MUPDF_BUILD = mupdf
    else:
        assert os.path.isdir( mupdf)
        jlib.system(
                f'cd {mupdf} && GIT_SSH_COMMAND="ssh -i ~/.ssh/id_rsa2" git push -fv git@github.com:ArtifexSoftware/MuPDF-julian.git',
                )
        branch = jlib.git_get_branch( mupdf)
        PYMUPDF_SETUP_MUPDF_BUILD = f'git:--branch {branch} https://github.com/ArtifexSoftware/MuPDF-julian.git'
        jlib.log( f'{PYMUPDF_SETUP_MUPDF_BUILD=}')
    
    # It seems that `git push` commands can return before the push has
    # completed, so we sleep here before doing the build.
    #
    time.sleep(2)
    
    local_dir = gh_release(
            sdist=sdist,
            wheels_linux_aarch64=wheels_linux_aarch64,
            wheels_linux_auto=wheels_linux_auto,
            wheels_linux_pyodide=wheels_linux_pyodide,
            wheels_macos_arm64=wheels_macos_arm64,
            wheels_macos_auto=wheels_macos_auto,
            wheels_windows_auto=wheels_windows_auto,
            download=download,
            upload=False,
            check=False,
            parallel=True,
            pyvs=pyvs,
            PYMUPDF_SETUP_MUPDF_BUILD=PYMUPDF_SETUP_MUPDF_BUILD,
            PYMUPDF_SETUP_MUPDF_BUILD_TYPE=PYMUPDF_SETUP_MUPDF_BUILD_TYPE,
            pymupdf=pymupdf,
            check_clean_tree = check_clean_tree,
            dual=dual,
            )
    return local_dir


def test_sysinstall(
        remote: jtest.Remote=None,
        mupdf='mupdf',
        mupdf_git=None,
        pymupdf='PyMuPDF',
        install_mupdf=True,
        install_pymupdf=True,
        test=True,
        rebased=True,
        root='pymupdf-fake-root',
        prefix='/usr/local',
        use_script=True,
        test_venv=None,
        tesseract5=False,
        ):
    '''
    Unix-only. checks system install.
    
    remote:
        .
    mupdf
        .
    pymupdf
        .
    install_mupdf:
        If false we assume mupdf is already installed; saves a little time.
    install_mupdf:
        If false we assume pymupdf is already installed; saves a little time.
    test:
        If false we don't test.
    root:
        Install directory. Must contain 'fake-root'. We delete anything that
        exists in this directory.
    prefix
        .
    use_script
        Use PyMuPDF:scripts/sysinstall.py.
    test_venv
        .
    tesseract5
    
    Installs MuPDF into fake root, then builds and installs PyMuPDF into the
    fake root using the installed MuPDF.
    '''
    if remote:
        if mupdf_git:
            # Don't rsync local mupdf directory to remote.
            return _run_remote( remote, pymupdf)
        else:
            return _run_remote( remote, mupdf, pymupdf)
    
    if root is None:
        root = 'pymupdf-fake-root'
    root = os.path.abspath(root)
    
    if use_script:
        command = f'{sys.executable} {pymupdf}/scripts/sysinstall.py'
        command += f' --mupdf-dir {mupdf}'
        if mupdf_git:
            command += f' --mupdf-git {shlex.quote(mupdf_git)}'
        command += f' --prefix {prefix}'
        command += f' --pymupdf-dir {pymupdf}'
        command += f' --root {root}'
        command += f' --tesseract5 {int(tesseract5)}'
        command += f' -m {int(install_mupdf)}'
        command += f' -p {int(install_pymupdf)}'
        command += f' -t {int(test)}'
        if test_venv == '':
            command += f' --test-venv ""'
        jlib.system(command)
        return
    
    # Build+install MuPDF.
    #
    if install_mupdf:
        # Avoid deleting anything important.
        assert 'fake-root' in root
        jlib.fs_ensure_empty_dir(root)
        command = f'cd {mupdf} &&'
        command += f' DESTDIR={root}'
        if pypackage.openbsd():
            command += ' CXX=c++ gmake'
        elif platform.node() == 'artifexs-Mac-mini.local':
            command += ' gmake'
        else:
            command += ' make'
        #command += ' shared=yes'
        #command += ' --debug=j'
        command += ' USE_SYSTEM_LIBS=yes'
        command += ' build_prefix=system-libs-'
        command += ' HAVE_LEPTONICA=yes HAVE_TESSERACT=yes'
        command += ' verbose=yes'
        #command += ' VENV_FLAG='
        command += ' INSTALL_MODE=755'
        if rebased:
            command += ' install-shared-python'
        else:
            command += ' install-shared-c'
        jlib.system( command)
    
    # Build+install PyMuPDF.
    #
    if install_pymupdf:
        flags_freetype2 = jlib.system('pkg-config --cflags freetype2', out='return').strip()
        compile_flags = f'-I {root}/usr/local/include {flags_freetype2}'
        link_flags = f'-L {root}/usr/local/lib'
        if 0:
            jlib.system(
                    f'cd {pymupdf} && {sys.executable} setup.py install --root {root}',
                    env_extra = dict(
                        CFLAGS=compile_flags,
                        CXXFLAGS=compile_flags,
                        LDFLAGS=link_flags,
                        PYMUPDF_SETUP_MUPDF_BUILD='',   # Use system mupdf.
                        ),
                    )
        else:
            # Need pip, e.g. `apt install python3-pip` on devuan.
            env_extra = dict(
                CFLAGS=compile_flags,
                CXXFLAGS=compile_flags,
                LDFLAGS=link_flags,
                PYMUPDF_SETUP_MUPDF_BUILD='',   # Use system mupdf.
                )
            if not rebased:
                env_extra['PYMUPDF_SETUP_IMPLEMENTATIONS'] = 'a'
            command = ''
            command += f' CFLAGS={shlex.quote(compile_flags)}'
            command += f' CXXFLAGS={shlex.quote(compile_flags)}'
            command += f' LDFLAGS={shlex.quote(link_flags)}'
            command += f' PYMUPDF_SETUP_MUPDF_BUILD='
            if not rebased:
                command += ' PYMUPDF_SETUP_IMPLEMENTATIONS=a'
            command += f' pip install -vv --root {root} ./{pymupdf}'
            jlib.system( command)
        
    if test:
        # Basic import test.
        test_py = 'pymupdf-fake-root-test.py'
        test_code = textwrap.dedent('''
                import fitz_old
                print(f'{fitz.__file__=}')
                assert hasattr(fitz, 'Document')
                ''')
        if rebased:
            test_code += textwrap.dedent('''
                    import fitz_old
                    print(f'{fitz_old.__file__=}')
                    assert hasattr(fitz_old, 'Document')
                    ''')
        jlib.fs_write( test_py, test_code)
        sys.path.insert(0, f'{pymupdf}')
        import pipcl
        root2 = pipcl.install_dir(root)
        jlib.log('{root2=}')
        
        jlib.system( f'LD_LIBRARY_PATH={root}/usr/local/lib PYTHONPATH={pipcl.install_dir(root)} {sys.executable} {test_py}')
        
        # Run pytest tests with classic and rebased implementations.
        #
        pytest = 'py.test' if pypackage.openbsd() else 'pytest'
        prefix = f'{pymupdf}/tests/run_compound.py ' if rebased else ''
        jlib.system( f'LD_LIBRARY_PATH={root}/usr/local/lib PYTHONPATH={pipcl.install_dir(root)} {prefix}{pytest} -k "not test_color_count" {pymupdf}')


def _run_remote(remote, *syncdirs, caller=0, py=None, tee=True, do_command=True):
    '''
    Runs caller on remote machine using ssh and aargs.fncall_text.
    '''
    command = _remote_command( remote, *syncdirs, caller=caller+1, py=py, tee=tee, do_command=do_command)
    out = None
    if tee is None:
        tee = True
    if tee:
        
        description = remote if isinstance(remote, str) else remote.description
        jlib.log('{description=}')
        colon = description.find(':')
        if colon >= 0:
            description = description[:colon]
        jlib.log('{description=}')
        path = f'out-{description}'
        if 1:
            with jlib.tee(path):
                jlib.system( command, bufsize=0)
        else:
            with open(path, 'w') as f:
                jlib.system( command, bufsize=0, out=[sys.stdout, f])
    else:
        jlib.system( command, bufsize=0)


def _remote_command(remote, *syncdirs, caller=0, py='py', tee=None, do_command=True):
    '''
    Returns command that will run caller on remote machine.
    '''
    command = f'./julian-tools/jtest.py remote {remote},julian-tools'
    for syncdir in syncdirs:
        if syncdir:
            command += f',{syncdir}'
        
    remote_command = ''
    if do_command:
        jlib.log('{type(remote)=} {remote=}')
        w = False
        if isinstance(remote, str):
            if 'windows' in remote and ' -p ' not in remote:
                w = True
        else:
            if 'windows' in remote.ssh and ' -p ' not in remote.ssh:
                w = True
        if py:
            remote_command += f'{py} '
        elif w:
            remote_command += 'py '
        #remote_command += f'./julian-tools/pymupdf.py '
        arg0 = os.path.basename( sys.argv[0])
        remote_command += f'./julian-tools/{arg0} '
        remote_command += aargs.fncall_text(caller+1, locals_override=dict(remote=None, py=None))
    command += ' '
    command += shlex.quote(remote_command)
    return command


def test_text_docx(
        do_build=True,
        ):
    
    build(
            Build( doit=do_build),
            )
    import fitz
    
    path = os.path.relpath( 'Gravity.pdf')
    document = fitz.open(path)
    page = document.load_page(11)
    
    # Use Page.get_text().
    text = page.get_text(flags=fitz.TEXT_PRESERVE_WHITESPACE | fitz.TEXT_MEDIABOX_CLIP | fitz.TEXT_DEHYPHENATE)
    n = text.count(' ')
    print( f'Text from page.get_text(): {n=}\n{text}')

    # Use MuPDF's Extract.
    buffer_ = fitz.mupdf.FzBuffer(1)
    out = fitz.mupdf.FzOutput( buffer_)
    space_guess = 0.3   # Expected width of spaces, relative to adjoining characters.
    writer = fitz.mupdf.FzDocumentWriter(
            out,
            f'text,space-guess={space_guess}',
            fitz.mupdf.FzDocumentWriter.OutputType_DOCX,
            )
    device = fitz.mupdf.fz_begin_page(writer, fitz.mupdf.fz_bound_page(page))
    fitz.mupdf.fz_run_page(page, device, fitz.mupdf.FzMatrix(), fitz.mupdf.FzCookie())
    fitz.mupdf.fz_end_page(writer)
    fitz.mupdf.fz_close_document_writer(writer)
    text = buffer_.fz_buffer_extract()
    text = text.decode('utf8')
    n = text.count(' ')
    print(f'Text from FzDocumentWriter.OutputType_DOCX: {n=}\n{text}')


def test_sdist_install(remote: jtest.Remote=None, mupdf='mupdf', pymupdf='PyMuPDF'):
    '''
    Test installation from PyMuPDF and PyMuPDFb sdists.
    '''
    if remote:
        return _run_remote(remote, mupdf, pymupdf)
    def make_sdist(env_extra=None):
        t = time.time()
        pypackage.venv_run(f'cd {pymupdf} && {sys.executable} setup.py sdist', env_extra=env_extra)
        tgz = jlib.fs_newfiles(f'{pymupdf}/dist', t, ge=True)
        assert len(tgz) == 1
        ret = tgz[0]
        assert ret.endswith('.tar.gz')
        # Looks like tarfile.TarFile() doesn't auto-handle compressions etc.
        with tarfile.open(ret) as t:
            names = t.getnames()
        return ret, names
    pymupdf_sdist, names = make_sdist()
    assert 150 < len(names) < 200, f'{len(names)=}'
    pymupdfb_sdist, names_b = make_sdist(env_extra=dict(PYMUPDF_SETUP_FLAVOUR='b'))
    assert 5 < len(names_b) < 10, f'{len(names_b)=}'
    pypackage.venv_run(f'pip uninstall -y pymupdf pymupdfb')
    pypackage.venv_run(f'pip install -vv {pymupdfb_sdist}')
    pypackage.venv_run(
            f'pip install -vvv {pymupdf_sdist}',
            env_extra=dict(PYMUPDF_SETUP_MUPDF_BUILD=os.path.abspath(mupdf)),
            )

def test_old_macos(
        remote=jtest.Remote('', ssh='ssh -p 2224 julian@rjw.ghostscript.com'),
        d=None,
        wheel=None,
        ):
    jlib.log(f'{remote=} {d=} {wheel=}')
    if remote != 'None':
        if not d:
            d = build_test_gh(
                    pyvs = '312',
                    wheels_macos_auto = True,
                    dual = False,
                    download = True,
                    PYMUPDF_SETUP_MUPDF_BUILD_TYPE = 'debug',
                    )
            jlib.log(f'{d=}.')
        wheels = glob.glob(f'{d}/PyMuPDF-*x86_64.whl')
        assert len(wheels) == 1, f'{wheels=}'
        wheel = wheels[0]
        remote = jtest.Remote('', ssh='ssh -p 2224 julian@rjw.ghostscript.com')
        jlib.system(f'rsync -ai --rsh {shlex.quote(remote.ssh)} {wheel} :artifex-remote/')
        wheel = os.path.basename(wheel)
        _run_remote(remote, 'PyMuPDF', py='python3.12')
    else:
        wheel = os.path.basename(wheel)
        # lldb python venv-pypackage-3.12.3-64/bin/pytest -s PyMuPDF
        # (lldb) r
        # (lldb) c
        #
        pypackage.venv_run(
                [
                    f'pip install --force-reinstall {wheel}',
                    f'MUPDF_trace=2 ./PyMuPDF/scripts/test.py -i r -f 0 -p -s test',
                ],
                )


if __name__ == '__main__':
    if platform.system().startswith('CYGWIN'):
        assert 0, '*** Running under cygwin does not generally work; prefix with "py" ***'
    if 0 and sys.stdout.isatty():
        with jlib.tee('out-pymupdf'):
            aargs.cli( show_return=False)
    else:
        jlib.log(f'Not redirecting stdout.')
        aargs.cli( show_return=False)
