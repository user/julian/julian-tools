Packaging a large Python binary extension - does it need to be so difficult?
============================================================================

Abstract
--------

Packaging a complicated Python binary extension with its own build system, was
harder than i expected; i really struggled with the packaging documentation
and abstractions. I will describe my eventual solution and try to open up a
discussion on how the offical documentation could perhaps be improved.

Description
-----------

I've programmed in Python for a couple of decades, but have only recently got
involved with the packaging system. Unfortunately i've found this a rather
difficult experience.

The project in question is a fairly complicated Python binary extension
for the `MuPDF <https://mupdf.readthedocs.io>`_ library, consisting of C,
auto-generated C++, and SWIG-generated C++ and Python. It does not use Cmake
or Scons or other common build system, and certainly cannot be built with
`setuptools`' SWIG support.

Still, building it only requires that one runs an external command, for example
with Python's `subprocess.run()`. How hard can this be?

Unfortunately i haven't found a packaging system that supports this
usage. People on the `distutils-sig` mailing list were very helpful but the
only available option seemed to be to do the build every time `setuptools.py`
was run, before we hand control over to `setuptools.setup()`. This is clearly
not a good solution.

So i ended up looking at how packaging works, and the specifications of
sdists and wheels. Unfortunately my impression is that the available
documentation is incomplete and inconsistent. For example `PEP-517
<https://peps.python.org/pep-0517/>`_ seems to outline the officially
recommended approach, but pip doesn't use PEP-517's `build_sdist()` function
for creating sdists, instead running `setup.py` as a command-line script
with args for which there appears to be no documentation. I've also found it
difficult to figure out exactly how sdists and wheels should be layed out, the
exact specifications of things like the `PKG-INFO` and `METADATA` files, and
exactly where files should be copied when installing a package.

I'll describe how, by using experimentation along with the official
documentation, i've ended up with a relatively simple Python module that can be
be use by a project's `setup.py` to do just enough to support the creation of
sdists and wheels, plus allow build and installation on users' machines from
plain source, sdist or wheel.

I'm hoping that relating these experiences might be interesting to other users
of the packaging system, and encourage people more knowledgable than me to
help me to better understand how things how things are supposed to work. And
it might suggest ways in which the official packaging documentation could be
improved.
