PyMuPDF code
============

[This document is concerned only with the rebased implementation.]

Code structure
--------------

Proposed changes
~~~~~~~~~~~~~~~~

*
  Make all public API functions, classes, constants etc, be defined explicitly
  in `src/__init__.py`. The existing rebased code largely does this already,
  except it still separately sets many class methods to functions in
  `src/utils.py`.
  
  We will end up with `src/__init__.py` defining the entire public API and
  looking like::

      class Document:
          def __init__(self, filename=None, stream=None, filetype=None, rect=None, width=0, height=0, fontsize=11):
              ...
          def add_layer(self, name, creator=None, on=None):
              ...
      class Page:
          ...
      ...

*
  To avoid `src/__init__.py` becoming too large, we should move much of the
  implementation of the various methods and functions into separate files.

  This must not change the top-level declarations, just the function/method
  bodies.

  [The existing code already does something similar to this, with public
  methods calling internal methods whose names start with underscores. We
  should change these internal methods to be functions in implementation `.py`
  files.]

*  
  Conversely we should avoid top-level functions and methods in
  `src/__init__.py` becoming just single calls to an implementation function
  - it's useful to have at least some code in these functions, for example so
  that one can see how it works without having to look elsewhere.

*
  Implementation files should have leafnames that start with an underscore, to
  make it clear that they are not for public use.
  
*
  Some of the implementation files could be per-class. For example we could
  have:

  .. code-block:: text
  
      src/__init__.py   # The public API.
      src/_annot.py     # Internal support for `class Annot`.
      src/_document.py  # Internal support for `class Document`.
      src/_page.py      # Internal support for `class Page`.
      src/table.py
      ...

*
  The functions in `src/utils.py` that are currently used directly as class
  methods, should be moved directly into `src/__init__.py`. Portions of these
  functions will then often be moved into new functions in implementation files
  such as `src/_page.py`.

*
  Implementation files such as `src/_document.py` will often need to import the
  top-level `fitz` module (`src/__init__.py`) so they can use classes such as
  `Document`, creating a two-way dependency.

  This sort of thing can be frowned upon, but in practise it works fine, and
  whatever the theoretical objections are, in Python it allows code to be
  logically structured in a useful way for humans, which is the primary concern
  here.

  *
    One can run into minor problems with top-level code (i.e. code that is run
    on import) trying to use a partially-imported module. This is actually
    quite rare in PyMuPDF, and should become rarer with these proposed changes.

  *
    Code that uses typing annotations for function parameters can cause similar
    problems, as it appears to effectively require types to be defined at
    import time.

    One way to avoid this if implementation code annotates args with public
    classes such as `Document`, woud be for `src/__init__.py` to import
    implementation files such as `src/_document.py` *after* the definitions of
    all classes::

        class Document:
            ...
        class Page:
            ...

        import _document
        import _page
        ...

    One could argue that annotating internal implementation code in this way is
    of limited use anyway, so it might be simpler to simply not do so.

*
  We could move all `JM_*` code into `src/_jm.py` and `TOOLS.*` into
  `src/_tools.py`.
  
  * 
    [2024-03-15: actually `fitz.TOOLS.*` is included in the public API
    (https://pymupdf.readthedocs.io/en/latest/tools.html), so we should
    either move `TOOLS.*` code into new file `src/TOOLS.py` or leave it in
    `src/__init__.py` as static methods in `class TOOLS`.]
  
  *
    I have experimented with doing this in my tree, and have managed to do so
    without breaking any tests.

  *
    Note that `JM_ code` calls `TOOLS` and visa-versa. If there was a different
    abstraction that removed this mutual dependency, it might simplify things.

*
  All global internal data in `src/__init__.py` should be named with a leading
  underscore so that users do not mistakenly think it is part of the public
  API. For example `Base14_fontdict` should be renamed to `_Base14_fontdict`.
  It would probably be good to move all such internal data into implementation
  files such as `src/_document.py`.


Why we should make these changes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  
*
  It will simplify the structure of PyMuPDF code, eliminating the current
  runtime creation of methods that point to fuctions in `src/utils.py`.

*
  It will clarify the difference between the public API and code that is part
  of the implementation only. This will aid development and generally help devs
  and users who want to read and understand the code.

*
  It will help automated tools understand the publc API, without having to
  follow aliases that are currently only defined at runtime, for example with
  things like `Page.apply_redactions = utils.apply_redactions`.

  This will in turn address https://github.com/pymupdf/PyMuPDF/issues/2883
  "Improve the Python type annotations for fitz_new", and also reduce the
  number of issues we get from PyCharm users.

*
  It will provide a well-defined convention for how to split up code that has
  becomes too unwieldy.


Explicit imports and usage
--------------------------

Proposed changes
~~~~~~~~~~~~~~~~

Functions in implementation files should be imported and called
explicitly.

*
  We should always import internal modules with::
  
      import <modulename>

  instead of::
  
      from <modulename> import *
      from <modulename> import foo

For example `Document.copy_page()` currently makes use of an internal
`Document._move_copy_page()` method. We should move this implementation
code into a new function `move_copy_page()` in `src/_document.py` and make
`Document.copy_page()` look like this::

    import _document
    ...
    class Document:
      ...
      def copy_page(self, pno: int, to: int =-1):
          ...
          if self.is_closed:
              raise ValueError("document closed")

          page_count = len(self)
          if (
                  pno not in range(page_count)
                  or to not in range(-1, page_count)
                  ):
              raise ValueError("bad page number(s)")
          before = 1
          copy = 1
          if to == -1:
              to = page_count - 1
              before = 0
        
          # Use internal implementation function.
          return _document.move_copy_page(pno, to, before, copy)

Similarly, in `table.py`, instead of importing specific names with::
  
    from . import (
        Rect,
        Matrix,
        TEXTFLAGS_TEXT,
        TOOLS,
        EMPTY_RECT,
        sRGB_to_pdf,
        Point,
    )

we should do `import fitz` and explicitly use `fitz.Rect` etc.

Why we should do this
~~~~~~~~~~~~~~~~~~~~~

*
  It makes code clearer - if one sees `return _document.move_copy_page(pno, to,
  before, copy)`, it is obvious that we are using an internal implementation
  function, and it is also obvious that the function can be found in
  `_document.py`.

*
  It allows a degree of static checking, because we can require that all uses
  of function/class names are prefixed by the required implementation module
  name.

  For example with the above example of `move_copy_page()` in
  `src/_document.py`:
  
  *
    All call sites outside of `src/_document.py` must call it with a prefix
    `_document.`.

  *
    All call sites in `src/_document.py` must call it with no prefix.

  I have a simple script that uses Python's `ast` modue to parse our `.py`
  files, looking for definitions and usages of these definitions, checking that
  they conform to these requirements.

  Thus we will eliminate a class of bugs that would otherwise only be
  discovered when a test case or a user actually causes the specific code to be
  run. This will be particularly important if we move code around - it's very
  easy to miss out a rename and end up with code that will fail at runtime.

  So we will be free to move code around between files in the future without
  risking breakages caused by call sites not using the correct prefix.

[I was surprised to find that the `flake8` Python checker doesn't seem able to
do this sort of check. It might be because it cannot assume in general that
code is always structured in this way - if code does `from foo import bar` or
`from foo import *`, or `bar = foo.bar`, then this sort of checking cannot
work.]


Other proposed minor changes
----------------------------

*
  Reduce/eliminate mutable global state.

  Rebased has already moved what was previously global state for use by
  devices, into the device class instances themselves, but we still have
  various pieces of mutable global state.
  
  For example `src/table.py` has::
  
      EDGES = []  # vector graphics from PyMuPDF
      CHARS = []  # text characters from PyMuPDF
      TEXTPAGE = None
  
*
  Remove all `.thisown` class members and `._erase()` methods.
  
  I *think* these are unnecessary with the rebased implementation.

*
  Put runtime information into exception text.
  
  For example `raise ValueError('bad page number(s)')` could be changed to
  `raise ValueError(f'bad page number(s) {pno=} {to=}.')`.

*
  Use Python f-strings (Formatted string literals, e.g. `f'foo={foo}'`) instead
  of the `%` operator or similar.

*
  Avoid named parameters also being usable as positional parameters, to avoid
  confusion.
  
  For example::
  
      def ez_save(
            self,
            filename,
            garbage=3,
            clean=False,
            deflate=True,
            deflate_images=True,
            deflate_fonts=True,
            incremental=False,
            ascii=False,
            expand=False,
            linear=False,
            pretty=False,
            encryption=1,
            permissions=4095,
            owner_pw=None,
            user_pw=None,
            no_new_id=True,
            ):

  should be::

      def ez_save(
            self,
            filename,
            *,
            garbage=3,
            clean=False,
            deflate=True,
            deflate_images=True,
            deflate_fonts=True,
            incremental=False,
            ascii=False,
            expand=False,
            linear=False,
            pretty=False,
            encryption=1,
            permissions=4095,
            owner_pw=None,
            user_pw=None,
            no_new_id=True,
            ):

*
  Do not swallow exceptions.
  
  Rebased differs from classic in that it removed many places where classic
  swallowed exceptions, but several places still catch exceptions and do not
  re-raise, generating a diagnostic only.

*
  Do not use `getattr()` to modify behaviour depending on what functions are
  provided by MuPDF. Instead switch on the mupdf version.
  
  *
    This means that we can easily see and remove code that is specific to an
    old MuPDF that we no longer support.

  *
    It also means that we will detect regressions in MuPFD itself if a function
    is unexpectedly removed.

*
  Do not use `getattr()` or `hasattr()` on PyMuPDF classes, for example looking
  for an `is_closed` or `parent` member. If we require this state it should be
  put in place by the class's constructor.

*
  Remove deprecated camelCase function/method names.

*
  Use MuPDF values for PyMuPDF constants.
  
  For example we could change `TEXT_CID_FOR_UNKNOWN_UNICODE = 128` to
  `TEXT_CID_FOR_UNKNOWN_UNICODE = mupdf.FZ_STEXT_USE_CID_FOR_UNKNOWN_UNICODE`.


Possibilities
-------------

*
  Install a top-level `fitz.py` file, along with a `_fitz/` directory
  containing the implementation files. I.e. an installed structure looking
  like::
  
      fitz.py
      _fitz/
          _document.py
          _page.py
          _jm.py
          ...
          mupdf.py
          _mupdf.so
  
  This would accurately reflect the new proposed structure, making it clear to
  the user where things are, and what constitutes the public API.

  However it is probably not following Python conventions about package
  structure, and so wouldn't actually help.

*
  Remove the `PDF_NAME()` function and change callsites to specify the
  MuPDF enum directly. For example change::
  
      PDF_NAME('A')

  to::
  
      mupdf.PDF_ENUM_NAME_A

  [Not sure this worth it, especially as MuPDF defines `PDF_NAME()` as a C
  macro.]


Rejected
--------

*
  Use a separate `.py` file to define each PyMuPDF class. For example
  `src/_document.py` would look like::
  
      class Document:
          def __init__(self):
              ...
          ...
  
  The problem with this is that we would need to import the contents of each
  of these files into `src/__init__.py` with::
  
      from _Document import *
      from _Page import *
      ...
    
  This obscures where the code is, breaking Python's "Explicit is better than
  implicit" mantra, and also makes `src/__init__.py` contain mostly useless (from
  a human code-reader point of view) boiler-plate code.


How to make these changes [Added 2024-03-15]
--------------------------------------------

Unlike with compiled languages, moving Python code around introduces the risk
of runtime errors caused by code not being correctly updated to use new names
for functions and classes.

If we get this wrong, we could end up with another round of customer bugs in
code that is not checked by our tests, similar to what happened (inevitably)
with rebased.

So we should first change all imports to the simple `import foo` form instead
of `from foo import bar` or `from foo import *`, and add a test that checks
that usages match definitions.

Then we can freely move code around, safe in the knowledge that the new test
will fail if we don't patch up all call sites.
