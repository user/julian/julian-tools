#!/usr/bin/env python3

# on openbsd:
#
# sudo pkg_add py3-PyPDF2 py3-numpy py3-pandas py3-cython py3-cryptography py3-sqlalchemy
# python3 -m venv pylocal --system-site-packages
# . pylocal/bin/activate
#
# can then import local camelot git tree with:
#    python
#    import os
#    import sys
#    sys.path.insert(0, os.path.abspath(f'{__file__}/../../../../camelot'))
#    import camelot
#
# This fails:
#    pip install camelot-py
#

import aargs
import jlib

import glob
import shutil
import subprocess
import sys
import time
import os


dir_julian_tools    = os.path.relpath(f'{__file__}/..')
dir_mupdf           = os.path.relpath(f'{__file__}/../../mupdf')
dir_camelot         = os.path.relpath(f'{__file__}/../../camelot')

dir_pdfs    = f'{dir_camelot}/docs/benchmark/lattice'
dir_extract = f'{dir_mupdf}/thirdparty/extract'

print(f'dir_mupd={dir_mupdf}')
print(f'dir_extract={dir_extract}')
print(f'dir_pdfs={dir_pdfs}')


def system(command):
    print(f'Running: {command}')
    sys.stdout.flush()
    subprocess.check_call(command, shell=1)


def test(leaf_match=None, do_camelot: bool=1, do_mutool: bool=1, do_mutool_cv: bool=0):
    '''
    '''
    sys.path.insert(0, dir_camelot)
    if do_camelot:
        print(f'Importing camelot...')
        import camelot
    def get_pdfs_camelot():
        root = f'{dir_camelot}/docs/benchmark'
        print(f'Looking for PDFs in {root}')
        sys.stdout.flush()
        for dirpath, dirnames, filenames in os.walk(root):
            for filename in filenames:
                if filename.endswith('.pdf'):
                    yield os.path.join(dirpath, filename)
    def get_pdfs():
        root = f'{dir_extract}/test'
        print(f'Looking for PDFs in {root}')
        sys.stdout.flush()
        for dirpath, dirnames, filenames in os.walk(root):
            for filename in filenames:
                if filename.endswith('.pdf'):
                    yield os.path.join(dirpath, filename)
    dir_out = 'extract-tables-out'
    os.makedirs(dir_out, exist_ok=True)
    
    path_out = f'{dir_out}/index.html'
    with open(path_out, 'w') as f:
        
        def p(text):
            print(text, file=f)
        
        p(f'<html>')
        p(f'<body>')
        p(f'<h1>Extract/camelot table output ({time.strftime("%F %T")})</h1>')
        p(f'<p>&lt;source&gt;')
        p(f'<p>&lt;source&gt; => &lt;html&gt; &lt;opencv-html&gt &lt;caemlot-html&gt; ; &lt;docx&gt; &lt;odt&gt;')
        p(f'<ul>')
        
        for path in get_pdfs():
            leaf = os.path.basename(path)
            
            if 1:
                # Add to html comparison page.
                p(f'    <li><a href="{leaf}">{leaf}</a>')
                p(f'    <ul>')
                p(f'        <li>')
                p(f'             <a href="{leaf}.mutool.html">{leaf}.mutool.html</a>')
                p(f'             <a href="{leaf}.mutool.cv.html">{leaf}.mutool.cv.html</a>')
                p(f'             <a href="{leaf}.camelot.html">{leaf}.camelot.html</a>')
                p(f'             <a href="{leaf}.camelot.html">{leaf}.acrobat.html</a>')
                p(f'        <li>')
                p(f'               <a href="{leaf}.mutool.docx">{leaf}.mutool.docx</a>')
                p(f'             | <a href="{leaf}.mutool.odt">{leaf}.mutool.odt</a>')
                p(f'        <li>')
                p(f'            <iframe width=19% height=300 src="{leaf}"></iframe>')
                p(f'            <iframe width=19% height=300 src="{leaf}.mutool.html"></iframe>')
                p(f'            <iframe width=19% height=300 src="{leaf}.mutool.cv.html"></iframe>')
                p(f'            <iframe width=19% height=300 src="{leaf}.camelot.html"></iframe>')
                p(f'            <iframe width=19% height=300 src="{leaf}.acrobat.html"></iframe>')
                #print(f'            <iframe width=25% height=300 src="test/generated/{leaf}.mutool.docx"></iframe>', file=f)
                p(f'    </ul>')
        
            if leaf_match and leaf_match != leaf:
                continue
            print(f'Looking at pdf: {path}')

            if 1:
                # copy pdf.
                shutil.copy(path, f'{dir_out}/{leaf}')

            if do_camelot:
                print(f'Processing with camelot: {path}')
                # We use particular extra args, see:
                # https://github.com/camelot-dev/camelot/wiki/Comparison-with-other-PDF-Table-Extraction-libraries-and-tools
                #
                # For details about camelot.read_pdf(), see:
                # https://camelot-py.readthedocs.io/en/master/api.html#main-interface
                #
                if 0:
                    pass
                elif leaf == 'background_lines_1.pdf':
                    tables = camelot.read_pdf(path, process_background=True)
                elif leaf == 'background_lines_2.pdf':
                    tables = camelot.read_pdf(path, process_background=True, line_scale=40)
                elif leaf == 'column_span_2.pdf':
                    tables = camelot.read_pdf(path, line_scale=40)
                elif leaf == 'electoral_roll.pdf':
                    tables = camelot.read_pdf(path, line_scale=40)
                elif leaf == 'row_span_1.pdf':
                    tables = camelot.read_pdf(path, line_scale=40, )
                elif leaf == 'budget.pdf':
                    tables = camelot.read_pdf(path, flavor='stream')
                else:
                    tables = camelot.read_pdf(path)
                print(f'len(tables)={len(tables)}')
                for table in tables:
                    print(f'{table}')
                    print(f'    {table.parsing_report}')
                    html = table.to_html(f'{dir_out}/{leaf}.camelot.html')
                    html = table.to_csv(f'{dir_out}/{leaf}.camelot.csv')
                if not tables:
                    with open(f'{dir_out}/{leaf}.camelot.html', 'w') as ff:
                        ff.write('<html><body>[Camelot generated zero tables.]</body></html>\n')

            mutool = f'{dir_mupdf}/build/debug/mutool'
            
            if do_mutool:
                system(f'{mutool} convert -F docx -O html -o {dir_out}/{leaf}.mutool.html {path}')
            
            if do_mutool_cv:
                system(f'{mutool} convert -O resolution=300 -o {dir_out}/{leaf}.mutool.cv..png {path}')
                system(f'EXTRACT_OPENCV_IMAGE_BASE={dir_out}/{leaf}.mutool.cv {mutool} convert -F docx -O html -o {dir_out}/{leaf}.mutool.cv.html {path}')
            
            if 1:
                # Copy *.pdf.acrobat.html.
                os.system(f'rsync -ai {dir_extract}/test/generated/{leaf}.acrobat.html {dir_extract}/test/generated/{leaf}.acrobat_files {dir_out}/')
            
        p(f'</ul>')
        p(f'</body>')
        p(f'</html>')
    
    path_out2 = f'{dir_out}/extract-table.html'
    shutil.copy2(path_out, path_out2)
    print(f'Have generated:')
    print(f'    {path_out}')
    print(f'    {path_out2}')


class Pdf:
    def __init__(self, path):
        self.pdf = path
        self.leaf = os.path.basename(path)
        self.out_html = f'{dir_extract}/test/generated/{self.leaf}.mutool.html'
        csv_refs_glob = f'{self.pdf[:-4]}-data-camelot-page-*-table-*.csv'
        self.csv_refs = glob.glob(csv_refs_glob)
        self.out_csv_arg = f'test/generated/{self.leaf}.mutool-%i.csv'
        if self.leaf == 'background_lines_1.pdf':
            self.csv_refs = self.csv_refs[1:]
        
    def csv_path(self, i):
        return f'test/generated/{self.leaf}.mutool-{i}.csv'
    
    def csvs(self):
        for i, csv_ref in enumerate(self.csv_refs):
            csv_generated = self.csv_path(i)
            yield csv_ref, csv_generated

def get_pdfs():
    pdfs_globs = f'{dir_pdfs}/*/*.pdf'
    print(f'pdfs_globs={pdfs_globs}')
    pdfs = glob.glob(pdfs_globs)
    for pdf in pdfs:
        print(f'yielding: {pdf}')
        yield Pdf(pdf)

def make_textfile(path_in, path_out):
    print(f'Converting {path_in} to {path_out}')
    try:
        f_in0 = open(path_in, 'rb')
    except Exception:
        return
    with f_in0 as f_in:
        with open(path_out, 'w') as f_out:
            for line in f_in:
                #print(f'line={line}')
                print(repr(line), file=f_out)

def run_tests():
    mutool = f'{dir_mupdf}/build/debug/mutool'
    num_errors = 0
    num = 0
    summary = ''
    for pdf in get_pdfs():
        print()
        print('-'*80)
        print(f'Testing: {pdf.pdf}')
        print(f'    Output: file://{os.path.abspath(pdf.out_html)}')
        command = f'{mutool} convert -F docx -O html,tables-csv-format={pdf.out_csv_arg} -o {pdf.out_html} {pdf.pdf}'
        print(f'Running: {command}')
        sys.stdout.flush()
        subprocess.check_call(command, shell=1)
        print(f'Have converted {pdf} to:')
        print(f'    {pdf.out_html}')
        print(f'    {pdf.out_csv_arg}')
        summary += f'{pdf.pdf}:\n'
        for csv_ref, csv_generated in pdf.csvs():
            make_textfile(csv_ref, f'{csv_ref}.txt')
            make_textfile(csv_generated, f'{csv_generated}.txt')
            if pdf.leaf == 'column_span_2.pdf':
                # Special case - modify reference cvs to match our (better)
                # output - we rmove a '-' in 'Preva-lence'.
                #
                with open(f'{csv_ref}.txt') as f:
                    t = f.read()
                t2 = t
                t2 = t2.replace(
                        'b\'"Investigations","No. ofHHs","Age/Sex/Physiological  Group","Preva-lence","C.I*","RelativePrecision","Sample sizeper State"\\n\'',
                        'b\'"Investigations","No. of HHs","Age/Sex/Physiological Group","Prevalence","C.I*","Relative Precision","Sample size per State"\\n\'',
                        )
                nl = '\n'
                assert t2 != t, f'lines are: {nl.join(t.split(nl))}'
                with open(f'{csv_ref}.txt', 'w') as f:
                    f.write(t2)
            # We use 'diff -w' because camelot output seems to be missing
            # spaces where it joins lines.
            #
            command_diff = f'diff -uw {csv_ref}.txt {csv_generated}.txt'
            print(f'Running command: {command_diff}')
            sys.stdout.flush()
            if subprocess.run(command_diff, shell=1).returncode:
                print(f'Diff returned non-zero.')
                num_errors += 1
                summary += f'    e=1: {csv_generated}\n'
            else:
                print(f'Diff returned zero.')
                summary += f'    e=0: {csv_generated}\n'
            num += 1
    print(f'{summary}')
    print(f'errors/all={num_errors}/{num}')

def compare_csv(ref_csv, csv):
    make_textfile(ref_csv, f'{ref_csv}.txt')
    make_textfile(csv, f'{csv}.txt')
    command = f'diff -u {ref_csv}.txt {csv}.txt'
    subprocess.run(command_diff, shell=1)

def upload():
    '''
    Upload to: julian@casper.ghostscript.com
    '''
    system('rsync -ai extract-tables-out/ julian@casper.ghostscript.com:public_html/extract/')


def main():
    parser = jlib.Arg('',
            subargs=[
                jlib.Arg('test',
                    subargs=[
                        jlib.Arg('mutool <mutool:bool>'),
                        jlib.Arg('mutool-cv <mutool-cv:bool>'),
                        jlib.Arg('camelot <camelot:bool>'),
                        jlib.Arg('-f <leaf>'),
                        ],
                    ),
                jlib.Arg('upload', help='Upload to julian@casper.ghostscript.com'),
                ],
            )
    result = parser.parse(sys.argv[1:])
    print(f'{result}')
    if result.test:
        leaf_match = result.test.f.leaf if result.test.f else None
        run_tests2(
                leaf_match = result.test.f.leaf if result.test.f else None,
                do_camelot = result.test.camelot.camelot if result.test.camelot else True,
                do_mutool = result.test.mutool.mutool if result.test.mutool else True,
                do_mutool_cv = result.test.mutool_cv.mutool_cv if result.test.mutool_cv else True,
                )
    if result.upload:
        system('rsync -ai extract-tables-out/ julian@casper.ghostscript.com:public_html/extract/')
    return
    
    args = iter(sys.argv[1:])
    while 0:
        try:
            arg = next(args)
        except Exception:
            break
        if 0:
            pass
        elif arg == 'build':
            command = f'{dir_julian_tools}/jtest.py -b ../../build/debug-extract/ .'
            subprocess.check_call(command, shell=1)
        
        elif arg == 'test':
            run_tests()
        
        elif arg == 'test2':
            run_tests2()
        
        elif arg == 'test2-1':
            run_tests2(next(args))
        
        elif arg == 'html':
            include_ref = 0
            path_out = f'{dir_mupdf}/thirdparty/extract/extract-table.html'
            print(f'Creating: {os.path.abspath(path_out)}')
            with open(path_out, 'w') as f:
                def p(text):
                    print(text, file=f)
                p(f'<html>')
                p(f'<body>')
                p(f'<h1>Extract table output ({time.strftime("%F %T")})</h1>')
                p(f'<p>&lt;source&gt;')
                if include_ref:
                    p(f' => &lt;reference html&gt;')
                p(f'<p>&lt;source&gt; => &lt;generated html&gt; &lt;generated cv.html&gt; &lt;generated docx&gt; &lt;generated odt&gt;')
                
                p(f'<ul>')
                for path in sorted(glob.glob(f'{dir_extract}/test/*.pdf')):
                    leaf = os.path.basename(path)
                    if leaf in ('Python2.pdf', 'Python2clipped.pdf', 'zlib.3.pdf', 'text_graphic_image.pdf'):
                        continue
                    p(f'    <li>{leaf} ')
                    p(f'    <ul>')
                    p(f'        <li>')
                    p(f'            <a href="test/{leaf}">{leaf}</a>')
                    if include_ref:
                        p(f'             | =&gt; <a href="test/{leaf}.mutool.html.ref">{leaf}.mutool.html.ref</a>')
                    p(f'             =&gt; ')
                    p(f'             <a href="test/generated/{leaf}.mutool.html">{leaf}.mutool.html</a>')
                    p(f'             <a href="test/generated/{leaf}.mutool.cv.html">{leaf}.mutool.cv.html</a>')
                    p(f'             | <a href="test/generated/{leaf}.mutool.docx">{leaf}.mutool.docx</a>')
                    p(f'             | <a href="test/generated/{leaf}.mutool.odt">{leaf}.mutool.odt</a>')
                    p(f'        <li>')
                    p(f'            <iframe width=30% height=300 src="test/{leaf}"></iframe>')
                    if include_ref:
                        p(f'            <iframe width=30% height=300 src="test/{leaf}.mutool.html.ref"></iframe>')
                    p(f'            <iframe width=30% height=300 src="test/generated/{leaf}.mutool.html"></iframe>')
                    p(f'            <iframe width=30% height=300 src="test/generated/{leaf}.mutool.cv.html"></iframe>')
                    #print(f'            <iframe width=25% height=300 src="test/generated/{leaf}.mutool.docx"></iframe>', file=f)
                    p(f'    </ul>')
                p(f'</ul>')
                
                print(f'</body>', file=f)
                print(f'</html>', file=f)
        
        elif arg == 'upload':
            #destination = next(args)
            destination = 'julian@casper.ghostscript.com:public_html/extract/'
            command = f'cd {dir_extract} && rsync -aiR \\\n'
            command += f' extract-table.html'
            for path in glob.glob('test/*.pdf'):
                leaf = os.path.basename(path)
                command += f' {path} \\\n'
                command += f' test/generated/{leaf}.mutool.html \\\n'
                command += f' test/generated/{leaf}.mutool.docx \\\n'
                command += f' test/generated/{leaf}.mutool.odt \\\n'
                command += f' test/{leaf}.mutool.html.ref \\\n'
                #for csv_ref, csv_generated in pdf.csvs():
                #    command += f' {csv_ref} \\\n'
                #for csv_ref, csv_generated in pdf.csvs():
                #    command += f' {csv_generated} \\\n'
            command += f' {destination}'
            print(f'Running: {command}')
            subprocess.check_call(command, shell=1)

        else:
            raise Exception(f'Unrecognised arg: {arg!r}')

if __name__ == '__main__':

    aargs.cli()
