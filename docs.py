#! /usr/bin/env python3

'''
Functions and command-line support for building sphinx documentation.

We use the :py:obj:`aargs` module to give an auto-generated command-line
interface to the Python functions in this module.

Command-line examples:

    Building sphinx docs (from .rst files, C headers and python scripts):
    
        * `./julian-tools/docs.py auto mupdf`
            Builds Sphinx MuPDF docs.

        * `./julian-tools/docs.py auto --builder pdf mupdf`
            Builds Sphinx MuPDF docs, generating PDF file instead of HTML.

        * `./julian-tools/docs.py auto --mupdf_h all mupdf`
            Builds Sphinx MuPDF docs, also including full C API description
            generated from MuPDF headers; takes approx 1 hour.

        * `./julian-tools/docs.py auto --mupdf_h doxygen mupdf`
            Builds Doxygen MuPDF docs.

        * `./julian-tools/docs.py auto julian-tools`
            Builds HTML documentation for `julian-tools` itself.

        These commands all output a description of the generated
        documentation's file/directory when they have completed.

    Building doxygen docs:
    
        * `./docs.py doxygen2 mupdf`
            Builds Doxygen docs for mupdf.
'''

import glob
import os
import platform
import re
import resource
import shlex
import shutil
import textwrap

import aargs
import conflib
import jlib
import pypackage


def _sphinx_index_rst_toctree( path):
    '''
    Finds the `.. toctree::` text in specified file.
    
    Returns `(text, toc_begin, toc_end)`.
    '''
    text = jlib.fs_read( path)
    begin = text.find( '\n.. toctree::')
    assert begin >= 0
    begin += 1
    end = begin
    pos = end
    begin = None
    assert text[pos] == '.'
    while 1:
        pos = text.find( '\n', pos)
        if begin is None:
            begin = pos+1 if pos >= 0 else len(text)
        if end is None:
            end = pos+1 if pos >= 0 else len(text)
        if pos == -1 or pos + 1 == len(text):
            break
        pos += 1
        if text[ pos] == ' ':
            end = None
        elif text[ pos] == '\n':
            pass
        else:
            break
    return text, begin, end


def _make_list( value):
    '''
    Converts None or comma-separated string into a list.
    '''
    if value is None:
        return []
    if isinstance( value, str):
        return value.split( ',')
    assert isinstance( value, list)
    return value


def _pythons_to_pythonpath( pythons):
    '''
    Returns: string suitable for :envvar:`$PYTHONPATH`, containing absolete
    paths.
    
    Args:
        pythons: List of directories.
    '''
    value = []
    for python in pythons:
        python = os.path.abspath( python)
        value.append( python)
    value = ':'.join( value)
    return dict( PYTHONPATH=value)

def _apidoc_git_excludes( directory):
    '''
    Returns list of files in `directory` that are
    not known to git.
    '''
    git_files = jlib.git_get_files( directory)
    ret = []
    for leaf in os.listdir( directory):
        if leaf.endswith( '.py') and leaf not in git_files:
            ret.append( leaf)
    return ret


def _sphinx_upload( local, name):
    '''
    Rsync documentation tree to remote site.
    '''
    jlib.system(
            f'rsync -ai {local} julian@casper.ghostscript.com:public_html/docs-{name}/',
            verbose=1,
            prefix='sphinx_upload: ',
            out='log',
            )
    if name.endswith('/'):
        jlib.log( 'https://ghostscript.com/~julian/docs-{name}')
    else:
        jlib.log( 'https://ghostscript.com/~julian/docs-{name}/{os.path.basename( local)}')


def doxygen(
        directory,
        clib_dir,
        name,
        upload=False,
        ):
    '''
    Makes Doxygen documentation.
    
    Args:
        directory:
            Output directry.
        clib_dir:
            Directory containing headers.
        name:
            Name of output documentation.
        upload:
            If true we upload.
    '''
    os.makedirs( directory, exist_ok=True)
    path = f'{directory}/doxygen.conf'
    
    jlib.system( f'doxygen -g {path}', verbose=1, prefix='doxygen -g: ')
    
    text = jlib.fs_read( path)
    text += f'\nINPUT = {clib_dir}\n'
    text += 'RECURSIVE = YES\n'
    doxygen_out = os.path.abspath( f'{directory}/_build/doxygen')
    os.makedirs( doxygen_out, exist_ok=True)
    text += f'OUTPUT_DIRECTORY = {doxygen_out}\n'
    text = re.sub( '(EXTRACT_ALL *=.*)', 'EXTRACT_ALL = YES', text)
    text = re.sub( '(GENERATE_TREEVIEW *=.*)', 'GENERATE_TREEVIEW = YES', text)
    jlib.fs_write( path, text)
    
    jlib.system( f'doxygen {path}', verbose=1, prefix='doxygen: ')
    
    root = f'{directory}/_build/doxygen/html'
    index_html = f'{root}/index.html'
    jlib.log( 'Have generated:')
    jlib.log( '    {os.path.relpath(index_html)}')
    
    if upload:
        _sphinx_upload( root, f'{name}-doxygen')


def sphinx(
        directory,
        name=None,
        version=None,
        author='',
        rst_dirs=None,
        rst_excludes=None,
        clib_dirs=None,
        python_dirs=None,
        python_excludes=None,
        no_maxwidth=True,
        theme='sphinx_rtd_theme',
        builder=None,
        extensions=None,
        black_foreground=False,
        j='auto',
        _apidoc=True,
        config: bool=True,
        build: bool=True,
        upload: bool=False,
        copy_to_downloads=True,
        pip=True,
        master_doc=None,
        ):
    '''
    Args are largely as conflib.py:init() except for `rst_dirs`, `clib_dirs`,
    `python_dirs`; relative paths in these args are made relative to
    `directory`.
    
    python_dirs:
        Items can be files.
    '''
    jlib.log( '{extensions=}')
    directory = os.path.abspath( directory)
    if builder is None:
        builder = 'html'
    
    if config:
        
        def transform_paths(paths):
            if not paths:
                return
            if isinstance(paths, str):
                paths = paths.split(',')
            ret = list()
            for path in paths:
                if not os.path.isabs(path):
                    path = os.path.relpath(path, directory)
                ret.append(path)
            return ','.join(ret)
        rst_dirs = transform_paths(rst_dirs)
        clib_dirs = transform_paths(clib_dirs)
        python_dirs = transform_paths(python_dirs)
        
        os.makedirs( directory, exist_ok=True)
        directory_git = jlib.git_get_files( directory, relative=False)
        conf_py = os.path.join( directory, 'conf.py')
        assert not conf_py in directory_git, f'Cannot config because file already in git: {conf_py}'
        shutil.copy2( os.path.abspath( f'{__file__}/../conflib.py'), directory)
        jlib.log( '{extensions=}')
        jlib.log( 'python_dirs: {python_dirs!r}')
        with open( conf_py, 'w') as f:
            f.write( textwrap.dedent(
            f'''
            """
            Example build command: sphinx-build -M html docs docs/_build
            """
            import os
            import sys

            sys.path.append( os.path.abspath( f'{{__file__}}/..'))
            import conflib
            del sys.path[-1]

            conflib.init(
                    name={name!r},
                    author={author!r},
                    version={version!r},
                    rst_dirs={rst_dirs!r},
                    rst_excludes={rst_excludes!r},
                    clib_dirs={clib_dirs!r},
                    python_dirs={python_dirs!r},
                    no_maxwidth={no_maxwidth!r},
                    theme={theme!r},
                    builder={builder!r},
                    extensions={extensions!r},
                    black_foreground={black_foreground},
                    master_doc={master_doc!r},
                    )
            '''))
    if build:
        
        # Run sphinx-build.
        jlib.log( 'Running sphinx-build. Using a venv so we can install breathe and exhale etc.')
        env_pythonpath = conflib._pythons_to_pythonpath( conflib._make_list(python_dirs))
        commands = []
        if pip:
            commands.append( 'pip install sphinx sphinx_rtd_theme furo sphinx_copybutton')
            if os.path.exists( f'{directory}/requirements.txt'):
                assert not extensions
                commands.append( f'pip install -r {directory}/requirements.txt')
            else:
                if isinstance(extensions, str):
                    extensions = extensions,
                elif extensions is None:
                    extensions = ()
                for extension in extensions:
                    commands.append( f'pip install {extension}')
                #if extensions and 'myst_parser' in extensions:
                #    commands.append( 'pip install myst_parser')
                if clib_dirs:
                    commands.append( 'pip install breathe exhale sphinx_rtd_theme')
                #if 0 or builder == 'pdf':
                if platform.system() != 'OpenBSD':
                    commands.append( 'pip install sphinx rst2pdf svglib')
        # We use the poorly-documented `-M <builder>` instead of `-b <builder>`
        # option, because output files seem to be better placed. For example
        # for normal html output, we get `_build/html/index.html` rather than
        # `_build/index.html`.
        #
        # Also, `-M` works better with `latexpdf`.
        #
        # But `-M` command line syntax is odd, it requires:
        #   `sphinx-build -M <builder> <source-dir> <build-dir> [<options>]`
        # See: https://github.com/sphinx-doc/sphinx/issues/6603
        #
        commands.append( f'sphinx-build -M {builder} . _build -j {j}')
        venv_directory = os.path.abspath( f'{__file__}/../docs-venv')
        pypackage.venv_run(
                commands,
                venv=venv_directory,
                directory=directory,
                #verbose=1,
                prefix='sphinx-build: ',
                env_extra=env_pythonpath,
                out='log',
                caller=2,
                pip_upgrade=pip,
                )
        
        if no_maxwidth:
            # Remove all maximum widths in generated .css files.
            jlib.log( '`no_maxwidth` is set: changing all `max-width` to 100% in .ccs files in {directory}/_build/html/_static.')
            for p in jlib.fs_paths( f'{directory}/_build/html/_static'):
                if p.endswith( '.css'):
                    text = jlib.fs_read( p)
                    text2 = text
                    text2 = re.sub( '[ ;]max-width: *[0-9]+px[;]', ';', text2)
                    text2 = re.sub( '[(]max-width: *[0-9]+px[)]', '(max-width: 100%)', text2)
                    if text2 == text:
                        jlib.log( 'No mention of max-width in: {p}')
                    else:
                        jlib.log( 'Removing mention of max-width from: {p}')
                    jlib.fs_write( p, text2)
        
    path = None
    path_remote = None
    if builder == 'html':
        path = f'{directory}/_build/html'
        path_remote = f'{path}/'
        jlib.log( 'Have generated:')
        jlib.log( '    {os.path.relpath( path)}')
        jlib.log( '    {os.path.relpath( path)}/index.html')
    elif builder == 'singlehtml':
        path = f'{directory}/_build/singlehtml'
        jlib.log( 'Have generated: {os.path.relpath( path)}/index.html')
    elif builder == 'pdf':
        path = f'{directory}/_build/pdf/{name}.pdf'
        jlib.log( 'Have generated: {os.path.relpath( path)}')
    else:
        jlib.log( 'No info about what we have generated: {builder=}')

    if copy_to_downloads:
        print(f'{name=} {path=} {path_remote=}')
        if not path:
            raise Exception( f'Unable to upload because do not know where output is from builder={builder}.')
        jlib.system(f'rsync -ai {path}/ ~/Downloads/docs-{name}/')
        #if name.endswith('/'):
        #    jlib.system(f'rsync -ai {path_remote} ~/Downloads/docs-{name}')
        #else:
        #    jlib.system(f'rsync -ai {path_remote}/ ~/Downloads/docs-{name}/{os.path.basename( path_remote)}')
        jlib.log(f'{os.path.expanduser("~/Downloads/docs-{name}")}')

    if upload:
        if not path:
            raise Exception( f'Unable to upload because do not know where output is from builder={builder}.')
        if not path_remote:
            path_remote = path
        _sphinx_upload( path_remote, name)
    
    
def sphinx_pymupdf2():
    '''
    Experimental PyMuPDF docs that include info from .py files and is generated
    in docs2/ subdirectory.

    The generated `modules.rst` is not added to the index so one needs to go to
    `modules.html` manually.
    '''
    pymupdf_dir = os.path.abspath( f'{__file__}/../../PyMuPDF')
    sphinx(
            f'{pymupdf_dir}/docs2',
            'PyMuPDF',
            author='Artifex',
            rst_dirs = 'PyMuPDF/docs',
            #python_dirs = '../build/lib.openbsd-7.1-amd64-3.9/',
            python_dirs = 'PyMuPDF/src/',
            #master_doc = 'toc',
            extensions='myst_parser',
            upload=1,
            )
            

def auto(
        root,
        config: bool=True,
        build: bool=True,
        upload: bool=False,
        copy_to_downloads=True,
        no_maxwidth=True,
        theme=None,
        builder=None,
        black_foreground=False,
        j='auto',
        _apidoc=True,
        pip=True,
        mupdf_h=None,
        ):
    '''
    Covenience fn for building documentation.
    
    Args:
        root:
            Path of project for which we build documentation.
            
            * If a file, we assume this file is a RST file and build docs
              using sphinx in directory `<root>-docs/`, using an auto-generated
              `conf.py` file that in turn uses :py:obj:`conflib`.
            
            * If leafname starts with `mupdf`:

                Assume `root` is a MuPDF directory.

                Generated documentation depends on `mupdf_h`:
                
                * '' or None:
                  
                  We generate documention using Sphinx and an auto-generated
                  `conf.py` file that in turn uses :py:obj:`conflib`.
                  
                  * Generate documentation from pre-existing `.rst files` in
                    `mupdf/docs/*.rst`.
                  
                  * Generate documentation from Python scripts/modules in
                    `mupdf/scripts`.
                
                * 'small':
                  Add documentation from a small number of header
                  files in `<name>/include/`.

                * 'large':
                  Add documentation from all header files in
                  `<name>/include/`. *Note*: Sphinx takes a long time to
                  process the many `.rst` files created from `mupdf/include/`,
                  so this can take a long time and require a large amount of
                  memory.
                
                * 'doxygen'
                  Do not generate sphinx documentation; instead generate only
                  doxygen documentation from mupdf/include/.
            
            * If leafname starts with 'PyMuPDF':

                We build PyMuPDF documentation in `../<name>/docs/`, using the
                existing `conf.py`.

            * If root contains this `docs.py` file, we build documentation for
              `julian-tools` itself.
        
        upload:
            If true, we upload the docs to a hard-coded location.
        
        mupdf_h:
            Only used for mupdf - see above.
        
    Other args are as in :py:obj:`sphinx()`. Most of the time the defaults
    suffice.
    '''
    root = os.path.abspath( root)
    assert os.path.exists(root), f'Does not exist: {root!r}'

    version = None

    leaf = os.path.basename( root)
    jlib.log( '{leaf=}')
    
    if os.path.isfile( root):
        jlib.log( 'Building docs for single file: {root}')
        
        if 1:
            path = os.path.abspath( root)
            leaf = os.path.basename( root)
            sphinx(
                    directory=os.path.abspath( f'{path}-docs'),
                    author='Jules',
                    name=leaf,
                    rst_dirs=path,
                    upload=upload,
                    no_maxwidth=True,
                    pip=pip,
                    extensions='myst_parser',
                    )
        else:
            directory = os.path.dirname( root)
            sphinx(
                    directory=f'{directory}/docs-raw',
                    author='Jules',
                    name=os.path.basename(root),
                    rst_dirs=directory,
                    python_dirs = directory,
                    upload=upload,
                    no_maxwidth=True,
                    pip=pip,
                    extensions='myst_parser',
                    )
        return
    
    if leaf.startswith( 'PyMuPDF'):
        # Use existing conf.py.
        jlib.log( 'Building PyMuPDF')
        sphinx(
                directory = f'{root}/docs',
                author='Artifex',
                config = False,
                name=leaf,
                upload=upload,
                builder=builder,
                )
        return
    
    extensions = None
    
    if 0:
        pass
        
    elif root == os.path.abspath( f'{__file__}/..'):
        # Build docs for julian-tools.
        jlib.log( 'Building julian-tools')
        author = 'Jules'
        clib_dirs = None
        directory = f'{root}/docs'
        name = 'julian-tools'
        python_dirs = root
        rst_dirs = root
        # This fails on OpenBSD.
        #extensions=['myst_parser']

    elif leaf.startswith('mupdfpy'):
        jlib.log( 'Building mupdfpy')
        if 0:
            jlib.system(
                    f'cd {root} && markdown_py -v -x markdown.extensions.toc README.md > README.md.html',
                    verbose=1,
                    )
            if upload:
                jlib.system(
                        f'rsync -ai {root}/README.md.html julian@ghostscript.com:public_html/mupdfpy/',
                        verbose=1,
                        )
                jlib.log( 'https://ghostscript.com/~julian/mupdfpy/README.md.html')
            return
        else:
            author = 'Jules'
            clib_dirs = None
            directory = f'{root}/docs'
            name = 'mupdfpy'
            python_dirs = [f'{root}/fitz', root]
            rst_dirs = root
            extensions=['myst_parser']
    
    elif leaf.startswith( 'mupdf.js'):
        jlib.log( 'Building mupdf.js')
        # Use existing conf.py.
        jlib.log( 'Building MuPDF.js')
        sphinx(
            directory = f'{root}/docs',
            #author='Artifex',
            config = False,
            name=leaf,
            upload=upload,
            #extensions=['sphinx_design', 'sphinx_copybutton', 'pydata_sphinx_theme'],
            )
        return
    
    elif leaf.startswith( 'mupdf'):
        jlib.log( 'Building mupdf')
        
        if 1:
            # Use existing conf.py.
            jlib.log( 'Building MuPDF')
            sphinx(
                directory = f'{root}/docs/src',
                #author='Artifex',
                config = False,
                name=leaf,
                upload=upload,
                )
            return
        
        
        author = 'Artifex'
        python_dirs=f'{root}/scripts'
        rst_dirs=f'{root}/docs'
        path_version_h = f'{root}/include/mupdf/fitz/version.h'
        with open(path_version_h) as f:
            text = f.read()
            m = re.search('#define FZ_VERSION "([0-9.]+)"', text)
            assert m, f'Cannot find FZ_VERSION in {path_version_h=}'
            version = m.group(1)
        if not mupdf_h:
            name = 'MuPDF'
            clib_dirs = None
            directory = f'{root}/docs/generated'
        
        elif mupdf_h == 'small':
            name = 'MuPDF-small'
            clib_dirs = [ f'{root}/include/mupdf/helpers', f'{root}/platform/c++/include/mupdf/exceptions.h']
            directory = f'{root}/docs/generated-{mupdf_h}'
        
        elif mupdf_h == 'all':
            name = 'MuPDF-all'
            clib_dirs = [ f'{root}/include/mupdf', f'{root}/platform/c++/include/mupdf']
            directory = f'{root}/docs/generated-{mupdf_h}'
        
        elif mupdf_h == 'doxygen':
            doxygen(
                    f'{root}/docs/generated-doxygen',
                    f'{root}/include',
                    'MuPDF',
                    upload=upload,
                    )
            return
        
        elif mupdf_h:
            raise Exception( f'Unrecognised {mupdf_h=}')
    
    else:
        raise Exception( f'Unrecognised {root=}.')
    
    sphinx(
            directory=directory,
            author=author,
            version=version,
            build=build,
            config=True,
            python_dirs=python_dirs,
            name=name,
            clib_dirs=clib_dirs,
            rst_dirs=rst_dirs,
            upload=upload,
            copy_to_downloads=copy_to_downloads,
            no_maxwidth=True,
            builder=builder,
            extensions=extensions,
            black_foreground=black_foreground,
            j=j,
            pip=pip,
            )

def blog( upload=False, pip: bool=True):
    path = os.path.abspath( f'{__file__}/../blog-mupdf-auto-c++.rst')
    name = 'Blog'
    sphinx(
            directory=os.path.abspath( f'{__file__}/../blog'),
            author='Artifex',
            name=name,
            rst_dirs=path,
            upload=upload,
            no_maxwidth=True,
            )
    if upload:
        # Upload original .rst and other versions.
        _sphinx_upload( f'{path}', name)
        for suffix in ( '.md', '.docx'):
            jlib.system( f'pandoc {path} -o {path}{suffix}')
            _sphinx_upload( f'{path}{suffix}', name)
    
    
if __name__ == '__main__':
    aargs.cli()
