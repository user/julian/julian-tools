PyMuPDF platform
================


The problem
-----------

I'm a little concerned about MuPDF.net and PyMuPDF duplicating code, and
diverging from each other as we separately add new features, fix bugs, write
new tests etc.

We have an opportunity to create an powerful cross-platform product, and
it would be a shame to duplicate development effort and have things become
inconsistent on different platforms.


A simple solution
-----------------

Probably the simplest solution would be to simply work hard on making initial
versions of MuPDF.net have the same functionality and tests as PyMuPDF, and
then keep things consistent by being rigorous about copying across changes to
code and tests, in both directions.

This would require a degree of care/obstinacy to apply it even when we're
rushing to fix/add functionality. But it's a relatively simple thing to do.

I think we should be committing to doing this as a default.


A more powerful approach
------------------------

More speculatively, i wonder whether we could take advantage of the C++ layer
that lies beneath both the Python and the C#/.net versions of what we could
call the PyMuPDF Platform API.

The C++ layer removes a whole set of book-keeping tasks from the C API 
(https://mupdf.readthedocs.io/en/latest/language-bindings.html#the-c-mupdf-api):

* Automatic handling of `fz_context`, including with threads.

* Automatic handling of reference counts for classes that wrap the underlying
  MuPDF C structs.

  * This means MuPDF instances can be treated as values - copied and moved
    around arbitrarily without any need to manually call `keep`/`drop`
    functions.

* Translation of setjmp/longjmp exceptions into C++ exceptions.

Because of these abstractions, C++ code that uses MuPDF is of a similar
abstraction level to PyMuPDF's Python and MuPDF.net's C#.

Of course some PyMuPDF Python code is Python-specific, and similarly some
MuPDF.net code is C#/.net-specific, and we would not want to rewrite this code
in C++ because it would not be usable by both PyMuPDF and MuPDF.net.

But a lot of PyMuPDF and MuPDF.net's code could be implemented in C++ code with
similar size and structure, and then be used by both PyMuPDF and MuPDF.net.

This could potentially remove a large amount of duplication from the two
products, and so improve development efficiency.

[Random thought - the rebased implementation of PyMuPDF basically moved the
classic implementation's C code up into Python. I'm talking here of moving much
of that code down into the C++ layer that is in between C and Python.]


Benefits to testing
-------------------

Moving common functionality into C++ would allow us to have tests that work on
just the C++ layer, eliminating a whole category of duplicate test code.

And of course the more functionality we manage to move into C++, the more we
will avoid test duplication.

In a sense this would be similar to some GUI designs where, because GUIs are
inherrently difficult to test, one tries to structure things so that as much
functionality as possible is independent of the GUI code. Automated testing can
then easily check all but a relatively small amount of GUI code.


Rewrite in C instead of C++?
----------------------------

Robin is looking at rewriting PyMuPDF's Python table code in C so that it runs
faster and is available beyond PyMuPDF. For tables and Robin, rewriting in C
makes more sense than rewriting in C++.

In general writing something in C instead of C++ will widen the potential uses
of it - it wil be available to all users of MuPDF, not just those that are
built on the C++ API (currently PyMuPDF and MuPDF.net). But in many cases such
code will be more difficult to write and less reliable because of C's lack of
abstractions and the need for manual resource tracking etc.

And in the specific case of rewriting PyMuPDF Python or MuPDF.net C# code, the
ability to preserve most of the code structure when using C++ will make this
particularly easy compared to C.


How this could be done
----------------------

To make it worthwhile, we'd have to identify areas where PyMuPDF Python and
MuPDF.net C# code could be split into a Python/C#-agnostic layer written in
C++, and separate Python/C# code that provides the PyMuPDF and MuPDF.net API.

Assuming there are a significant number of places where this can be done, we
would end up with a new enhanced C++ API that provides abstractions on the
MuPDF C++ API. PyMuPDF and MuPDF.net would be changed to use a SWIG interface
on to the enhanced C++ API as well as the existing SWIG interface onto the core
MuPDF C++ API.

We would rewrite some PyMuPDF/MuPDF.net tests so that they work on just the
enhanced C++ API.

We could structure things in different ways:

*
  Have a new Git repository containing just the new enhanced C++ API, which
  is then used by the PyMuPDF and MuPDF.net repositories, possibly as a
  sub-module.

*
  Or merge PyMuPDF and MuPDF.net into a single repostory that also contains the
  enhanced C++ API.
