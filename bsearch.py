#! /usr/bin/env python3


def find_zero(fn, *, epsilon_x=None, epsilon_y=None, x_start=None, x_min=None, x_max=None):
    '''
    Finds x where fn(x) is near to zero or region of x where fn(x) changes from
    negative to positive.
    
    Returns `x, (x_min, x_max)` where all of the following hold.
        `x = (x_min+x_max)/2`
        `fn(x_min) < 0`
        `fn(x_max) > 0`
        One or both of:
            `epsilon_x` is not `None` and `x_max-x_min < epsilon_x`.
            `epsilon_y` is not `None` and `abs(fn(x)) < epsilon_y`.
    
    Args:
        fn:
            fn(x) can be discontinuous but must never decrease with increasing
            x.
        epsilon_x:
            If not None, we return if `x_max-x_min < epsilon_x`.
        epsilon_y:
            If not None, we return if `abs(fn(x)) < epsilon_y`.
        x_start:
            If not None, the starting position of x.
        x_min:
            Minimum allowed x.
        x_max:
            Maximum allowed x.
    
    Constraints:
        `epsilon_x` and `epsilon_y` must not both be None.
        `x_start`, `x_min`, and `x_max` must not all be None.
    
    >>> import math
    >>> import sys
    >>> def fn(x):
    ...     return math.sin(x) - 0.5
    >>> x, _ = find_zero(fn, epsilon_y=0.001, x_start=-1)
    >>> print(f'{x=}', file=sys.stderr)
    >>> assert fn(x) < 0.001
    '''
    assert epsilon_x is not None or epsilon_y is not None
    if x_min is None or x_max is None:
        # Find missing x_min or x_max so we can do the binary search.
        if x_start is None:
            if x_min is None:
                assert x_max is not None
                x = x_max
            else:
                x = x_min
        else:
            x = x_start
        y = fn(x)
        #print(f'{x=} {y=}')
        if y <= 0:
            if x_max is None:
                x_max = find_external(fn, x, +1)
            if y < 0:
                x_min = x
        if y >= 0:
            if x_min is None:
                x_min = find_external(fn, x, -1)
            if y > 0:
                x_max = x
    #print(f'{x_min=} {x_max=}')
    assert x_min is not None
    assert x_max is not None
    
    if x_start is None:
        x_start = (x_min + x_max) / 2
    assert x_min <= x_start <= x_max
    
    # Do the binary search in region x_min..x_max.
    x = x_start
    while 1:
        if epsilon_x is not None and x_max - x_min < epsilon_x:
            #print(f'{epsilon_x=} {x_max=} {x_min=} {x_max-x_min=}')
            return x, (x_min, x_max)
        y = fn(x)
        if epsilon_y is not None and abs(y) < epsilon_y:
            #print(f'{epsilon_y=} {x_max=} {x_min=} {x=} {y=}')
            return x, (x_min, x_max)
        if y > 0:
            x_max = x
        else:
            x_min = x
        x = (x_min + x_max) / 2


def find_zero_region(fn, epsilon_x, epsilon_y, *, x_start=None, x_min=None, x_max=None):
    '''
    Returns `(x_min, x_max)` for region in which `abs(fn(x)) < epsilon_y`.
    
    Args:
        fn:
            As for `bsearch()`, but will usually have an extended region where
            `abs(fn(x)) < epsilon_y`.
        epsilon_x:
            Maximum error in returned `x_min` and `x_max` values.
        epsilon_y:
            Maximum value of `abs(fn(x))` in flat region.
        x_start:
            Starting position.
    
    For example this function returns zero for 11 <= x < 13
    
        >>> def fn(x):
        ...     if x < 11:
        ...         return -1
        ...     if x < 13:
        ...         return 0
        ...     return +1
    
    And we can find this zero region with:
    
        >>> x_min, x_max = find_zero_region(fn, 0.001, 0.5, x_start=8, x_min=0)
        >>> import sys
        >>> print(f'{x_min=} {x_max=}', file=sys.stderr)
        >>> abs(x_min - 11) < 0.001
        True
        >>> abs(x_max - 13) < 0.001
        True
    '''
    # We do separate searches for when fn(x) first goes above -epsilon_y and
    # when it first goes above +epsilon_y.
    def fn2(x):
        return fn(x) + epsilon_y
    x_min2, _ = find_zero(fn2, epsilon_x=epsilon_x, x_start=x_start, x_min=x_min, x_max=x_max)
    def fn2(x):
        return fn(x) - epsilon_y
    x_max2, _ = find_zero(fn2, epsilon_x=epsilon_x, x_min=x_min2, x_max=x_max)
    return x_min2, x_max2


def find_external(fn, x, direction):
    '''
    Searches from x in positive/negative direction until one of:
    
        `direction==+1 and fn(x) > 0`
        `direction==-1 and fn(x) < 0`
    
    Args:
        fn:
            .
        x:
            Starting point.
        direction:
            +1 or -1.
    '''
    if direction == 1:
        if x < 0:
            x = abs(x)
        elif x == 0:
            x = 1
        else:
            x *= 2
    elif direction == -1:
        if x > 0:
            x = -abs(x)
        elif x == 0:
            x = -1
        else:
            x *= 2
    else:
        assert 0, f'{direction=}'
    while 1:
        y = fn(x)
        if direction == +1 and y > 0:
            return x
        if direction == -1 and y < 0:
            return x
        x *= 2


