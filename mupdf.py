#!/usr/bin/env python3

import textwrap

import os
import platform
import shlex
import sys

sys.path.append(f'{__file__}/..')
import aargs
import jlib
import pypackage
import pymupdf


julian_tools_dir = os.path.relpath(os.path.abspath(f'{__file__}/../'))
g_mupdf_dir = os.path.relpath(os.path.abspath(f'{julian_tools_dir}/../mupdf-master'))
pymupdf_dir = os.path.relpath(os.path.abspath(f'{julian_tools_dir}/../PyMuPDF'))

def mupdf_dir(d):
    global g_mupdf_dir
    g_mupdf_dir = d

def system(command):
    return jlib.system(command, verbose=1)

def test1():
    system(f'cd {g_mupdf_dir} && HAVE_GLUT=no HAVE_PTHREAD=yes CXX=clang++ gmake shared=yes verbose=yes build=debug')
    src = textwrap.dedent(f'''
            #include "mupdf/fitz.h"
            
            int main()
            {{
                fz_context* ctx = fz_new_context(NULL, NULL, FZ_STORE_DEFAULT);
                const char* html = "<p style=\\"font-family: test;color: blue\\">We shall meet again at a place where there is no darkness.</p>";
                fz_archive* archive = fz_open_directory(ctx, ".");
                fz_buffer* buffer = fz_new_buffer_from_copied_data(ctx, html, strlen(html)+1);
                fz_story* story = fz_new_story(
                        ctx,
                        buffer,
                        "@font-face {{font-family: test; src: url({julian_tools_dir}/mupdf_test1.otf);}}",
                        11,
                        archive
                        );
                fz_document_writer* writer = fz_new_pdf_writer(ctx, "mupdf_test1.pdf", "");
                fz_rect rect = {{10, 10, 300, 300}};
                for(;;)
                {{
                    fz_device* dev = fz_begin_page(ctx, writer, rect);
                    fz_rect filled;
                    int more = fz_place_story(ctx, story, rect, &filled);
                    fz_draw_story(ctx, story, dev, fz_identity);
                    fz_end_page(ctx, writer);
                    if (!more)
                        break;
                }}
                fz_drop_story(ctx, story);
                fz_drop_buffer(ctx, buffer);
                fz_close_document_writer(ctx, writer);
                fz_drop_document_writer(ctx, writer);
                fz_drop_archive(ctx, archive);
                return 0;
            }}
            ''')
    with open('/tmp/mupdf_test1.c', 'w') as f:
        f.write(src)
    system(f'cc -g -o /tmp/mupdf_test1.exe /tmp/mupdf_test1.c -I {g_mupdf_dir}/include -L {g_mupdf_dir}/build/shared-debug -l mupdf')
    system(f'LD_LIBRARY_PATH={g_mupdf_dir}/build/shared-debug /tmp/mupdf_test1.exe')


def test2():
    src = textwrap.dedent(f'''
            #include "mupdf/fitz.h"
            
            int main()
            {{
                fz_context* ctx = fz_new_context(NULL, NULL, FZ_STORE_DEFAULT);
                fz_register_document_handlers(ctx);
                fz_document* document = fz_open_document(ctx, "{pymupdf_dir}/tests/resources/v110-changes.pdf");
                fz_page* page = fz_load_page(ctx, document, 0);
                fz_rect rect = fz_bound_page(ctx, page);
                fz_stext_page* stext_page = fz_new_stext_page(ctx, rect);
                fz_stext_options    options = {{0}};
                fz_device* device = fz_new_stext_device(ctx, stext_page, &options);
                fz_run_page(ctx, page, device, fz_identity, NULL);
                fz_close_device(ctx, device);
                fz_drop_device(ctx, device);
                fz_drop_stext_page(ctx, stext_page);
                fz_drop_page(ctx, page);
                fz_drop_document(ctx, document);
                fz_drop_context(ctx);
                return 0;
            }}
            ''')
    with open('/tmp/mupdf_test2.c', 'w') as f:
        f.write(src)
    build = 'memento'
    system(f'cd {g_mupdf_dir} && ./scripts/mupdfwrap.py -d build/shared-{build} -b m')
    system(f'cc -g -o /tmp/mupdf_test2.exe /tmp/mupdf_test2.c -I {g_mupdf_dir}/include -L {g_mupdf_dir}/build/shared-{build} -l mupdf -lm')
    system(f'LD_LIBRARY_PATH={g_mupdf_dir}/build/shared-{build} /tmp/mupdf_test2.exe')


def test3():
    name = 'test3'
    src = textwrap.dedent(f'''
            #include "mupdf/fitz.h"
            
            int main()
            {{
                fz_context* ctx = fz_new_context(NULL, NULL, FZ_STORE_DEFAULT);
                fz_register_document_handlers(ctx);
                fz_document* document = fz_open_document(ctx, "{pymupdf_dir}/tests/resources/v110-changes.pdf");
                fz_drop_document(ctx, document);
                fz_drop_context(ctx);
                return 0;
            }}
            ''')
    with open(f'/tmp/mupdf_{name}.c', 'w') as f:
        f.write(src)
    build = 'debug'
    system(f'cd {g_mupdf_dir} && ./scripts/mupdfwrap.py -d build/shared-{build} -b m')
    system(f'cc -g -o /tmp/mupdf_{name}.exe /tmp/mupdf_{name}.c -I {g_mupdf_dir}/include -L {g_mupdf_dir}/build/shared-{build} -l mupdf -lm')
    system(f'LD_LIBRARY_PATH={g_mupdf_dir}/build/shared-{build} valgrind --leak-check=full --show-leak-kinds=all /tmp/mupdf_{name}.exe')

def test4():
    name = 'test4'
    src = textwrap.dedent(f'''
            import mupdf
            mupdf.fz_register_document_handlers()
            document = mupdf.fz_open_document("{pymupdf_dir}/tests/resources/v110-changes.pdf")
            #document = None
            if 0:
                import mupdf
                mupdf.fz_register_document_handlers()
                document = mupdf.fz_open_document("{pymupdf_dir}/tests/resources/v110-changes.pdf")
                document = None
            ''')
    with open(f'/tmp/mupdf_{name}.py', 'w') as f:
        f.write(src)
    build = 'debug'
    system(f'python3 -m venv pylocal')
    system(f'./pylocal/bin/python -m pip install --upgrade pip libclang swig')
    system(f'./pylocal/bin/python {g_mupdf_dir}/scripts/mupdfwrap.py -d {g_mupdf_dir}/build/shared-{build} -b all')
    system(f'MUPDF_trace_keepdrop=1 MUPDF_trace=1 PYTHONPATH={g_mupdf_dir}/build/shared-{build} PYTHONMALLOC=malloc valgrind ./pylocal/bin/python /tmp/mupdf_{name}.py')


def test_python_pip():
    venv_name = 'pylocal'
    d = os.path.relpath(g_mupdf_dir)
    command = ''
    command += f'pip install -vv ./{d} && python {d}/scripts/mupdfwrap.py -d - --test-python'
    pypackage.venv_run(command)


def venv( **args):
    import pymupdf
    pymupdf.venv(**args)


def build_test(
        remote=None,
        mupdf='mupdf',
        test='python',
        venv: bool=None,
        build='release',
        b0=True,
        do_build=True,
        pip=True,
        py=None,
        m_target=None,
        ):
    '''
    MuPDF build and test inside a venv, optionally on remote machine.
        remote
            Name of remote machine or None to run on local machine.
        mupdf
            Location of MuPDF checkout.
        test
            Should be one of:
            
                pip     Does `pip install`.
                
                python  Builds Python bindings and does --test-python.
                
                csharp  Builds C# bindings and does --test-csharp.
                
                cpp     Builds C++ bindings and does --test-cpp.
                
                pdfdata Builds C++ bindings and runs scripts/pdfdata.py.
                
                internal-cpp    Does --test-internal-cpp
        venv
            Internal.
        build
            'release' or 'debug'.
        py
        m_target
    '''
    assert test in ('pip', 'python', 'csharp', 'cpp', 'pdfdata', 'internal-cpp')
    import pymupdf
    openbsd = (platform.system() == 'OpenBSD')
    if venv is None and not openbsd:
        venv = True
    if remote:
        if 'windows' in remote:
            if py is None:
                py = 'py'
        return pymupdf._run_remote(remote, mupdf, py=py)
        #remote_command = ''
        #remote_command += f'./julian-tools/mupdf.py build_test'
        #remote_command += pymupdf._args( locals(), ('venv', 'mupdf', 'test', 'build', 'b0', 'do_build'))
        #command = f'{sys.executable} julian-tools/jtest.py remote {remote},{mupdf},julian-tools {shlex.quote(remote_command)}'
        #jlib.system(command, bufsize=0)
    
    elif venv:
        command = f'python julian-tools/mupdf.py build_test --venv=0'
        command += pymupdf._args( locals(), ('mupdf', 'test', 'build', 'b0', 'do_build', 'm_target'))
        packages = []
        packages.append('--upgrade')
        if test != 'pip':
            sys.path.insert(0, mupdf)
            import setup
            del sys.path[0]
            packages += setup.get_requires_for_build_wheel()
        command = '"swig" -version && ' + command
        pymupdf.venv( command, packages=packages, system_site_packages=openbsd)
    
    else:
        if test == 'pip':
            if platform.system() == 'OpenBSD':
                assert 0, f'On OpenBSD pyproject.toml libclang does not work.'
                # `--no-deps` does not seem to prevent pip from installing libclang.
                #jlib.log('On OpenBSD pyproject.toml libclang does not work.')
                #command = f'pip install --no-deps -vv ./{mupdf}'
            else:
                jlib.system( f'pip install -vv ./{mupdf}')
        elif test == 'internal-cpp':
            command = f'{sys.executable} {mupdf}/scripts/mupdfwrap.py --test-internal-cpp'
            jlib.system( command)
        else:
            command = f'{sys.executable} {mupdf}/scripts/mupdfwrap.py'
            d = ''
            if build == 'release':
                pass
            elif build == 'debug':
                command += f' -d {mupdf}/build/shared-{build}'
            else:
                assert 0, f'Unrecognised {build=}.'
            
            if do_build:
                command += ' -b'
                if m_target:
                    command += f' --m-target {m_target}'
                if test == 'csharp':
                    command += ' --csharp'
                if test in ('python', 'csharp', 'pdfdata'):
                    command += ' all' if b0 else ' m123'
                else:
                    command += ' m01' if b0 else ' m1'
            if test == 'python':
                command += ' --test-python'
            if test == 'csharp':
                command += ' --test-csharp'
            if test == 'cpp':
                command += ' --test-cpp'
            
            jlib.system( command)
            
            if test == 'pdfdata':
                if pip:
                    pypackage.venv_run( 'pip install jsonpickle')
                #pypackage.venv_run( f'PYTHONPATH={mupdf}/build/shared-{build} ./{mupdf}/scripts/pdfdata.py {mupdf}/thirdparty/zlib/zlib.3.pdf')
                pypackage.venv_run(
                        #f'PYTHONPATH={mupdf}/build/shared-{build} ./{mupdf}/scripts/pdfdata.py -r 0 -s jsonpickle {mupdf}/thirdparty/zlib/zlib.3.pdf',
                        f'PYTHONPATH={mupdf}/build/shared-{build} ./{mupdf}/scripts/pdfdata.py -r 1 -i -refs -s jsonpickle,json {mupdf}/thirdparty/zlib/zlib.3.pdf',
                        pip_upgrade=pip,
                        )
                

def build_wheel(remote=None, mupdf='mupdf', build_isolation=True):
    if remote:
        remote_command = ''
        if 'windows' in remote:
            remote_command += 'py '
        remote_command += 'julian-tools/mupdf.py '
        remote_command += aargs.fncall_text( locals_override=dict(remote=None))
        #remote_command += f'./julian-tools/mupdf.py build_wheel'
        #remote_command += pymupdf._args( locals(), ('mupdf'))
        command = f'{sys.executable} julian-tools/jtest.py remote {remote},{mupdf},julian-tools {shlex.quote(remote_command)}'
        return jlib.system(command)
    
    pypackage.venv_run( f'cd {mupdf} && pip wheel {"" if build_isolation else "--no-build-isolation"} -vv --wheel-dir dist .')
    jlib.log( f'Wheel is in: {mupdf}/dist')


if __name__ == '__main__':
    aargs.cli()
