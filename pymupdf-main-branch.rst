PyMuPDF single `main` branch
============================

We have moved all PyMuPDF development to a single new branch `main`.

The new branch `main` is now the repository's default branch; so for example a
`git clone` will result in a checkout of `main`, not `master`.

The old branches `1.21` and `master` will no longer be used.

Reasons for doing this:

* The use of a separate release branch wasn't really helping PyMuPDF
  development, because we don't generally release patches for old releases.

*
  We've recently started testing the latest PyMuPDF in git with different
  versions of MuPDF, using a small number of `#if...#endif` constructs in the
  code to cope with the occasional changes to the MuPDF API.

  This has proved to be much simpler in practise than, for example, having
  a separate branch of PyMuPFD for the current MuPDF `1.xx` release and for
  MuPDF's master branch.

*
  Github doesn't make it easy for workflows to be on a non-default branch. So
  we previously had (most) workflows on `master`, even though all code/docs
  work was on branch `1.21`. This was confusing to work with.

*
  We are using `main` instead of `master` for the reasons described in
  https://github.com/github/renaming.

The new branch `main` is a copy of the old `1.21` branch, with some new commits
added:

*
  A commit to revert the small number of unused commits to workflows that were
  made on branch `1.21` after it branched off `master`.

*
  Cherry-picks of all workflow-related commits on `master` since `1.21` was
  created.

Thus branch `main` now contains all of the useful code, docs and workflows from
the old branches `1.21` and `master`.

Notes about changing history:

*
  The git history of code/docs commits (from branch `1.21`) is unchanged.

*
  The history of workflow scripts etc (from `master`) is changed in git.

Moving to a single branch will always involve changing history somewhere; we've
choosen to do this for the relatively small number of workflow commits, not
the large number of code/docs commits.

Here's an ascii-art description of the branches, oldest commits first.
(f0b0a93a is the git sha where branch `1.21` was created from `master`.)::

    branch 1.21         master          main
    -----------         ------          ----------
    ...                 ...             ...
    f0b0a93a            f0b0a93a        f0b0a93a
    code-1              workflow-1      code-1
    workflow-unused     code-unused     workflow-unused
    code-2              workflow-2      code-2
    code-3              workflow-3      code-3
    ...                                 ...
    code-N                              code-N
                                        ~workflow-unused (revert)
                                        workflow-1
                                        workflow-2
                                        workflow-3
