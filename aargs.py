'''
Summary:
    Provides an automatically-generated command-line interface onto a Python
    module's functions.

    * We support required (positional) and optional args, determined by whether
      function args have default values.
    
    * Where function args have `Python type annotations
      (<https://peps.python.org/pep-3107/>`_
      `<https://docs.python.org/3/library/typing.html>`_) the command-line API
      will only accept items that can be converted into the specified types.

    * We can show help on available functions and their args.

    * We show detailed information after errors.

    * All functionality is provided by the function `aargs.cli()`.

Basic example:
    If `foo.py` contains::

        import math

        class Point:
            def __init__( self, x: float, y: float):
                self.x = x
                self.y = y

        def area( width: float, height: float):
            return width * height

        def distance( a: Point, b: Point):
            return math.sqrt( (a.x - b.x)**2 + (a.y - b.y)**2)

        def mul_text( x, n: int=1):
            return x * n
        
        def mul_number( x: float, n: int=1):
            return x * n
        
        if __name__ == '__main__':
            import aargs
            aargs.cli()

    Then it can be used from the command like this::

        $ python foo.py area 2.5 7  distance 1 2 4 6  mul_text 123  mul_text -n 3 123  mul_number -n 3 123
        17.5
        5.0
        123
        123123123
        369.0
        $

    See below for various `doctest
    <https://docs.python.org/3/library/doctest.html>`_ examples which explain
    things in more detail.

Background:
    * We emphasize giving a useful automatically-generated command-line API for
      unmodified Python functions in a module.
    
    * We do not require that functions are decorated or enumerated manually.

    * We do not attempt to treat dashes as underscores when matching
      command-line items to Python function/arg names. So for example to call a
      function `foo_bar()`, one has to specify **foo_bar ...**, not **foo-bar
      ...**.

    * We implement typing by providing generic support for Python class
      annotation types, instead of requiring Python functions to have custom
      annotations.

    * By default we provide possibly too much information about parse failures.
      This can be controled with `aargs.cli()`'s `infos` arg.

    * We do not handle decorators specially or support custom parsers.

    * We do not attempt to parse docstrings (for example to constrain arg types
      separately from annotations); we show them verbatim when displaying help.

    * Works with python-3.6 or later, using only modules that are part of a
      normal Python installation.

    * Partly inspired by `clize <https://github.com/epsy/clize>`_ and `argh
      <https://github.com/E-Tahta/python-argh>`_.
      
      Unlike `clize`, aargs uses only standard Python Modules, and uses native
      Pythn class annotation types instead of custom annotation functions,
'''

import doctest
import gc
import inspect
import io
import os
import re
import shlex
import shutil
import subprocess
import sys
import textwrap
import traceback
import typing

try:
    import jlib
except ImportError: 
    jlib = None


def cli( argv=None, fns=1, a=None, infos='tch', fileline=False, do_exit=True, help_post=True, show_return=True):
    '''
    Implements an auto-generated command-line interface onto a set of
    functions, by calling function with args created from sequences of
    items in `argv`.

    One can give a command line interface onto all Python functions in a `.py`
    file with::

        if __name__ == '__main__':
            import aargs
            aargs.cli()

    Args:
        argv:
            The command line to parse.

            If `None`, we use `sys.argv`. Otherwise a list of
            strings, or a string which we split using `shlex.split()
            <https://docs.python.org/3/library/shlex.html#shlex.split>`_.
        fns:
            Specifies the set of functions that we can call:

            * A list of items, each of which is either `(name, fn)` or `fn`,
              where `fn` is a callable. If `name` is not specified, we use
              `fn.__name__`.

            * An integer, how far up the stack to look for our caller's
              module.

            * Default value `1` finds all functions in our caller's module.
        a:
            Index of first item to consider in `argv`.

            * If `None`:

              * If `argv` is also `None` we default to `1` so we work with
                naturally with `sys.argv`.
              * Otherwise defaults to `0`.
        infos:
            Controls diagnostics after errors. A string made from
            single-character flags:

            * **c**  - Show arg string failure regions with caret markers (e.g. '^^^^').

            * **h**  - Show help for the function whose args we failed to match.

            * **t**  - Show a text description of argv failure.

            See `doctest <https://docs.python.org/3/library/doctest.html>`_
            samples below for example output.
        fileline:
            If true, help and diagnostics will include `file:line` of functions
            in `fns`.
        do_exit:
            See 'Returns' below.
        help_post:
            If true, we show help if first arg to a function is **-h** or
            **--help**. This can prevent correct handling of optional args
            called `h` or `help`.
        show_return:
            If true, we show the result of calling each function, unless it
            returned `None`.

    Returns:
        By default (`do_exit` is `True`) we call `sys.exit(0)` on success or
        `sys.exit(1)` on failure.

        Otherwise we return `None` on success or -1 on failure.

    Overview:
        If `argv[a]` matches a function name, we attempt to match the
        function's optional and required args with some or all of the remaining
        items in `argv`, ending up with a `(*args, **kwargs)` pair which is
        used to execute the function call `<function>(*args, **kwargs)`.
        
        By default (`show_return` is true) we print the result of the function
        call to `stdout` (unless it returned `None`).

        We then repeat for the remaining items in `argv` until there are no
        more items left.

        * A function arg is considered optional if it has a default value.

        * If a function's args have `Python type annotations
          <https://peps.python.org/pep-3107/>`_, we construct instances of the
          required types from items in `argv` using, for example, `int()` or
          `float()` or class `__init__()` constructors etc.

        * If a function call raises an exception, we show information about the
          exception and usage information, and by default (`do_exit` is true)
          call `sys.exit(1)`.

        * By default (`help_post` is true) we show help if we find **-h**,
          **--help** or **--help-full** when trying to match a function name.

        * If called with no args, i.e. `aargs.cli()`, we look at `sys.argv[1:]`
          and match with all of the functions in our caller's module. So one
          can give a command line interface onto all Python functions in a
          `.py` file simply by calling `aargs.cli()`.

    Matching a function's args:
        When attempting to match a function, we first try to match optional
        args. These match if the next item in `argv` is **--<arg-name>** (or
        **-<arg-name>** if `arg-name` is a single character).

        Then we match each required (positional) arg in turn. [So while Python
        function definitions have positional arguments first followed by
        optional arguments, on an `aargs` command line this is reversed.]

        For each arg that is annotated with a type, we create an instance of
        this type from some of the remaining items in `argv`, and add this
        instance to our `(*args, **kwargs)`. This happens recursively if the
        annotation type's `__init__()` constructor itself has one or more args
        that are annotated with a type.

        We don't yet attempt to match annotations that use `typing.Union` etc.

    Special case handling of args annotated with `bool`:
        If a required arg is annotated as bool, we match the following values:

            * **0**
            * **false**
            * **False**
            * **1**
            * **true**
            * **True**

        If a bool arg is optional with default value false, the presence of
        **--<fn_arg.name>** on its own sets it to `True` immediately; one does
        not specify the bool value with a second arg.

    Use of `<name>=<value>` args:
        One can optionally specify the value of a simple arg that is
        constructed from a single item in `argv` (for example an arg with
        no annotation, or an arg annotated with a basic type such as `int`,
        `float`, `bool`, `str`) by using **=**.

        For example **--foo 1.23** can be written **--foo=1.23**.

        With optional bool args this allows one to change the value without
        adding/removing the whole arg name:

            * **--opt=0**       - Sets `opt` to `False`.
            * **--opt=false**   - Sets `opt` to `False`.
            * **--opt=1**       - Sets `opt` to `True`.

    Here is an example of basic operation calling a function with simple
    required (positional) and optional args:

        >>> def simple( r, opt=None):
        ...     """
        ...     This is simple().
        ...
        ...     Explanation for how to use simple().
        ...     """
        ...     print( f'simple(): r={r!r} opt={opt!r}')

        Specify just the required arg `r`:

            >>> cli( 'simple 0')
            simple(): r='0' opt=None

        Set optional arg `opt` to 'qwerty' with **--opt**:

            >>> cli( 'simple --opt qwerty 2')
            simple(): r='2' opt='qwerty'

        One can also use **=** within args to specify name=value:

            >>> cli( 'simple --opt=qwerty r=2')
            simple(): r='2' opt='qwerty'

    Now define a function with type annotations:

        >>> def foo( r: int, opt: bool=False):
        ...     """
        ...     This is foo
        ...
        ...     Explanation for how to use foo().
        ...     """
        ...     if r < 0:
        ...         raise Exception( f'r must not be negative: r={r}')
        ...     print( f'foo(): r={r} opt={opt}')

        Specify just the required arg `r`:

            >>> cli( 'foo 0')
            foo(): r=0 opt=False

        Set optional bool arg `opt` to `True` with **--opt**:

            >>> cli( 'foo --opt 2')
            foo(): r=2 opt=True

        One can also use **=** within args to specify name=value:

            >>> cli( 'foo --opt=false r=2')
            foo(): r=2 opt=False

        We show detailed information if the parse fails (see `aargs.cli()`'s
        `info` arg).
        
            For example if an optional arg is specified more than once:

                >>> cli( 'foo --opt=false --opt r=2')
                <BLANKLINE>
                Duplicate optional arg specified: [--opt], in argv 2: --opt
                This caused: No match for foo [--opt] <r:int>, in argv 0..2: foo --opt=false --opt
                <BLANKLINE>
                argv 0..2: foo --opt=false --opt
                                           ^^^^^ Duplicate optional arg specified: [--opt]
                           ^^^^^^^^^^^^^^^^^^^^^ No match for foo [--opt] <r:int>
                <BLANKLINE>
                Usage:
                    foo [--opt] <r>
                        r: An integer.
                <BLANKLINE>
                        This is foo
                <BLANKLINE>
                <BLANKLINE>
                        Explanation for how to use foo().
                <BLANKLINE>
                <BLANKLINE>

            Or if an arg does not match the required type:

                >>> cli( 'foo qwerty r=2')    # doctest: +REPORT_UDIFF +ELLIPSIS
                <BLANKLINE>
                Exception when handling argv 1: qwerty
                ValueError: invalid literal for int() with base 10: 'qwerty'
                Traceback (most recent call last):
                    ...
                This caused: No match for <r:int>, in argv 1: qwerty
                This caused: No match for foo [--opt] <r:int>, in argv 0..1: foo qwerty
                <BLANKLINE>
                argv 0..1: foo qwerty
                               ^^^^^^ invalid literal for int() with base 10: 'qwerty'
                               ^^^^^^ No match for <r:int>
                           ^^^^^^^^^^ No match for foo [--opt] <r:int>
                <BLANKLINE>
                Usage:
                    foo [--opt] <r>
                        r: An integer.
                <BLANKLINE>
                        This is foo
                <BLANKLINE>
                <BLANKLINE>
                        Explanation for how to use foo().
                <BLANKLINE>
                <BLANKLINE>

        Show detailed help on calling `foo()` with **-h foo** or **foo -h**:

            >>> cli( '-h foo')
            Detailed help for function 'foo':
            <BLANKLINE>
                foo [--opt] <r:int>
                    r: An integer.
            <BLANKLINE>
                    This is foo
            <BLANKLINE>
            <BLANKLINE>
                    Explanation for how to use foo().
            <BLANKLINE>
            <BLANKLINE>
            Use "-h" or "--help" to show help for all available functions.
            Use "--help-full" to show detailed help for all available functions.
            Use "-h <fn1> <fn2> ..." or "--help <fn1> <fn2> ..." to show detailed help on specific functions.
            <BLANKLINE>

        If calling `foo()` raises an exception, we show the error:

            >>> cli( 'foo -1')  # doctest: +REPORT_UDIFF +ELLIPSIS
            <BLANKLINE>
            Exception: r must not be negative: r=-1
            Traceback (most recent call last):
                ...
            This caused: Function call failed: foo(-1)
            <BLANKLINE>

    We treat untyped args with default value `True` or `False`, as though they
    were annotated with type `bool`.

        >>> def foo( opt=True):
        ...     print( repr(opt))
        >>> cli( 'foo --opt=0')
        0

    Similarly we treat untyped args with an int default value as though they
    were annotated with type `int`.

        >>> def foo( opt=23):
        ...     print( repr(opt))
        >>> cli( 'foo --opt=0.4')   # doctest: +REPORT_UDIFF +ELLIPSIS
        <BLANKLINE>
        Exception when handling argv 1: 0.4
        ValueError: invalid literal for int() with base 10: '0.4'
        ...
        This caused: No match for [--opt <opt:int>], in argv 1: --opt=0.4
        This caused: No match for foo [--opt <opt:int>], in argv 0..1: foo --opt=0.4
        <BLANKLINE>
        argv 0..1: foo --opt=0.4
                       ^^^^^^^^^ invalid literal for int() with base 10: '0.4'
                       ^^^^^^^^^ No match for [--opt <opt:int>]
                   ^^^^^^^^^^^^^ No match for foo [--opt <opt:int>]
        <BLANKLINE>
        Usage:
            foo [--opt <opt=23>]
                opt: An integer, default=23.
        <BLANKLINE>

    To call a function with an arg that is annotated with a class, one has to
    specify the class's `__init__()` constructor's args:

        >>> class User:
        ...     def __init__( self, firstname, lastname):
        ...         self.firstname = firstname
        ...         self.lastname = lastname
        >>> def handle_user( user: User):
        ...     assert isinstance( user, User)
        ...     print( f'User is: {user.firstname} {user.lastname}')

        >>> cli( 'handle_user joe blogs')
        User is: joe blogs

    One can call a function with args that are annotated with classes, whose
    `__init__()` constructor's args are in turn annotated:

        >>> class Host:
        ...     def __init__( self, name, port: int):
        ...         self.name = name
        ...         self.port = port

        >>> class Remote:
        ...     """
        ...     Help for Remote.
        ...
        ...     Represents a remote host.
        ...     """
        ...     def __init__( self, user: User, host: Host, directory: str):
        ...         """
        ...         Creates from user host and directory name.
        ...         """
        ...         self.user = user
        ...         self.host = host
        ...         self.directory = directory
        ...     def __str__( self):
        ...         return f'{self.user.firstname}-{self.user.lastname}@{self.host.name}:{self.directory}'

        >>> def handle_remote( remote: Remote, f: float=0, command: str='', misc=None):
        ...     """
        ...     Help for testfn.
        ...     """
        ...     assert isinstance( remote, Remote)
        ...     if f < 0:
        ...         raise Exception( f'f is negative: {f}')
        ...     print( f'remote={remote} f={f} command={command!r} misc={misc}')

        We need to specify the various nested constructor args in the correct
        order:

            >>> cli( 'handle_remote joe blogs myhost 22 mydirectory')
            remote=joe-blogs@myhost:mydirectory f=0 command='' misc=None

        It can help to use `name=value`-style args for clarity:

            >>> cli( 'handle_remote firstname=joe lastname=blogs name=myhost port=22 directory=mydirectory')
            remote=joe-blogs@myhost:mydirectory f=0 command='' misc=None

        Specify optional arg `command` with **--command <command>**; this needs
        to be before the required (positional) args:

            >>> cli( 'handle_remote --command "foo bar" -f 0.5 firstname=joe lastname=blogs name=myhost port=22 directory=mydirectory')
            remote=joe-blogs@myhost:mydirectory f=0.5 command='foo bar' misc=None

        We get detailed diagnostics if we specify a non-float for
        `handle_remote()`'s `f` arg:

            >>> cli( 'handle_remote --command "foo bar" -f xyz firstname=joe lastname=blogs name=myhost port=22 directory=mydirectory') # doctest: +ELLIPSIS
            <BLANKLINE>
            Exception when handling argv 4: xyz
            ValueError: could not convert string to float: 'xyz'
            Traceback (most recent call last):
                ...
            This caused: No match for [-f <f:float>], in argv 3..4: -f xyz
            This caused: No match for handle_remote [-f <f:float>] [--command <command:str>] [--misc <misc>] <remote:Remote>, in argv 0..4: handle_remote --command 'foo bar' -f xyz
            <BLANKLINE>
            argv 0..4: handle_remote --command 'foo bar' -f xyz
                                                            ^^^ could not convert string to float: 'xyz'
                                                         ^^^^^^ No match for [-f <f:float>]
                       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ No match for handle_remote [-f <f:float>] [--command <command:str>] [--misc <misc>] <remote:Remote>
            <BLANKLINE>
            Usage:
                handle_remote [-f <f=0>] [--command <command=''>] [--misc <misc=None>] <remote>
                    f: A float, default=0.
                    command: A string, default=''.
                    misc: Anything, default=None.
                    remote: A Remote.
                        Help for Remote.
                        Represents a remote host.
                        <user> <host> <directory>
                            user: A User.
                                <firstname> <lastname>
                            host: A Host.
                                <name> <port>
                                    name: Anything.
                                    port: An integer.
                            directory: A string.
            <BLANKLINE>
                            Creates from user host and directory name.
            <BLANKLINE>
            <BLANKLINE>
                    Help for testfn.
            <BLANKLINE>
            <BLANKLINE>

        We get detailed diagnostics in more complicated cases where parsing
        fails for a nested type:

            >>> cli( 'handle_remote --command "foo bar" -f 0.5 firstname=joe lastname=blogs name=myhost port=x22 directory=mydirectory')    # doctest: +REPORT_UDIFF +ELLIPSIS
            <BLANKLINE>
            Exception when handling argv 8: x22
            ValueError: invalid literal for int() with base 10: 'x22'
            Traceback (most recent call last):
                ...
            This caused: No match for <port:int>, in argv 8: port=x22
            This caused: No match for <host:Host>: <name> <port:int>, in argv 7..8: name=myhost port=x22
            This caused: No match for <remote:Remote>: <user:User> <host:Host> <directory:str>, in argv 5..8: firstname=joe lastname=blogs name=myhost port=x22
            This caused: No match for handle_remote [-f <f:float>] [--command <command:str>] [--misc <misc>] <remote:Remote>, in argv 0..8: handle_remote --command 'foo bar' -f 0.5 firstname=joe lastname=blogs name=myhost port=x22
            <BLANKLINE>
            argv 0..8: handle_remote --command 'foo bar' -f 0.5 firstname=joe lastname=blogs name=myhost port=x22
                                                                                                         ^^^^^^^^ invalid literal for int() with base 10: 'x22'
                                                                                                         ^^^^^^^^ No match for <port:int>
                                                                                             ^^^^^^^^^^^^^^^^^^^^ No match for <host:Host>: <name> <port:int>
                                                                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ No match for <remote:Remote>: <user:User> <host:Host> <directory:str>
                       ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ No match for handle_remote [-f <f:float>] [--command <command:str>] [--misc <misc>] <remote:Remote>
            <BLANKLINE>
            Usage:
                handle_remote [-f <f=0>] [--command <command=''>] [--misc <misc=None>] <remote>
                    f: A float, default=0.
                    command: A string, default=''.
                    misc: Anything, default=None.
                    remote: A Remote.
                        Help for Remote.
                        Represents a remote host.
                        <user> <host> <directory>
                            user: A User.
                                <firstname> <lastname>
                            host: A Host.
                                <name> <port>
                                    name: Anything.
                                    port: An integer.
                            directory: A string.
            <BLANKLINE>
                            Creates from user host and directory name.
            <BLANKLINE>
            <BLANKLINE>
                    Help for testfn.
            <BLANKLINE>
            <BLANKLINE>

    One can also create classes at top-level, as well as call functions:

        >>> class Foo:
        ...     def __init__( self, x: int, y: bool):
        ...         self.x = x
        ...         self.y = y
        ...         print( f'Foo constructor, x={x} y={y}.')
        ...     def __str__( self):
        ...         return f'Foo( x={self.x} y={self.y})'

        >>> cli( 'Foo 42 false', Foo)
        Foo constructor, x=42 y=False.
        Foo( x=42 y=False)

        (But currently we do not auto-detect available classes.)

    Use with typing.Union:
    
        >>> class UTest:
        ...     def __init__(self, p: int, q: int):
        ...         self.p = p
        ...         self.q = q
        ...     def __repr__(self):
        ...         return f'{self.p} {self.q}'
        >>> def foo( a: typing.Union[UTest, int]):
        ...     print( f'{type(a)=} {a=}')
        ...     return
        >>> cli( 'foo 23', foo)
        type(a)=<class 'int'> a=23
        
        Error messages for unions is very poor at present.
        #>>> cli( 'foo abc', foo)
        
        >>> cli( 'foo 3 40', foo)
        type(a)=<class 'aargs.UTest'> a=3 40

    Show help with **-h**, **--help** and **--help-full**:

        >>> def foo( text: str, opt: bool=False, opt2: bool=True, x: float=-1.0):
        ...     """
        ...     foo() does this and that.
        ...
        ...     More details about foo().
        ...     """
        ...     pass
        >>> def bar( text, opt=False, opt2: bool=True, x: str=None):
        ...     """
        ...     bar() does other things.
        ...
        ...     More details about bar().
        ...     """
        ...     pass

        Basic help with **-h** or **--help** shows one or two lines per
        available function, with syntax information and the first line of any
        docstring.

            >>> cli( '-h', [foo, bar])
            Help for all available functions:
            <BLANKLINE>
                foo [--opt] [--opt2=True] [-x <x:float=-1.0>] <text:str>
                    foo() does this and that.
            <BLANKLINE>
                bar [--opt] [--opt2=True] [-x <x:str=None>] <text>
                    bar() does other things.
            <BLANKLINE>
            <BLANKLINE>
            Use "-h" or "--help" to show help for all available functions.
            Use "--help-full" to show detailed help for all available functions.
            Use "-h <fn1> <fn2> ..." or "--help <fn1> <fn2> ..." to show detailed help on specific functions.
            <BLANKLINE>

        Show full help on all available functions with **--help-full**:

            >>> cli( '--help-full', [foo, bar])
            Detailed help for all available functions:
            <BLANKLINE>
                foo [--opt] [--opt2=True] [-x <x=-1.0>] <text>
                    opt2: A bool with default value true; setting to false should be done with one of: --opt2=0 --opt2=false --opt2=False
                    x: A float, default=-1.0.
                    text: A string.
            <BLANKLINE>
                    foo() does this and that.
            <BLANKLINE>
            <BLANKLINE>
                    More details about foo().
            <BLANKLINE>
            <BLANKLINE>
                bar [--opt] [--opt2=True] [-x <x=None>] <text>
                    opt2: A bool with default value true; setting to false should be done with one of: --opt2=0 --opt2=false --opt2=False
                    x: A string, default=None.
                    text: Anything.
            <BLANKLINE>
                    bar() does other things.
            <BLANKLINE>
            <BLANKLINE>
                    More details about bar().
            <BLANKLINE>
            <BLANKLINE>
            <BLANKLINE>
            Use "-h" or "--help" to show help for all available functions.
            Use "--help-full" to show detailed help for all available functions.
            Use "-h <fn1> <fn2> ..." or "--help <fn1> <fn2> ..." to show detailed help on specific functions.
            <BLANKLINE>

        Show full help on a specific function with **-h <name>**, **--help
        <name>**, **<name> -h** or **<name> --help**:

            >>> cli( '--help foo')
            Detailed help for function 'foo':
            <BLANKLINE>
                foo [--opt] [--opt2=True] [-x <x:float=-1.0>] <text:str>
                    opt2: A bool with default value true; setting to false should be done with one of: --opt2=0 --opt2=false --opt2=False
                    x: A float, default=-1.0.
                    text: A string.
            <BLANKLINE>
                    foo() does this and that.
            <BLANKLINE>
            <BLANKLINE>
                    More details about foo().
            <BLANKLINE>
            <BLANKLINE>
            Use "-h" or "--help" to show help for all available functions.
            Use "--help-full" to show detailed help for all available functions.
            Use "-h <fn1> <fn2> ..." or "--help <fn1> <fn2> ..." to show detailed help on specific functions.
            <BLANKLINE>

            >>> cli( 'foo --help')
            Detailed help for function 'foo':
            <BLANKLINE>
                foo [--opt] [--opt2=True] [-x <x:float=-1.0>] <text:str>
                    opt2: A bool with default value true; setting to false should be done with one of: --opt2=0 --opt2=false --opt2=False
                    x: A float, default=-1.0.
                    text: A string.
            <BLANKLINE>
                    foo() does this and that.
            <BLANKLINE>
            <BLANKLINE>
                    More details about foo().
            <BLANKLINE>
            <BLANKLINE>
            Use "-h" or "--help" to show help for all available functions.
            Use "--help-full" to show detailed help for all available functions.
            Use "-h <fn1> <fn2> ..." or "--help <fn1> <fn2> ..." to show detailed help on specific functions.
            <BLANKLINE>

    Some specific doctest test cases:

        Annotation with a function uses the return value of the function when
        called with the arg:

            >>> def bar( text):
            ...     return f'bar_{text}'
            >>> def testfn( foo: bar):
            ...     print( f'testfn(): foo={foo}')

            >>> cli( 'testfn qwerty')
            testfn(): foo=bar_qwerty

        Test handling of bool args with `=`:

            >>> def main( config: bool=True, build: bool=True, upload: bool=False):
            ...     print( f'config={config} build={build} upload={upload}')

            >>> cli( 'main --config=false')
            config=False build=True upload=False

            >>> cli( 'argv0 main --config=0', a=1)
            config=False build=True upload=False

            >>> cli( 'argv0 main --config=x', a=1)
            <BLANKLINE>
            Unrecognised bool value 'x' should be one of: 0, false, False, 1, true, True, in argv 2: x
            This caused: No match for [--config]: <args> <kwargs>, in argv 2: --config=x
            This caused: No match for main [--config] [--build] [--upload], in argv 1..2: main --config=x
            <BLANKLINE>
            argv 1..2: main --config=x
                            ^^^^^^^^^^ Unrecognised bool value 'x' should be one of: 0, false, False, 1, true, True
                            ^^^^^^^^^^ No match for [--config]: <args> <kwargs>
                       ^^^^^^^^^^^^^^^ No match for main [--config] [--build] [--upload]
            <BLANKLINE>
            Usage:
                main [--config=True] [--build=True] [--upload]
            <BLANKLINE>

        Call a fn with no args:

            >>> def testfn0():
            ...    print('testfn0(): ok')
            >>> cli( 'testfn0')
            testfn0(): ok

        Call fn that does not exist:

            >>> cli( 'testfn1', testfn0)
            <BLANKLINE>
            No function matches argv[0]: testfn1, in argv 0: testfn1
            <BLANKLINE>
            argv 0..0: testfn1
                       ^^^^^^^ No function matches argv[0]: testfn1
            <BLANKLINE>
            Available functions are:
                testfn0
            <BLANKLINE>

        Test missing required arg:

            >>> def foo( a, b=0):
            ...     pass
            >>> cli( 'foo -b 1')    # doctest: +REPORT_UDIFF
            <BLANKLINE>
            Run out of args, in argv 3: 
            This caused: No match for <a>, in argv 3: 
            This caused: No match for foo [-b <b:int>] <a>, in argv 0..2: foo -b 1
            <BLANKLINE>
            argv 0..2: foo -b 1
                                Run out of args
                                No match for <a>
                       ^^^^^^^^ No match for foo [-b <b:int>] <a>
            <BLANKLINE>
            Usage:
                foo [-b <b=0>] <a>
                    b: An integer, default=0.
                    a: Anything.
            <BLANKLINE>

        It is an error if next unused arg starts with `-` or 0-9 - this
        avoids running functions with unintended args due to mis-typing of an
        optional arg.

            >>> def foo( a):
            ...     print('foo called')
            >>> cli( 'foo 1 -B')
            <BLANKLINE>
            Unrecognised trailing arg: '-B', in argv 2: -B
            This caused: No match for foo <a>, in argv 0..2: foo 1 -B
            <BLANKLINE>
            argv 0..2: foo 1 -B
                             ^^ Unrecognised trailing arg: '-B'
                       ^^^^^^^^ No match for foo <a>
            <BLANKLINE>
            Usage:
                foo <a>
            <BLANKLINE>

            >>> cli( 'foo 1 5')
            <BLANKLINE>
            Unrecognised trailing arg: '5', in argv 2: 5
            This caused: No match for foo <a>, in argv 0..2: foo 1 5
            <BLANKLINE>
            argv 0..2: foo 1 5
                             ^ Unrecognised trailing arg: '5'
                       ^^^^^^^ No match for foo <a>
            <BLANKLINE>
            Usage:
                foo <a>
            <BLANKLINE>

    A note about our use of doctest:
        To make the above examples easier to read, we use some slightly
        modified defaults when we are run by doctest:

        * We detect running under doctest by searching the stack for a frame
          that looks like it could be from Python's `doctest` module. If we
          find such a frame, we set `do_exit` to special value 'doctest' which
          makes us always return `None` regardless of success or failure.
          
          This avoids our default behaviour of calling `sys.exit()`, which
          would otherwise kill the doctest session. It also avoids the need for
          a potentially confusing trailing '-1' in the doctest expected output
          when testing error cases.

        * If our caller does not seem to be running in a module
          (`inspect.getmodule()` returns `None`, which seems to happen
          when we are running under doctest) we look for functions in
          `frame_info.frame.f_globals`; this appears to pick up all functions
          defined in the current doctest invocation.
        
        Without these hacks we'd have to specify doctest with::

            >> def fn1():
            ..     ...
            >> def fn2():
            ..     ...
            >> cli( 'foo bar -s', [fn1, fn2], do_exit=False)

        With these hacks we can specify things more cleanly::

            >> def fn1():
            ..     ...
            >> def fn2():
            ..     ...
            >> cli( 'foo bar -s')
    '''
    if argv is None:
        argv = sys.argv
        if a is None:
            a = 1
    if a is None:
        a = 0
    if isinstance( argv, str):
        argv = shlex.split( argv)
    if fns is None:
        fns = 1
    doc_top_level = None
    if isinstance( fns, int):
        fns, doc_top_level = _get_fns( fns+1)
    if not isinstance( fns, ( tuple, list)):
        fns = fns,

    # Crude hack to force do_exit=False if we are being run by doctest.
    #
    if _in_doctest():
        do_exit = 'doctest'

    def get_fns():
        for name_fn in fns:
            if isinstance( name_fn, tuple):
                name, fn = name_fn
            else:
                fn = name_fn
                assert callable( fn)
                name = fn.__name__
            yield name, fn

    out = sys.stdout
    show_help = None
    fn = None
    name = None
    errors = []
    leaf_fn_failed = False
    while 1:
        if a == len( argv):
            break
        if argv[a] in ( '-h', '--help', '--help-full'):
            with _Pager() as pager:
                _show_help(
                        pager,
                        help_type=argv[a],
                        doc_top_level=doc_top_level,
                        fns=get_fns(),
                        fn_names=argv[a+1:],
                        fileline=fileline,
                        )
            break

        if help_post and a+1 < len( argv) and argv[a+1] in ( '-h', '--help'):
            with _Pager() as pager:
                _show_help(
                        pager,
                        help_type=argv[a+1],
                        doc_top_level=doc_top_level,
                        fns=get_fns(),
                        fn_names=argv[a:a+1],
                        fileline=fileline,
                        )
            break

        for name, fn in get_fns():
            if argv[ a] != name:
                continue
            a0 = a
            a, value, errors = _eval( argv, a+1, fn, top=True)
            if len( errors) == 1 and isinstance( errors[-1], _ErrorFnCallFailed):
                assert 0
            elif errors:
                e = errors[-1]
                assert isinstance( e, _Error)
                out2 = io.StringIO()
                _show_help_line( out2, name, fn, show_types=True, width=None)
                if isinstance( e, _ErrorFnCallFailed):
                    description = 'Function invocation failed:'
                    leaf_fn_failed = True
                else:
                    description = 'No match for'
                    errors.append( _Error( argv, a0, e.end, f'{description} {out2.getvalue()}'))
            else:
                if show_return and value is not None:
                    print( value)
            break
        else:
            fn = None
            errors.append(
                    _Error(
                        argv, a, a+1,
                        f'No function matches argv[{a}]: {argv[a]}'
                        )
                    )
            break
        if errors:
            break

    if errors:
        # Describe the error.
        #
        out.write( '\n')
        if leaf_fn_failed:
            # Don't show command line syntax - this was ok, it was the final
            # function call that failed.
            infos = infos.replace( 'h', '')
            infos = infos.replace( 'c', '')
        _fail_info( out, argv, infos, errors, fn, name, get_fns)

        if do_exit == 'doctest':
            return
        elif do_exit:
            sys.exit(1)
        else:
            return -1

    if do_exit == 'doctest':
        pass
    elif do_exit:
        sys.exit( 0)

g_current_args = None
'''
When a function is called, this is set to `(argv, a0, a)`. The function can set
to None to consume all args.
'''


def fncall_text( caller=0, locals_override=None):
    '''
    Returns string command suitable for passing to aargs.cli() that replicates
    function call `caller` steps up stack.
    
    locals_override:
        Dict with arg=value items to override.
    '''
    frame_info = inspect.stack()[ caller+1]
    fn = frame_info_fn( frame_info)
    signature = inspect.signature( fn)
    
    ret_optional = ''
    ret_required = ''
    for name, parameter in signature.parameters.items():
        if locals_override and name in locals_override:
            value = locals_override[name]
        else:
            value = frame_info.frame.f_locals[name]
        #jlib.log(f'{name=} {parameter=} {parameter.default=} {value=}')
        if parameter.default == inspect.Parameter.empty:
            ret_required += f' {shlex.quote(value)}'
        else:
            if value != parameter.default:
                prefix = '-' if len(name)==1 else '--'
                if isinstance(value, bool):
                    ret_optional += f' {prefix}{name}'
                    if not value:
                        ret_optional += f'={value}'
                else:
                    ret_optional += f' {prefix}{name} {shlex.quote(str(value))}'
    
    ret = fn.__name__ + ret_optional + ret_required
    if jlib:
        jlib.log('returning {ret=}')
    return ret


def code_fn( code_obj):
    '''
    Returns function object for specified code object.
    
    https://stackoverflow.com/questions/52715425/get-function-signature-from-frameinfo-or-frame-in-python
    '''
    gc.collect()
    fns = gc.get_referrers( code_obj)
    assert len(fns) == 1, f'{len(fns)=}'
    return fns[0]

def frame_fn( frame):
    '''
    Returns function object for specified frame.
    '''
    return code_fn( frame.f_code)

def frame_info_fn( frame_info):
    '''
    Returns function object for specified `inspect.FrameInfo`.
    '''
    return frame_fn( frame_info.frame)


class _Error:
    def __init__( self, argv, begin, end, description):
        self.argv = argv
        self.begin = begin
        self.end = end
        self.description = description

class _ErrorFnCallFailed( _Error):
    pass

def _arg_annotation_name( arg):
    try:
        return arg.annotation.__name__
    except Exception:
        return str(type(arg.annotation))

def _arginfo( arg, show_types):
    optional = arg.default is not inspect.Parameter.empty
    isbool = arg.annotation is bool
    dash = '-' if len( arg.name) == 1 else '--'
    typestring = ''
    if show_types and arg.annotation is not inspect.Parameter.empty:
        typestring = f':{_arg_annotation_name(arg)}'
    return optional, isbool, dash, typestring


class _Pager:
    '''
    Looks like a stream, but sends text to a pager.
    
    Example use as a context manager::
    
        with _Pager() as pager:
            pager.write()
            ...
    
    If not used as a context manager, `.close()` must be called in order to
    wait for child process to terminate.
    '''
    def __init__( self, pager=None, out=None):
        '''
        pager:
            Should be a list of args suitable for passing to `os.execvp()`
            or a string command such as `more` that is turned into a list of
            args using `shlex.split()`. If `None`, we use `$PAGER` if set, or
            default to 'more'.
        '''
        #out = sys.stdout
        if not out and _in_doctest():
            out = sys.stdout
        self.out = out
        if not out:
            if pager is None:
                pager = os.environ.get( 'PAGER')
                if pager is None:
                    pager = 'more'
            r, w = os.pipe()
            p = os.fork()
            if p:
                # Parent.
                os.close( r)
                self.w = w
                self.p = p
            else:
                # Child.
                os.close( w)
                os.dup2( r, 0)
                os.close( r)
                argv = pager
                if isinstance( argv, str):
                    argv = shlex.split( argv)
                os.execvp( argv[0], argv)
    def __enter__( self):
        return self
    def __exit__( self, exc_type, exc_value, traceback):
        self.close()
    def write( self, text):
        if self.out:
            self.out.write( text)
            return
        assert self.p is not None
        assert self.w is not None
        os.write( self.w, text.encode('utf8'))
    def flush( self):
        pass
    def close( self):
        if self.out:
            return
        assert self.p is not None
        assert self.w is not None
        os.close( self.w)
        self.w = None
        os.waitpid( self.p, 0)
        self.p = None


def _docstring_get_split( x):
    '''
    Returns ( summary, body) for <x>'s docstring.
    '''
    if not x:
        return '', ''
    text = x.__doc__
    return _docstring_split( text)


def _docstring_split( text):
    '''
    Splits <text> and returns ( summary, body).
    '''
    if not text:
        return '', ''
    text = textwrap.dedent( text)
    lines = text.split( '\n')
    lines = _linestrip( lines)

    # Summary ends with blank line or line ending with '.'.
    for i, line in enumerate( lines):
        if line.endswith( '.'):
            summary = lines[ 0 : i+1]
            body = lines[ i+1:]
            break
        if not line.strip():
            summary = lines[ 0 : i]
            body = lines[ i+1 :]
            break
    else:
        summary = lines
        body = []
    summary = _linestrip( summary)
    body = _linestrip( body)

    def join( lines):
        ret = ''
        for line in lines:
            ret += line + '\n'
        return ret
    summary = join( summary)
    body = join( body)
    return summary, body


def _eval( argv, a, fn_or_class, top=False):
    '''
    Main aargs functionality for matching `argv` items with a function or class
    constructor's args and calling the function or class constructor.

    argv:
        Args to look at.
    a:
        Index of first arg to look at.
    fn_or_class:
        A function or class.
    top:
        If true, we return an error if first unused arg starts with `-`.

    Returns:
        (a, value, errors)
            a:
                Index of next unused item in argv.
            value:
                The value returned from calling/constructing fn_or_class.
            errors:
                List of error messages or exceptions that occurred (if any),
                from low-level to high-level. If this list is not empty,
                `value` should be ignored.

                If we failed because a function, or class constructor,
                raised an exception, this exception will be the first
                item in `errors`; the remaining items will be strings or
                `aargs._aargsArgvRegion` instances which describe the context
                in which the original exception happened, such as what arg we
                were attempting evaluate.
    '''
    a0 = a
    errors = []

    # We return without recursion when there is no type annotation or the type
    # annotation is a basic type.
    #
    #print(f'{fn_or_class=}')
    if fn_or_class in ( typing.Any, inspect.Parameter.empty):
        return a + 1, argv[ a], errors
    elif str(fn_or_class).startswith('typing.Union['):
        aa_max = a
        for fn_or_class2 in fn_or_class.__args__:
            aa2, value2, error2 = _eval( argv, a, fn_or_class2, top=top)
            if not error2:
                return aa2, value2, error2
            errors += error2
            aa_max = max( aa_max, aa2)
        return a + aa_max, None, errors
            
    elif fn_or_class == bool:
        text = argv[ a]
        if text in ( '0', 'false', 'False'):
            value = False
        elif text in ( '1', 'true', 'True'):
            value = True
        else:
            value = None
            errors.append(
                    _Error(
                        argv, a, a+1,
                        f'Unrecognised bool value {text!r} should be one of: 0, false, False, 1, true, True'
                        )
                    )
        return a + 1, value, errors
    elif fn_or_class in ( str, int, float):
        try:
            return a + 1, fn_or_class( argv[ a]), errors
        except Exception as e:
            errors.append( _Error( argv, a, a+1, e))
            return a + 1, None, errors                

    # Use __init__() constructor if a class.
    if isinstance( fn_or_class, type):
        class_ = fn_or_class
        fn = getattr( class_, '__init__')
    else:
        class_ = None
        fn = fn_or_class

    def handle_arg( argv, a, fn_arg: inspect.Parameter, args, kwargs):
        '''
        Attempts to match function argument `fn_arg` against zero or more items
        in argv[a:]; adds evaluated value to `args` or `kwargs`, and returns
        `(a, errors)` where `a` is index of next unused item in `argv` and
        `errors` is list of errors.
        '''
        assert isinstance( fn_arg, inspect.Parameter)
        if a == len( argv):
            errors = []
            errors.append( _Error( argv, a, a, 'Run out of args'))
            return a, errors

        # Recurse to handle each arg type; this will consume zero or more items
        # in argv[a:] in order to create an instance of type fn_arg.annotation.
        a, value, errors = _eval( argv, a, fn_arg.annotation)
        if errors:
            return a, errors

        # Assert that <value> has the expected type. If fn_arg.annotation is
        # a class we assert that <value> is an instance of this class. If
        # fn_arg.annotation is a function we accept whatever it returns.
        assert (
                fn_arg.annotation is inspect.Parameter.empty
                or str(fn_arg.annotation).startswith('typing.Union[')
                or inspect.isfunction(fn_arg.annotation)
                or isinstance( value, fn_arg.annotation)
                ), f'fn_arg.annotation={fn_arg.annotation} value={value}'
        if fn_arg.default is inspect.Parameter.empty:
            # <fn_arg> does not have a default value so it is required, so we
            # put it into <args>.
            args.append( value)
        else:
            # <fn_arg> has a default value so it is optional, so we put it into
            # <kwargs>.
            assert fn_arg.name not in kwargs    # checked by our caller.
            kwargs[ fn_arg.name] = value
        return a, []

    failed_match_a = [0]
    failed_match_attempts = []
    def add_failed_match_attempt( a, arg):
        if a < failed_match_a[0]:
            return
        if a > failed_match_a[0]:
            failed_match_a[0] = a
            del failed_match_attempts[:]
        failed_match_attempts.append( arg)

    # First match all optional args.
    fn_args_optional, fn_args_required = _fnargs_optional_required( fn, skip_first=class_)
    args = list()
    kwargs = dict()
    while 1:
        # Try to match argv[ a] with a function/class name in
        # <fn_args_optional>.
        #
        for fn_arg in fn_args_optional:
            if a >= len( argv):
                continue
            prefix = '--' if len( fn_arg.name) > 1 else '-'
            prefix2 = f'{prefix}{fn_arg.name}'
            prefix3 = f'{prefix2}='
            if argv[ a] == prefix2:
                eq_value = None
            elif argv[ a].startswith( prefix3):
                eq_value = argv[ a][ len(prefix2) + 1:]
            else:
                if not fn_arg.name in kwargs:
                    add_failed_match_attempt( a, prefix2)
                continue
            a1 = a
            if fn_arg.name in kwargs:
                # Duplicate optional args.
                fn_arg_text =  _show_arg( fn_arg, show_types=False, show_defaults=False, show_extra=False)
                errors.append( _Error( argv, a1, a+1, f'Duplicate optional arg specified: {fn_arg_text}'))
                break
            if eq_value is None:
                # --foo 123 qwerty ...
                if fn_arg.annotation is bool:
                    # Special case bool - the presence of the optional arg
                    # itself is evaluated as True, e.g. '--flag'.
                    kwargs[ fn_arg.name] = True
                    a += 1
                else:
                    a1 = a
                    a, errors = handle_arg( argv, a + 1, fn_arg, args, kwargs)
            else:
                # Special case of named optional value such as '--foo=123'. We
                # use a temporary fake argv.
                argv2 = argv[:a] + [ eq_value]
                a, errors = handle_arg( argv2, a, fn_arg, args, kwargs)
            if errors:
                fn_arg_text =  _show_arg( fn_arg, show_types=True, show_defaults=False, show_extra=True)
                errors.append( _Error( argv, a1, a, f'No match for {fn_arg_text}'))
                break
            break
        else:
            # No item in <fn_args_optional> matched argv[a:].
            break
        if errors:
            break

    # Now match required args.
    if not errors:
        for fn_arg in fn_args_required:
            prefix = f'{fn_arg.name}='
            a1 = a
            if ( a < len(argv) and argv[a].startswith( prefix)):
                # Handle special case of a named value such as 'foo=123'.
                value = argv[a][ len(prefix):]
                argv2 = argv[:a] + [value]
                a, errors = handle_arg( argv2, a, fn_arg, args, kwargs)
            else:
                a, errors = handle_arg( argv, a, fn_arg, args, kwargs)
            if errors:
                fn_arg_text =  _show_arg( fn_arg, show_types=True, show_defaults=False, show_extra=True)
                errors.append( _Error( argv, a1, a, f'No match for {fn_arg_text}'))
                break

    if not errors:
        if top and a < len( argv) and not re.match('^[a-zA-Z_]', argv[ a]):
            # Next unused arg must be a fn name.
            errors.append(
                    _Error(
                        argv,
                        a,
                        a+1,
                        f'Unrecognised trailing arg: {argv[a]!r}',
                        )
                    )

    if errors:
        if class_:
            description = f'class {fn_or_class.__name__} constructor'
        else:
            try:
                description = f'function {fn_or_class.__name__}()'
            except Exception as e:
                description = f'{fn_or_class}'

        # Set <e> to most recent (i.e. highest-level) error.
        e = errors[ -1]
        assert isinstance( e, _Error)
        return a, None, errors

    # Finally return the evaluated fn/class_ value.
    global g_current_args
    g_current_args = argv, a0, a
    try:
        ret = fn_or_class( *args, **kwargs)
        if g_current_args:
            _, _, a1 = g_current_args
        else:
            a1 = len(argv)
        if a1 != a:
            #jlib.log( '### changing {a=} to {a1=}')
            a = a1
        return a, ret, errors
    except Exception as e:
        # Indicate that the failure involved all args we've used.
        args_description = ''
        sep = ''
        for arg in args:
            args_description += f'{sep}{arg!r}'
            sep = ', '
        for n, v in kwargs.items():
            args_description += f'{sep}{n}={v!r}'
            sep = ', '
        errors.append( e)
        errors.append( _ErrorFnCallFailed( argv, a0, a, f'Function call failed: {fn_or_class.__name__}({args_description})'))
        return a, None, errors


def _fail_info( out, argv, infos, errors, fn, name, get_fns):
    '''
    Show information about a failure.
    Args:
        out:
            Where to write to.
        argv:
            
        infos:
            String containing flags:
                t   Show text description of failure.
                c   Show caret description of failure.
                h   Show help.
        errors:
            List of errors, each an _Error or str or Exception.
        fn:
            The function that failed.
        name:
            Name of function that failed.
        get_fns:
            .
    '''
    because = True
    because = 0
    
    def write_exception( e):
        if jlib:
            # Use jlib's condensed exception formatting.
            jlib.exception_info(e,
                    file=out,
                    outer=False,
                    limit=None,
                    chain='because-compact' if because else True,
                    )
        else:
            lines = traceback.format_exception(
                    type(e),
                    e,
                    e.__traceback__,
                    limit=None,
                    chain=True,
                    )
            out.write( '\n'.join( lines).strip())
            out.write( '\n')

    errors2 = errors[:]
    if because:
        errors2 = list(reversed( errors))
    for info in infos:
        if info == 't':
            # Show text description of failure.
            for i, e in enumerate( errors2):
                if i:
                    if because:
                        out.write( 'Because: ')
                    else:
                        out.write( 'This caused: ')
                if isinstance( e, _ErrorFnCallFailed):
                    # Don't bother to write out argv[] information.
                    out.write( f'{e.description}\n')
                elif isinstance( e, _Error):
                    if isinstance( e.description, Exception):
                        #assert i == len( errors) - 1, f'{i=} {len( errors)=}'
                        #assert i == 0
                        out.write( 'Exception when handling argv')
                    else:
                        out.write( f'{e.description}, in argv')
                    out.write( f' {e.begin}')
                    if e.end > e.begin + 1:
                        out.write( f'..{e.end-1}')
                    
                    if hasattr( shlex, 'join'):
                        t = shlex.join(e.argv[e.begin:e.end])
                    else:
                        t = ' '.join( [shlex.quote(a) for a in e.argv[e.begin:e.end]])
                    out.write( f': {t}')
                    out.write( '\n')
                    if isinstance( e.description, Exception):
                        write_exception( e.description)
                elif isinstance( e, str):
                    out.write( e + '\n')
                elif isinstance( e, Exception):
                    if because:
                        assert i == len( errors2) - 1
                    else:
                        assert i == 0
                    write_exception( e)
                else:
                    assert 0, f'{e=}'
            out.write( '\n')
        elif info == 'c':
            # Show caret description of failure.
            e_last = errors[-1]
            prefix = f'argv {e_last.begin}..{e_last.end-1}:'
            prefix_len = len( prefix) + 1
            out.write( prefix)
            for i in range( e_last.begin, min(e_last.end, len(argv))):  # Need min() to cope with unions.
                out.write( ' ' + shlex.quote( argv[ i]))
            out.write( '\n')
            for e in errors2:
                if not isinstance( e, _Error):
                    continue
                active_c = None
                out.write( ' ' * prefix_len)
                for i in range( e_last.begin, min(e_last.end, len(argv))):
                    l = len( shlex.quote( argv[i]))
                    if i == e.begin:
                        active_c = '^'
                    elif i == len( argv)-1 and i+1 == e.begin:
                        active_c = ' '    # For when we have run out of required args.
                    if active_c:
                        out.write( active_c * l)
                        if i+1 == e.end:
                            active_c = False
                            out.write( f' {e.description}')
                        else:
                            out.write( '^')
                    else:
                        out.write( ' ' * (l+1))
                out.write( '\n')
            out.write( '\n')

        elif info == 'h':
            # Show help.
            if fn:
                # Show detailed usage of fn that failed.
                out.write( f'Usage:\n')
                with _Pager() as pager:
                    _show_help_detailed( pager, name, fn, '    ')
            else:
                # argv[a] did not match any fn; show brief info about all fns.
                out.write( f'Available functions are:\n')
                for name, fn in get_fns():
                    out.write( f'    {name}\n')
            out.write( '\n')
        else:
            assert 0, f'Unrecognised info flag: {repr(info)}'

def _fnargs_optional_required( fn, skip_first):
    '''
    Returns `(args_optional, args_required)`, each a list of
    `inspect.Parameter`'s.
    '''
    signature = inspect.signature( fn)
    args_optional = []
    args_required = []
    for i, (_, p) in enumerate( signature.parameters.items()):
        if i==0 and skip_first:
            continue
        if p.default is inspect.Parameter.empty:
            args_required.append( p)
        else:
            if p.annotation is inspect.Parameter.empty and p.default is not None and type( p.default) in (bool, int, float):
                # No type specified but default is True or False, so we force
                # annotation type to be bool. This can avoid confusion.
                #
                p = p.replace( annotation=type( p.default))
            args_optional.append( p)
    return args_optional, args_required


def _get_fns( caller):
    '''
    Returns `(doc, name_fns)`. `doc` is top-level doc-string. `name_fns` is a
    list of `(name, fns)` items for functions in the module of the function
    associated with frame `caller` steps up the stack.
    '''
    fns = []
    frame_info = inspect.stack()[ caller]
    module = inspect.getmodule(frame_info.frame)
    if module:
        doc = module.__doc__
        for name, fn in inspect.getmembers( module, inspect.isfunction):
            fns.append( ( name, fn))
    else:
        # `module` seems to be None if we are called from doctest. We
        # use a hack to instead retrieve functions from dict
        # frame_info.frame.f_globals; this allows us to run aargs.cli() in
        # doctests without having to specify the list of available functions
        # explicitly.
        #
        doc = ''
        for n, v in frame_info.frame.f_globals.items():
            if inspect.isfunction( v):
                fns.append( ( n, v))
    return fns, doc


def _linestrip( lines):
    '''
    Strips leading and trailing blank lines.
    '''
    for first, line in enumerate( lines):
        if line.strip():
            break
    else:
        return []
    for last, line in enumerate( reversed( lines)):
        if line.strip():
            break
    last = len( lines) - last
    ret = lines[ first : last]
    return ret


def _show_arg( arg, *, show_types=False, show_defaults=False, show_extra=False):
    '''
    Returns string containin syntax of specified arg.
    
    arg:
        The arg to describe.
    show_types:
        If true, we append ':<type>' where applicable.
    show_defaults:
        If true, we append '=<default>' where applicable.
    show_extra:
        If true, and `arg.annotation` is a class, we append the syntax of the
        class's `__init__()` constructor. For example if `arg` has type `Host`
        with constructor `__init__(self, name, port:int)`, we might return:

            `<host:Host>: <name> <port:int>`
    '''
    optional, isbool, dash, typestring = _arginfo( arg, show_types)
    ret = ''
    if optional:
        if isbool:
            if show_defaults and arg.default:
                # We only show bool's default if it is true (which is unusual).
                ret += f'[{dash}{arg.name}={arg.default}]'
            else:
                ret += f'[{dash}{arg.name}]'
        else:
            if show_defaults:
                ret += f'[{dash}{arg.name} <{arg.name}{typestring}={arg.default!r}>]'
            else:
                ret += f'[{dash}{arg.name} <{arg.name}{typestring}>]'
    else:
        ret += f'<{arg.name}{typestring}>'

    if ( show_extra
            and arg.annotation is not inspect.Parameter.empty
            and arg.annotation not in ( int, float, str)
            ):
        # Also show syntax of arg.annotation class's `__init__()` constructor:
        fn = getattr( arg.annotation, '__init__')
        out2 = io.StringIO()
        _show_help_line( out2, None, fn, skip_first=True, show_types=True)
        ret += f': {out2.getvalue()}'

    return ret


def _show_help( out, help_type, doc_top_level, fns, fn_names, fileline):
    '''
    out:
        Where to write.
    help_type:
        If '--help-full' we ignore `fn_names` and show detailed help on all
        functions. Otherwise we show detailed help if `fn_names` is not empty.
    doc_top_level:
        If a non-empty string we show  this first.
    fns:
        Generator of `(name, fn)` pairs. We ignore items where `name` starts
        with an underscore.
    fn_names:
        Function names to document. If None or empty list we show all functions
        in `fns`.
    fn_names:
        If None we show information about all functions. Otherwise a list of
        function names to document.
    '''
    detailed = None
    if help_type == '--help-full':
        fn_names = None
        detailed = True
    if fn_names:
        # Show help on specific function names in argv[a:].
        for fn_name in fn_names:
            # Search for function called <name>.
            name2 = None
            for name2, fn in fns:
                if name2 == fn_name:
                    break
            if name2 != fn_name:
                out.write( f'No available function called {fn_name!r}.\n')
            elif detailed is None or detailed:
                out.write( f'Detailed help for function {fn_name!r}:\n\n')
                _show_help_detailed( out, fn_name, fn, show_types=True, prefix='    ', fileline=fileline)
            else:
                out.write( f'Help for function {fn_name!r}:\n\n')
                _show_help_line( out, fn_name, fn, show_types=True, fileline=fileline)
            out.write( '\n')
    else:
        # Show help on all functions.
        if doc_top_level:
            out.write( doc_top_level)
            out.write( '\n')
        if detailed is None or not detailed:
            out.write( 'Help for all available functions:\n\n')
            for name, fn in fns:
                if name.startswith( '_'):
                    continue
                _show_help_line(
                        out,
                        name,
                        fn,
                        show_types=True,
                        show_defaults=True,
                        fileline=fileline,
                        initial_indent=' '*4,
                        subsequent_indent=' '*12,
                        )
                out.write( '\n')
                # Show one-line docstring summary if available.
                summary, body = _docstring_get_split( fn)
                if summary:
                    with _Wrapper( out, indent=8) as out2:
                        out2.write( summary)
                        #out.write( textwrap.indent( summary.replace( '\n', ' '), ' '*8))
                        #out.write( '\n')
        else:
            out.write( 'Detailed help for all available functions:\n\n')
            for name, fn in fns:
                if name.startswith( '_'):
                    continue
                _show_help_detailed( out, name, fn, '    ', fileline=fileline)
                out.write( '\n')
        out.write( '\n')

    out.write( 'Use "-h" or "--help" to show help for all available functions.\n')
    out.write( 'Use "--help-full" to show detailed help for all available functions.\n')
    out.write( 'Use "-h <fn1> <fn2> ..." or "--help <fn1> <fn2> ..." to show detailed help on specific functions.\n')
    out.write( '\n')


def _show_help_detailed(
        out,
        name,
        fn,
        prefix='',
        skip_first=False,
        show_types=False,
        #show_defaults=False,
        fileline=False,
        width='terminal',
        ):
    '''
    Writes information about aargs command-line usage of a function.

    Args:
        out
            Stream to which we write output.
        name
            Name of function.
        fn
            The function whose args we describe.
        prefix
            Prefix for all lines of output.
        skip_first
            If true we skip `fn`'s first arg; typically for class constructors'
            `self` arg.
        show_types
            If true we append ":<type>" to args in brief descriptions.
        fileline
            If true we prepend single-line help with `file:line`.

    >>> class Remote:
    ...     """
    ...     A connection to a remote machine and directory.
    ...     """
    ...     def __init__( self, text, bar: float):
    ...         """
    ...         <ssh-params>[:<port>][:<remote-dir>][,<sync-directories>] bar
    ...
    ...         If no ':' we default to <remote-dir>='artifex-remote'.
    ...         bar is ignored.
    ...         """
    >>> def fn( remote: Remote, foo: int=0, command: str=''):
    ...     """
    ...     fn summary.
    ...
    ...     fn details and
    ...     more details.
    ...     """
    ...     pass
    >>> out = io.StringIO()

    >>> _show_help_detailed( sys.stdout, 'fn', fn, show_types=True)
    <BLANKLINE>
    fn [--foo <foo:int=0>] [--command <command:str=''>] <remote:Remote>
        foo: An integer, default=0.
        command: A string, default=''.
        remote: A Remote.
            A connection to a remote machine and directory.
            <text> <bar:float>
                text: Anything.
                bar: A float.
    <BLANKLINE>
                <ssh-params>[:<port>][:<remote-dir>][,<sync-directories>] bar
    <BLANKLINE>
    <BLANKLINE>
                If no ':' we default to <remote-dir>='artifex-remote'.
                bar is ignored.
    <BLANKLINE>
    <BLANKLINE>
        fn summary.
    <BLANKLINE>
    <BLANKLINE>
        fn details and
        more details.
    <BLANKLINE>

    >>> _show_help_detailed( sys.stdout, 'fn', fn, show_types=False)
    <BLANKLINE>
    fn [--foo <foo=0>] [--command <command=''>] <remote>
        foo: An integer, default=0.
        command: A string, default=''.
        remote: A Remote.
            A connection to a remote machine and directory.
            <text> <bar>
                text: Anything.
                bar: A float.
    <BLANKLINE>
                <ssh-params>[:<port>][:<remote-dir>][,<sync-directories>] bar
    <BLANKLINE>
    <BLANKLINE>
                If no ':' we default to <remote-dir>='artifex-remote'.
                bar is ignored.
    <BLANKLINE>
    <BLANKLINE>
        fn summary.
    <BLANKLINE>
    <BLANKLINE>
        fn details and
        more details.
    <BLANKLINE>

    >>> class A:
    ...     """
    ...     This is a A class
    ...     """
    ...     def __init__( self, x: float):
    ...         """Constructor for A"""
    ...         pass
    >>> class B:
    ...     """
    ...     This is a B class
    ...     """
    ...     def __init__( self, a: A):
    ...         """Constructor for B"""
    ...         pass
    >>> class C:
    ...     """
    ...     This is a C class
    ...     """
    ...     def __init__( self, b: B):
    ...         """Constructor for C"""
    ...         pass
    >>> def fn( c: C):
    ...     pass
    >>> _show_help_detailed( sys.stdout, 'fn', fn, show_types=True)
    <BLANKLINE>
    fn <c:C>
        c: A C.
            This is a C class
            <b:B>
                b: A B.
                    This is a B class
                    <a:A>
                        a: A A.
                            This is a A class
                            <x:float>
                                x: A float.
    <BLANKLINE>
                                Constructor for A
    <BLANKLINE>
    <BLANKLINE>
                        Constructor for B
    <BLANKLINE>
    <BLANKLINE>
                Constructor for C
    <BLANKLINE>
    '''
    if width == 'terminal':
        width = None if _in_doctest() else shutil.get_terminal_size().columns
    args_optional, args_required = _fnargs_optional_required( fn, skip_first)

    # Write brief info on a single line.
    #
    #out.write( f'{prefix}')
    with _Wrapper( out, indent=len(prefix), indent_extra=8) as out2:
        _show_help_line(
                out2,
                name,
                fn,
                skip_first=skip_first,
                show_types=show_types,
                show_defaults=True,
                fileline=fileline,
                #initial_indent=prefix,
                #subsequent_indent=prefix + ' '*8,
                width=width,
                )
        #out.write( '\n')

    # Write detailed info.
    #
    prefix1 = prefix + ' '*4
    prefix2 = prefix + ' '*8
    prefix3 = prefix + ' '*12
    
    # Only show per-arg info if at least one arg has non-trivial type. This
    # avoids unhelpful boiler-place output.
    #
    any_args_interesting = False
    for arg in args_optional + args_required:
        optional, isbool, dash, typestring = _arginfo( arg, show_types)
        if isbool or (arg.annotation is inspect.Parameter.empty):
            pass
        else:
            any_args_interesting = True
            break
    if any_args_interesting:
        for arg in args_optional + args_required:
            optional, isbool, dash, typestring = _arginfo( arg, show_types)
            #if isbool or (arg.annotation is inspect.Parameter.empty):
            if isbool:
                if optional:
                    if arg.default:
                        pre = '-' if len(arg.name) == 1 else '--'
                        out.write( f'{prefix}    {arg.name}: A bool with default value true; setting to false should be done with one of: {pre}{arg.name}=0 {pre}{arg.name}=false {pre}{arg.name}=False\n')
                # No extra information is required here.
                continue
            
            out.write( f'{prefix}    ')
            out.write( f'{arg.name}')

            detailed = False
            out.write( ': ')
            if arg.annotation in ( inspect.Parameter.empty, typing.Any):
                out.write( f'Anything')
            elif arg.annotation is str:
                out.write( f'A string')
            elif arg.annotation is int:
                out.write( f'An integer')
            elif arg.annotation is float:
                out.write( f'A float')
            elif arg.annotation is bool:
                out.write( f'A bool (can have suffix from: =0 =false =False =1 =true =True)')
            else:
                out.write( f'A {_arg_annotation_name(arg)}')
                detailed = True

            if optional:
                out.write( f', default={arg.default!r}')
            out.write( '.\n')
            if detailed:
                # Show docstring for class.
                summary, body = _docstring_get_split( arg.annotation)
                out.write( textwrap.indent( summary, prefix2))
                out.write( textwrap.indent( body, prefix2))
                fn2 = getattr( arg.annotation, '__init__', None)
                if fn2:
                    _show_help_detailed(
                            out,
                            name=None,
                            fn=fn2,
                            prefix=prefix2,
                            skip_first=True,
                            show_types=show_types,
                            fileline=fileline,
                            )

    # Write docstring for function. For classes this is the `__init__()`
    # function.
    #
    summary, body = _docstring_get_split( fn)
    if any_args_interesting and (summary or body):
        out.write( '\n')
    with _Wrapper( out, indent=len(prefix1)) as out2:
        out2.write( summary)
        #out.write( textwrap.indent( summary, prefix1))
    if body:
        out.write( '\n')
        with _Wrapper( out, indent=len(prefix1)) as out2:
            out2.write( body)
            #out.write( textwrap.indent( body, prefix1))


def _in_doctest():
    '''
    Returns true if we think we are being run by doctest.
    '''
    for frame_info in inspect.stack()[2:]:
        if 'doctest.py' in frame_info.filename and frame_info.function == 'testmod':
            return True


class _Wrapper:
    '''
    Looks like a stream, but wraps/indents text paragraphs.
    '''
    def __init__( self, out, split='auto', width='terminal', indent=0, indent_extra=0):
        '''
        Args:
            out:
                Where we write the wrapped text.
            split:
                Paragraphs separator string.
            width:
                If false we don't wrap.
                If 'terminal' we use the width of the current terminal.
                Otherwise integer width of output.
            indent_extra:
                Extra indent after first line when wrapping long lines.
            split:
                Number of newline characters on which to split, or 'auto' to
                split on double-newline and indentation changes.
        '''
        if width == 'terminal':
            width = None if _in_doctest() else shutil.get_terminal_size().columns
        self.out = out
        self.split = split
        self.width = width
        self.indent = indent
        self.indent_extra = indent_extra
        self.buffer = ''
    def __enter__( self):
        return self
    def __exit__( self, exc_type, exc_value, traceback):
        self.close()
    def write( self, text):
        self._write( text, eof=False)
    def flush( self):
        pass
    def close( self):
        '''
        Must be called if last character was not '\n'.
        '''
        self._write( '', eof=True)
    def set_indent_extra( self, indent_extra):
        self.indent_extra = indent_extra
    
    def _write( self, text, eof):
        if self.split == 'auto':
            self._write_auto( text, eof=eof)
        else:
            self._write_basic( text, eof=eof)
    def _write_auto( self, text, eof):
        self.buffer += text
        paras = self.buffer.split('\n\n')
        if not eof:
            self.buffer = paras[-1]
            paras = paras[:-1]
        def get_paras():
            '''
            Yields (indent, backslash, text) for each paragraph in <text>,
            splitting on double newlines. We also split when indentation
            changes unless lines end with backslash. <backslash> is true iff
            text contains backslash at end of line.
            '''
            for para in paras:
                indent_prev = None
                prev_backslash = False
                prev_prev_backslash = False
                i0 = 0
                lines = para.split('\n')
                for i, line in enumerate(lines):
                    indent = len( re.search('^( *)', line).group(1))
                    if i and not prev_backslash and indent != indent_prev:
                        yield indent_prev, prev_prev_backslash, '\n'.join(lines[i0:i])
                        i0 = i
                    backslash = line.endswith('\\')
                    if i == 0 or not prev_backslash:
                        indent_prev = indent
                    prev_prev_backslash = prev_backslash
                    prev_backslash = backslash
                yield indent_prev, prev_prev_backslash, '\n'.join(lines[i0:])
        paras2 = []
        indent_prev = 0
        for (indent, backslash, para) in get_paras():
            indent += self.indent
            para = textwrap.dedent(para)
            # Don't fill paragraph if contains backslashes.
            if not backslash and self.width is not None:
                para = textwrap.fill(para, self.width - indent)
            para = textwrap.indent(para, ' ' * indent)
            if indent == indent_prev:
                # Put blank lines in between paragraphs with same indentation.
                paras2.append('')
            paras2.append(para)
            indent_prev = indent
        ret = '\n'.join( paras2)
        self.out.write( ret)
        if ret:
            self.out.write( '\n')
        return
    
    def _write_basic( self, text, eof):
        text = re.sub( '\n +\n', '\n\n', self.buffer)
        paras = self.buffer.split( '\n' * self.split)
        if not eof:
            self.buffer = paras[ -1]
            paras = paras[ : -1]
        for para in paras:
            indent = len( re.match( '^( *)', para).group(1))
            para = para[ indent:]
            indent += self.indent
            if self.split == 2:
                para = re.sub( '\n *', ' ', para)
            if self.width:
                para = textwrap.fill( para, self.width - indent)
            nl = para.find( '\n')
            if nl >= 0 and self.indent_extra:
                t1, t2 = para[ :nl] , para[ nl+1]
                t1 = textwrap.indent( t1, indent)
                if self.width:
                    t2 = textwrap.fill( t2, self.width - (indent + indent_extra))
                t2 = textwrap.indent( t2, indent + indent_extra)
                t = f'{t1}\n{t2}'
            else:
                t = textwrap.indent( para, ' ' * indent)
            self.out.write( t)
            self.out.write( '\n')


def _show_help_line(
        out,
        name,
        fn,
        *,
        skip_first=False,
        show_types=False,
        show_defaults=False,
        fileline=False,
        initial_indent='',
        subsequent_indent=' '*8,
        width='terminal',
        ):
    '''
    Outputs one-line syntax for a function. By default we actually wrap to the
    terminal width, splitting in between args rather than at any space.
    
    out
        Stream to write to.
    name
        Name of function.
    fn
        Function to describe.
    skip_first
        Ignore first arg, e.g. `fn` is a class `__init__()` function.
    show_types
        If true we show any annotation types.
    show_defaults
    fileline
        If true we show `file:line`.
    initial_indent
        Initial indentation string.
    subsequent_indent
        Indentation string for subsequent lines.
    width
        Maximum width of output. If 'terminal' we use terminal width. If zero
        or None, we do not wrap.
    '''
    if width == 'terminal':
        width = None if _in_doctest() else shutil.get_terminal_size().columns
    #jlib.log('*** {width=}')
    class State: pass
    state = State()
    state.pos = 0
    state.first_item = True
    
    def write( text):
        nl = text.rfind( '\n')
        out.write( text)
        if nl >= 0:
            state.pos = len( text) - nl
        else:
            state.pos += len( text)
    
    def write_item( text):
        if width and state.pos + 1 + len( text) >= width:
            write( '\n')
            write( subsequent_indent)
        else:
            if not state.first_item:
                write( ' ')
        write( text)
        state.first_item = False
    
    args_optional, args_required = _fnargs_optional_required( fn, skip_first)
    write( initial_indent)
    if fileline:
        filename = inspect.getfile( fn)
        if filename.startswith( '<doctest __main__.'):
            pass
        else:
            if filename.startswith( './'):
                filename = filename[2:]
            _, line = inspect.getsourcelines( fn)
            write( f'{filename}:{line}: ')
    if name:
        write( f'{name}')
        state.first_item = False
    for arg in args_optional + args_required:
        write_item( _show_arg( arg, show_types=show_types, show_defaults=show_defaults))            


if __name__ == '__main__':
    if 0:
        # Run doctest on a single fn.
        doctest.run_docstring_examples(
                _show_help_detailed,
                globs=None,
                )
    else:
        print( 'Running doctest.testmod().')
        doctest.testmod(
                optionflags=0
                        #| doctest.FAIL_FAST
                        | doctest.REPORT_UDIFF
                        | doctest.ELLIPSIS
                        ,
            )
