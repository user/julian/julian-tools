#include <stdexcept>
#include <string>
#include <iostream>


struct MyError : std::exception
{
    MyError(int code, const char* text, float f)
    :
    m_code(code),
    m_text(text)
    {
    }
    MyError(int code, float x, const char* text)
    :
    m_code(code),
    m_text(text)
    {
    }
    MyError(const MyError& rhs)
    :
    m_code(rhs.m_code),
    m_text(rhs.m_text)
    {
    }
    const char* what() const throw()
    {
        m_what = "";
        m_what += "MyError: code=";
        m_what += std::to_string(m_code);
        m_what += ": ";
        m_what += m_text;
        return m_what.c_str();
    }
    
    int m_code;
    std::string m_text;
    mutable std::string m_what;
};

int myfn();
