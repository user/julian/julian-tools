#!/usr/bin/env python3

import subprocess

def run(command):
    print(f'Running: {command}')
    try:
        return subprocess.run( command, shell=1, check=1, text=1)
    except Exception as e:
        print(f'Command failed: {command}')
        raise

run(f'swig -c++ -python -outdir . -o texcept.i.cpp -Wextra -w-201,-314,-302,-312,-321,-322,-362,-451,-503,-512,-509,-560 texcept.i')
run(f'c++ -g -std=c++14 -o _texcept.so -Wl,-rpath,\'$ORIGIN\',-z,origin -fPIC -shared `pkg-config --cflags --libs python3` texcept.i.cpp texcept.cpp')
run('python3 test.py')
