%module texcept

%{
#include <assert.h>
#include "texcept.h"

/* Returns equivalent of `repr(x)`. */
static std::string repr(PyObject* x)
{
    PyObject* repr = PyObject_Repr(x);
    PyObject* repr_str = PyUnicode_AsEncodedString(repr, "utf-8", "~E~");
    const char* repr_str_s = PyBytes_AS_STRING(repr_str);
    std::string ret = repr_str_s;
    Py_DECREF(repr_str);
    Py_DECREF(repr);
    return ret;
}

static std::string dict_repr(PyObject* dict)
{
    PyObject* l = PyMapping_Items(dict);
    return repr(l);
}

%}


/*
This is only documented for Ruby, but is mentioned for Python at
https://sourceforge.net/p/swig/mailman/message/4867286/.

It makes the Python wrapper for `MyError` inherit Python's `Exception` instead
of `object`, which in turn means it can be caught in Python with `except
Exception as e: ...` or similar.

Note that while it will have the underlying C++ class's `what()` method, this
is not used by the `__str__()` and `__repr__()` methods. Instead:

    `__str__()` appears to return a tuple of the constructor args
    that were originally used to create the exception object with
    `PyObject_CallObject(class_, args)`.

    `__repr__()` returns a SWIG-style string such as `<texcept.MyError; proxy
    of <Swig Object of type 'MyError *' at 0xb61ebfabc00> >`.
*/
%feature("exceptionclass")  MyError;

%include exception.i

%{
PyObject* class_MyError = NULL;

/**/
void internal_set_class_MyError(PyObject* classes)
{
    std::cerr << "classes=" << repr(classes) << "\n";
    assert(PyList_Check(classes));
    for (int i=0; i<PyList_Size(classes); ++i)
    {
        PyObject* class_ = PyList_GetItem(classes, i);
        std::cerr << "class_=" << class_ << "\n";
        class_MyError = class_;
    }
    //class_MyError = class_;
    std::cerr << "class_MyError=" << class_MyError << "\n";
}
void set_exception(const char* name, int code, const char* text)
{
    PyObject* args = Py_BuildValue("(i:s:f)", code, text, 3.4);
    PyObject* instance = PyObject_CallObject(class_MyError, args);
    PyErr_SetObject(class_MyError, instance);
    Py_XDECREF(args);
}

%}

%exception
{
    try
    {
        $action
    }
    catch ( MyError& e)
    {
        set_exception("MyError", e.m_code, e.m_text.c_str());
        SWIG_fail;
    }
    catch ( std::exception& e)
    {
        std::cerr
                #ifndef _WIN32
                << __PRETTY_FUNCTION__ << ": "
                #endif
                << "Converting C++ std::exception into {language} exception: " << e.what()
                << "\n";
        SWIG_exception( SWIG_RuntimeError, e.what());
    }
    catch (...)
    {
        std::cerr
                #ifndef _WIN32
                << __PRETTY_FUNCTION__ << ": "
                #endif
                << "Converting unknown C++ exception into {language} exception."
                << "\n";
        SWIG_exception( SWIG_RuntimeError, "Unknown exception");
    }
}

%include "texcept.h"

void internal_set_class_MyError(PyObject* classes);

/* This must be after the declaration of MyError in texcept.h and declaration
of internal_set_class_MyError, otherwise generated code is before the
declaration of the Python class or similar. */
%pythoncode
%{
    internal_set_class_MyError([MyError, MyError])
%}

