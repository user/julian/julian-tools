#! /usr/bin/env python3

import texcept

if 0:
    class MyError(Exception):
        def __init__(self, code, text):
            self.code = code
            self.text = text
        def __repr__(self):
            return f'test.py:MyError {self.code=} {self.text=}'

try:
    texcept.myfn()
except Exception as e:
    print(f'Exception: {type(e)=} {repr(e)=} {str(e)=}.')
    print(f'{e.what()=}.')
