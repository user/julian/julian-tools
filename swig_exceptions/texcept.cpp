#include "texcept.h"

int myfn()
{
    throw MyError(3, "MyFn failed", 7.9);
    return 45;
}
