#!/usr/bin/env python3

import os
import sys

import jlib

# [ ] test with banding
# [ ] specify a device to avoid opening a window.
# Robin_Watts: If you don't define a device, it'll use the display device, and open a window.
# Robin_Watts: If you do -sDEVICE=ppmraw -o out.ppm then it'll send the output to that file, formatted as ppm. 
# Robin_Watts: A page mode command to test would be: gpcl6 -sDEVICE=ppmraw -o out.ppm -dMaxBitmap=1500M -r300 in.pcl
# Robin_Watts: To test with banding, use something like: gpcl6 -sDEVICE=ppmraw -o out.ppm -dMaxBitmap=1000 -r300 in.pcl
# Robin_Watts: If that banding command runs too slowly you can probably do: gpcl6 -sDEVICE=ppmraw -o out.pbm -dMaxBitmap=1000 -r75 -dBandHeight=128 in.pcl
# Robin_Watts: PCL likes to run at multiples/divisors of 600dpi.


def gpcl6_command( infile, make=True, fetch=True, outfile='gpcl6.out', rr=False):
    '''
    Returns command that runs gpcl6 on specified file, optionally also making
    gpcl6 and fetching <infile> from ghostscript.com.
    '''
    prefix = 'tests_private/'
    infile_is_tests = False
    
    if infile.startswith(prefix) and not os.path.isfile( infile):
        jlib.log( 'local file does not exist: {infile=}')
        infile_to_find = infile[ len(prefix):]
        infile_is_tests = True
    
    exe = 'ghostpdl/mem-extract-bin/gpcl6'
    #exe = 'ghostpdl/extract-bin/gpcl6'
    #exe = 'ghostpdl/mem-bin/gpcl6'
    exe = 'ghostpdl/extract-bin/gpcl6'
    #exe = 'ghostpdl/debug-extract-bin/gpcl6'
    
    command_pre = ''
    if make or (fetch and infile_is_tests):
        command_pre += './julian-tools/jtest.py'
        if make:
            command_pre += f' -b {exe} m'
        if fetch and infile_is_tests:
            command_pre += f' --remote julian@ghostscript.com --remote-get-testfile {infile_to_find}'

    command = ''    
    command += (''
            + f' MEMENTO_ABORT_ON_LEAK=1'
            + f' MEMENTO_ABORT_ON_CORRUPTION=1'
            + f' MEMENTO_SHOW_DETAILED_BLOCKS=10'
            #+ f' MEMENTO_PARANOIA=1'
            #+ f' MEMENTO_PARANOIDAT=1'
            + f' LD_LIBRARY_PATH=libbacktrace/.libs'
            + (f' rr record' if rr else '')
            #+ f' valgrind'
            + f' {exe}'
            )
    if 0:
        command += (''
            + f' -dNOPAUSE -dBATCH'
            + f' -sDEVICE=ppmraw'
            + f' -dMaxBitmap=1000'
            + f' -r300'
            + f' -o {outfile} {infile}'
            )
    else:
        # Mimic clusterpush command line.
        command += ' -dMaxBitmap=10000 -sDEVICE=bitrgbtags -r600 -Z: -dNOPAUSE -dBATCH -K2000000'
    
    command += f' -o {outfile} {infile}'
    
    return command_pre, command

def run_1( infile):
    command_pre, command = gpcl6_command( infile, make=True, fetch=True, rr=True)
    jlib.log( '{command=}')
    jlib.system( command_pre, out='log', verbose=1)
    jlib.system( command, out='log', verbose=1)

def run_1_remote( infile):
    '''
    Returns command that remotely runs gpcl6 on specified file.
    '''
    jlib.system(
            f'./julian-tools/jtest.py -r jules-devuan,ghostpdl,julian-tools'
                + f' "'
                + f'./julian-tools/{os.path.basename(__file__)} --run_1 {infile}'
                + f'"'
            ,
            out='log',
            )

def run_all_git_pcl( make='m'):
    '''
    Returns command that remotely runs gpcl6 on all .pcl files within ghostpdl checkout.
    '''
    if make:
        command = f'./jtest.py -b ghostpdl/mem-extract-bin/gpcl6 {make}'
    else:
        command = 'true'
    root = 'ghostpdl/pcl/examples'
    for i, path in enumerate( jlib.get_gitfiles( root)):
        if not path.endswith( '.pcl'):
            continue
        infile = f'{root}/{path}'
        command_pre, command = gpcl6_command(infile, make=False)
        if i == 0:
            command = f'{command_pre} && {command}'
        command += f' \\\n    && echo "== Testing {infile}" 1>&1 && date && {command}'
    command = f'./julian-tools/jtest.py -r jules-devuan,ghostpdl "{command}"'
    jlib.log( '{command=}')
    return command


macro_pwhite_cs_files = (
        #'tests_private/customer_tests/bug693381.pcl',
        #'tests_private/customer_tests/bug691204.pcl',
        #'tests_private/customer_tests/5478.pcl',
        #'tests_private/customer_tests/folg_d.pcl',
        #'tests_private/customer_tests/bug691747.pcl',
        #'tests_private/customer_tests/Vpstest.pcl',
        #'tests_private/customer_tests/bug692490.pcl',
        )

def run_remote_run_all_tests_private(first=None):
    '''
    Remotely runs gpcl6 on all .pcl files in
    casper:/home/regression/cluster/tests_private.git.
    
    first:
        If true, we skip all files until we reach one that contains <first>.
    '''
    if first == '.':
        first = None
    prefix = '/home/regression/cluster/tests_private.git/'
    skip_macro_pwhite_cs_files = 0
    if 1:
        skip_macro_pwhite_cs_files = True
        suffix = '.pcl'
        #suffix = '.pxl'
        pcls = jlib.system(
                f'ssh julian@casper.ghostscript.com find /home/regression/cluster/tests_private.git -name "*{suffix}"',
                out='return',
                verbose=1,
                )
        pcls = pcls.split( '\n')
    else:
        pcls = [(prefix + i).replace( '/tests_private/', '/') for i in macro_pwhite_cs_files]
    jlib.log( '{len(pcls)=}')
    jlib.system( './jtest.py -b ghostpdl/mem-extract-bin/gpcl6 m', out='log', verbose=1)
    for i, pcl in enumerate( pcls):
        jlib.log( '{i}/{len(pcls)}: {pcl}')
        if not pcl:
            continue
        assert pcl.startswith( prefix), f'pcl={pcl}'
        pcl_tail = pcl[ len(prefix):]
        pcl_local = f'tests_private/{pcl_tail}'
        if first:
            if first in (pcl, pcl_local):
                first = None
            else:
                jlib.log( '{first!r=} so skipping {pcl_local}')
                continue
        if skip_macro_pwhite_cs_files and pcl_local in macro_pwhite_cs_files:
            # leak of pwhite_cs in pcl_execute_macro().
            jlib.log( 'Ignoring because is leak of pwhite_cs in pcl_execute_macro() that we are deferring: {pcl_local=}')
            continue
        jlib.log( 'Testing {pcl_local=}')
        if not os.path.isfile( pcl_local):
            # rsync's --mkpath seems to fail if directory already exists?
            jlib.ensure_parent_dir( pcl_local)
            jlib.system( f'rsync -ai julian@casper.ghostscript.com:{pcl} {pcl_local}', out='log', verbose=1)
        command_pre, command = gpcl6_command(pcl_local, make=0, fetch=0)
        e = jlib.system( command_pre, out='log', verbose=1, raise_errors=False)
        jlib.log( 'Starting test at: {jlib.date_time()}')
        e = jlib.system( command, out='log', verbose=1, raise_errors=False)
        if 1 and (e < 0 or e > 128):
            # The shell appears to translate WIFSIGNALED into exit code 128+sig.
            raise Exception( f'Command failed with signal, e={e}: {command}')
        elif e > 0:
            jlib.log( 'Ignoring non-zero exit code {e=} from: {command}')
            
def run_remote_run_all_tests_private_remote( first='.'):
    jlib.system(
            f'./julian-tools/jtest.py -r jules-devuan,ghostpdl,julian-tools'
                + f' "'
                + f'./julian-tools/{os.path.basename(__file__)} --run_remote_run_all_tests_private "{first}"'
                + f'"'
            ,
            out='log',
            )

def main():
    args = iter( sys.argv[1:])
    while 1:
        try:
            arg = next( args)
        except StopIteration:
            break
        if arg == '--run_remote_run_all_tests_private':
            run_remote_run_all_tests_private( next(args))
            return
        elif arg == '--run_1':
            run_1( next(args))
            return
    
    # Set current_input to first file to test.
    current_input = 'tests_private/customer_tests/bug689453.pcl'
    current_input = 'tests_private/customer_tests/bug693381.pcl'
    current_input = 'tests_private/customer_tests/bug691204.pcl'
    current_input = 'tests_private/customer_tests/5478.pcl'
    current_input = 'tests_private/customer_tests/p38359.pcl'
    current_input = 'tests_private/customer_tests/HUD_noescnew.pcl'
    current_input = 'tests_private/customer_tests/bug691747.pcl'
    
    current_input = 'tests_private/customer_tests/bug693543.pcl'
    current_input = 'tests_private/pcl/pcl5efts/extra_line.pcl'
    
    current_input = 'tests_private/customer_tests/bug690844.pxl'
    #current_input = 'tests_private/customer_tests/jpg_mem.pxl'
    
    # broken with segv by 'pcl/pxl/: fix leak if pxEndImage not called.'
    current_input = 'tests_private/customer_tests/bug690844.pxl'
    
    if 0:
        jlib.system( run_all_git_pcl(), out='log', verbose=1)
    elif 1:
        run_1_remote(
                #current_input,
                #'tests_private/performance/pcl/05_PCL5_A4_kzv-hessen_00001-01500.pcl',
                'tests_private/customer_tests/bug690844.pxl'
                )
    else:
        run_remote_run_all_tests_private_remote(
                #current_input,
                )


if __name__ == '__main__':
    try:
        main()
    except:
        sys.stderr.write( jlib.exception_info())
        sys.exit(1)
