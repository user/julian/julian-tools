#! /usr/bin/env python3

'''
Compares PyMuPDF's fitz Python module with MuPDF's fitz Python module. Outputs
lines like:

<path>: <name> ()|<type>
    Both fitz modules have this name.
<path>: + <name>
    This name is in MuPDF's fitz but not in PyMuPDF's fitz.
<path>: - <name>
    This name is in PyMuPDF's fitz but not in MuPDF's fitz - i.e. missing in
    MuPDF's fitz.

Run with:
    LD_LIBRARY_PATH=mupdf/build/shared-debug PYTHONPATH=PyMuPDF:PySimpleGUI:mupdf/build/shared-debug julian-tools/pymupdf-check.py

'''

import inspect
import os
import sys
import time

if __name__ == '__main__':

    do_exec = False
    #print(f'LD_LIBRARY_PATH={os.environ.get("LD_LIBRARY_PATH")}')
    #print(f'PYTHONPATH={os.environ.get("PYTHONPATH")}')
    if 'LD_LIBRARY_PATH' not in os.environ:
        os.environ['LD_LIBRARY_PATH'] = 'mupdf/build/shared-debug'
        do_exec = True
        print(f'Have set LD_LIBRARY_PATH={os.environ["LD_LIBRARY_PATH"]}')
    if 'PYTHONPATH' not in os.environ:
        os.environ['PYTHONPATH'] = 'PyMuPDF:PySimpleGUI:mupdf/build/shared-debug'
        do_exec = True
        print(f'Have set PYTHONPATH={os.environ["PYTHONPATH"]}')
    if do_exec:
        print('Re-execing...')
        sys.stdout.flush()
        os.execvp(sys.argv[0], sys.argv)


import fitz
print('Have imported fitz')

import mupdf
print('Have imported mupdf')

e = os.system('cd mupdf && ln -sf fitz.py fitz2.py')
assert not e
sys.path.append('mupdf')
import fitz2
print('Have imported fitz2')


print(f'type(fitz.CS_CMYK)={type(fitz.CS_CMYK)} fitz.CS_CMYK={fitz.CS_CMYK}')

num_same = 0
num_missing = 0
num_new = 0
num_changed = 0

def compare(a, b, verbose, depth=0, path=''):
    global num_same
    global num_missing
    global num_new
    global num_changed
    if path is None:
        path = []
    def p(text, pm=''):
        print(f'{" "*4*0*depth}{pm:3} {path}: {text}')
    aa = sorted( inspect.getmembers(a))
    bb = sorted( inspect.getmembers(b))
    ia = 0
    ib = 0
    def exclude_name( name):
        return name.startswith('___') or name == 'thisown'
    while 1:
        def type_text(value):
            if callable(value):
                return '()'
            return ''
        while ia < len(aa) and (ib == len(bb) or aa[ia][0] < bb[ib][0]):
            name, value = aa[ia]
            if not exclude_name( name):
                p(f'{name}{type_text(value)}', '-')
                num_missing += 1
            ia += 1
        while ib < len(bb) and (ia == len(aa) or bb[ib][0] < aa[ia][0]):
            name, value = bb[ib]
            if not exclude_name( name):
                p(f'{name}{type_text(value)}', '+')
                num_new += 1
            ib += 1
        if ia < len(aa) and ib < len(bb):
            name_a, value_a = aa[ia]
            name_b, value_b = bb[ib]
            if name_a == name_b:
                name = name_a
                if exclude_name( name):
                    pass
                else:
                    type_a = type(value_a)
                    type_b = type(value_b)
                    if type_a != type_b:
                        if verbose:
                            p(f'{name} -{type(value_a)} +{type(value_b)}', '-+')
                        num_changed += 1
                    else:
                        type_ = type_a
                        if type_ in (int, float, str) or inspect.ismodule(value_a):
                            if verbose:
                                p(f'{name}')
                            num_same += 1
                        else:
                            if verbose:
                                if inspect.ismethod(value_a) or inspect.isfunction(value_a):
                                    p(f'{name}()')
                                else:
                                    p(f'{name} [{type_a}]')
                                num_same += 1
                            if not name.startswith( '__'):
                                compare(aa[ia][1], bb[ib][1], verbose, depth + 1, f'{path}.{name}')
                ia += 1
                ib += 1
        if ia == len(aa) and ib == len(bb):
            break

#compare(sys, os)
#compare(fitz, fitz2)

if __name__ == '__main__':

    args = iter(sys.argv[1:])
    verbose = True
    while 1:
        try:
            arg = next(args)
        except StopIteration:
            break
        if arg == '-v':
            verbose = int(next(args))
        else:
            raise Exception(f'Unrecognised arg: {arg}')
    
    compare(fitz, fitz2, verbose)
    
    num_orig = num_same + num_missing + num_changed
    print( f'num_orig={num_orig}')
    print( f'num_same={num_same}')
    print( f'num_missing={num_missing}')
    print( f'num_new={num_new}')
    print( f'num_changed={num_changed}')

    assert num_orig==2808
    assert num_missing==5
    assert num_new==334
    assert num_changed==5
