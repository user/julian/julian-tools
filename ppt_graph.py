#! /usr/bin/env python3

import aargs
import dictpath
import jlib

import calendar
import datetime
import glob
import json
import os
import sys
import time


def _mpl_time( unix_time):
    '''
    Converts Unix time (seconds since 1970) into days suitable for matplotlib.
    '''
    unix_time_tuple = datetime.datetime.fromtimestamp( unix_time, datetime.timezone.utc)
    days = matplotlib.dates.date2num( unix_time_tuple)
    return days


def _sorted_items( d):
    '''
    Like dict.items() but uses sorted keys.
    '''
    keys = list( d.keys())
    keys.sort()
    for key in keys:
        value = d[ key]
        yield key, value


def plotly(root, out, max_subplots: int=None):
    '''
    Make graph using plotly.
    '''
    figure = make(root, plotly=1, max_subplots=max_subplots)
    figure.write_html(
            out,
            
            # See: https://github.com/plotly/plotly.js/blob/master/src/plot_api/plot_config.js
            config=dict(
                scrollZoom=True,
                #doubleClick='reset',
                showTips=True,
                )
            )


def make(root, show=False, plotly=False, max_subplots=None):
    '''
    Makes graphs from data files in `root`. Returns a
    `matplotlib.figure.Figure` or plotly `Figure` instance.
    '''
    # Map from date to test-type to input-path to tool-name to result-time.
    #jlib.log( f'Sorting data...')
    results = dict()
    tools = set()
    for path in glob.glob( f'{root}/results-2*.json'):
        leaf = os.path.basename( path)
        tt = time.strptime( leaf, 'results-%Y-%m-%d-%H-%M.json')
        date = calendar.timegm( tt)
        with open( path) as f:
            results_t = json.load(f)
        for data in results_t[ 'data']:
            tools.add( data[ 'toolname'])
            dictpath.setpath(
                    results,
                    data[ 'testname'],
                    data[ 'path'],
                    data[ 'toolname'],
                    date,
                    (data[ 'e'], data[ 't']),
                    )
    #jlib.log f'{json.dumps(results, indent="    ")}')
    num_testnames = 0
    num_subplots = 0
    for testname, _ in results.items():
        num_testnames += 1
        for path, _ in _.items():
            num_subplots += 1
    
    if plotly:
        return _plotly( results, tools, max_subplots=max_subplots)
    else:
        return _matplotlib(results, tools)


def _matplotlib(results, tools):

    import matplotlib.pyplot
    import numpy

    num_subplots = 0
    for testname, _ in _sorted_items( results):
        for path, tools_ in _sorted_items( _):
            num_subplots += 1
    # Create axes, one for each (test, path) combination.
    scale = 10  # governs size relative to text font size.
    figure, axs = matplotlib.pyplot.subplots(
            num_subplots,   # rows
            1,  # columns
            figsize=(scale, scale*0.75*num_subplots),
            )
    i = 0
    for testname, _ in _sorted_items( results):
        for path, tools_ in _sorted_items( _):
            #jlib.log( f'Processing {testname=}: {path}')
            ax = axs[i]
            i += 1
            ax.xaxis.set_major_formatter( matplotlib.dates.DateFormatter('%Y-%b-%d'))
            tools2 = list()
            tools2_x_max = 0
            # Plot a line for each tool. Also gather info about last coordinate
            # of each tool's line so we can label each line on rhs.
            for tool in sorted( tools):
                _ = tools_.get( tool, dict())
                xs = []
                ys = []
                for date, (e, t) in _sorted_items(_):
                    if e == 0:
                        xs.append( _mpl_time( date))
                        ys.append( t)
                # We add a plot for each tool, regardless of whether we have
                # any data for it, so that we use consistent colours for each
                # tool in each sub-plot.
                ax.plot(
                        xs,
                        ys,
                        'x-',
                        label=tool,
                        linewidth=1,
                        )
                if xs and ys:
                    x, y = xs[-1], ys[-1]
                    tools2.append( (x, y, tool))
                    tools2_x_max = max(tools2_x_max, x)
            
            # Always include elapsed time = zero.
            #
            ax.set_ylim(ymin=0)
            
            # Sort by y coordinate.
            #
            tools2.sort( key=lambda t: t[1])
            
            # Create a label on right of each line, showing the tool name.
            #
            
            # ? Without this, we end up not translating any label positions...?
            ax.get_xbound()
            
            yy_prev = None
            
            for x, y, tool in tools2:
                # (x, y) are in data coordinates.
                # (xx, yy) are in display coordinates.
                #
                if tool == 'pymupdf':
                    # There is one datapoint, but otherwise this tool is not used.
                    continue
                xx, yy = ax.transData.transform( (tools2_x_max, y))
                xx += 40    # place tool text to right of the tool line.
                yy0 = yy
                if yy_prev is not None:
                    # Ensure that tool text's do not overlap. Fixme: this
                    # should use the actual font bbox height.
                    yy = max( yy, yy_prev + 20)
                x2, y2 = ax.transData.inverted().transform( (xx, yy))
                arrowprops = dict(
                        #facecolor='black',
                        #facecolor='0.7',
                        facecolor='#04040480',
                        arrowstyle='->',
                        relpos=(0, 0.5),
                        )
                ax.annotate(
                        f'{y:.02f} {tool}',
                        xy=(x, y),
                        xytext=(x2, y2),
                        arrowprops=arrowprops,
                        annotation_clip=False,
                        )
                yy_prev = yy
            
            ax.set_title( f'{testname}: {path}')
            #ax.set_xlabel('Date')
            ax.set_ylabel('Elapsed time (s)')
            ax.legend(loc='center left')
    if 0:#show:
        matplotlib.pyplot.show( block=True)
    else:
        #jlib.log('Returning {type(figure)=}.')
        return figure


def _max(a, b):
    if a is None:
        return b
    if b is None:
        return 1
    return max( a, b)
def _min(a, b):
    if a is None:
        return b
    if b is None:
        return 1
    return min( a, b)

def _plotly(results, tools, max_subplots=None):
    import plotly.subplots
    import plotly.graph_objects
    import numpy
    
    # Define a color for each tool.
    #
    # We use hard-coded strong colors for pymupdf tools, then
    # plotly.colors.DEFAULT_PLOTLY_COLORS for the rest.
    #
    palette = [
            'rgb(255, 0, 0)',
            'rgb(0, 0, 255)',
            'rgb(128, 0, 128)',
            ] + list(plotly.colors.DEFAULT_PLOTLY_COLORS)
    tool_colors = dict()
    def tool_order(tool):
        if tool != 'pymupdf' and (tool.startswith('pymupdf') or tool.startswith('mupdfpy')):
            return 'aaaaa' + tool
        return tool
    for i, tool in enumerate( sorted(tools, key=tool_order)):
        tool_colors[ tool] = palette[ i % len(palette)]
        #jlib.log('{tool}: {tool_colors[ tool]}')
    
    # We need to find, in advance, the number of subplots and the subplot
    # titles (e.g. 'copy: DB-Systems.pdf').
    #
    num_subplots = 0
    for testname, _ in _sorted_items( results):
        for path, tools_ in _sorted_items( _):
            num_subplots += 1
    if max_subplots:
        num_subplots = min(num_subplots, max_subplots)
    subplot_titles = list()
    i = 0
    for testname, _ in _sorted_items( results):
        for path, tools_ in _sorted_items( _):
            subplot_titles.append(f'{1+i}/{num_subplots}: {testname}: {path}')
            i += 1
    
    # Create the subplots.
    #
    height = 400 * num_subplots
    vertical_spacing_max = 1 / num_subplots
    vertical_spacing = vertical_spacing_max * 0.2
    figure = plotly.subplots.make_subplots(
            rows=num_subplots,
            cols=1,
            vertical_spacing=vertical_spacing,
            subplot_titles=subplot_titles,
            #print_grid=True,   # print ids for each suplot.
            )
    
    figure2_data_x = []
    figure2_data_y = []
    figure2_data_annotation = []
    date_max = 0
    
    # Add plots for each tool to each subplot.
    #
    i = 0
    annotations = list()
    for testname, _ in _sorted_items( results):
        
        for path, tools_ in _sorted_items( _):
            
            # We store up information about each tool's last point in this
            # subplot, so that we can then add annotations, sorted by y
            # coordinate so that we can ensure they do not overlap.
            #
            rhs = []
            
            figure2_t_pymupdf_master = None
            figure2_t_mupdfpy = None
            for tool_i, tool in enumerate( sorted( tools)):
                _ = tools_.get( tool, dict())
                
                # Find points for this tool.
                xs = []
                ys = []
                for date, (e, t) in _sorted_items(_):
                    if e == 0:
                        date = int(date)
                        date_max = max( date_max, date)
                        date2 = numpy.datetime64( date, 's')
                        xs.append( date2)
                        ys.append( t)
                
                # Add trace for this tool.
                figure.add_trace(
                        plotly.graph_objects.Scatter(
                            x = xs,
                            y = ys,
                            marker=dict(color=tool_colors[tool]),
                            ),
                        row = 1+i,
                        col = 1,
                        )
                
                # Update `rhs` with location of final point.
                if xs and ys:
                    x, y = xs[-1], ys[-1]
                    x0 = xs[0]
                    rhs.append( (x, y, tool))
                    
                    # Add last point to figure2_data.
                    #jlib.log( f'{tool=}')
                    if tool == 'pymupdf_mupdf_master':
                        figure2_t_pymupdf_master = y
                    if tool == 'mupdfpy_mupdf_master':
                        figure2_t_mupdfpy_master = y
            
            #jlib.log( f'{figure2_t_pymupdf_master=} {figure2_t_mupdfpy_master=}')
            if figure2_t_pymupdf_master and figure2_t_mupdfpy_master:
                figure2_data_x.append( figure2_t_pymupdf_master)
                figure2_data_y.append( figure2_t_mupdfpy_master)
                figure2_data_annotation.append( f'{testname}:{path}: {100*figure2_t_mupdfpy_master/figure2_t_pymupdf_master:.1f}%')
                
            
            # Create dummy transparent trace with y=0 so that y axis always
            # includes zero.
            #
            if rhs:
                # Use any existing x coordinate.
                x, _, _ = rhs[0]
                figure.add_trace(
                        plotly.graph_objects.Scatter(
                            x = [x],
                            y = [0],
                            marker=dict(opacity=[0]),
                            ),
                        row = 1+i,
                        col = 1,
                        )
            
            # Add specifications for annotations on the right hand side of each
            # of this subplot's tool lines.
            #
            # We need to do this in order of y coordinate, so that we can add
            # an increasing pixel-based y offset so that the annotations do not
            # overlap each other.
            #
            rhs.sort( key=lambda item: item[1])
            for j, (x, y, tool) in enumerate(rhs):
                pixel_offset_x = 60
                pixel_offset_y = - (j+0.5 - len(rhs)/2) * 12    # Default font is 12 pixels hight?
                annotations.append(
                        dict(
                            # Identify subplot axes.
                            xref=f'x{1+i if i else ""}',
                            yref=f'y{1+i if i else ""}',
                            
                            # Where the annotation arrow points to.
                            x=x,
                            y=y,
                            
                            text=f'{y:.2f} {tool}',
                            
                            # Place annotation text at pixel offset from (x, y).
                            axref='pixel',
                            ayref='pixel',
                            ax=pixel_offset_x,
                            ay=pixel_offset_y,
                            
                            # Specify arrow head near to (x, y).
                            showarrow=True,
                            arrowhead=3,    # Shape of arrow head.
                            arrowsize=1,
                            standoff=4, # arrow head distance from (x, y) in pixels.
                            
                            # startarrow* seems to have no effect?
                            #startarrowhead=2,
                            #startarrowsize=3,
                            
                            xanchor='left', # Make tail of arrow be at left of annotation text.
                            
                            # Set annotation color to color of the tool's line.
                            font=dict(color=tool_colors[tool])
                            )
                        )
            
            i += 1
            if max_subplots and i >= max_subplots:
                break
        if max_subplots and i >= max_subplots:
            break
    
    # fixme: doubling the list of annotations seems to fix issue where if there
    # are a large numbers of annotations, the first few seem to be omitted.
    #
    annotations += annotations
    
    figure.update_layout(
            annotations=annotations,
            autosize=True,
            margin=dict(
                autoexpand=True,
                ),
            height=height,
            showlegend=False,
            )
    
    if 0:
        import pprint
        pprint.pp(figure, indent=4, width=9999)
    
    # Create figure2 - scatter plot of latest results of pymupdf_mupdf_master
    # vs mupdfpy_mupdf_master.
    #
    figure2_data = plotly.graph_objects.Scatter(
            x = figure2_data_x,
            y = figure2_data_y,
            mode = 'markers',
            hovertext = figure2_data_annotation,
            name = 'PyMuPDF vs mupdfpy',
            marker = dict(
                    size = [5] * len(figure2_data_x),
                    color = ['rgb(255, 0, 0)'] * len(figure2_data_x),
                    ),
            )
    figure2_baseline = plotly.graph_objects.Scatter(
            x = [0, max(figure2_data_x)],
            y = [0, max(figure2_data_x)],
            mode = 'lines',
            name = 'PyMuPDF vs PyMuPDF',
            line = dict(
                    color = 'rgb(0, 255, 0)',
                    ),
            )
    
            
    figure2 = plotly.graph_objects.Figure(figure2_data)
    figure2.add_trace( figure2_baseline)
    figure2.update_layout(
            title = f'mupdfpy vs PyMuPDF {jlib.date_time( date_max)}',
            xaxis_title = 'PyMuPDF',
            yaxis_title = 'mupdfpy',
            )
    
    return figure, figure2
                

if __name__ == '__main__':
    aargs.cli()
